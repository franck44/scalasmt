Up: [User Manual](user-manual.md), Next: [Building ScalaSMT master](building.md), Prev: [Solver Configuration](solver-config.md)

### Basic usage

The following example shows how to get a running solver and check a simple term:
you may run it from within the `console` after running
~~~
sbt console
~~~
in the project root directory.

~~~
scala> import org.bitbucket.franck44.scalasmt.Theory_QF_LIA._
import org.bitbucket.franck44.scalasmt.Theory_QF_LIA._

scala> implicit val solver = startSolver("Z3")
solver: org.bitbucket.franck44.scalasmt.interpreters.SMTSolver = org.bitbucket.franck44.scalasmt.interpreters.SMTSolver@26640c7f

scala> val x = Ints( "x" )
x: org.bitbucket.franck44.scalasmt.typedterms.VarTerm[org.bitbucket.franck44.scalasmt.theories.IntTerm] = TypedTerm(Set(SortedQId(SymbolId(SSymbol(x)),IntSort())),QIdTerm(SimpleQId(SymbolId(SSymbol(x)))))

scala> isSat( x === 1 )
res1: scala.util.Try[org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.SatResponses] = Success(Sat())

scala> getValue(x)
res2: scala.util.Try[org.bitbucket.franck44.scalasmt.typedterms.Value] =
 Success(Value(ConstantTerm(NumLit(1))))

scala> res4.get.show.toInt
res3: Int = 1

scala> solver.destroy()
~~~

You may notice that the solver can be manually started and has to be manually destroyed (killed) at the end.
Otherwise you may end up with unreleased resources and after a few hours dozens of solver processes.

### Recommended Usage

The recommended way to use a solver is to do it with the `using` method as follows:
~~~
import org.bitbucket.franck44.scalasmt.Theory_QF_LIA._
import org.bitbucket.franck44.scalasmt.configurations.SMTOptions._

scala> val result = using ( startSolver("Z3", List(MODELS))) {
        implicit solver =>
            //  computation to perform in solver
            isSat(x === 1 ) flatMap { _ => getValue(x) }
        }

result: scala.util.Try[org.bitbucket.franck44.scalasmt.typedterms.Value] =
 Success(Value(ConstantTerm(NumLit(1))))

scala> result.get.show.toInt
res4: Int = 1
~~~

The `implicit solver =>` line is fixed and should be used verbatim. The `startSolver` line configures a solver for QF_LIA (the theory we imported at the beginning). The computation to perform in the solver should return a `Try`.

### Solver Initialisation

The following example illustrates the basic usage with the `SMTSolver` class

~~~
import org.bitbucket.franck44.scalasmt.interpreters.Resources
import org.bitbucket.franck44.scalasmt.theories.{ Core, IntegerArithmetics}
import org.bitbucket.franck44.scalasmt.typedterms.{ TypedTerm, Commands}

object ExampleLIA extends Core with IntegerArithmetics with Resources with Commands {

    import org.bitbucket.franck44.scalasmt.parser.Implicits._
    import org.bitbucket.franck44.scalasmt.typedterms.TypedTerm
    import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.SatResponses
    import org.bitbucket.franck44.scalasmt.configurations.SMTLogics._
    import org.bitbucket.franck44.scalasmt.configurations.SMTOptions.MODELS
    import org.bitbucket.franck44.scalasmt.configurations.SMTInit
    import org.bitbucket.franck44.scalasmt.interpreters.SMTSolver

    //  create two SMTLIB2 Int variables using the DSL
    val x = Ints( "x" )
    val y = Ints( "y" )

    val result  = using( new SMTSolver("Z3", new SMTInit(QF_LIA, List(MODELS))) ) {
        implicit solver ⇒
            //  Check simple sat query
            isSat(
                    x + 1 <= y,
                    y % 4 === 3
                ) flatMap
                { _ =>
                    scala.util.Success((getValue(x), getValue(y)))
                }
    }
}

defined object ExampleLIA

scala> ExampleLIA.result
res5: scala.util.Try[(scala.util.Try[org.bitbucket.franck44.scalasmt.typedterms.Value], scala.util.Try[org.bitbucket.franck44.scalasmt.typedterms.Value])] =
 Success((Success(Value(NegTerm(ConstantTerm(NumLit(2))))),Success(Value(NegTerm(ConstantTerm(NumLit(1)))))))

scala> val (xr, yr) = ExampleLIA.result.get

xr: scala.util.Try[org.bitbucket.franck44.scalasmt.typedterms.Value] =
 Success(Value(NegTerm(ConstantTerm(NumLit(2)))))
yr: scala.util.Try[org.bitbucket.franck44.scalasmt.typedterms.Value] =
 Success(Value(NegTerm(ConstantTerm(NumLit(1)))))

scala> xr.get.show.toInt
res6: Int = -2

scala> yr.get.show.toInt
res7: Int = -1
~~~
The command `new SMTSolver("Z3", new SMTInit(QF_LIA, List(MODELS)))` creates a solver running "Z3" (defined in [application.conf](../application.conf)  by the solver with a name field equal to "Z3") and initially configured with the Theory `QF_LIA`, and `:produce-models true`.
Note that if you create an instance of `SMTSolver` and use it more than once, the same solver is used as `using` does not create a solver process, but `new SMTSolver` does.

## More Examples

The file [General-usage-tests.scala](../src/test/scala/scalasmt/General-usage-tests.scala) provides a few examples how to use the library.
