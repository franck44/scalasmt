
Up: [User Manual](user-manual.md), Next: [Basic Usage](basic-usage.md)

### Supported Solvers

As of May 17, 2017, ScalaSMT supports :

- [Z3 4.5.0](https://github.com/Z3Prover/z3/releases/download/z3-4.5.0/z3-4.5.0-x64-ubuntu-14.04.zip)
- [SMTInterpol 2.1-335-g4c543a5](https://ultimate.informatik.uni-freiburg.de/smtinterpol/smtinterpol-2.1-335-g4c543a5.jar)
- [CVC4 1.4](http://cvc4.cs.stanford.edu/downloads/builds/x86_64-linux-opt/cvc4-1.4-x86_64-linux-opt) (note: homebrew version crashes)
- [MathSAT 5.4.1](http://mathsat.fbk.eu/download.php?file=mathsat-5.4.1-linux-x86_64.tar.gz)
- [Yices 2.5.2](http://yices.csl.sri.com)

Links to supported versions for OSX and Linux can be found in [application.conf](../application.conf).
Commands to install the solvers can be found  in the [shippable.yml](../shippable.yml) or [bitbucket-pipelines.yml](../bitbucket-pipelines.yml)
files (used for continuous build integration).
Note that the solver executable must be in your path for the library to find it and run it
(alternatively you may provide full paths in [application.conf](../application.conf)).

[SMTInterpol](https://ultimate.informatik.uni-freiburg.de/smtinterpol/) is distributed as a Java .jar file and we use a shell script wrapper to run it:
~~~~
#!/bin/bash

# wrapper for calling the JAR file
java -jar ~/development/SMTinterpol/bin/smtinterpol.jar "$@"

exit 0
~~~~
This executable shell script should be named `interpol` and  be in your path (you may change the  `SMTInterpol` section in [application.conf](../application.conf) if you want to use a different name).

### Configuration file
The file [application.conf](../application.conf) provides the standard configurations for some solvers.
This includes the name of the solver, the executable, and some other useful fields.

When you  run the tests, you may enable/disable a solver by altering the line

~~~~~
enabled = [ "all", "-Z3", "CVC4", "-Yices", "-MathSat", "SMTInterpol"]
~~~~~
in  [application.conf](../application.conf).
To decide whether tests should use a solver, the following algorithm is used:

- if "all" is in `enabled` the every solver is used
- otherwise, solver named `x` is used if `x` is in `enabled`.

As a consequence, you may disable solvers by simply adding a character to the names in the list:.
~~~~~
enabled = [ "-all", "-Z3", "CVC4", "-Yices", "-MathSat", "SMTInterpol"]
~~~~~
enables **CVC4** and **SMTInterpol** in the tests.
