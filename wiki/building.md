
Up: [User Manual](user-manual.md), Prev: [Basic Usage](basic-usage.md)

### Building ScalaSMT Master

You can clone the master branch of this repository.
Once this is done, you may enter the following command:
~~~
sbt compile
~~~
It should fetch all the dependencies and build the ScalaSMT package.

If you want to make the library available locally you may also run
~~~
sbt publishLocal
~~~

The documentation is built with
~~~
sbt doc
~~~
and can be found in the target/scala-2.12/api directory.

#### Testing

To test that the solvers are correctly found you can run the following test:

~~~~~
sbt testOnly *CreateSolver*
[info] Formatting 6 Scala sources  ...
[info] Compiling 52 Scala sources to ...
[info] Create running solver with no logic/option:
[info] - [Z3] Create (managed) solver process
[info] - [MathSat] Create (managed) solver process
[info] - [SMTInterpol] Create (managed) solver process
[info] - [CVC4] Create (managed) solver process
[info] - [Yices] Create (managed) solver process
[info] ScalaTest
[info] Run completed in 1 second, 452 milliseconds.
[info] Total number of tests run: 5
[info] Suites: completed 1, aborted 0
[info] Tests: succeeded 5, failed 0, canceled 0, ignored 0, pending 0
[info] All tests passed.
[info] Passed: Total 5, Failed 0, Errors 0, Passed 5
[success] Total time: 31 s, completed 10/05/2017 10:02:41 PM
~~~~~

If this is successful (as above), you can run the tests that configure the solvers with basic configurations they should support (according to the [application.conf](../application.conf) file):
~~~
sbt testOnly *SolverConfigTests*
~~~

If successful you may run the full test suite:
~~~
sbt test
~~~

All tests should pass (and a few may be ignored).
If some tests fail you may report it and open an issue ticket.
Note that the processes running the solvers are _resource managed_ so when the tests end, there should be no leftovers i.e. no solver processes should show up when issuing the `ps` command.
