

# User Manual (in progress)

The file [General-usage-tests.scala](../src/test/scala/scalasmt/General-usage-tests.scala) provides a few examples how to use the library.

You may build the scaladoc (sbt doc) to generate the documentation and use some of the examples.

1. [Solver configuration](solver-config.md)
2. [Basic Usage](basic-usage.md)
3. [Building ScalaSMT master](building.md)
