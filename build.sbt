
//  define project common settings for current directory
lazy val commonSettings = Seq(
    organization := "org.bitbucket.franck44.scalasmt",
    scalaVersion := "2.12.10"
)

ensimeScalaVersion in ThisBuild := "2.12.10"

// sbt settings

ThisBuild / resolvers ++= Seq(
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots")
)

libraryDependencies ++=
    Seq (
        "org.bitbucket.inkytonik.kiama" %% "kiama" % "2.3.0-SNAPSHOT",
        "org.bitbucket.inkytonik.kiama" %% "kiama" % "2.3.0-SNAPSHOT" % "test" classifier ("tests"),
        "org.bitbucket.inkytonik.kiama" %% "kiama-extras" % "2.3.0-SNAPSHOT",
        "org.bitbucket.inkytonik.kiama" %% "kiama-extras" % "2.3.0-SNAPSHOT" % "test" classifier ("tests"),
        "org.scalatest" %% "scalatest" % "3.0.8" % "test",
        "org.scalacheck" %% "scalacheck" % "1.14.1" % "test",
        "org.bitbucket.franck44.expect" %% "expect-for-scala" % "1.1.1-SNAPSHOT",
        "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
        "com.jsuereth" % "scala-arm_2.12" % "2.0",
        "ch.qos.logback" % "logback-classic" % "1.2.3",
        "com.typesafe" % "config" % "1.4.0",
        "com.iheart" %% "ficus" % "1.4.7",
        "com.twitter" %% "util-collection" % "18.10.0"
    )
// scalafixSettings

//
// sbtfixSettings

//  options for it
IntegrationTest/ javaOptions  ++= Seq("-Xss200M")

//  create a configuration for coverage test
lazy val ShippableCITest = config("shippableci") extend(Test)

// test options for generating formatted test results in IntegrationTest config for shippable
ShippableCITest / testOptions += {
        (target in Test) map {
            t => Tests.Argument(TestFrameworks.ScalaTest, "-u", "%s" format (t / "../shippable/testresults"))
        }
    }.value

ShippableCITest / fork  := true

// scoverage package
ShippableCITest / coverageEnabled  := true
ShippableCITest / coverageMinimum := 80
ShippableCITest / coverageFailOnMinimum := false   //  false is safer as otherwise the build breaks
ShippableCITest / coverageHighlighting := true     //  enable highlighting of covered/non-covered
// exclude some package from coverage
ShippableCITest / coverageExcludedPackages := ".*sbtrats.*;.*resources.*;.*ParserSupport.*;.*SMTLIB2*.*"
coverageExcludedPackages := ".*sbtrats.*;.*resources.*;.*ParserSupport.*;.*SMTLIB2*.*"
ShippableCITest / logBuffered := false
ShippableCITest / logLevel:= Level.Info

ShippableCITest / scalaSource:= baseDirectory.value / "src"
ShippableCITest / resourceDirectory := baseDirectory.value / "src/test/resources"

//  parallel execution
Test / parallelExecution := true
ShippableCITest / parallelExecution := true

ThisBuild / logLevel := Level.Info

//  creating a logger and setting level to warn/whatever for console
initialCommands in console :=
 """|
    | import ch.qos.logback.classic.Logger
    | import org.slf4j.LoggerFactory
    | val root = LoggerFactory.getLogger("root").asInstanceOf[Logger]
    | import ch.qos.logback.classic.Level
    | root.setLevel(Level.OFF)
    | """.stripMargin

//  sbt shell prompt
ThisBuild / shellPrompt := {
    state =>
        Project.extract(state).currentRef.project + ":" + name.value + ":" + version.value +
            " " + "> "
}

//      project settings
lazy val root = (project in file(".")).
    configs(ShippableCITest).
    settings(commonSettings: _*).
    settings(inConfig(ShippableCITest)(Defaults.testTasks): _*).
    settings(
      name := "ScalaSMT",
      scalacOptions  :=
          Seq (
              "-deprecation",
              "-feature",
              "-sourcepath", baseDirectory.value.getAbsolutePath,
              "-unchecked",
              "-Xlint:-stars-align,-unused,_",
              "-Xcheckinit",
              "-Yrangepos"
          ),

            // Publishing

            publishTo := {
                val nexus = "https://oss.sonatype.org/"
                if (version.value.trim.endsWith("SNAPSHOT"))
                    Some("snapshots" at nexus + "content/repositories/snapshots")
                else
                    Some("releases" at nexus + "service/local/staging/deploy/maven2")
            },
            publishConfiguration := publishConfiguration.value.withOverwrite(true),
            publishLocalConfiguration := publishLocalConfiguration.value.withOverwrite(true),
            publishMavenStyle := true,
            publishArtifact in Test := true,
            pomIncludeRepository := { x => false },
            pomExtra := (
                <url>https://bitbucket.org/franck44/scalasmt/</url>
                <licenses>
                    <license>
                        <name>LGPL, v. 3.0</name>
                        <url>https://www.gnu.org/licenses/lgpl-3.0.en.html</url>
                        <distribution>repo</distribution>
                    </license>
                </licenses>
                <scm>
                    <url>https://bitbucket.org/franck44/scalasmt</url>
                    <connection>scm:git:https://bitbucket.org/franck44/scalasmt</connection>
                </scm>
                <developers>
                    <developer>
                       <id>franck44</id>
                       <name>Franck Cassez</name>
                       <url>https://bitbucket.org/franck44</url>
                    </developer>
                </developers>
            )
          )


//  set profile for publishing
sonatypeProfileName  := "org.bitbucket.franck44"
// Add the default sonatype repository setting
publishTo := sonatypePublishTo.value

Compile / doc / scalacOptions ++= Seq(
    "-groups",
    "-implicits",
    "-diagrams",
    "-diagrams-dot-restart", "80",
    "-diagrams-dot-path", "/usr/local/bin/dot"
    // ,"-diagrams-debug"
)

// Rats! setup


ratsScalaRepetitionType := Some (ListType)

ratsUseScalaOptions := true

ratsUseScalaPositions := true

ratsDefineASTClasses := true

ratsDefinePrettyPrinter := true

// ratsUseDefaultLayout := true

ratsUseDefaultComments := false

// ratsVerboseOutput := true

ratsUseKiama := 2

// ScalariForm

import scalariform.formatter.preferences._
import com.typesafe.sbt.SbtScalariform
import com.typesafe.sbt.SbtScalariform.ScalariformKeys

// scalariformSettings
//scalariformSettingsWithIt
//
ScalariformKeys.preferences := ScalariformKeys.preferences.value
    .setPreference (AlignSingleLineCaseStatements, true)
    .setPreference (IndentSpaces, 4)
    .setPreference (SpaceBeforeColon, true)
    .setPreference (SpaceInsideBrackets, true)
    .setPreference (SpaceInsideParentheses, true)
    .setPreference (SpacesWithinPatternBinders, true)
    .setPreference(SpacesAroundMultiImports, true)
    .setPreference (SpacesAroundMultiImports, true)
    .setPreference (PreserveSpaceBeforeArguments, true)
    .setPreference (SpacesWithinPatternBinders, true)
    .setPreference (RewriteArrowSymbols, true)
   .setPreference (AlignParameters, true)
   .setPreference(AlignArguments, true)
   // .setPreference(doubleIndentMethodDeclaration, true)

// headers

headerMappings := headerMappings.value + (HeaderFileType.scala -> HeaderCommentStyle.cStyleBlockComment)

// headers :=
headerLicense := Some(HeaderLicense.Custom(
    """|This file is part of ScalaSMT.
       |
       |Copyright (C) 2015-2019 Franck Cassez.
       |
       |ScalaSMT is free software: you can redistribute it and/or modify it un-
       |der the terms of the  GNU Lesser General Public License as published by
       |the Free Software Foundation,  either version 3  of the License, or (at
       |your option) any later version.
       |
       |ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
       |ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
       |FITNESS FOR A PARTICULAR PURPOSE.
       |
       |See the GNU Lesser General Public License for more details.
       |
       |You should have received a copy of the GNU Lesser General Public License
       |along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
       |<http://www.gnu.org/licenses/>.
       |""".stripMargin
    )
)

excludeFilter.in(headerSources) := HiddenFileFilter || "src/generated/**"

//  note: use headerCreate in sbt to generate the headers
//  headerCheck to check which files need new headers
//  headers generations can also be automated at compile time
//  to generate headers for test files, test:headerCreate
