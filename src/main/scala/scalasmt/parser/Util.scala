/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package parser

import SMTLIB2Syntax.ASTNode
import org.bitbucket.inkytonik.kiama.util.{
    CompilerBase,
    Config
}

/**
 * Generic parser.
 *
 * @tparam  T   The type (non-terminal) the parser should return.
 * @param   x   A map that associates a subparser to an SMTLIB2 object,
 *              Typically this is _ => _.pTerm or _ => _pCommand
 *
 */
class SMTLIB2Parser[ T <: ASTNode ]( x : SMTLIB2 ⇒ ( Int ⇒ xtc.parser.Result ) ) extends CompilerBase[ ASTNode, T, Config ] {

    import org.bitbucket.inkytonik.kiama.output.PrettyPrinterTypes.Document
    import org.bitbucket.inkytonik.kiama.util.{ Emitter, OutputEmitter, Source, FileSource }
    import org.bitbucket.inkytonik.kiama.util.Messaging.Messages

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = getLogger( this.getClass )

    val name = "smt2"

    def createConfig( args : Seq[ String ] ) : Config = new Config( args )

    //  build an AST from a Source if possible

    override def makeast( source : Source, config : Config ) : Either[ T, Messages ] = {
        logger.debug( "entering makeast" )
        val p = new SMTLIB2( source, positions )

        logger.info( "getting a parser for {}", x( p )( 0 ) )

        // get the parser for x

        val pr = x( p )( 0 )
        if ( pr.hasValue )
            Left( p.value( pr ).asInstanceOf[ T ] )
        else
            Right( Vector( p.errorToMessage( pr.parseError ) ) )
    }

    //  not needed as only parsing is required

    def process( source : Source, ast : T, config : Config ) : Unit = {}

    def format( ast : T ) : Document =
        SMTLIB2PrettyPrinter.format( ast, 5 )

}

/**
 * Parser factory.
 *
 * To get a parser for temrs of type T, use SMTLIB2Parser2[T].
 * `T` must be  strict subtype of ASTNode
 */
object SMTLIB2Parser {
    import SMTLIB2Syntax._
    import scala.reflect.ClassTag
    import scala.util.{ Failure, Success, Try }
    import org.bitbucket.inkytonik.kiama.util.Source

    /**
     * An parser exception
     */
    case class ParseErrorException( msg : String ) extends Exception( msg )

    /**
     * associate a parser of type T with ClassOf[T]
     *
     * @param mf This an implicit and there is no need to pass a parameter.
     *
     */
    def apply[ T <: ASTNode ]( implicit mf : ClassTag[ T ] ) : Source ⇒ Try[ T ] = {
        try {
            parser[ T ](
                dispatch.find(
                    { case ( clazz, _ ) ⇒ clazz isAssignableFrom mf.runtimeClass } ).map( { case ( _, parser ) ⇒ parser } ).get )
        } catch {
            case scala.util.control.NonFatal( e ) ⇒ throw ( new Exception( s"Type $mf not supported in object SMTLIB2Parser -- ${e.getMessage}" ) )
        }
    }

    //  the available parsers descriptions
    import xtc.parser.Result

    //  format: OFF
    private val attributeParserTester        : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pAttributeTester
    private val assertResponseParser         : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pAssertResponse
    private val smtlib2SymbolParser          : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pSMTLIB2Symbol
    private val smtlib2SymbolParserTester    : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pSMTLIB2SymbolTester
    private val termSpecConstantTester       : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pSpecConstantTester
    private val qualifiedIdParser            : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pQualifiedId
    private val termParser                   : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pTerm
    private val termParserTester             : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pTermTester
    private val termSort                     : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pSort
    private val termSortTester               : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pSortTester
    private val termIdTester                 : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pIdTester
    private val termIdx                      : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pIdx
    private val termIdxTester                : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pIdxTester
    private val termIdxPlusTester            : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pIdxPlusTester
    private val commandParser                : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pCommand
    private val successResponseParser        : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pSuccessResponse
    private val getDeclCmdResponseParser     : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pGetDeclCmdResponse
    private val exitCmdResponseParser        : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pExitCmdResponse
    private val getOptionResponsesParser     : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pGetOptionResponses
    private val getModelResponsesParser      : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pGetModelResponses
    private val getUnSatCoreResponsesParser  : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pUnSatCoreResponses
    private val getGetValueResponsesParser   : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pGetValueResponses
    private val getGetInfoResponsesParser    : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pGetInfoResponses
    private val getGetItpParser              : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pGetInterpolantResponses
    private val getCheckSatReponses          : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pCheckSatResponses
    private val getGeneralResponse           : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pGeneralResponse
    private val modelResponseTester          : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pModelResponseTester
    private val getValueResponseTester       : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pGetValueResponseTester
    private val scriptParser                 : SMTLIB2 ⇒ ( Int ⇒ Result ) = _.pScript
    //  format: ON

    //  the map from some types to a parser

    //  format: OFF
    private val dispatch = Map(
        classOf[ AssertResponse ]            → assertResponseParser,
        classOf[ AttributeTester ]           → attributeParserTester,
        classOf[ CheckSatResponses ]         → getCheckSatReponses,
        classOf[ Command ]                   → commandParser,
        classOf[ ExitCmdResponse ]           → exitCmdResponseParser,
        classOf[ GeneralResponse ]           → getGeneralResponse,
        classOf[ GetDeclCmdResponse ]        → getDeclCmdResponseParser,
        classOf[ GetInfoResponses ]          → getGetInfoResponsesParser,
        classOf[ GetInterpolantResponses ]   → getGetItpParser,
        classOf[ GetModelResponses ]         → getModelResponsesParser,
        classOf[ GetOptionResponses ]        → getOptionResponsesParser,
        classOf[ GetValueResponseTester ]    → getValueResponseTester,
        classOf[ GetValueResponses ]         → getGetValueResponsesParser,
        classOf[ IdTester ]                  → termIdTester,
        classOf[ Idx ]                       → termIdx,
        classOf[ IdxTester ]                 → termIdxTester,
        classOf[ IdxPlusTester ]             → termIdxPlusTester,
        classOf[ ModelResponseTester ]       → modelResponseTester,
        classOf[ QualifiedId ]               → qualifiedIdParser,
        classOf[ SMTLIB2Symbol ]             → smtlib2SymbolParser,
        classOf[ SMTLIB2SymbolTester ]       → smtlib2SymbolParserTester,
        classOf[ SpecConstantTester ]        → termSpecConstantTester,
        classOf[ SortTester ]                → termSortTester,
        classOf[ Sort ]                      → termSort,
        classOf[ Script ]                    → scriptParser,
        classOf[ SuccessResponse ]           → successResponseParser,
        classOf[ Term ]                      → termParser,
        classOf[ TermTester ]                → termParserTester,
        classOf[ UnSatCoreResponses ]        → getUnSatCoreResponsesParser
    )
    //  format: ON

    /**
     * Create a parser for terms of type String => Try[T] that parses a String into T.
     *
     * @param T The target type of the prser.
     */
    private def parser[ T <: ASTNode ] : ( SMTLIB2 ⇒ ( Int ⇒ xtc.parser.Result ) ) ⇒ ( Source ⇒ Try[ T ] ) = {
        p1 ⇒
            { s ⇒
                val p = new SMTLIB2Parser[ T ]( p1 )
                p.makeast( s, p.createConfig( List() ) ) match {
                    case Left( x ) ⇒ Success( x )
                    case Right( m ) ⇒
                        //  m is a vector of messages
                        Failure( new ParseErrorException( s"Parse error ${p.messaging.formatMessages( m )}" ) )
                }
            }
    }
}

trait PredefinedParsers {

    import org.bitbucket.inkytonik.kiama.util.{ Source, StringSource, FileSource }
    import SMTLIB2Syntax.{
        SuccessResponse,
        GeneralResponse,
        AssertResponse,
        GetDeclCmdResponse,
        ExitCmdResponse
    }
    import scala.util.Try

    private[ scalasmt ] val parseAsSuccess : String ⇒ Try[ SuccessResponse ] = {
        x ⇒
            val parse = SMTLIB2Parser[ SuccessResponse ]
            parse( StringSource( x ) )
    }

    private[ scalasmt ] val parseAsGeneralResponse : String ⇒ Try[ GeneralResponse ] = {
        x ⇒
            val parse = SMTLIB2Parser[ GeneralResponse ]
            parse( StringSource( x ) )
    }

    private[ scalasmt ] val parseAsAssertResponse : String ⇒ Try[ AssertResponse ] = {
        x ⇒
            val parse = SMTLIB2Parser[ AssertResponse ]
            parse( StringSource( x ) )
    }

    //  DeclCmd parser
    private[ scalasmt ] val parserDeclCmdResponse : String ⇒ Try[ GetDeclCmdResponse ] = {
        x ⇒
            val parse = SMTLIB2Parser[ GetDeclCmdResponse ]
            parse( StringSource( x ) )
    }

    //  ExitCmd parser
    private[ scalasmt ] val parseAsExitCmdResponse : String ⇒ Try[ ExitCmdResponse ] = {
        x ⇒
            val parse = SMTLIB2Parser[ ExitCmdResponse ]
            parse( StringSource( x ) )
    }
}

/**
 * Convert a String or File source into a Kiama Source for parsing
 */
object Implicits {

    import org.bitbucket.inkytonik.kiama.util.{ Source, StringSource, FileSource }
    import scala.language.implicitConversions

    /**
     * Make a {@link http://org.bitbucket.inkytonik.kiama.util.Source} from a String
     */
    implicit def stringToSource( s : String ) : Source = StringSource( s )
}
