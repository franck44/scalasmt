/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package typedterms

import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax._

/**
 * A term with attributes.
 *
 * @param typeDefs      As for [[TypedTerm]]
 * @param termDef       As for [[TypedTerm]]
 * @param theAttributes The attributes in the form (name, value)
 */
class Attributed[ +A, +B <: Term ](
    typeDefs :      Set[ SortedQId ],
    termDef :       B,
    theAttributes : Map[ Keyword, AttValueSymbol ] ) extends TypedTerm[ A, B ]( typeDefs, termDef ) {

    /**
     * Retrieve the attribute value
     */
    def attVal( k : Keyword ) : Option[ AttValueSymbol ] = theAttributes.get( k )
}

/**
 * A term with attributes, must include a named attribute
 *
 * @param typeDefs      As for [[TypedTerm]]
 * @param termDef       As for [[TypedTerm]]
 * @param theAttributes The attributes in the form (name, value)
 *                      and one of them must be ("named", _)
 */
class Named[ +A, +B <: Term ](
    theDefs :       Set[ SortedQId ],
    theTerm :       B,
    theAttributes : Map[ Keyword, AttValueSymbol ] ) extends Attributed[ A, B ]( theDefs, theTerm, theAttributes ) {

    require (
        theAttributes.exists( { case ( key, _ ) ⇒ key == Keyword( "named" ) } ),
        "You need to provide the attribute \"named\" to build a named term" )

    /**
     * The name of the TypedTerm retrieved from the list of attributes
     */
    lazy val AttValueSymbol( SSymbol( nameDef : String ) ) =
        attVal( Keyword( "named" ) ) match {
            case Some( name ) ⇒ name
            case None         ⇒ sys.error( "Use of AttValueSymbol/class Named with missing attributes name" )
        }

}
