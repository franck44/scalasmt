/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package typedterms

import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax._

/**
 * Terms that are variables of some simple sort.
 *
 * @param id      The name of the variable
 * @param sort    The sort (ints, reals, array of some sort)
 * @param index   If provided the index to attach to the variable
 *
 */
class VarTerm[ A ](
    val id :    String,
    val sort :  Sort,
    val index : Option[ Int ] = None ) extends TypedTerm[ A, QIdTerm ](
    //  Set of variables is the single Id
    Set( SortedQId(
        SymbolId(
            if ( index.isEmpty ) SSymbol( id )
            else ISymbol( id, index.get ) ),
        sort ) ),
    //  term is a SimpleQId
    QIdTerm( SimpleQId(
        SymbolId(
            if ( index.isEmpty ) SSymbol( id )
            else ISymbol( id, index.get ) ) ) ) ) {

    /**
     * The SymbolId for the variable
     */
    val symbol : SMTLIB2Symbol =
        if ( index.isEmpty ) SSymbol( id )
        else ISymbol( id, index.get )

    /**
     * Set (or reset) the index of the variable
     *
     * @note If index is already set 'i' replaces the old one
     */
    def indexed( i : Int ) = new VarTerm[ A ]( id, sort, Some( i ) )

}

/**
 * The value of a term of Simple Sort
 */
case class Value( t : Term ) {

    /**
     * Make a String from a Value. Standard types like Int, Real are
     * extracted from the SMTYLIB2 representation.
     */
    def show : String = t match {
        case ConstantTerm( NumLit( e ) )            ⇒ e
        case NegTerm( ConstantTerm( NumLit( e ) ) ) ⇒ "-" + e
        case RealDivTerm(
            ConstantTerm(
                NumLit( d ) ),
            List( ConstantTerm( NumLit( n ) ) )
            ) ⇒ d + "/" + n
        case RealDivTerm(
            ConstantTerm(
                DecLit( d, _ ) ),
            List( ConstantTerm( DecLit( n, _ ) ) )
            ) ⇒ d + "/" + n
        case _ ⇒
            sys.error( s"Conversion from $t not supported" )
        // parser.SMTLIB2PrettyPrinter.show( t )
    }
}

/**
 * A model when the solver stack is SAT and the solver can generate one.
 *
 * A  model can be either defined using fundef (e.g. Z3) or valuation pairs (e.g. CVC4).
 * What is useful is to retrieve the value of a given term (variable).
 *
 */
trait Model {
    /**
     * The value of a variable in the model
     *
     * @param symbol    The name of the variable
     * @param sort      The sort of the variable
     */
    def valueOf[ A ]( s : VarTerm[ A ] ) : Option[ Value ]
}

/**
 * A model defined using fundef (e.g. Z3) for the current stack of assertions (SAT).
 */
case class ModelFunDef( xl : List[ ModelResponse ] ) extends Model {

    /**
     * make a map from pairs (SMTLIB2Symbol, Sort) to Value
     */
    private lazy val funDecltoTerm : Map[ ( SMTLIB2Symbol, Sort ), Value ] =
        (
            for {
                ModelResponse( FunDef( symbol, xs, tsort, t ) ) ← xl
            } yield ( ( symbol, tsort ), Value( t ) ) ).toMap

    /**
     * The value of a variable in the model
     *
     * @param s    The ScalaSMT variable to retrieve the value of.
     */
    def valueOf[ A ]( s : VarTerm[ A ] ) : Option[ Value ] =
        funDecltoTerm.get( ( s.symbol, s.sort ) )
}

/**
 * A model defined as valuation pairs (e.g. CVC4) for the current stack of assertions (SAT).
 */
case class ModelValPair( xl : List[ SymbolValuationPair ] ) extends Model {

    /**
     * make a map from SMTLIB2Symbol to Value
     */
    private lazy val valPairToTerm : Map[ SMTLIB2Symbol, Value ] =
        (
            for {
                SymbolValuationPair( symbol, t ) ← xl
            } yield ( symbol, Value( t ) ) ).toMap

    /**
     * The value of a variable in the model
     *
     * @param s    The ScalaSMT variable to retrieve the value of.
     */
    def valueOf[ A ]( s : VarTerm[ A ] ) : Option[ Value ] =
        valPairToTerm.get( s.symbol )
}
