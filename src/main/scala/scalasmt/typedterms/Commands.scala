/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package typedterms

import scala.util.{
    Try,
    Success,
    Failure
}
import parser.PredefinedParsers
import parser.SMTLIB2PrettyPrinter.show

/**
 * Provides Standard SMTLIB2 commands to check whether a list of predicates is SAT,
 * extract interpolants (if UNSAT), push and pop.
 *
 */
trait Commands extends CoreCommands with PredefinedParsers {

    import theories.BoolTerm
    import parser.SMTLIB2Syntax._
    import interpreters.SMTSolver
    import parser.SMTLIB2Parser
    import parser.Implicits._
    import scala.annotation.tailrec
    import scala.concurrent.duration._

    /**
     * Check wether a sequence of 'typed terms' is satisfiable
     *
     * @param timeout   A timeout to perform the operation
     *
     * @param xb        The sequence of terms t1, t2, ... tk. The corresponding
     *                  logical statement is t1 &and; t2 and; ... &and; tk.
     * @param solver    The context should provide an implicit solver.
     *                  You can use the convenience method
     *                  [[[interpreters.Resources.using monadic using]]] to create a solver
     *                  and  use it.
     *
     * @return          Either a Success with the sat status or a
     *                  failure and (reason for failure).
     */
    final def isSat[ T <: Term ](
        timeout : Duration )(
        xb : TypedTerm[ BoolTerm, T ]* )(
        implicit
        solver : SMTSolver ) : Try[ SatResponses ] = isSat( Some( timeout ) )( xb : _* )( solver )

    /**
     * Check wether a sequence of 'typed terms' is satisfiable
     *
     * @param xb        The sequence of terms t1, t2, ... tk. The corresponding
     *                  logical statement is t1 &and; t2 and; ... &and; tk.
     * @param solver    The context should provide an implicit solver.
     *                  You can use the convenience method
     *                  [[[interpreters.Resources.using monadic using]]] to create a solver
     *                  and  use it.
     *
     * @return          Either a Success with the sat status or a
     *                  failure and (reason for failure).
     */
    final def isSat[ T <: Term ](
        xb : TypedTerm[ BoolTerm, T ]* )(
        implicit
        solver : SMTSolver ) : Try[ SatResponses ] = isSat( None )( xb : _* )( solver )

    /**
     * Check whether a sequence of 'typed terms' is satisfiable
     *
     * @param timeout   A timeout to perform the operation
     *
     * @param xb        The sequence of terms t1, t2, ... tk. The corresponding
     *                  logical statement is t1 &and; t2 &and; ... &and; tk.
     * @param solver    The context should provide an implicit solver.
     *                  You can use the convenience method
     *                  [[[interpreters.Resources.using monadic using]]] to create a solver
     *                  and use it.
     *
     * @return          Either a Success with the sat status or a
     *                  failure and (reason for failure).
     */
    @tailrec
    final def isSat[ T <: Term ](
        timeout : Option[ Duration ] )(
        xb : TypedTerm[ BoolTerm, T ]* )(
        implicit
        solver : SMTSolver ) : Try[ SatResponses ] = {

        import com.typesafe.scalalogging.Logger
        import org.slf4j.LoggerFactory

        //  logger
        val logger = Logger( LoggerFactory.getLogger( this.getClass + ".isSat" ) )

        logger.info( "Entering isSat" )

        /*
         * Use if/then and case instead of flatMap (initial version) as
         * otherwise the compiler does not accept the tailrec annotation
         */
        if ( xb.isEmpty ) {
            logger.info( "Calling CheckSat()" )
            checkSat( timeout )( solver )
        } else {
            //  assert head and if Success evaluate the tail
            logger.info( "Evaluating list of terms" )
            ( |= ( xb.head ) ) match {
                case Failure( e ) ⇒
                    logger.error( s"|= returned Error: $e -- aborting evaluation" )
                    Failure( e )
                case Success( ok ) ⇒
                    logger.info( s"|= was a success $ok" )
                    isSat( timeout )( xb.tail : _* )
            }
        }
    }

    /**
     * Check sat for a sequence of 'typed terms' by asserting them while the prefix is
     * satisfiable.
     *
     * This is the same as `isSat` except the prefixes of the term sequence `xb` are checked
     * in turn. As soon as the prefix has a status which is not Sat, then the method returns that
     * result without checking longer prefixes.
     *
     * @param timeout   A timeout to perform the operation
     *
     * @param count     The count of terms in the previously checked sequence.
     * @param xb        The sequence of terms (t1,b1), (t2,b2), ... (bk,tk). The corresponding
     *                  logical statement is t1 &and; t2 &and; ... &and; tk.
     *                  The bis indicate whether a checkSat should be preformed after asserting ti.
     * @param solver    The context should provide an implicit solver.
     *                  You can use the convenience method
     *                  [[[interpreters.Resources.using monadic using]]] to create a solver
     *                  and use it.
     *
     * @return          A pair whose first element is either a Success with the sat status
     *                  or a failure and (reason for failure). Its second element will be
     *                  the count of terms that were used in the check that is being reported.
     */
    @tailrec
    final def isSatWithAssertWhileSat[ T <: Term ](
        timeout : Option[ Duration ] )(
        count :    Int,
        checkNow : Boolean,
        xb :       ( TypedTerm[ BoolTerm, T ], Boolean )* )(
        implicit
        solver : SMTSolver ) : ( Try[ SatResponses ], Int ) = {

        import com.typesafe.scalalogging.Logger
        import org.slf4j.LoggerFactory

        //  logger
        val logger = Logger( LoggerFactory.getLogger( this.getClass + ".isSatByPrefix" ) )

        logger.info( "Entering isSatByPrefix" )

        val lastCheck = if ( checkNow ) checkSat( timeout )( solver ) else Success( Sat() )

        lastCheck match {
            case result @ Success( UnSat() ) ⇒
                logger.info( s"short-circuiting satisfiability check, result was (UnSat) after asserting $count terms" )
                ( result, count )
            case result @ Success( Sat() ) ⇒
                logger.info( s"Satisfiability check result is  (Sat) after asserting $count terms. Processing remaining terms." )
                xb match {
                    case Seq() ⇒ ( result, count )
                    case ( first, c ) +: l ⇒
                        |= ( first ) match {

                            case Success( AssertResponseSuccess( _ ) ) | Success( AssertResponseStatus( _ ) ) ⇒
                                isSatWithAssertWhileSat( timeout )( count + 1, c, l : _* )

                            case Success( AssertResponseError( e ) ) ⇒
                                logger.error( s"Assert term failed for $first in isSatWithAssertWhileSat" )
                                ( Failure( new Exception( show( e ) ) ), count )

                            case Failure( e ) ⇒
                                logger.error( s"Assert term failed for $first in isSatWithAssertWhileSat" )
                                ( Failure( e ), count )
                        }
                }
            case other ⇒
                logger.info( s"Satisfiability check result is $other after asserting $count terms. **Not** processing remaining terms." )
                ( other, count )
        }

    }

    /**
     * Check by prefixes whether a sequence of 'typed terms' is satisfiable. This is
     * the same as `isSat` except the prefixes of the term sequence `xb` are checked
     * in turn. If one of them is not satisfiable, then the method returns that
     * result without checking longer prefixes.
     *
     * @param xb        The sequence of terms t1, t2, ... tk. The corresponding
     *                  logical statement is t1 &and; t2 &and; ... &and; tk.
     * @param solver    The context should provide an implicit solver.
     *                  You can use the convenience method
     *                  [[[interpreters.Resources.using monadic using]]] to create a solver
     *                  and use it.
     *
     * @return          A pair whose first element is either a Success with the sat status
     *                  or a failure and (reason for failure). Its second element will be
     *                  the count of terms that were used in the check that is being reported.
     */
    final def isSatWithAssertWhileSat[ T <: Term ](
        xb : TypedTerm[ BoolTerm, T ]* )(
        implicit
        solver : SMTSolver ) : ( Try[ SatResponses ], Int ) =
        isSatWithAssertWhileSat( None )(
            0,
            true,
            ( ( xb map { case c ⇒ ( c, true ) } ) : _* ) )( solver )

    /**
     * Pop scopes.
     *
     * @param n  The number of elements to pop.
     *
     */
    def pop(
        n : Int = 1 )(
        implicit
        solver : SMTSolver ) : Try[ GeneralResponse ] = {

        eval( Seq( PopCmd( n.toString ) ) ) flatMap parseAsGeneralResponse
    }

    /**
     * Push new scopes.
     *
     * @param  n  The number of scopes to push.
     *
     */
    def push(
        n : Int = 1 )(
        implicit
        solver : SMTSolver ) : Try[ GeneralResponse ] = {

        eval( Seq( PushCmd( n.toString ) ) ) flatMap parseAsGeneralResponse
    }

    /**
     * When check-sat returned UnSat, provides interpolants if possible
     *
     * @todo: Clear the isInstanceOf and asInstanceOf
     */
    private def getInterpolantsAsTerms[ T <: Term ](
        x1 : Named[ BoolTerm, T ],
        x2 : Named[ BoolTerm, T ],
        xb : Named[ BoolTerm, T ]* )(
        implicit
        solver : SMTSolver ) : Try[ List[ Term ] ] = {

        //  If solver does not support interpolants, abort.
        require (
            solver.getInterpolantCmdString.nonEmpty,
            "Solver does not support interpolants (interpolantCmd not set in config file)" )

        val parseInterpolants = SMTLIB2Parser[ GetInterpolantResponses ]

        //  send command to solver
        eval (
            Seq (
                GetInterpolantCmd(
                    solver.getInterpolantCmdString.get,
                    ( x1 :: x2 :: xb.toList ).map( _.nameDef )
                        .map( x ⇒ QIdTerm( SimpleQId( SymbolId( SSymbol( x ) ) ) ) ) ) ) ) flatMap {
                solverResponse ⇒
                    parseInterpolants ( solverResponse ) match {
                        //  solver returned a response which is not a SatStatus
                        case Success(
                            GetInterpolantResponsesSuccess(
                                InterpolantResponse( xl )
                                )
                            ) ⇒ Success( xl )

                        case Success( GetInterpolantResponsesError( e ) ) ⇒
                            Failure( new Exception( "GetDeclCmd returned: " + e ) )

                        //  Problem occurred in getting interpolants
                        case Failure( e ) ⇒ Failure( e )
                    }
            }

    }

    /**
     * Returns interpolants.
     *
     * The solver must support interpolants. The subject terms must be of type
     * [parser.SMTLIB2Syntax.NamedTerm] and there must be more than 2 terms.
     *
     * Given `x1` and `x2` such that x1 &and; x2 is UNSAT, an interpolant
     * is a predicate I such that:
     *
     * <ol>
     *      <li>x1 &rArr; I</li>
     *      <li> I &and; x2 is UNSAT</li>
     *      <li>the variables in I are variables in both `x1` and `x2`</li>
     * </ol>
     *
     * When `xb` is not empty the previous definition generalises to inductive
     * interpolants.
     *
     * @param x1        First predicate.
     * @param x2        Second predicate.
     * @param xb        Optional list of predicates.
     *
     * @param  return     A list of 1 + |xb| predicates
     */
    def getInterpolants[ T <: Term ](
        x1 : Named[ BoolTerm, T ],
        x2 : Named[ BoolTerm, T ],
        xb : Named[ BoolTerm, T ]* )(
        implicit
        solver : SMTSolver ) : Try[ List[ TypedTerm[ BoolTerm, Term ] ] ] =
        {
            val parserDeclCmdResponse = SMTLIB2Parser[ GetDeclCmdResponse ]

            //  get interpolants as solver terms
            getInterpolantsAsTerms( x1, x2, xb : _* ) match {

                case Success( xitp ) ⇒

                    //  Success. Retrieve the list of free vars in each term and
                    //  their declarations on the solver stack to make a TypedTerm

                    eval( Seq( GetDeclCmd() ) ) flatMap
                        //
                        { solverResponse ⇒ parserDeclCmdResponse ( solverResponse ) } flatMap
                        //
                        {
                            case DeclCmdResponseSuccess( solverDeclStack ) ⇒
                                Success ( xitp map {
                                    case i ⇒

                                        import parser.Analysis
                                        //  collect the ids for interpolant i
                                        val b = Analysis( i ).ids

                                        //  Filter the declarations on the stack.
                                        //  elements on the stack are SortedQId
                                        val defs = for {
                                            SortedQId( x, s ) ← solverDeclStack if b.contains( SimpleQId( x ) )
                                        } yield SortedQId( x, s )

                                        TypedTerm[ BoolTerm, Term ]( defs.toSet, i )
                                } )

                            case DeclCmdResponseError( s ) ⇒
                                Failure( new Exception( s"Get declare command failed: $s.getMessage" ) )
                        }

                //  Interpolants could not be computed.
                case Failure( e ) ⇒ Failure( e )
            }
        }
}
