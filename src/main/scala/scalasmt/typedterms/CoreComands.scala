/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package typedterms

import scala.util.{
    Try,
    Success,
    Failure
}
import parser.PredefinedParsers

/**
 * Basic solver commands.
 */
trait CoreCommands extends PredefinedParsers {

    import theories.BoolTerm
    import parser.SMTLIB2Syntax._
    import interpreters.SMTSolver
    import parser.SMTLIB2Parser
    import parser.Implicits._
    import scala.collection.mutable.ListBuffer
    import parser.SMTLIB2PrettyPrinter.format

    /**
     * Assert a term on the solver's stack
     *
     * @param t         The term to assert
     * @param solver    The context should provide an implicit solver.
     *                  You can use the convenience method
     *                  [[interpreters.Resources.using]] to create
     *                  a solver and  use it.
     */
    def |=[ T <: Term ](
        t : TypedTerm[ BoolTerm, T ] )(
        implicit
        solver : SMTSolver ) : Try[ AssertResponse ] = {

        import com.typesafe.scalalogging.Logger
        import org.slf4j.LoggerFactory

        //  logger
        val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

        logger.trace( "Entering Assert" )

        //  build a script with the commands to be pushed to the solver
        val buildCmdScript = new ListBuffer[ Command ]()

        //  try to retrieve the variables already declared in the solver
        eval( Seq( GetDeclCmd() ) ) flatMap {

            solverResponse ⇒
                logger.info(
                    "Solver response to getDeclCmd: {}", solverResponse )
                parserDeclCmdResponse ( solverResponse ) match {

                    //  the variables could be collected
                    case Success(
                        DeclCmdResponseSuccess( declaredVars )
                        ) ⇒

                        logger.info(
                            "Context retrieved: {}",
                            declaredVars.map( format( _ ).layout ) )
                        logger.info(
                            "Context retrieved: {}",
                            declaredVars.map( _.toString ) )

                        /*
                         * Make a fresh declareCmd for the variables not
                         * already declared in the context.
                         * Create a declare-fun command with the name and sort
                         */
                        for ( v ← t.typeDefs if ( !declaredVars.contains( v ) ) ) {
                            logger.info( "Variable {} is not declared -- Adding declare-fun ", v )
                            //  v should be of type SortedQId by construction of TypedTerm
                            val SortedQId( SymbolId( i ), sort ) = v
                            //  add declare-fun to the script
                            buildCmdScript +=
                                DeclareFunCmd( FunDecl( i, List(), sort ) )
                        }

                        //  Append the assert command to the script
                        buildCmdScript += AssertCmd( t.termDef )

                        //  Send the script to the solver and return raw result
                        eval( buildCmdScript.result ) flatMap
                            //
                            parseAsAssertResponse match {

                                //  Assert resulted in Error message
                                case Success( AssertResponseError( e ) ) ⇒
                                    logger.error( "Assert produced an error in Solver." )
                                    logger.error( " Message is :{}", e )
                                    Failure( new Exception( "Assert failed" + e ) )

                                //  succeeded with either sat status or success
                                case Success( ok ) ⇒
                                    logger.info( "Assert produced an success in Solver." )
                                    logger.info( " Message is :{}", ok )
                                    Success( ok )

                                case Failure( e ) ⇒
                                    logger.error( "Assert produced an error in Solver." )
                                    logger.error( " Message is :{}", e )
                                    Failure( e )
                            }

                    //  another response type came back
                    case Success( DeclCmdResponseError( e ) ) ⇒
                        logger.error( "Could not get context. GetDeclCmd returned :{}", e )
                        Failure( new Exception( "Could not retrieve declared vars :" + e ) )

                    //  another response type came back
                    case Success( other ) ⇒
                        logger.error( "Could not get context. GetDeclCmd returned :{}", other )
                        Failure( new Exception( "Could not retrieve declared vars :" + other ) )

                    //  something went wrong
                    case Failure( e ) ⇒
                        logger.error( "GetDeclCmd failed: {}" + e )
                        Failure( e )
                }
        }
    }

    /**
     * Send the (check-sat) command to the solver and collect result.
     * @return  a sat reponse, Sat, UnSAt or Unknown.
     */
    def checkSat()(
        implicit
        solver : SMTSolver ) : Try[ SatResponses ] = checkSat( None )( solver )

    import scala.concurrent.duration._

    /**
     * Send the (check-sat)  command to the solver with a timeout and collect result.
     *
     * @param   timeout Maximum time to check sat in the solver.
     * @return  a sat reponse, Sat, UnSAt or Unknown.
     */
    def checkSat(
        timeout : Duration )(
        implicit
        solver : SMTSolver ) : Try[ SatResponses ] = checkSat( Some( timeout ) )( solver )

    /**
     * Send the (check-sat) command to the solver and collect result.
     *
     * @param   timeout An optional maximum time to check sat in the solver.
     * @return  a sat reponse, Sat, UnSAt or Unknown.
     */
    def checkSat(
        timeout : Option[ Duration ] )(
        implicit
        solver : SMTSolver ) : Try[ SatResponses ] = {

        import com.typesafe.scalalogging.Logger
        import org.slf4j.LoggerFactory

        //  logger
        val logger = Logger( LoggerFactory.getLogger( this.getClass + "checkSat" ) )

        logger.trace( "Entering checkSat" )

        val parseCheckSat = SMTLIB2Parser[ CheckSatResponses ]

        eval( Seq( CheckSatCmd() ), timeout ) flatMap {

            solverResponse ⇒
                parseCheckSat( solverResponse ) match {

                    //  Sat status was returned, remove response types and
                    //  send Sat/UnSat/UnKnown
                    case Success( CheckSatResponseSuccess( x ) ) ⇒
                        Success ( x )

                    //  Solver returned a response which is not a SatStatus
                    case Success( CheckSatResponseError( e ) ) ⇒ Failure( new Exception( "Check sat did not succeed:" + e ) )

                    //  Problem occured
                    case Failure( e )                          ⇒ Failure( e )
                }
        }
    }

    /**
     * Get the declarations currently on the stack
     */
    def getDeclCmd()(
        implicit
        solver : SMTSolver ) : Try[ List[ SortedQId ] ] = {

        import com.typesafe.scalalogging.Logger
        import org.slf4j.LoggerFactory

        //  logger
        val logger = Logger( LoggerFactory.getLogger( "getDeclCmd" ) )

        eval( Seq( GetDeclCmd() ) ) flatMap
            //
            { solverResponse ⇒ parserDeclCmdResponse ( solverResponse ) } flatMap
            // get the list of QualifiedId
            {
                case DeclCmdResponseSuccess( x ) ⇒ Success( x )
                case DeclCmdResponseError( x ) ⇒
                    Failure( new Exception( s"Could not getDclCmd(): $x" ) )
            } flatMap
            // map them to SortedQId
            { x ⇒
                //  check whether the list contains something which is nit a SortedQId
                if ( x.exists( x ⇒ x match { case _ : SortedQId ⇒ false; case _ ⇒ true } ) ) {
                    logger.error( "Some variables in {} are not SortedQId", x )
                    Failure( new Exception( "Some variables on the stack are not of type SortedQId" ) )
                } else {
                    //  each item in the list is a SortedQId
                    Success( x.map( _.asInstanceOf[ SortedQId ] ) )
                }

            }
    }

    /**
     * When check-sat returns UnSat, returns an unsat core.
     *
     * The unsat core is a list of names, so all the asserted terms must be
     * named terms otherwise the list returned is a partial unsat core that
     * comprises the named terms in an unsat core.
     *
     * @param solver    The context should provide an implicit solver.
     *                  You can use the convenience method
     *                   [[interpreters.Resources.using]] to
     *                  create a solver and  use it.
     *
     * @return          Either a Success with a list of names
     *                  or a failure and (reason for failure).
     */
    def getUnsatCore()(
        implicit
        solver : SMTSolver ) : Try[ Set[ String ] ] = {

        val parseUnsatCore = SMTLIB2Parser[ UnSatCoreResponses ]

        eval( Seq( GetUnsatCoreCmd() ) ) flatMap {

            solverResponse ⇒

                parseUnsatCore( solverResponse ) match {

                    //  A list of strings was returned
                    case Success( GetUnSatCoreResponseSuccess( xl ) ) ⇒
                        Success ( ( xl map { case SepSimpleSymbol( s ) ⇒ s } ).toSet )

                    //  solver returned a response which is not a GetUnSatCoreResponseSuccess
                    case Success( GetUnSatCoreResponseError( e ) ) ⇒
                        Failure( new Exception( "Get-unsat-core did not succeed:" + e ) )

                    //  Problem occured
                    case Failure( e ) ⇒ Failure( e )
                }
        }
    }

    /**
     * Assuming the previous (check-sat) was Sat or Uknown, returns the values of some terms.
     *
     * @param solver    The context should provide an implicit solver.
     *                  You can use the convenience method
     *                  [[interpreters.Resources.using]] to create
     *                  a solver and  use it.
     *
     * @return          Either a Success with a model response
     *                  a failure and (reason for failure).
     */
    def getValue[ A, T <: Term ](
        tt : TypedTerm[ A, T ] )(
        implicit
        solver : SMTSolver ) : Try[ Value ] = {

        import com.typesafe.scalalogging.Logger
        import org.slf4j.LoggerFactory

        //  logger
        val logger = Logger( LoggerFactory.getLogger( "getValue" ) )

        val parseValues = SMTLIB2Parser[ GetValueResponses ]

        logger.info( s"Trying to get value for term $tt" )

        eval( Seq( GetValueCmd( List( tt.termDef ) ) ) ) flatMap {

            solverResponse ⇒

                parseValues ( solverResponse ) match {

                    //  A list of ValuationPair  was returned
                    case Success( GetValueResponseSuccess( xl ) ) ⇒
                        //  xl is a List of ValuationPair (Term,Term)
                        //  retrieve the term tt.termDef
                        ( xl.collectFirst {
                            case ValuationPair( tt.termDef, x ) ⇒ Value( x )
                        } ) match {
                            case Some( v ) ⇒ Success( v )
                            case None      ⇒ Failure( new Exception( s"Could not find value for $tt" ) )
                        }

                    //  solver returned a response which is not a GetValueResponseSuccess
                    case Success( GetValueResponseError( e ) ) ⇒
                        Failure( new Exception( "Get value did not succeed:" + e ) )

                    //  Problem occurred
                    case Failure( e ) ⇒
                        Failure( new Exception( "Get value: cannot parse as GetValueReponses. " + e ) )
                }
        }
    }

    /**
     *  When check-sat returns Sat, returns a model
     *
     * @param solver    The context should provide an implicit solver.
     *                  You can use the convenience method
     *                   [[interpreters.Resources.using]] to
     *                  create a solver and  use it.
     *
     * @return          Either a Success with a model response
     *                  a failure and (reason for failure).
     */
    def getModel()(
        implicit
        solver : SMTSolver ) : Try[ Model ] = {

        val parseModel = SMTLIB2Parser[ GetModelResponses ]

        eval( Seq( GetModelCmd() ) ) flatMap {

            solverResponse ⇒

                parseModel( solverResponse ) match {

                    //  A model  was returned
                    case Success( GetModelFunDefResponseSuccess( xl ) ) ⇒ Success ( new ModelFunDef( xl ) )

                    case Success( GetModelPairResponseSuccess( xl ) )   ⇒ Success ( ModelValPair( xl ) )

                    //  solver returned a response which is not a GetModelResponseSuccess
                    case Success( GetModelResponseError( e ) ) ⇒
                        Failure( new Exception( "Get model did not succeed:" + e ) )

                    //  Problem occured
                    case Failure( e ) ⇒ Failure( e )
                }
        }
    }
}
