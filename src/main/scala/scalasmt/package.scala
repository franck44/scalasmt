/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44

/**
 * This is the documentation for package [[scalasmt]].
 *
 * == Overview ==
 * This package provides an abstraction over the SMTLIB standard. This means you
 * can interact with SMTLIB2 compliant SMT-solvers via a Scala API.
 * Currently, the supported solvers are: Z3, CVC4, SMTInterpol and MathSAT.
 *
 * == Usage ==
 * Predefined solvers are available as well as predefined logics.
 * To get a running SMT solver, say Z3, configured with the logic QF_LIA,
 * you can run the Scala REPL and issue the following imports and command:
 *
 * {{{
 * import org.bitbucket.franck44.scalasmt.interpreters.{SMTSolver, Resources}
 * import org.bitbucket.franck44.scalasmt.configurations.Configurations._
 * import org.bitbucket.franck44.scalasmt.solvers._
 * import org.bitbucket.franck44.scalasmt.theories.PredefinedLogics.QF_LIA
 *
 * scala> val z3 = SMTSolver(new Z3 with QF_LIA)
 * z3: scala.util.Try[org.bitbucket.franck44.scalasmt.interpreters.ExtendedSMTLIB2Interpreter] =
 * Success(org.bitbucket.franck44.scalasmt.interpreters.SMTSolver$$anon\$17be6b288)
 * }}}
 *
 * After this command, you should have a process running with the name "z3".
 * You can check with the `ps aux | grep z3` command in a terminal.
 * An easy way to use the solver is to extract it from the previously `Try`
 * returned instance and make it `implicit`:
 * {{{
 * scala> implicit val solver = z3.get
 * solver: org.bitbucket.franck44.scalasmt.interpreters.ExtendedSMTLIB2Interpreter =
 * org.bitbucket.franck44.scalasmt.interpreters.SMTSolver$$anon\$17be6b288
 * }}}
 *
 * You can now use the predefined commands as follows:
 * {{{
 * import org.bitbucket.franck44.scalasmt.typedterms.Commands
 * object usefulCommands extends Commands
 * import usefulCommands._
 *
 * scala> checkSat()
 * res2: scala.util.Try[org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.SatResponses] =
 * Success(Sat())
 * }}}
 *
 * If you want to build simple terms, predefined theoriesare defined in the [[theories]]
 * package:
 * {{{
 * import org.bitbucket.franck44.scalasmt.theories.{Core, IntegerArithmetics}
 * object usefulLogics extends Core with IntegerArithmetics
 * import usefulLogics._
 *
 * scala> val a = Ints("a")            // define a solver variable a of sort Int
 * a: org.bitbucket.franck44.scalasmt.typedterms.VarTerm[org.bitbucket.franck44.scalasmt.theories.IntTerm] =
 * TypedTerm(Set(SortedQId(SymbolId(SSymbol(a)),IntSort())),QIdTerm(SimpleQId(SymbolId(SSymbol(a)))))
 *
 * scala> val b = Ints("b")
 * b: org.bitbucket.franck44.scalasmt.typedterms.VarTerm[org.bitbucket.franck44.scalasmt.theories.IntTerm] =
 * TypedTerm(Set(SortedQId(SymbolId(SSymbol(b)),IntSort())),QIdTerm(SimpleQId(SymbolId(SSymbol(b)))))
 *
 * scala> isSat( a <= b & b <= 2)
 * res3: scala.util.Try[org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.SatResponses] =
 * Success(Sat())
 *
 * scala> val m = getModel()
 * m: scala.util.Try[org.bitbucket.franck44.scalasmt.typedterms.Model] =
 * Success(Model(List(ModelResponse(...
 *
 * scala> m.get.valueOf(a)
 * res5: Option[org.bitbucket.franck44.scalasmt.typedterms.Value] =
 * Some(Value(ConstantTerm(NumLit(0))))
 *
 * scala> res5.get.show    //  pretty print the result
 * res10: String = 0
 * }}}
 * To kill the running solver use the `destroy()` command:
 * {{{
 * scala> solver.destroy()
 * }}}
 * Notice that if you do not issue the `destroy()` command you may end up with lots of wandering
 * processes running (and the resources they hold, e.g. pipes,  are not released).
 *
 * To avoid manual management of processes, we provide a convenience method.
 * Automatic resource management (i.e. releasing the resources acquired by
 * the solver) is provided by the `using` convenience method as described
 * in the next section.
 *
 * == Advanced usage ==
 *
 * {{{
 *
 * scala> object usefulDefs extends Core with IntegerArithmetics with Commands with Resources
 * defined object usefulDefs
 *
 * scala> import usefulDefs._
 * import usefulDefs._
 *
 * scala> val response = using( new Z3 with QF_LIA  ) {
 *      implicit withSolver ⇒       //  make an implicit solver visible
 *                  checkSat()      //  your script to evaluate by the solver
 * }
 * response: scala.util.Try[org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.SatResponses] =
 * Success(Sat())
 *
 * }}}
 * The previous snippet triggered the following actions:
 *
 *  - import some traits to create configured solvers, some logics and Commands
 *  - the `using` construct makes a solver of type Z3 configured with the logic QF_LIA
 *  - within the `using` scope, a solver process running Z3 can be used to answer queries. The `checkSat()` command
 * is the simplest one. As nothing is asserted on the solver's stack, it successfully returns `Sat()`.
 * At the end of `using`  the process running Z3 is killed and the resources (pipes) used to communicate
 * with the Scala program are released.
 *
 * === Uninterpreted functions ===
 *
 * The theory QF_UF, quantifier-free uninterpreted functions, is the core theory to
 * express Boolean logic queries.
 * An example is as follows:
 *
 * {{{
 * import org.bitbucket.franck44.scalasmt.interpreters.{SMTSolver, Resources}
 * import org.bitbucket.franck44.scalasmt.configurations.Configurations._
 * import org.bitbucket.franck44.scalasmt.solvers._
 * import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.{ SuccessResponse }
 * import org.bitbucket.franck44.scalasmt.typedterms.Commands
 * import org.bitbucket.franck44.scalasmt.theories.Core
 * import org.bitbucket.franck44.scalasmt.theories.PredefinedLogics.QF_UF
 *
 * scala> object usefulDefs extends Core with Commands with Resources
 * defined object usefulDefs
 *
 * scala>  import usefulDefs._
 * import usefulDefs._
 *
 * scala> val p = Bools("p")        // define a variable of sort Bool
 * p: org.bitbucket.franck44.scalasmt.typedterms.VarTerm[org.bitbucket.franck44.scalasmt.theories.BoolTerm]
 * = TypedTerm(Set(SortedQId(SymbolId(SSymbol(p)),BoolSort())),QIdTerm(SimpleQId(SymbolId(SSymbol(p)))))
 *
 * scala> val q = Bools("q")
 * q: org.bitbucket.franck44.scalasmt.typedterms.VarTerm[org.bitbucket.franck44.scalasmt.theories.BoolTerm]
 * = TypedTerm(Set(SortedQId(SymbolId(SSymbol(q)),BoolSort())),QIdTerm(SimpleQId(SymbolId(SSymbol(q)))))
 *
 * scala> val response = using( new Z3 with QF_LIA) {
 *      implicit withSolver ⇒
 *              checkSat()
 * }
 * }}}
 *
 */
package object scalasmt {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    /**
     * Get a new logger whose name is formed from the name of the given
     * class with the suffix appended (default: no suffix).
     */
    def getLogger( clazz : Class[ _ ], suffix : String = "" ) : Logger =
        Logger( LoggerFactory.getLogger( clazz.getName + suffix ) )

    /**
     * For top level logger
     * use org.slf4j.Logger.ROOT_LOGGER_NAME to get the root logger
     */
    private val logger = Logger( LoggerFactory.getLogger( "org.bitbucket.franck44.scalasmt" ) )

    /**
     *   Same as Kiama: convenient type constructor for partial functions.
     */
    type ==>[ -T, +U ] = PartialFunction[ T, U ]

    import org.bitbucket.franck44.scalasmt.interpreters.{ SMTSolver, Resources }
    import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax._
    import scala.util.{ Try, Success, Failure }
    import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Parser
    import scala.concurrent.duration._

    /**
     * Evaluate a <strong>non-empty</strong> sequence of commands
     *
     * The timeout for each command is the default one for the solver.
     *
     * @param cmds          The sequence of commands to be pushed to the solver
     * @param timemout      The timeout for evaluating the commands. If 'None'
     *                      the default solver timeout is used.
     * @param s             A solver (implicit)
     *
     * @return              The result of the evaluation as a stream
     *
     * @see                  [[interpreters.Resources.using]] for example.
     */
    def eval[ C <: Command ](
        cmds :    Seq[ C ],
        timeout : Option[ Duration ] = None )(
        implicit
        s : SMTSolver ) : Try[ String ] =

        cmds match {

            // Allow this and return success (can be parsed a successResponse )
            case Nil ⇒
                logger.info( "eval-cmd on an empty sequence -- returns Success" )
                Success( "success" )

            //  one command to evaluate
            case cmd :: Nil ⇒

                logger.info( s"eval-cmd $cmd" )
                s.eval( cmd, timeout )

            //  sequence of commands: chain possible Failures/Success
            case cmd :: xc ⇒

                logger.info( s"eval-cmd::xc $cmd" )
                s.eval( cmd, timeout ) flatMap ( _ ⇒ eval( xc, timeout )( s ) )
        }

    import org.bitbucket.inkytonik.kiama.util.StringSource
    import scala.reflect.ClassTag

    /**
     * Evaluate a command and parse result with a parser of type T
     */
    def evalAndParseAs[ T <: ASTNode ](
        cmds :    Seq[ Command ],
        timeout : Option[ Duration ] = None )(
        implicit
        mf : ClassTag[ T ],
        s :  SMTSolver ) : Try[ T ] =
        eval( cmds, timeout ) flatMap
            //
            {
                x ⇒
                    val parse = SMTLIB2Parser[ T ]
                    parse( StringSource( x ) )
            }

    /**
     * REPL for SMTLIB2 compliant solvers.
     *
     * Evaluates commands and prints out responses.
     *
     * @param s a solver.
     */
    def repl( s : SMTSolver ) : Try[ String ] = {

        //  using
        object managedResources extends Resources
        import managedResources.using

        //  get a parser for Command
        val parseCommand = SMTLIB2Parser[ Command ]

        using ( s ) {

            implicit solver ⇒
                {
                    //  loop until exit or exception
                    def replRec() : Try[ String ] = scala.io.StdIn.readLine() match {

                        case theLine ⇒
                            ( parseCommand( StringSource( theLine ) ) match {

                                // if success we eval the AST
                                case Success( c ) ⇒
                                    logger.info( "Command parsed -- Sending AST to solver" )

                                    eval( Seq( c ) )

                                // if not, we eval the raw string
                                case Failure( _ ) ⇒
                                    logger.info( "Command cannot be parsed -- Sending raw line to solver" )

                                    eval( Seq( Raw( theLine ) ) )

                            } ) match {

                                //  hopefully this is a success response
                                case Success( "Solver exit" ) ⇒
                                    logger.trace( "Exit command sent" )
                                    println( Console.GREEN + "Terminating: " + Console.RESET )

                                    Success( "" )

                                case Success( r ) ⇒
                                    logger.trace( "Specific success response: |{}|", r )

                                    println( r )
                                    replRec()

                                case Failure( e ) ⇒
                                    println( Console.RED + "Error occurred: " + e + Console.RESET )
                                    logger.error( "{}", e )
                                    Failure( e )
                            }

                    }

                    //  Start the REPL
                    println( Console.GREEN +
                        s"Running ${s.name} configured with ${s.initSetUp.show}" + Console.RESET )
                    println( Console.BLUE + "Enter your command>" + Console.RESET )

                    replRec()

                }
        }
    }

    import org.bitbucket.franck44.scalasmt.theories.{ IntegerArithmetics, Core, RealArithmetics }
    import org.bitbucket.franck44.scalasmt.typedterms.Commands

    /**
     * A ready-to-use QF_LIA theory
     */
    object Theory_QF_LIA
        extends IntegerArithmetics
        with Core
        with Commands
        with Resources {

        import org.bitbucket.franck44.scalasmt.configurations.SMTLogics.QF_LIA
        import org.bitbucket.franck44.scalasmt.configurations.SMTOptions._
        import org.bitbucket.franck44.scalasmt.configurations.SMTInit

        /**
         * The logic of the solver
         */
        lazy val logik = QF_LIA

        /**
         * Get a running solver
         * @param  name   The solver to run
         * @param  l      The list of options to initialise the solver with
         */
        def startSolver( name : String, l : List[ SMTLIBOption ] = List() ) =
            new SMTSolver( name, new SMTInit( logik, l ) )
    }

    /**
     * A  ready-to-use QF_LRA theory
     */
    object Theory_QF_LRA
        extends RealArithmetics
        with Core
        with Commands
        with Resources {

        import org.bitbucket.franck44.scalasmt.configurations.SMTLogics.QF_LRA
        import org.bitbucket.franck44.scalasmt.configurations.SMTOptions._
        import org.bitbucket.franck44.scalasmt.configurations.SMTInit

        /**
         * The logic of the solver
         */
        lazy val logik = QF_LRA

        /**
         * Get a running solver
         * @param  name   The solver to run
         * @param  l      The list of options to initialise the solver with
         */
        def startSolver( name : String, l : List[ SMTLIBOption ] = List() ) =
            new SMTSolver( name, new SMTInit( logik, l ) )
    }

}
