/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package configurations

import scala.concurrent.duration.FiniteDuration
import SMTLogics.ConfigLogics
import SMTOptions._

/**
 * Parse a solver configuration file.
 */
object AppConfig {

    import com.typesafe.config.{ ConfigFactory, ConfigException, Config }
    import net.ceedubs.ficus.readers.ArbitraryTypeReader._
    import net.ceedubs.ficus.readers.EnumerationReader._
    import net.ceedubs.ficus.Ficus._
    import scala.util.{ Try, Success, Failure }

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = getLogger( this.getClass )

    /** Read the default configuration. */
    def readDefaultConfig() = {
        /**
         * Use the classLoader as in some cases (not JVM's main thread)
         * this can result in errors.
         * {@link https://github.com/lightbend/config#debugging-your-configuration}
         */
        val c = ConfigFactory.load( getClass().getClassLoader() )
        logger.info( c.root().render() )
        c
    }

    /**
     * Read the default config using the standard typesafe.config behaviour.
     * {@link https://github.com/lightbend/config#standard-behavior}
     */
    lazy val rawConfig = readDefaultConfig()

    /**
     * The default config.
     */
    lazy val config = getConfig( rawConfig )

    /**
     * Extract the configuration or abort.
     */
    def getConfig( c : Config ) : List[ SolverConfig ] = Try( c.as[ List[ RawSolverConfig ] ]( "app" ) ) match {
        case Success( s ) ⇒
            logger.info( s"Config read $s" )
            s.map( _.toSolverConfig )
        case Failure( f ) ⇒
            logger.error( s"Could not configure solvers ${f.getMessage}" )
            sys.error( s"Could not configure solvers ${f.getMessage}" )
    }

    /**
     * Unused and broken.
     * TODO: fix it by adding a REPL config to application.conf that sets the solver configuration for the REPL.
     */
    // private def getConfigREPL( c : Config ) : SMTInit = Try( c.as[ RawSolverConfig ]( "repl" ) ) match {
    //     case Success( s ) ⇒ s.toSolverConfig
    //     case Failure( f ) ⇒ sys.error( s"Could not configure solvers ${f.getMessage} for REPL" )
    // }

    /**
     * The list of enabled solvers.
     */
    lazy val enabledSolvers : List[ String ] = Try( rawConfig.as[ List[ String ] ]( "enabled" ) ) match {
        case Failure( f ) ⇒ sys.error( s"Enabled solvers not defined: ${f.getMessage}" )
        case Success( c ) ⇒ c
    }
}
