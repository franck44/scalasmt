/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package configurations

import scala.concurrent.duration._
import SMTLogics.ConfigLogics
import SMTOptions._
import parser.SMTLIB2Syntax.SMTLIBOption

/**
 * An initial configuration for a solver.
 *
 * @param logik     The logic to be set if any
 * @param options   The list of set options.
 */
case class SMTInit(
    logik :   Option[ ConfigLogics ],
    options : Seq[ SMTLIBOption ] ) {

    /** Empty configuration */
    def this() = this( None, List() )

    /** Set logic and some options */
    def this( l : ConfigLogics, o : Seq[ SMTLIBOption ] = List() ) = this( Some( l ), o )

    /** Set options only */
    def this( options : Seq[ SMTLIBOption ] ) = this( None, options )

    /** Dislay the configurations */
    def show = s"Logic: ${logik.getOrElse( "Not set" )}. Options: ${options}"

    //  Build an  SMTLIB2Syntax AST (Commands)
    import parser.SMTLIB2Syntax.{ Command, SetOptionCmd, SetLogicCmd }

    /**
     * Generate a list of commands to set up
     * the solver with the configuartion
     */
    def toSMTLIBCmds : List[ Command ] = {
        val opCmds : List[ Command ] = options.map( SetOptionCmd ).toList
        logik match {
            case None      ⇒ opCmds
            case Some( l ) ⇒ opCmds :+ SetLogicCmd( SMTLogics.toSMTLIBLogic( l ) )
        }
    }
}

/**
 * Make a solver configuration from a configuration file (HOCON style)
 * @param executable             The name of the executable for the solver
 * @param name                   The name of the solver
 * @param version                The version number (unused)
 * @param args                   The argument to be passed to the executable to run *                                the solver
 * @param timeout                The maximum amount of time for each command to
 *                               complete
 * @param interpolantCmd         If defined, the name of the command to get
 *                               interpolants. Otherwise solver does not support interpolants.
 * @param prompt1                TBD
 * @param promp2                 TBD
 * @param supportedLogics        The logics supported by the solver
 * @param supportedOptions       The options config tags supported by the solver
 * @param supportedBoolOptions   The ScalaSMT boolean options supported by the solver.
 * @param supportedIntOptions    The ScalaSMT boolean options supported by the solver.
 * @param supportedStringOptions The ScalaSMT boolean options supported by the solver.
 */
case class SolverConfig(
    executable :                             String,
    name :                                   String,
    version :                                String,
    args :                                   List[ String ],
    timeout :                                FiniteDuration,
    interpolantCmd :                         Option[ String ],
    prompt1 :                                String,
    prompt2 :                                String,
    private[ scalasmt ] val supportedLogics :Set[ ConfigLogics ],
    private[ scalasmt ] val supportedOptions :Set[ SMTOptions.RawConfigOptions ],
    val supportedBoolOptions :               Set[ SMTOptions[ Boolean ] ],
    val supportedIntOptions :                Set[ SMTOptions[ Int ] ],
    val supportedStringOptions :             Set[ SMTOptions[ String ] ] ) {

    import scala.util.matching.Regex

    /**
     * The string sent as an echo command to check if previous command has
     * completed.
     */
    val cmdSentinelleString = "<EOC>"

    /**
     * The prompt (solver) that is expected after each command completes.
     */
    val expectedPromptAfterCommand : Regex = ( prompt1 + cmdSentinelleString + prompt2 ).r

    /**
     * The maximum amount of time for a command to complete.
     */
    val timeoutPerEval = timeout

    /**
     *  Optional command string for get-interpolants
     */
    val getInterpolantCmdString = interpolantCmd

    /**  */
    // val supportedOptions = supportedBoolOptions

    import AppConfig.enabledSolvers
    /**
     * @return  Solver enabled or not
     */
    lazy val enabled =
        if ( enabledSolvers.contains( "all" ) )
            true
        else
            enabledSolvers.contains( name )

    /**
     * Whether the solver supports a ScalaSMT option.
     * @param  o   The option.
     * @return     Whether `this` supports option `o`.
     */
    def supports[ T ]( o : SMTOptions[ T ] ) : Boolean = supports( o.name )

    /**
     * Whether the solver supports a set SMTLIBOption option.
     * @param  o   The option.
     * @return     Whether `this` supports option `o`.
     */
    def supports( o : SMTLIBOption ) : Boolean = supportedOptions.contains( optionTag( o ) )

    /**
     * Whether the solver supports `SMTOptions.Value` option.
     * @param  o   The option.
     * @return     Whether `this` supports option `o`.
     */
    def supports( o : SMTOptions.RawConfigOptions ) : Boolean = supportedOptions.contains( o )

    /**
     * Whether the solver supports `SMTLogics.ConfigLogics` option.
     * @param  l   The logic.
     * @return     Whether `this` supports logic `l`.
     */
    def supportsLogic( l : SMTLogics.ConfigLogics ) : Boolean = supportedLogics.contains( l )

}

/**
 * A configuration in its raw form (options are configuration tags).
 */
private case class RawSolverConfig(
    executable :                 String,
    name :                       String,
    version :                    String,
    args :                       List[ String ],
    timeout :                    FiniteDuration,
    interpolantCmd :             Option[ String ],
    prompt1 :                    String,
    prompt2 :                    String,
    val supportedLogics :        Set[ ConfigLogics ],
    val supportedBoolOptions :   Set[ SMTOptions.RawConfigOptions ],
    val supportedIntOptions :    Set[ SMTOptions.RawConfigOptions ],
    val supportedStringOptions : Set[ SMTOptions.RawConfigOptions ] ) {

    /**  */
    private def toOptBool( o : SMTOptions.RawConfigOptions ) : SMTOptions[ Boolean ] = o match {
        case PRINTSUCCESS            ⇒ SMTPrintSuccess
        case PROOFS                  ⇒ SMTProduceProofs
        case MODELS                  ⇒ SMTProduceModels
        case GLOBALDECL              ⇒ SMTGlobalDeclarations
        case UNSATCORES              ⇒ SMTProduceUnsatCores
        case PRODUCEASSERTIONS       ⇒ SMTProduceAssertions
        case PRODUCEASSIGNMENTS      ⇒ SMTProduceAssignments
        case PRODUCEUNSATASSUMPTIONS ⇒ SMTProduceUnsatAssumptions
        case INTERPOLANTS            ⇒ SMTProduceInterpolants
        case INTERACTIVEMODE         ⇒ SMTInteractiveMode
        case _                       ⇒ sys.error( s"Cannot get tag for SMTLIBOption $o" )
    }

    private def toOptInt( o : SMTOptions.RawConfigOptions ) : SMTOptions[ Int ] = o match {
        case RANDOMSEED  ⇒ SMTSetRandomSeed
        case REPRESLIMIT ⇒ SMTSetReproResLimit
        case VERBOSITY   ⇒ SMTSetVerbosity
        case _           ⇒ sys.error( s"Cannot get tag for SMTLIBOption $o" )
    }

    private def toOptString( o : SMTOptions.RawConfigOptions ) : SMTOptions[ String ] = o match {
        case DIAGOUTPUTCHANNEL ⇒ SMTSetDiagnosticOutputChannel
        case REGOUTPUTCHANNEL  ⇒ SMTSetRegOutputChannel
        case _                 ⇒ sys.error( s"Cannot get tag for SMTLIBOption $o" )
    }

    /**
     * Make a ScalaSMT configuration from the raw config file.
     *
     */
    def toSolverConfig = SolverConfig(
        this.executable,
        this.name,
        this.version,
        this.args,
        this.timeout,
        this.interpolantCmd,
        this.prompt1,
        this.prompt2,
        this.supportedLogics,
        this.supportedBoolOptions ++ this.supportedIntOptions ++ this.supportedStringOptions,
        this.supportedBoolOptions.map( toOptBool ),
        this.supportedIntOptions.map( toOptInt ),
        this.supportedStringOptions.map( toOptString ) )
}
