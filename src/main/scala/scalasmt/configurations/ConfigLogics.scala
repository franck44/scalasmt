/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package configurations

/**
 * Logics used in SMTSolvers.
 * {@link http://smtlib.cs.uiowa.edu/logics.shtml}
 */
object SMTLogics extends Enumeration {

    type ConfigLogics = Value

    /** {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_LIA} */
    val QF_LIA = Value( "QF_LIA" )

    /** {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_LRA} */
    val QF_LRA = Value( "QF_LRA" )

    /** {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_NRA} */
    val QF_NRA = Value( "QF_NRA" )

    /** {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#AUFNIRA} */
    val AUFNIRA = Value( "AUFNIRA" )

    /** {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#AUFNIRA} */
    val QF_AUFLIRA = Value( "QF_AUFLIRA" )

    /** {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_UFLIRA} */
    val QF_UFLIRA = Value( "QF_UFLIRA" )

    /** {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_LIRA} */
    val QF_LIRA = Value( "QF_LIRA" )

    /** {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_NIRA} */
    val QF_NIRA = Value( "QF_NIRA" )

    /** {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_AUFLIA} */
    val QF_AUFLIA = Value( "QF_AUFLIA" )

    /** {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_AUFNIA} */
    val QF_AUFNIA = Value( "QF_AUFNIA" )

    /** {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_BV} */
    val QF_BV = Value( "QF_BV" )

    /** {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_BV} */
    val QF_ABV = Value( "QF_ABV" )

    /** {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_UF} */
    val QF_UF = Value( "QF_UF" )

    /** {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_FPBV} */
    val QF_FPBV = Value( "QF_FPBV" )

    /** {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_AFPBV} */
    val QF_AFPBV = Value( "QF_AFPBV" )

    import parser.SMTLIB2Syntax.SSymbol
    /**
     * Map enumeration type options to SMTLIB2Syntax AST SMTLIBOption
     */
    def toSMTLIBLogic( l : ConfigLogics ) : SSymbol = SSymbol( l.toString )

}