/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package configurations

/**
 * An SMT option of type T.
 * @tparam   T       The type of this SMTOption.
 * @param    name    The config file tag name of this option.
 */
abstract class SMTOptions[ T ]( val name : SMTOptions.RawConfigOptions ) {

    /**
     * Generate the SMTLIB command to set the option to a value of type T.
     * @param v    The value to set for the option.
     * @return     The SMTLIBOption term that sets this option to `v`.
     */
    def apply( v : T ) : parser.SMTLIB2Syntax.SMTLIBOption

    val keyword : parser.SMTLIB2Syntax.Keyword
}

/**
 * Options used in SMTSolvers.
 */
object SMTOptions extends Enumeration {

    type RawConfigOptions = Value

    //  Standard option tags in config file.

    //  Boolean options

    /** Print Success. */
    val PRINTSUCCESS = Value( "PRINTSUCCESS" )

    /** Global declarations. */
    val GLOBALDECL = Value( "GLOBALDECL" )

    /** Generate proofs (when sat) */
    val PROOFS = Value( "PROOFS" )

    /** Generate models (when unsat) */
    val MODELS = Value( "MODELS" )

    /** Support for unsat core */
    val UNSATCORES = Value( "UNSATCORES" )

    /** Producer Assertions.. */
    val PRODUCEASSERTIONS = Value( "PRODUCEASSERTIONS" )

    /** Produce Assignments. */
    val PRODUCEASSIGNMENTS = Value( "PRODUCEASSIGNMENTS" )

    /** Producer Assignments.. */
    val PRODUCEUNSATASSUMPTIONS = Value( "PRODUCEUNSATASSUMPTIONS" )

    /** Support for interactive mode  */
    val INTERACTIVEMODE = Value( "INTERACTIVEMODE" )

    //  Numeric options

    /** Support for setting random seed  */
    val RANDOMSEED = Value( "RANDOMSEED" )

    /** Support for reproducible resource limit  */
    val REPRESLIMIT = Value( "REPRESLIMIT" )

    /** Support for verbosity level option  */
    val VERBOSITY = Value( "VERBOSITY" )

    //  String options

    /** Support for setting diagnostic channel  */
    val DIAGOUTPUTCHANNEL = Value( "DIAGOUTPUTCHANNEL" )

    /** Support for setting regular output channel   */
    val REGOUTPUTCHANNEL = Value( "REGOUTPUTCHANNEL" )

    //  Non-standard options

    /** Support for interpolants (logic dependent) */
    val INTERPOLANTS = Value( "INTERPOLANTS" )

    import parser.SMTLIB2Syntax.{
        SMTLIBOption,
        PrintSuccess,
        GlobalDeclarations,
        ProduceModels,
        ProduceProofs,
        ProduceUnsatCores,
        ProduceAssertions,
        ProduceAssignments,
        ProduceUnsatAssumptions,
        ProduceInterpolants,
        RandomSeed,
        ReproducibleResourceLimit,
        Verbosity,
        InteractiveMode,
        DiagnosticOutputChannel,
        RegularOutputChannel,
        StringLiteral,
        Keyword
    }

    /**
     * The ScalaSMT boolean options. They match the boolean options of SMTLIB2.
     * PrintSuccess is not an actionable option, so it is not in this list.
     */
    lazy val allBoolOptions = List[ SMTOptions[ Boolean ] ](
        SMTProduceProofs,
        SMTProduceModels,
        SMTGlobalDeclarations,
        SMTProduceUnsatCores,
        SMTProduceInterpolants,
        SMTProduceAssertions,
        SMTProduceAssignments,
        SMTProduceUnsatAssumptions,
        SMTInteractiveMode )

    /**
     * The ScalaSMT numeric options. They match the numeric options of SMTLIB2.
     */
    lazy val allIntOptions = List[ SMTOptions[ Int ] ](
        SMTSetRandomSeed,
        SMTSetReproResLimit,
        SMTSetVerbosity )

    /**
     * The ScalaSMT string options. They match the string options of SMTLIB2.
     */
    lazy val allStringOptions = List[ SMTOptions[ String ] ](
        SMTSetDiagnosticOutputChannel,
        SMTSetRegOutputChannel )

    //  ScalaSMT options objects.

    /** Print success option */
    object SMTPrintSuccess extends SMTOptions[ Boolean ]( PRINTSUCCESS ) {
        def apply( b : Boolean ) : SMTLIBOption = PrintSuccess( b )

        val keyword = Keyword( "print-success" )
    }

    /** Produce proofs option */
    object SMTProduceProofs extends SMTOptions[ Boolean ]( PROOFS ) {
        def apply( b : Boolean ) : SMTLIBOption = ProduceProofs( b )

        val keyword = Keyword( "produce-proofs" )
    }

    /** Produce models option */
    object SMTProduceModels extends SMTOptions[ Boolean ]( MODELS ) {
        def apply( b : Boolean ) : SMTLIBOption = ProduceModels( b )

        val keyword = Keyword( "produce-models" )

    }

    /** Global Declarations option */
    object SMTGlobalDeclarations extends SMTOptions[ Boolean ]( GLOBALDECL ) {
        def apply( b : Boolean ) : SMTLIBOption = GlobalDeclarations( b )

        val keyword = Keyword( "global-declarations" )

    }

    /** Produce Unsat Cores  option */
    object SMTProduceUnsatCores extends SMTOptions[ Boolean ]( UNSATCORES ) {
        def apply( b : Boolean ) : SMTLIBOption = ProduceUnsatCores( b )

        val keyword = Keyword( "produce-unsat-cores" )

    }

    /** Produce assertion  option */
    object SMTProduceAssertions extends SMTOptions[ Boolean ]( PRODUCEASSERTIONS ) {
        def apply( b : Boolean ) : SMTLIBOption = ProduceAssertions( b )

        val keyword = Keyword( "produce-assertions" )

    }

    /** Produce assignments  option */
    object SMTProduceAssignments extends SMTOptions[ Boolean ]( PRODUCEASSIGNMENTS ) {
        def apply( b : Boolean ) : SMTLIBOption = ProduceAssignments( b )

        val keyword = Keyword( "produce-assignments" )

    }

    /** Produce unsat assumptions  option */
    object SMTProduceUnsatAssumptions extends SMTOptions[ Boolean ]( PRODUCEUNSATASSUMPTIONS ) {
        def apply( b : Boolean ) : SMTLIBOption = ProduceUnsatAssumptions( b )

        val keyword = Keyword( "produce-unsat-assumptions" )

    }

    /** Produce interpolants  option */
    object SMTProduceInterpolants extends SMTOptions[ Boolean ]( INTERPOLANTS ) {
        def apply( b : Boolean ) : SMTLIBOption = ProduceInterpolants( b )

        val keyword = Keyword( "produce-interpolants" )

    }

    /** Interactive mode option */
    object SMTInteractiveMode extends SMTOptions[ Boolean ]( INTERACTIVEMODE ) {
        def apply( b : Boolean ) : SMTLIBOption = InteractiveMode( b )

        val keyword = Keyword( "interactive-mode" )

    }

    //  Int options.

    /** Set random seed option */
    object SMTSetRandomSeed extends SMTOptions[ Int ]( RANDOMSEED ) {
        def apply( i : Int ) : SMTLIBOption = RandomSeed( i )

        val keyword = Keyword( "random-seed" )

    }

    /** Set reproducible resource limit option */
    object SMTSetReproResLimit extends SMTOptions[ Int ]( REPRESLIMIT ) {
        def apply( i : Int ) : SMTLIBOption = ReproducibleResourceLimit( i )

        val keyword = Keyword( "reproducible-resource-limit" )

    }

    /** Set verbosity option */
    object SMTSetVerbosity extends SMTOptions[ Int ]( VERBOSITY ) {
        def apply( i : Int ) : SMTLIBOption = Verbosity( i )

        val keyword = Keyword( "verbosity" )

    }

    //  String options.

    /** Set diagnostic output channel  option */
    object SMTSetDiagnosticOutputChannel extends SMTOptions[ String ]( DIAGOUTPUTCHANNEL ) {
        def apply( s : String ) : SMTLIBOption = DiagnosticOutputChannel( StringLiteral( s ) )

        val keyword = Keyword( "diagnostic-output-channel" )

    }

    /** Set regular output channel  option */
    object SMTSetRegOutputChannel extends SMTOptions[ String ]( REGOUTPUTCHANNEL ) {
        def apply( s : String ) : SMTLIBOption = RegularOutputChannel( StringLiteral( s ) )

        val keyword = Keyword( "regular-output-channel" )

    }

    /**
     *  Extract the config tag for this SMTLIBoption.
     *  @param o    The set SMTLIBOption.
     *  @return     The corresponding option config tag.
     */
    def optionTag( o : SMTLIBOption ) : SMTOptions.RawConfigOptions = o match {
        case PrintSuccess( _ )              ⇒ PRINTSUCCESS
        case ProduceProofs( _ )             ⇒ PROOFS
        case ProduceModels( _ )             ⇒ MODELS
        case GlobalDeclarations( _ )        ⇒ GLOBALDECL
        case ProduceUnsatCores( _ )         ⇒ UNSATCORES
        case ProduceAssertions( _ )         ⇒ PRODUCEASSERTIONS
        case ProduceAssignments( _ )        ⇒ PRODUCEASSIGNMENTS
        case ProduceUnsatAssumptions( _ )   ⇒ PRODUCEUNSATASSUMPTIONS
        case ProduceInterpolants( _ )       ⇒ INTERPOLANTS
        case InteractiveMode( _ )           ⇒ INTERACTIVEMODE
        case RandomSeed( _ )                ⇒ RANDOMSEED
        case ReproducibleResourceLimit( _ ) ⇒ REPRESLIMIT
        case Verbosity( _ )                 ⇒ VERBOSITY
        case DiagnosticOutputChannel( _ )   ⇒ DIAGOUTPUTCHANNEL
        case RegularOutputChannel( _ )      ⇒ REGOUTPUTCHANNEL
        case _                              ⇒ sys.error( s"Cannot get tag for SMTLIBOption $o" )
    }

}
