/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories

trait IntTerm extends RealTerm with IndexTerm

/**
 * This theory of Ints contains the basic elements of integer arithmetic.
 * {@link http://smtlib.cs.uiowa.edu/theories-Ints.shtml} for current version
 * :sorts ((Int 0))
 *
 * :funs (
 *      (NUMERAL Int)
 *      (- Int Int)                  ; negation
 *      (- Int Int Int :left-assoc)  ; subtraction
 *      (+ Int Int Int :left-assoc)
 *      (* Int Int Int :left-assoc)
 *      (div Int Int Int :left-assoc)
 *      (mod Int Int Int)
 *      (abs Int Int)
 *      (<= Int Int Bool :chainable)
 *      (<  Int Int Bool :chainable)
 *      (>= Int Int Bool :chainable)
 *      (>  Int Int Bool :chainable)
 *
 * <ul>
 *  <li> The Int sort </li>
 *  <li> The numerals as Int constants </li>
 *  <li> +, -, *, mod, div, abs </li>
 *  <li> multiplication, div, mod only permitted with a scalar</li>
 *  <li> comparison operators between terms yielding a Bool</li>
 * </ul>
 */
trait IntegerArithmetics {

    import typedterms.{ TypedTerm, VarTerm }

    /**
     *  Provide factory and methods to build  integer terms in SMTLIB2
     */
    object Ints {

        import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.{
            Term,
            SSymbol,
            QIdTerm,
            SortedQId,
            SymbolId,
            NumLit,
            SimpleQId,
            ConstantTerm,
            IntSort
        }

        /**
         * Create an integer variable.
         *
         * @param name The identifier of the variable
         */
        def apply( name : String ) = new VarTerm[ IntTerm ]( name, IntSort() )

        /**
         *  Create an Integer constant
         *
         * @param i The value of the constant as a BigInt (arbitrary precision).
         */
        def apply( i : BigInt ) : TypedTerm[ IntTerm, Term ] = ( i >= 0 ) match {

            //  positive integers are constantTerms
            case true ⇒
                TypedTerm[ IntTerm, ConstantTerm ](
                    Set[ SortedQId ](),
                    ConstantTerm( NumLit( i.toString ) ) )

            //  negative integers are QIdAndTermsTerm
            case false ⇒
                //  return negation of the ConstantTerm of the absolute value
                -TypedTerm[ IntTerm, ConstantTerm ](
                    Set[ SortedQId ](),
                    ConstantTerm( NumLit( ( -i ).toString ) ) )
        }
    }

    import scala.language.implicitConversions
    /**
     * Make an Ints from an Int
     */
    implicit def intToTypedTerm( i : Int ) = Ints( i )

    import parser.SMTLIB2Syntax.{ Term, AbsTerm }

    /**
     * Make an Reals from an Int
     */
    implicit def intToRealTypedTerm[ T <: Term ]( s : TypedTerm[ IntTerm, T ] ) : TypedTerm[ RealTerm, T ] =
        s

    /**
     * Absolute value
     */
    def absI[ T1 <: Term ]( op1 : TypedTerm[ IntTerm, T1 ] ) =
        TypedTerm[ IntTerm, AbsTerm ]( op1.typeDefs, AbsTerm( op1.termDef ) )

    /**
     * Integer Arithmetic operations
     */
    implicit class Operations[ T1 <: Term ]( op1 : TypedTerm[ IntTerm, T1 ] ) {

        import parser.SMTLIB2Syntax.{
            Term,
            PlusTerm,
            SubTerm,
            MultTerm,
            NegTerm,
            IntDivTerm,
            IntModTerm,
            IntRemTerm
        }

        /**
         * Addition
         */
        def +[ T2 <: Term ]( op2 : TypedTerm[ IntTerm, T2 ] ) =
            TypedTerm[ IntTerm, PlusTerm ](
                op1.typeDefs ++ op2.typeDefs,
                PlusTerm( op1.termDef, List( op2.termDef ) ) )

        /**
         * Subtraction
         */
        def -[ T2 <: Term ]( op2 : TypedTerm[ IntTerm, T2 ] ) =
            TypedTerm[ IntTerm, SubTerm ](
                op1.typeDefs ++ op2.typeDefs,
                SubTerm( op1.termDef, List( op2.termDef ) ) )

        /**
         * Unary minus
         */
        def unary_- =
            TypedTerm[ IntTerm, NegTerm ]( op1.typeDefs, NegTerm( op1.termDef ) )

        /**
         * Multiplication
         */
        def *[ T2 <: Term ]( op2 : TypedTerm[ IntTerm, T2 ] ) =
            TypedTerm[ IntTerm, MultTerm ](
                op1.typeDefs ++ op2.typeDefs,
                MultTerm( op1.termDef, List( op2.termDef ) ) )

        /**
         * Division
         */
        def /[ T2 <: Term ]( op2 : TypedTerm[ IntTerm, T2 ] ) =
            TypedTerm[ IntTerm, IntDivTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                IntDivTerm( op1.termDef, List( op2.termDef ) ) )

        /**
         * Modulus
         */
        def %[ T2 <: Term ]( op2 : TypedTerm[ IntTerm, T2 ] ) =
            TypedTerm[ IntTerm, IntModTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                IntModTerm( op1.termDef, op2.termDef ) )

        /**
         * Remainder
         *
         * @note This is not a standard operator only supported by Z3
         */
        def rem[ T2 <: Term ]( op2 : TypedTerm[ IntTerm, T2 ] ) =
            TypedTerm[ IntTerm, IntRemTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                IntRemTerm( op1.termDef, op2.termDef ) )

    }

    /**
     * Comparison operators that can be applied to [[IntTerm]]
     */
    implicit class IntIntToBoolOperators[ T2 <: Term ]( op1 : TypedTerm[ IntTerm, T2 ] ) {

        import typedterms.TypedTerm
        import parser.SMTLIB2Syntax.{
            LessThanEqualTerm,
            LessThanTerm,
            GreaterThanEqualTerm,
            GreaterThanTerm
        }

        /**
         * Less than or equal
         */
        def <=[ T4 <: Term ]( op2 : TypedTerm[ IntTerm, T4 ] ) =
            TypedTerm[ BoolTerm, LessThanEqualTerm ](
                op1.typeDefs ++ op2.typeDefs,
                LessThanEqualTerm( op1.termDef, op2.termDef ) )

        /**
         * Less than
         */
        def <[ T4 <: Term ]( op2 : TypedTerm[ IntTerm, T4 ] ) =
            TypedTerm[ BoolTerm, LessThanTerm ](
                op1.typeDefs ++ op2.typeDefs,
                LessThanTerm( op1.termDef, op2.termDef ) )

        /**
         * Greater than or equal
         */
        def >=[ T4 <: Term ]( op2 : TypedTerm[ IntTerm, T4 ] ) =
            TypedTerm[ BoolTerm, GreaterThanEqualTerm ](
                op1.typeDefs ++ op2.typeDefs,
                GreaterThanEqualTerm( op1.termDef, op2.termDef ) )

        /**
         * Greater than
         */
        def >[ T4 <: Term ]( op2 : TypedTerm[ IntTerm, T4 ] ) =
            TypedTerm[ BoolTerm, GreaterThanTerm ](
                op1.typeDefs ++ op2.typeDefs,
                GreaterThanTerm( op1.termDef, op2.termDef ) )
    }

}
