/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories

/**
 * Indexable floating point bit vector terms
 */
trait FPBVTerm extends IndexTerm

/**
 * Indexable floating point bit vector rounding mode terms
 */
trait RMFPBVTerm extends IndexTerm

/**
 * This theory contains the basic elements of floating point BV.
 *
 * For operators on floating point Bit vectors, logics QF_FPBV see
 * {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_FPBV}
 * Another useful link is the IEEE 754 standard {@link https://en.wikipedia.org/wiki/IEEE_754}
 */
trait FPBitVectors {

    import typedterms.{ TypedTerm, VarTerm }

    object RMs {
        import parser.SMTLIB2Syntax.{
            RoundingModeSort,
            RoundingMode,
            SortedQId,
            ConstantTerm,
            RoundingModeLit
        }

        /**
         * Create an RoundingMode variable.
         *
         * @param   name    The identifier of the variable.
         */
        def apply( name : String ) = new VarTerm[ RMFPBVTerm ](
            name,
            RoundingModeSort() )

        /**
         * Make a RoundingMode constant from a RoundingMode
         *
         * @param   rm      The rounding mode.
         */
        def apply( rm : RoundingMode ) = TypedTerm[ RMFPBVTerm, ConstantTerm ] (
            Set[ SortedQId ](),
            ConstantTerm( RoundingModeLit( rm ) ) )
    }

    /**
     *  Provide factory and methods to build FPBV terms in SMTLIB2.
     */
    object FPBVs {

        import parser.SMTLIB2Syntax.{
            Term,
            SSymbol,
            QIdTerm,
            SortedQId,
            SymbolId,
            NumLit,
            HexaLit,
            BinLit,
            SimpleQId,
            ConstantTerm,
            FPBitVectorSort,
            Sort,
            FPBVvalueTerm,
            DecBVLit,
            BVvalue,
            DecLit
        }

        /**
         * Create an FPBitVector variable.
         *
         * @param   name    The identifier of the variable
         * @param   ebits   The number of bits of the exponent
         * @param   sbits   The number of bits of the significand
         */
        def apply( name : String, ebits : Int, sbits : Int ) =
            new VarTerm[ FPBVTerm ](
                name,
                FPBitVectorSort( ebits.toString, sbits.toString ) )

        /**
         *  Create an FPBV constant with sign, exponent and significand from BitVectors terms
         *
         * @param   sign    The sign as a (_ BitVec 1) true = 1 (negative), false = 0 (positive)
         * @param   e       The exponent as a (_ BitVec eb)
         * @param   s       The significand as a (_ BitVec i)
         * @return          A constant FPBV of type (_ FloatingPoint eb i + 1)
         *
         * @example         FPBVs(1,BVs(1,5), BVs("#b010010")) creates a FPBV with sign <0.
         *                  Exponent is on 5 bits 1 and significand on 5 + 1 bits 1.010010.
         */
        def apply( sign : TypedTerm[ BVTerm, Term ], e : TypedTerm[ BVTerm, Term ], s : TypedTerm[ BVTerm, Term ] ) =
            TypedTerm[ FPBVTerm, Term ](
                sign.typeDefs ++ e.typeDefs ++ s.typeDefs,
                FPBVvalueTerm(
                    sign.termDef,
                    e.termDef,
                    s.termDef ) )
    }

    /**
     * Make a FPBVs variable from a string
     *
     * @param   name    The identifier of the variable
     */
    implicit class StringToFPBVs( name : String ) {

        /**
         * Make a FPBV variable from a String
         *
         * @param   ebits   The number of bits of the exponent
         * @param   sbits   The number of bits of the significand
         */
        def asFPBV( ebits : Int, sbits : Int ) = FPBVs( name, ebits, sbits )

        /**
         * Make a Float16
         *
         * Float16 is a synonym for (_ FloatingPoint  5  11)
         */
        def asFloat16() = new VarTerm[ FPBVTerm ](
            name,
            parser.SMTLIB2Syntax.FPFloat16() )

        /**
         * Make a Float32
         *
         * Float32 is a synonym for (_ FloatingPoint  8  24)
         */
        def asFloat32() = new VarTerm[ FPBVTerm ](
            name,
            parser.SMTLIB2Syntax.FPFloat32() )

        /**
         * Make a Float64
         *
         * Float64 is a synonym for (_ FloatingPoint  11  53)
         *
         */
        def asFloat64() = new VarTerm[ FPBVTerm ](
            name,
            parser.SMTLIB2Syntax.FPFloat64() )

        /**
         * Make a Float128
         *
         * Float128 is a synonym for (_ FloatingPoint  15  113)
         *
         */
        def asFloat128() = new VarTerm[ FPBVTerm ](
            name,
            parser.SMTLIB2Syntax.FPFloat128() )
    }

    /**
     * Make a FPBVs Float64 variable from a Double
     *
     * @param   d    The double
     */
    implicit class DoubleToFPBVs( d : Double ) {
        import theories.RealArithmetics.Reals
        import parser.SMTLIB2Syntax.{ RoundingModeSort, RNE }
        def asFloat64() = d match {
            case v if v.isNaN         ⇒ NaN( 11, 53 )
            case v if v.isNegInfinity ⇒ MinusInfinity( 11, 53 )
            case v if v.isPosInfinity ⇒ PlusInfinity( 11, 53 )
            case v if v equals 0.0    ⇒ PlusZero( 11, 53 )
            case v if v equals -0.0   ⇒ MinusZero( 11, 53 )
            case v ⇒
                //  Convert a Real to a FPBV Float64
                Reals( d ).toFPBV( 11, 53 )( RMs( RNE() ) )
        }
    }

    /**
     * Make a FPBVs Float32 variable from a Float
     *
     * @param   d    The float
     */
    implicit class FloatToFPBVs( d : Float ) {
        import theories.RealArithmetics.Reals
        import parser.SMTLIB2Syntax.{ RoundingMode, RNE }
        def asFloat32() = d match {
            case v if v.isNaN               ⇒ NaN( 8, 24 )
            case v if v.isNegInfinity       ⇒ MinusInfinity( 8, 24 )
            case v if v.isPosInfinity       ⇒ PlusInfinity( 8, 24 )
            case v if v equals 0.0.toFloat  ⇒ PlusZero( 8, 24 )
            case v if v equals -0.0.toFloat ⇒ MinusZero( 8, 24 )
            case v ⇒
                //  Convert a Real to a FPBV Float64
                Reals( d ).toFPBV( 8, 24 )( RMs( RNE() ) )
        }
    }

    import parser.SMTLIB2Syntax.{
        ConstantTerm,
        Term
    }

    /**
     * Plus infinity FPBVs
     */
    object PlusInfinity {
        /**
         *  Create an FPBV constant +infinity
         *
         * @param   e       The exponent in binary
         * @param   s       The significand in binary
         */
        def apply( ebits : Int, sbits : Int ) = TypedTerm[ FPBVTerm, ConstantTerm ](
            Set(),
            ConstantTerm( parser.SMTLIB2Syntax.FPPlusInfinity( ebits, sbits ) ) )
    }

    /**
     * Minus Infinity FPBVs
     */
    object MinusInfinity {
        /**
         *  Create an FPBV constant -infinity
         *
         * @param   e       The exponent in binary
         * @param   s       The significand in binary
         */
        def apply( ebits : Int, sbits : Int ) = TypedTerm[ FPBVTerm, ConstantTerm ](
            Set(),
            ConstantTerm( parser.SMTLIB2Syntax.FPMinusInfinity( ebits, sbits ) ) )
    }

    /**
     * Plus zero FPBV
     */
    object PlusZero {
        /**
         *  Create an FPBV constant +zero
         *
         * @param   e       The exponent in binary
         * @param   s       The significand in binary
         */
        def apply( ebits : Int, sbits : Int ) = TypedTerm[ FPBVTerm, ConstantTerm ](
            Set(),
            ConstantTerm( parser.SMTLIB2Syntax.FPBVPlusZero( ebits, sbits ) ) )
    }

    /**
     * Minus zero FPBV
     */
    object MinusZero {
        /**
         *  Create an FPBV constant -zero
         *
         * @param   e       The exponent in binary
         * @param   s       The significand in binary
         */
        def apply( ebits : Int, sbits : Int ) = TypedTerm[ FPBVTerm, ConstantTerm ](
            Set(),
            ConstantTerm( parser.SMTLIB2Syntax.FPBVMinusZero( ebits, sbits ) ) )
    }

    /**
     * Not a Number FPBV
     */
    object NaN {
        /**
         *  Create an FPBV constant NaN
         *
         * @param   e       The exponent in binary
         * @param   s       The significand in binary
         */
        def apply( ebits : Int, sbits : Int ) = TypedTerm[ FPBVTerm, ConstantTerm ](
            Set(),
            ConstantTerm( parser.SMTLIB2Syntax.FPBVNaN( ebits, sbits ) ) )
    }

    /**
     * Conversions from BV to FPBV.
     * @link{http://smtlib.cs.uiowa.edu/theories-FloatingPoint.shtml}
     *
     * @todo Add check that m = eb + sb. Retrieve BV size from the value
     */
    implicit class BVsToFPBVs[ T <: Term ]( b : TypedTerm[ BVTerm, T ] ) {

        import parser.SMTLIB2Syntax.{ FPBVFromBV, FPBVFromFPBVRealSInt, FPBVFromUInt, ToFPBV, ToFPBVu }

        /**
         * Bit string representation of a BV to FPBV.
         *          * @link{http://smtlib.cs.uiowa.edu/theories-FloatingPoint.shtml}
         *
         * ; from single bitstring representation in IEEE 754-2008 interchange format,
         * ; with m = eb + sb
         * ((_ to_fp eb sb) (_ BitVec m) (_ FloatingPoint eb sb))
         *
         * @param ebits   Number of bits of exponent of target FPBV.
         * @param sbits   Number of bits of significand of target FPBV.
         * @return        An FPBV representation (split) of `b` on ebits, sbits.
         */
        def bitStringToFPBV( ebits : Int, sbits : Int ) = {
            TypedTerm[ FPBVTerm, Term ](
                b.typeDefs,
                FPBVFromBV(
                    ToFPBV( ebits.toString, sbits.toString ),
                    b.termDef ) )
        }

        /**
         * Signed BV to FPBV.
         *
         * @link{http://smtlib.cs.uiowa.edu/theories-FloatingPoint.shtml}
         *
         * ; from signed machine integer, represented as a 2's complement bit vector
         * ((_ to_fp eb sb) RoundingMode (_ BitVec m) (_ FloatingPoint eb sb))
         *
         * @param ebits   Number of bits of exponent of target FPBV.
         * @param sbits   Number of bits of significand of target FPBV.
         * @param rm      A rounding mode.
         * @return        An FPBV representation of `b` on ebits, sbits when considered signed.
         */
        def signedToFPBV[ V <: TypedTerm[ RMFPBVTerm, Term ] ]( ebits : Int, sbits : Int )( implicit rm : V ) = {
            TypedTerm[ FPBVTerm, Term ](
                b.typeDefs ++ rm.typeDefs,
                FPBVFromFPBVRealSInt(
                    ToFPBV( ebits.toString, sbits.toString ),
                    rm.termDef,
                    b.termDef ) )
        }

        /**
         * Unsigned BV to FPBV.
         *
         * ; from unsigned machine integer, represented as bit vector
         * ((_ to_fp_unsigned eb sb) RoundingMode (_ BitVec m) (_ FloatingPoint eb sb))
         *
         * @link{http://smtlib.cs.uiowa.edu/theories-FloatingPoint.shtml}
         *
         * @param ebits   Number of bits of exponent of target FPBV.
         * @param sbits   Number of bits of significand of target FPBV.
         * @param rm      A rounding mode.
         * @return        An FPBV representation of `b` on ebits, sbits when considered unsigned.
         */
        def unSignedToFPBV[ V <: TypedTerm[ RMFPBVTerm, Term ] ]( ebits : Int, sbits : Int )( implicit rm : V ) = {
            TypedTerm[ FPBVTerm, Term ](
                b.typeDefs ++ rm.typeDefs,
                FPBVFromUInt(
                    ToFPBVu( ebits.toString, sbits.toString ),
                    rm.termDef,
                    b.termDef ) )
        }
    }

    /**
     * Conversion to FPBV from Real.
     */
    implicit class RealToFPBVs[ T <: TypedTerm[ RealTerm, Term ] ]( b : T ) {

        import parser.SMTLIB2Syntax.{ FPBVFromFPBVRealSInt, ToFPBV, RoundingMode }

        /**
         * Real number to FPBV.
         *
         * @link{http://smtlib.cs.uiowa.edu/theories-FloatingPoint.shtml}
         *
         *  ; from real
         * ((_ to_fp eb sb) RoundingMode Real (_ FloatingPoint eb sb))
         *
         *
         * @param ebits   Number of bits of exponent of target FPBV.
         * @param sbits   Number of bits of significand of target FPBV.
         * @param rm      A rounding mode.
         * @return        An FPBV representation of real number `b` on ebits, sbits.
         */
        def toFPBV[ V <: TypedTerm[ RMFPBVTerm, Term ] ]( ebits : Int, sbits : Int )( implicit rm : V ) =
            TypedTerm[ FPBVTerm, Term ](
                b.typeDefs ++ rm.typeDefs,
                FPBVFromFPBVRealSInt(
                    ToFPBV( ebits.toString, sbits.toString ),
                    rm.termDef,
                    b.termDef ) )
    }

    /**
     * Conversion from FPNV to FPBV.
     *
     * @link{http://smtlib.cs.uiowa.edu/theories-FloatingPoint.shtml}
     *
     * ; from another floating point sort
     * ((_ to_fp eb sb) RoundingMode (_ FloatingPoint mb nb) (_ FloatingPoint eb sb))
     *
     */
    implicit class FPBVsToFPBVs[ T <: TypedTerm[ FPBVTerm, Term ] ]( b : T ) {
        import parser.SMTLIB2Syntax.{ FPBVFromFPBVRealSInt, ToFPBV, RoundingMode }

        /**
         *  To FPBV (of arbitrary size).
         *
         * ; from another floating point sort
         * ((_ to_fp eb sb) RoundingMode (_ FloatingPoint mb nb) (_ FloatingPoint eb sb))
         * @param ebits   Number of bits of exponent of target FPBV.
         * @param sbits   Number of bits of significand of target FPBV.
         * @param rm      A rounding mode.
         * @return        An FPBV representation of `b` on ebits, sbits.
         */
        def toFPBV[ V <: TypedTerm[ RMFPBVTerm, Term ] ]( ebits : Int, sbits : Int )( implicit rm : V ) =
            TypedTerm[ FPBVTerm, Term ](
                b.typeDefs ++ rm.typeDefs,
                FPBVFromFPBVRealSInt(
                    ToFPBV( ebits.toString, sbits.toString ),
                    rm.termDef,
                    b.termDef ) )
    }

    /**
     * Convert from FPBV to other sorts.y
     */
    implicit class FPBVsToOthers( b : TypedTerm[ FPBVTerm, Term ] ) {

        import parser.SMTLIB2Syntax.{ FPBVToReal, FPBVToUBV, FPBVToSBV }

        /**
         * To Real.
         * @link{http://smtlib.cs.uiowa.edu/theories-FloatingPoint.shtml}
         * ; to real
         * (fp.to_real (_ FloatingPoint eb sb) Real)
         *
         * @return        A real number of the FPBV representation of `b`.
         */
        def toReal() =
            TypedTerm[ RealTerm, Term ](
                b.typeDefs,
                FPBVToReal( b.termDef ) )

        /**
         * To Unsigned Machine BV.
         * @link{http://smtlib.cs.uiowa.edu/theories-FloatingPoint.shtml}
         * ; to unsigned machine integer, represented as a bit vector
         * ((_ fp.to_ubv m) RoundingMode (_ FloatingPoint eb sb) (_ BitVec m))
         *
         * @param  bits    The number of bits of the target BV.
         * @param rm      A rounding mode.
         * @return        An unsigned BV of the FPBV representation of `b`.
         */
        def toUBV[ V <: TypedTerm[ RMFPBVTerm, Term ] ]( bits : Int )( implicit rm : V ) =
            TypedTerm[ BVTerm, Term ](
                b.typeDefs ++ rm.typeDefs,
                FPBVToUBV(
                    bits.toString,
                    rm.termDef,
                    b.termDef ) )

        /**
         * To Unsigned Machine BV.
         * @link{http://smtlib.cs.uiowa.edu/theories-FloatingPoint.shtml}
         * ; to signed machine integer, represented as a 2's complement bit vector
         * ((_ fp.to_sbv m) RoundingMode (_ FloatingPoint eb sb) (_ BitVec m))
         *
         * @param  bits    The number of bits of the target BV.
         * @param rm      A rounding mode.
         * @return        An signed BV of the FPBV representation of `b`.
         */
        def toSBV[ V <: TypedTerm[ RMFPBVTerm, Term ] ]( bits : Int )( implicit rm : V ) =
            TypedTerm[ BVTerm, Term ](
                b.typeDefs ++ rm.typeDefs,
                FPBVToSBV(
                    bits.toString,
                    rm.termDef,
                    b.termDef ) )
    }

    /**
     * Operators on FloatingPointBitVectors to FloatingPointBitVectors
     */
    implicit class Operations[ T1 <: Term ]( op1 : TypedTerm[ FPBVTerm, T1 ] ) {

        import parser.SMTLIB2Syntax.{
            FPBVAbsTerm,
            FPBVAddTerm,
            FPBVSubTerm,
            FPBVMulTerm,
            FPBVNegTerm,
            FPBVDivTerm,
            FPBVRemTerm,
            FPBVFmaTerm,
            FPBVSqrtTerm,
            FPBVRoundToITerm,
            FPBVMinTerm,
            FPBVMaxTerm
        }

        /**
         * Absolute value
         */
        def abs =
            TypedTerm[ FPBVTerm, FPBVAbsTerm ]( op1.typeDefs, FPBVAbsTerm( op1.termDef ) )

        /**
         * Unary minus
         */
        def unary_- =
            TypedTerm[ FPBVTerm, FPBVNegTerm ]( op1.typeDefs, FPBVNegTerm( op1.termDef ) )

        /**
         * Addition
         */
        def +[ T2 <: Term, T3 <: Term ]( op2 : TypedTerm[ FPBVTerm, T2 ] )( implicit rm : TypedTerm[ RMFPBVTerm, T3 ] ) =
            TypedTerm[ FPBVTerm, FPBVAddTerm ](
                op1.typeDefs ++ op2.typeDefs ++ rm.typeDefs,
                FPBVAddTerm( rm.termDef, op1.termDef, op2.termDef ) )

        /**
         * Subtraction
         */
        def -[ T2 <: Term, T3 <: Term ]( op2 : TypedTerm[ FPBVTerm, T2 ] )( implicit rm : TypedTerm[ RMFPBVTerm, T3 ] ) =
            TypedTerm[ FPBVTerm, FPBVSubTerm ](
                op1.typeDefs ++ op2.typeDefs ++ rm.typeDefs,
                FPBVSubTerm( rm.termDef, op1.termDef, op2.termDef ) )

        /**
         * Multiplication
         */
        def *[ T2 <: Term, T3 <: Term ]( op2 : TypedTerm[ FPBVTerm, T2 ] )( implicit rm : TypedTerm[ RMFPBVTerm, T3 ] ) =
            TypedTerm[ FPBVTerm, FPBVMulTerm ](
                op1.typeDefs ++ op2.typeDefs ++ rm.typeDefs,
                FPBVMulTerm( rm.termDef, op1.termDef, op2.termDef ) )

        /**
         *  Division
         */
        def /[ T2 <: Term, T3 <: Term ]( op2 : TypedTerm[ FPBVTerm, T2 ] )( implicit rm : TypedTerm[ RMFPBVTerm, T3 ] ) =
            TypedTerm[ FPBVTerm, FPBVDivTerm ](
                ( op1.typeDefs ++ op2.typeDefs ++ rm.typeDefs ),
                FPBVDivTerm( rm.termDef, op1.termDef, op2.termDef ) )

        /**
         *  Remainder
         */
        def %[ T2 <: Term, T3 <: Term ]( op2 : TypedTerm[ FPBVTerm, T2 ] ) =
            TypedTerm[ FPBVTerm, FPBVRemTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                FPBVRemTerm( op1.termDef, op2.termDef ) )

        /**
         *  Fused multiplication and addition  (op1 * op2) + op3
         */
        def *+[ T2 <: Term, T3 <: Term, T4 <: Term ]( op2 : TypedTerm[ FPBVTerm, T2 ], op3 : TypedTerm[ FPBVTerm, T3 ] )( implicit rm : TypedTerm[ RMFPBVTerm, T4 ] ) =
            TypedTerm[ FPBVTerm, FPBVFmaTerm ](
                ( op1.typeDefs ++ op2.typeDefs ++ op3.typeDefs ++ rm.typeDefs ),
                FPBVFmaTerm( rm.termDef, op1.termDef, op2.termDef, op3.termDef ) )

        /**
         *  Square root
         */
        def sqrt[ T <: Term ]( implicit rm : TypedTerm[ RMFPBVTerm, T ] ) =
            TypedTerm[ FPBVTerm, FPBVSqrtTerm ](
                op1.typeDefs ++ rm.typeDefs,
                FPBVSqrtTerm( rm.termDef, op1.termDef ) )

        /**
         *  Rounding to integral
         */
        def roundToI[ T <: Term ]( implicit rm : TypedTerm[ RMFPBVTerm, T ] ) =
            TypedTerm[ FPBVTerm, FPBVRoundToITerm ](
                op1.typeDefs ++ rm.typeDefs,
                FPBVRoundToITerm( rm.termDef, op1.termDef ) )

        /**
         * Minimun
         */
        def min[ T2 <: Term ]( op2 : TypedTerm[ FPBVTerm, T2 ] ) =
            TypedTerm[ FPBVTerm, FPBVMinTerm ](
                op1.typeDefs ++ op2.typeDefs,
                FPBVMinTerm( op1.termDef, op2.termDef ) )

        /**
         * Maximum
         */
        def max[ T2 <: Term ]( op2 : TypedTerm[ FPBVTerm, T2 ] ) =
            TypedTerm[ FPBVTerm, FPBVMaxTerm ](
                op1.typeDefs ++ op2.typeDefs,
                FPBVMaxTerm( op1.termDef, op2.termDef ) )
    }

    /**
     * Comparison operators on [[FPBVTerm]]
     */
    implicit class FPBVToBoolOperators[ T2 <: Term ]( op1 : TypedTerm[ FPBVTerm, T2 ] ) {

        import typedterms.TypedTerm
        import parser.SMTLIB2Syntax.{
            FPBVLeqTerm,
            FPBVLtTerm,
            FPBVGeqTerm,
            FPBVGtTerm,
            FPBVEqTerm,
            FPBVisNaN,
            FPBVisZero,
            FPBVisNormal,
            FPBVisSubnormal,
            FPBVisInfinite,
            FPBVisNegative,
            FPBVisPositive
        }

        /**
         * FPBV Less than or equal
         */
        def <=[ T4 <: Term ]( op2 : TypedTerm[ FPBVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, FPBVLeqTerm ](
                op1.typeDefs ++ op2.typeDefs,
                FPBVLeqTerm( op1.termDef, op2.termDef ) )

        /**
         * FPBV lower than
         */
        def <[ T4 <: Term ]( op2 : TypedTerm[ FPBVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, FPBVLtTerm ](
                op1.typeDefs ++ op2.typeDefs,
                FPBVLtTerm( op1.termDef, op2.termDef ) )

        /**
         * FPBV Greater than or equal
         */
        def >=[ T4 <: Term ]( op2 : TypedTerm[ FPBVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, FPBVGeqTerm ](
                op1.typeDefs ++ op2.typeDefs,
                FPBVGeqTerm( op1.termDef, op2.termDef ) )

        /**
         * FPBV Greater than
         */
        def >[ T4 <: Term ]( op2 : TypedTerm[ FPBVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, FPBVGtTerm ](
                op1.typeDefs ++ op2.typeDefs,
                FPBVGtTerm( op1.termDef, op2.termDef ) )

        /**
         * FPBV equality
         */
        def fpeq[ T4 <: Term ]( op2 : TypedTerm[ FPBVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, FPBVEqTerm ](
                op1.typeDefs ++ op2.typeDefs,
                FPBVEqTerm( op1.termDef, op2.termDef ) )

        /**
         * FPBV isNaN
         */
        def isNaN = TypedTerm[ BoolTerm, FPBVisNaN ](
            op1.typeDefs,
            FPBVisNaN( op1.termDef ) )

        /**
         * FPBV isNormal
         */
        def isNormal = TypedTerm[ BoolTerm, FPBVisNormal ](
            op1.typeDefs,
            FPBVisNormal( op1.termDef ) )

        /**
         * FPBV isSubnormal
         */
        def isSubNormal = TypedTerm[ BoolTerm, FPBVisSubnormal ](
            op1.typeDefs,
            FPBVisSubnormal( op1.termDef ) )

        /**
         * FPBV isZero
         */
        def isZero = TypedTerm[ BoolTerm, FPBVisZero ](
            op1.typeDefs,
            FPBVisZero( op1.termDef ) )

        /**
         * FPBV isInfinite
         */
        def isInfinite = TypedTerm[ BoolTerm, FPBVisInfinite ](
            op1.typeDefs,
            FPBVisInfinite( op1.termDef ) )
        /**
         * FPBV isNegative
         */
        def isNegative = TypedTerm[ BoolTerm, FPBVisNegative ](
            op1.typeDefs,
            FPBVisNegative( op1.termDef ) )

        /**
         * FPBV isPositive
         */
        def isPositive = TypedTerm[ BoolTerm, FPBVisPositive ](
            op1.typeDefs,
            FPBVisPositive( op1.termDef ) )

    }

}

object FPBitVectors extends FPBitVectors
