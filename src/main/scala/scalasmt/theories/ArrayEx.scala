/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories

/**
 *  SMTLIB2 Array terms
 *
 *  This is an array with indices in Int and elements in T
 */
trait ArrayTerm[ T ]

/**
 * Provides factory for arrays of IntTerm, dimensions 1 and 2
 */
trait ArrayExInt {

    /**
     * Array of Ints
     */
    object ArrayInt1 {
        def apply( name : String ) = ArrayEx.ArrayEx1[ IntTerm ]( name )
    }

    /**
     * Array of Array of Ints
     */
    object ArrayInt2 {
        def apply( name : String ) = ArrayEx.ArrayEx2[ IntTerm ]( name )
    }
}

/**
 * Provides factory for arrays of BVs, dimensions 1
 */
trait ArrayExBV {

    /**
     * Array of Ints
     */
    object ArrayBV1 {
        /**
         *  Create a one-dimensional array of BV.
         *
         * @param name  The name of the array variable
         * @param bits1 The number of bits to encode the index
         * @param bits2 The numberr of bits to encode the values
         *
         */
        def apply( name : String, bits1 : Int, bits2 : Int ) =
            ArrayEx.ArrayEx1[ BVTerm ]( name, bits1, bits2 )
    }

    /**
     * Array of arrays of Ints
     */
    object ArrayBV2 {
        /**
         *  Create a two-dimensional array of BV.
         *
         * @param name  The name of the array variable
         * @param bits1 The number of bits to encode the first index
         * @param bits2 The number of bits to encode the second index
         * @param bits3 The numberr of bits to encode the values
         *
         */
        def apply( name : String, bits1 : Int, bits2 : Int, bits3 : Int ) =
            ArrayEx.ArrayEx2[ BVTerm ]( name, bits1, bits2, bits3 )
    }
}

/**
 * Provides factory for arrays of RealTerm, dimensions 1 and 2
 */
trait ArrayExReal {

    /**
     * Array of Reals
     */
    object ArrayReal1 {
        def apply( name : String ) = ArrayEx.ArrayEx1[ RealTerm ]( name )
    }

    /**
     * Array of Array of Reals
     */
    object ArrayReal2 {
        def apply( name : String ) = ArrayEx.ArrayEx2[ RealTerm ]( name )
    }

}

/**
 * Provides factory for arrays of BoolTerm, dimensions 1 and 2
 */
trait ArrayExBool {

    /**
     *  Array of Bool
     */
    object ArrayBool1 {
        def apply( name : String ) = ArrayEx.ArrayEx1[ BoolTerm ]( name )
    }

    /**
     * Array of Array of Bool
     */
    object ArrayBool2 {
        def apply( name : String ) = ArrayEx.ArrayEx2[ BoolTerm ]( name )
    }

}
/**
 * Provide factory for Arrays of dimensions 1 and 2 of element type T
 */
private object ArrayEx {

    import parser.SMTLIB2Syntax.{
        SSymbol,
        SortedQId,
        SymbolId,
        Array1Sort,
        Array2Sort,
        Array1BV,
        Array2BV,
        BitVectorSort
    }

    import scala.reflect.ClassTag

    /**
     * Build an Array of dimension 1 of some sort V
     */
    object ArrayEx1 {
        /**
         * Standard arrays Ints, Reals, Bool
         */
        def apply[ V ]( name : String )( implicit mf : ClassTag[ V ] ) = {
            //  Create an ID of sort Array1Sort TermToSort[ V ]
            val q = SortedQId(
                SymbolId( SSymbol( name ) ),
                Array1Sort( TermToSort[ V ] ) )
            //  Make an Term
            makeArrayVarTerm[ V ]( q )
        }

        /**
         * BitVector arrays
         */
        def apply[ V <: BVTerm ]( name : String, bits1 : Int, bits2 : Int )( implicit mf : ClassTag[ V ] ) = {
            import typedterms.VarTerm

            //  Create an ID of sort Array1Sort TermToSort[ V ]
            val q = SortedQId(
                SymbolId( SSymbol( name ) ),
                Array1BV( BitVectorSort( bits1.toString ), BitVectorSort( bits2.toString ) ) )
            //  Now build a Term
            val SymbolId( SSymbol( id ) ) = q.id
            new VarTerm[ ArrayTerm[ V ] ]( id, q.sort )
        }
    }

    /**
     * Build and Array of dimension 2 ot some sort V
     */
    object ArrayEx2 {
        /**
         * Standard arrays Ints, Reals, Bool
         */
        def apply[ V ]( name : String )( implicit mf : ClassTag[ V ] ) = {
            //  Create an ID of sort Array2Sort TermToSort[ V ]
            val q = SortedQId(
                SymbolId( SSymbol( name ) ),
                Array2Sort( Array1Sort( TermToSort[ V ] ) ) )
            //  Make an Term
            makeArrayVarTerm[ ArrayTerm[ V ] ]( q )
        }

        /**
         * BitVector arrays
         */
        def apply[ V <: BVTerm ]( name : String, bits1 : Int, bits2 : Int, bits3 : Int )( implicit mf : ClassTag[ V ] ) = {
            import typedterms.VarTerm

            val q = SortedQId(
                SymbolId( SSymbol( name ) ),
                Array2BV(
                    BitVectorSort( bits1.toString ),
                    Array1BV( BitVectorSort( bits2.toString ), BitVectorSort( bits3.toString ) ) ) )
            val SymbolId( SSymbol( id ) ) = q.id
            new VarTerm[ ArrayTerm[ ArrayTerm[ V ] ] ]( id, q.sort )
        }
    }

    /**
     * Make a typed term with a parametric array type
     */
    private def makeArrayVarTerm[ T ]( q : SortedQId ) = {
        import typedterms.VarTerm
        val SymbolId( SSymbol( id ) ) = q.id
        new VarTerm[ ArrayTerm[ T ] ]( id, q.sort )
    }

}
