/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories

trait RealTerm

trait RealArithmetics {

    import typedterms.{ TypedTerm, VarTerm }

    /**
     *  Provide factory and methods to build  real terms in SMTLIB2
     */
    object Reals {

        import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.{
            Term,
            SSymbol,
            QIdTerm,
            SortedQId,
            SymbolId,
            DecLit,
            SimpleQId,
            ConstantTerm,
            RealSort
        }

        /**
         * Create a real variable.
         *
         * @param name The identifier of the variable
         */
        def apply( name : String ) = new VarTerm[ RealTerm ]( name, RealSort() )

        /**
         * Extract  integraland fractiunal parts of a BigDecimal.
         *
         * @param   r   The number to extract from.
         * @return      The plain strinsg (no exponent notation) for
         *              intergal and fractional parts.
         */
        def extractIntFrac( r : BigDecimal ) : ( String, String ) = {
            require( r >= 0, s"Conversion to DecLit only defined for positive BigDecimal. $r" )
            //  Convert r to plain text
            //  Add 0,0 to make sure that string has a decimal part
            val s = new java.math.BigDecimal( ( r + 0.0 ).toString ).toPlainString()
            ( s.takeWhile( _ != '.' ), s.dropWhile( _ != '.' ).drop( 1 ) )
        }

        /**
         * Make a Real constant from a BigDecimal
         * In SMTLIB2, this is QIdterm
         *
         * @note The SMTLIB standard does not allow the scientific notation e.g. 1e-7
         * so we use the java.math.BigDecimal to normalise the string
         */
        def apply( r : BigDecimal ) : TypedTerm[ RealTerm, Term ] = ( r >= 0 ) match {

            //  positive real numbers are constantTerms
            case true ⇒
                TypedTerm[ RealTerm, ConstantTerm ](
                    Set[ SortedQId ](),
                    ConstantTerm( DecLit( extractIntFrac( r )._1, extractIntFrac( r )._2 ) ) )

            //  negative real numbers are QIdAndTermsTerm
            case false ⇒
                -TypedTerm[ RealTerm, ConstantTerm ](
                    Set[ SortedQId ](),
                    ConstantTerm( DecLit( extractIntFrac( -r )._1, extractIntFrac( -r )._2 ) ) )
        }

        /**
         * Make a Real constant from a double that is not infinity nor NaN
         * In SMTLIB2, this is QIdterm
         *
         * @note The SMTLIB standard does not allow the scientific notation e.g. 1e-7
         * so we use the java.math.BigDecimal to normalise the string
         */
        def apply( r : Double ) : TypedTerm[ RealTerm, Term ] = {
            require( !( r.isInfinity || r.isNaN ), s"$r is not a real number (infinity or NaN) and cannot be converted to SMTLIB Reals" )
            ( r >= 0 ) match {

                //  positive real numbers are constantTerms
                case true ⇒
                    TypedTerm[ RealTerm, ConstantTerm ](
                        Set[ SortedQId ](),
                        ConstantTerm( DecLit( extractIntFrac( r )._1, extractIntFrac( r )._2 ) ) )

                //  negative real numbers are QIdAndTermsTerm
                case false ⇒
                    -TypedTerm[ RealTerm, ConstantTerm ](
                        Set[ SortedQId ](),
                        ConstantTerm( DecLit( extractIntFrac( -r )._1, extractIntFrac( -r )._2 ) ) )
            }
        }
    }

    import scala.language.implicitConversions

    /**
     * Make an Reals from a Double
     */
    implicit def realToTypedTerm( i : Double ) = Reals( i )

    import parser.SMTLIB2Syntax.{
        Term,
        AbsTerm
    }

    /**
     * Absolute value
     */
    def absR[ T1 <: Term ]( op1 : TypedTerm[ RealTerm, T1 ] ) =
        TypedTerm[ RealTerm, AbsTerm ]( op1.typeDefs, AbsTerm( op1.termDef ) )

    /**
     * Real Arithmetic operations
     */
    implicit class Operations[ T1 <: Term ]( op1 : TypedTerm[ RealTerm, T1 ] ) {

        import parser.SMTLIB2Syntax.{
            PlusTerm,
            SubTerm,
            MultTerm,
            NegTerm,
            RealDivTerm
        }

        /**
         * Addition
         */
        def +[ T2 <: Term ]( op2 : TypedTerm[ RealTerm, T2 ] ) =
            TypedTerm[ RealTerm, PlusTerm ](
                op1.typeDefs ++ op2.typeDefs,
                PlusTerm( op1.termDef, List( op2.termDef ) ) )

        /**
         * Subtraction
         */
        def -[ T2 <: Term ]( op2 : TypedTerm[ RealTerm, T2 ] ) =
            TypedTerm[ RealTerm, SubTerm ](
                op1.typeDefs ++ op2.typeDefs,
                SubTerm( op1.termDef, List( op2.termDef ) ) )

        /**
         * Unary minus
         */
        def unary_- =
            TypedTerm[ RealTerm, NegTerm ]( op1.typeDefs, NegTerm( op1.termDef ) )

        /**
         * Multiplication
         */
        def *[ T2 <: Term ]( op2 : TypedTerm[ RealTerm, T2 ] ) =
            TypedTerm[ RealTerm, MultTerm ](
                op1.typeDefs ++ op2.typeDefs,
                MultTerm( op1.termDef, List( op2.termDef ) ) )

        /**
         * Division
         */
        def /[ T2 <: Term ]( op2 : TypedTerm[ RealTerm, T2 ] ) =
            TypedTerm[ RealTerm, RealDivTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                RealDivTerm( op1.termDef, List( op2.termDef ) ) )
    }

    /**
     * Comparison operators that can be applied to [[RealTerm]]
     */
    implicit class RealRealToBoolOperators[ T2 <: Term ]( op1 : TypedTerm[ RealTerm, T2 ] ) {
        import typedterms.TypedTerm
        import parser.SMTLIB2Syntax.{
            LessThanEqualTerm,
            LessThanTerm,
            GreaterThanEqualTerm,
            GreaterThanTerm
        }

        /**
         * Less than or equal
         */
        def <=[ T4 <: Term ]( op2 : TypedTerm[ RealTerm, T4 ] ) =
            TypedTerm[ BoolTerm, LessThanEqualTerm ](
                op1.typeDefs ++ op2.typeDefs,
                LessThanEqualTerm( op1.termDef, op2.termDef ) )

        /**
         * Less than
         */
        def <[ T4 <: Term ]( op2 : TypedTerm[ RealTerm, T4 ] ) =
            TypedTerm[ BoolTerm, LessThanTerm ](
                op1.typeDefs ++ op2.typeDefs,
                LessThanTerm( op1.termDef, op2.termDef ) )

        /**
         * Greater than or equal
         */
        def >=[ T4 <: Term ]( op2 : TypedTerm[ RealTerm, T4 ] ) =
            TypedTerm[ BoolTerm, GreaterThanEqualTerm ](
                op1.typeDefs ++ op2.typeDefs,
                GreaterThanEqualTerm( op1.termDef, op2.termDef ) )

        /**
         * Greater than
         */
        def >[ T4 <: Term ]( op2 : TypedTerm[ RealTerm, T4 ] ) =
            TypedTerm[ BoolTerm, GreaterThanTerm ](
                op1.typeDefs ++ op2.typeDefs,
                GreaterThanTerm( op1.termDef, op2.termDef ) )
    }
}

object RealArithmetics extends RealArithmetics
