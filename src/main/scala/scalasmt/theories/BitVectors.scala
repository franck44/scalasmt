/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories

/**
 *  A [[BVTerm]] is either a constant, a variable,
 *  or a term built from operations on [[BVTerm]]. The number of bits is not checked
 *  so you may build a [[BVTerm]] of the form `BVs(1,16) + BVs(1,8)`. This may not be
 *  accepted by an SMT-solver (sizes do not match).
 */
trait BVTerm extends IndexTerm

/**
 * This theory contains the basic elements of bitVector arithmetic.
 *
 * <ul>
 *  <li> a sort `(_ BitVec n)` is defined for each non-zero numeral n </li>
 *  <li> the function concat is defined that combines two bit-vectors into a longer one </li>
 *  <li> a family of functions `(_ extract i j)`` is defined that extracts a contiguous sub-vector
 *          from index i to j (inclusive) of a bit-vector </li>
 *  <li> unary functions `bvnot` and `bvneg`</li>
 *  <li> binary functions `bvand` `bvor` `bvadd` `bvmul` `bvudiv` `bvurem` `bvshl` `bvlshr`</li>
 *  <li> the binary comparison function `bvult`</li>
 * </ul>
 *
 * For operators on Bit vectors, logics `QF_BV` see
 * {@link http://smtlib.cs.uiowa.edu/logics-all.shtml#QF_BV}
 */
trait BitVectors {

    import typedterms.{ TypedTerm, VarTerm }

    /**
     *  Provide factory and methods to build bit vector terms
     */
    object BVs {

        import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.{
            Term,
            SSymbol,
            QIdTerm,
            SortedQId,
            SymbolId,
            NumLit,
            HexaLit,
            BinLit,
            SimpleQId,
            ConstantTerm,
            BitVectorSort,
            DecBVLit,
            BVvalue,
            DecLit
        }

        /**
         * Create an BitVector variable.
         *
         * @param name The identifier of the variable
         * @param bits The number of bits variable
         */
        def apply( name : String, bits : Int ) =
            new VarTerm[ BVTerm ]( name, BitVectorSort( bits.toString ) )

        /**
         *  Create an BV constant from a binary or hexadecimal string
         *
         * @param   s   The value of the constant. Must if the form #x for
         *              hexadecimal, and #b binary
         *
         * @example     `BVs("#b10101")` or `BVs("#xaf03")`
         */
        def apply( s : String ) : TypedTerm[ BVTerm, ConstantTerm ] = {
            require( s.length >= 2, "Must provide a hexadecimal(#x)  or binary (#b)" )
            TypedTerm[ BVTerm, ConstantTerm ](
                Set(),
                ConstantTerm(
                    s( 0 ) match {

                        case '#' ⇒ s( 1 ) match {
                            case 'x' ⇒ HexaLit( s.takeRight( s.length - 2 ) )
                            case 'b' ⇒ BinLit( s.takeRight( s.length - 2 ) )
                        }

                        //  otherwise not supported
                        case _ ⇒ sys.error( s"calling BVs($s). Need # followed by x or b to create a BVs" )
                    } ) )
        }

        /**
         * BitVector from a (decimal) integer number. The integer `n` must be within
         * <math>-2^{(bits -1)} .. 2^{(bits -1)} - 1</math> (<strong>signed</strong> integer).
         *
         *  @param  n       The integer to encode as BitVector, must be within
         *                  <math>-2^{(bits -1)} .. 2^{(bits -1)} - 1]</math>
         *  @param  bits    The number of bits
         */
        def apply( n : BigInt, bits : Int ) = {
            require(
                bits >= 1 && n <= ( BigInt( 1 ) << ( bits - 1 ) ) - 1 && n >= -( BigInt( 1 ) << ( bits - 1 ) ),
                s"$n cannot be encoded on $bits signed bits" )
            ( n, bits ) match {
                //  0 <= n <= 2^(bits -1) - 1
                case _ if ( n >= 0 && n <= ( BigInt( 1 ) << ( bits - 1 ) ) - 1 ) ⇒
                    TypedTerm[ BVTerm, ConstantTerm ](
                        Set(),
                        ConstantTerm( DecBVLit( BVvalue( n.toString ), bits.toString ) ) )

                //  - 2^(bits -1) < n < 0
                case _ if ( n < 0 && n > -( BigInt( 1 ) << ( bits - 1 ) ) ) ⇒
                    -TypedTerm[ BVTerm, ConstantTerm ](
                        Set(),
                        ConstantTerm( DecBVLit( BVvalue( ( -n ).toString ), bits.toString ) ) )

                //  Must be - 2^(bits - 1), encode as binary number
                case _ ⇒
                    TypedTerm[ BVTerm, ConstantTerm ](
                        Set(),
                        ConstantTerm(
                            BinLit( ( 1 :: List.fill( bits - 1 )( 0 ) ).mkString ) ) )
            }
        }

        /**
         * BitVector from a BigInt. Unsigned. Need to specify the number of bits.
         */
        def apply( n : BigInt, bits : Int, unsigned : Boolean ) = {
            require(
                bits >= 1 && n <= ( BigInt( 1 ) << bits ) - 1 && n >= 0,
                s"$n cannot be encoded on $bits unsigned bits" )

            TypedTerm[ BVTerm, ConstantTerm ](
                Set(),
                ConstantTerm( DecBVLit( BVvalue( n.toString ), bits.toString ) ) )
        }

        /**
         * BitVector from a decimal string. Need to specify the number of bits. This method
         * can't be an overload of `apply` since the `String` version of `apply` is used
         * for named bit vector values.
         *
         */
        @deprecated( "This method will be removed in the next release. Use BVs(s.toInt, bits) instead", "FooLib 2.1.0" )
        def fromString( s : String, bits : Int ) = // BVs( s.toInt, bits )
            if ( s( 0 ) == '-' )
                -TypedTerm[ BVTerm, ConstantTerm ](
                    Set(),
                    ConstantTerm( DecBVLit( BVvalue( s.tail ), bits.toString ) ) )
            else
                TypedTerm[ BVTerm, ConstantTerm ](
                    Set(),
                    ConstantTerm( DecBVLit( BVvalue( s ), bits.toString ) ) )

    }

    /**
     * Make a BVs from an integer and a number of bits
     *
     * @example 12.withBits(4) or -64.withBits(8) or 7.withUBits(3)
     */
    implicit class IntToBVs( i : Int ) {

        /**
         * Make a signed Int16
         */
        def i16() = BVs( i, 16 )

        /**
         * Make a signed Int32
         */
        def i32() = BVs( i, 32 )

        /**
         * Make a signed Int64
         */
        def i64() = BVs( i, 64 )

        /**
         * Bit vector (signed) representation of `i` over `b` bits
         */
        def withBits( b : Int ) = BVs( i, b )

        /**
         * Bit vector (unsigned) representation of `i` over `b` bits
         */
        def withUBits( b : Int ) = BVs( i, b, true )
    }

    import parser.SMTLIB2Syntax.Term
    /**
     * BitVector arithmetic and bit-wise operations
     */
    implicit class Operations[ T1 <: Term ]( op1 : TypedTerm[ BVTerm, T1 ] ) {

        import parser.SMTLIB2Syntax.{
            BVAddTerm,
            BVSubTerm,
            BVMultTerm,
            BVNegTerm,
            BVNotTerm,
            BVuDivTerm,
            BVuRemTerm,
            BVsDivTerm,
            BVsRemTerm,
            BVOrTerm,
            BVXorTerm,
            BVAndTerm,
            BVShiftLeftTerm,
            BVSLogicalShiftRightTerm,
            BVSArithmeticShiftRightTerm,
            BVSignExtendTerm,
            BVZeroExtendTerm,
            BVExtractTerm,
            BVConcatTerm,
            NumLit
        }

        /**
         * Addition
         */
        def +[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVAddTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVAddTerm( op1.termDef, op2.termDef ) )

        /**
         * Subtraction
         */
        def -[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVSubTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVSubTerm( op1.termDef, op2.termDef ) )

        /**
         * Unary minus
         */
        def unary_- =
            TypedTerm[ BVTerm, BVNegTerm ]( op1.typeDefs, BVNegTerm( op1.termDef ) )

        /**
         * Unary not
         */
        def unary_! =
            TypedTerm[ BVTerm, BVNotTerm ]( op1.typeDefs, BVNotTerm( op1.termDef ) )

        /**
         * Multiplication
         */
        def *[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVMultTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVMultTerm( op1.termDef, op2.termDef ) )

        /**
         * Unsigned division
         */
        def /[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVuDivTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVuDivTerm( op1.termDef, op2.termDef ) )

        /**
         * Signed division.
         */
        def sdiv[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVsDivTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVsDivTerm( op1.termDef, op2.termDef ) )

        /**
         * Unsigned Modulus
         */
        def %[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVuRemTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVuRemTerm( op1.termDef, op2.termDef ) )

        /**
         * Signed Modulus
         */
        def srem[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVsRemTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVsRemTerm( op1.termDef, op2.termDef ) )

        /**
         * Or
         */
        def or[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVOrTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVOrTerm( op1.termDef, op2.termDef ) )

        /**
         * Xor
         */
        def xor[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVXorTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVXorTerm( op1.termDef, op2.termDef ) )

        /**
         * And
         */
        def and[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVAndTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVAndTerm( op1.termDef, op2.termDef ) )

        /**
         * Shift left
         */
        def <<[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVShiftLeftTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVShiftLeftTerm( op1.termDef, op2.termDef ) )

        /**
         * Logical shift right
         */
        def >>[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVSLogicalShiftRightTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVSLogicalShiftRightTerm( op1.termDef, op2.termDef ) )

        /**
         * Artihmetic shift right
         */
        def ashr[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVSArithmeticShiftRightTerm ](
                ( op1.typeDefs ++ op2.typeDefs ),
                BVSArithmeticShiftRightTerm( op1.termDef, op2.termDef ) )

        /**
         * Sign extend
         */
        def sext( bits : Int ) =
            TypedTerm[ BVTerm, BVSignExtendTerm ](
                op1.typeDefs,
                BVSignExtendTerm( NumLit( bits.toString ), op1.termDef ) )

        /**
         * Zero extend
         */
        def zext( bits : Int ) =
            TypedTerm[ BVTerm, BVZeroExtendTerm ](
                op1.typeDefs,
                BVZeroExtendTerm( NumLit( bits.toString ), op1.termDef ) )

        /**
         * Extract
         *
         * @return A bit vector made of the bits between `low` and `high` inclusive
         */
        def extract( low : Int, high : Int ) =
            TypedTerm[ BVTerm, BVExtractTerm ](
                op1.typeDefs,
                BVExtractTerm( NumLit( low.toString ), NumLit( high.toString ), op1.termDef ) )

        /**
         * Concat
         *
         * @return A bit vector made of concatenation of two terms.
         */
        def concat[ T2 <: Term ]( op2 : TypedTerm[ BVTerm, T2 ] ) =
            TypedTerm[ BVTerm, BVConcatTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVConcatTerm( op1.termDef, op2.termDef ) )
    }

    /**
     * Comparison operators for [[BVTerm]], result is [[BoolTerm]]
     *
     * @example Unsigned comparison operators are `uxx` where `xx` is `le`, `lt`,
     * `ge`, `gt` and counter parts signed operators are `sxx`.
     *
     * If `v1` and `v2` are two [[BVTerm]], `v1 ule v2` is the unsigned
     * comparison `v1 &leq; v2`.
     */
    implicit class BVBVToBoolOperators[ T2 <: Term ]( op1 : TypedTerm[ BVTerm, T2 ] ) {

        import typedterms.TypedTerm
        import parser.SMTLIB2Syntax.{
            BVULessThanEqualTerm,
            BVULessThanTerm,
            BVUGreaterThanEqualTerm,
            BVUGreaterThanTerm,
            BVSLessThanEqualTerm,
            BVSLessThanTerm,
            BVSGreaterThanEqualTerm,
            BVSGreaterThanTerm
        }

        /**
         * BV Less than or equal unsigned
         */
        def ule[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVULessThanEqualTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVULessThanEqualTerm( op1.termDef, op2.termDef ) )

        /**
         * BV Less than unsigned
         */
        def ult[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVULessThanTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVULessThanTerm( op1.termDef, op2.termDef ) )

        /**
         * BV Greater than or equal unsigned
         */
        def uge[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVUGreaterThanEqualTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVUGreaterThanEqualTerm( op1.termDef, op2.termDef ) )

        /**
         * BV Greater than unsigned
         */
        def ugt[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVUGreaterThanTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVUGreaterThanTerm( op1.termDef, op2.termDef ) )

        /**
         * BV Less than or equal signed
         */
        def sle[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVSLessThanEqualTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVSLessThanEqualTerm( op1.termDef, op2.termDef ) )

        /**
         * BV Less than signed
         */
        def slt[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVSLessThanTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVSLessThanTerm( op1.termDef, op2.termDef ) )

        /**
         * BV Greater than or equal signed
         */
        def sge[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVSGreaterThanEqualTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVSGreaterThanEqualTerm( op1.termDef, op2.termDef ) )

        /**
         * BV Greater than signed
         */
        def sgt[ T4 <: Term ]( op2 : TypedTerm[ BVTerm, T4 ] ) =
            TypedTerm[ BoolTerm, BVSGreaterThanTerm ](
                op1.typeDefs ++ op2.typeDefs,
                BVSGreaterThanTerm( op1.termDef, op2.termDef ) )
    }

}
