/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package typedterms

import org.bitbucket.inkytonik.kiama.rewriting.Rewriter

/**
 *  Create terms with quantifiers
 *
 * == Usage ==
 *
 * The logical statement `&forall; x &sdot; x + 2 &le; 1` can be written as:
 * {{{
 * forall ( SSymbol("x") ) {
 *    val x = Ints("x")     //  declare SMTLIB Int variable x named "x"
 *    x + 2 <= 1            //  write a standard TypedTerm
 * }
 * }}}
 * corresponds to the following SMTLIB term:
 *
 * <pre>
 * ( forall
 *      (
 *          (x Int)
 *      )
 *      ( <=
 *           ( + x 2 )
 *           1
 *      )
 * )
 * </pre>
 *
 * The following declaration mixes bounded (`x1`) and unbounded (`x`, `y`)
 * variables and stands for
 *  `&forall; x1 &sdot; x1 + 2 &le; y &and; x = 3` :
 * {{{
 * val x = Ints("x")   //  declare Ints variable named "x"
 * val y = Ints("y")   //  declare Ints variable named "y"
 *
 * ( forall( SSymbol("x1") ) {
 *     val x1 = Ints("x1")
 *     x1 + 2 <= y
 *   }
 * )
 * & (x === 3)
 * }}}
 * and the corresponding SMTLIB2 term is:
 * <pre>
 * ( and
 *      ( forall
 *          (
 *              (x1 Int )
 *          )
 *          ( <= ( + x1 2 ) y )
 *      )
 *      (= x 3 )
 * )
 * </pre>
 *
 * `ForAll` terms can be nested.
 * `&forall; x1 &sdot; [ x1  &le; 0 &rArr;
 *    ( &forall; y1 &sdot; y1 &ge; 0  &rArr;  y1 &ge; x1 ) ]`
 * is written as:
 * {{{
 * forall( SSymbol("x1") ) {
 *      val x1 = Ints("x1")
 *      x1 <= 0 imply
 *          forall( SSymbol("y1") ) {
 *              val y1 = Ints("y1")
 *              y1 >= 0 imply y1 >= x1
 *          }
 * }
 * }}}
 * and the result is a nested `forall` SMTLIB2 term:
 * <pre>
 * (forall
 *      (
 *          (x1 Int )
 *      )
 *      ( =>
 *          ( <= x1 0 )
 *          ( forall
 *              (
 *                  (y1 Int )
 *              )
 *              ( =>
 *                  ( >= y1 0 )
 *                  ( >= y1 x1 )
 *              )
 *          )
 *      )
 * )
 * </pre>
 *
 */
trait QuantifiedTerm {

    /**
     * A binding can be created like this:
     * Let (
     *  x = BVar("x",Ints(1))
     *  y = BVar("y",Ints(2))
     *  z = BVar("z", t + 2)
     *  t + x - y
     * )
     *
     * and we would like to print it as:
     * (let
     *  (
     *      (x 1)
     *      (y 2)
     *      (z (+ t 2) )
     *  )
     *  (+ t (- x y) )
     * )
     */

    import parser.SMTLIB2Syntax.{
        VarBinding,
        SSymbol,
        SortedVar,
        LetTerm,
        ForAllTerm,
        ExistsTerm,
        Term,
        QIdTerm,
        SimpleQId,
        SymbolId,
        SortedQId,
        QualifiedId,
        BVarDef,
        SMTLIB2Symbol
    }

    /**
     * Create a binding.
     *
     * @param n     The name of the bounded term.
     * @param expr  The expression bounded to 'n'.
     */
    object BoundedVar {
        def apply[ A, T <: Term ]( n : String, expr : TypedTerm[ A, T ] ) = TypedTerm[ A, BVarDef ](
            expr.typeDefs,
            BVarDef ( SSymbol( n ), expr.termDef ) )
    }

    /**
     * Rewrite BVarFdefs into QIdTerm.
     * This object creates a rewrite rule that can be used to replace
     * occurrences of BVarDef(x,term) by QIdTerm(x).
     *
     */
    private object BVarDefToQIdTerm extends Rewriter {

        /**
         * Rule to replace BDefVar by QIdTerm.
         */
        lazy val simplify =
            rule[ Term ] {
                case BVarDef( x, _ ) ⇒ QIdTerm( SimpleQId( SymbolId( x ) ) )
            }

        def apply( t : Term ) : Term = rewrite ( reduce( simplify ) ) ( t )
    }

    /**
     * Create a LetTerm.
     *
     * @param defs A sequence of BoundedVar assignments followed by a Term.
     *
     */
    def let[ A, T <: Term ]( defs : ⇒ TypedTerm[ A, T ] ) = {

        /*
         * Compute the VarBinding in the term from the bvarDefs
         *
         * 1. retrieve the BVarDefs in the term Analysis (set)
         * 2. map to List
         * 3. transform every BVarDef into a VarBinding
         */
        val bindings : List[ VarBinding ] = parser.Analysis( defs.termDef )
            .bvarDefs
            .toList
            .map {
                case BVarDef( bv, e ) ⇒ VarBinding( bv, e )
            }

        /*
         * Build the LetTerm (TypedTerm) using the bindings.
         *
         * Also rewrite every BVarDef into simple QIdTerm.
         */
        TypedTerm[ A, LetTerm ](
            defs.typeDefs,
            LetTerm(
                bindings,
                BVarDefToQIdTerm( defs.termDef ) ) )
    }

    import theories.BoolTerm

    /**
     * Split the t.typeDefs into bounded/unbounded vars
     * result is a Map m. m(true) <=> bounded, m(false) <=> free vars
     *
     *
     * @param symbolSet   The symbols that should be bounded
     * @param t           The term to analyse
     */
    private def splitBounded[ A, T <: Term ](
        symbolSet : Seq[ SMTLIB2Symbol ], t : TypedTerm[ A, T ] ) : Map[ Boolean, Set[ SortedQId ] ] = {

        t.typeDefs.groupBy {
            //  discriminator for grouping
            v ⇒
                v match {

                    //  bounded
                    case SortedQId( SymbolId( name ), _ ) if symbolSet.contains( name )  ⇒ true

                    //  not bounded
                    case SortedQId( SymbolId( name ), _ ) if !symbolSet.contains( name ) ⇒ false

                    //  should never happen
                    case e ⇒
                        // logger.error( "Variable {} encountered in typeDefs for {}", e, defs )
                        throw new Exception( "Type different to SortedQid encountered in typeDefs for term" )
                }
        }

    }

    /**
     * Create a ForAllTerm
     *
     * A ForAllTerm is created by identifying the symbols that should be quantified over
     * (the bounded variables), and writing a term.
     *
     * @param x     An SMTLIB2Symbol to universally quantify over
     * @param xs    Optional other SMTLIB2Symbols to universally quantify over
     * @param defs  A term, must be a [[theories.BoolTerm]]
     *
     * @note        By using SMTB2Symbols instead of VarTerm, we allow to quantify
     *              over variables with different type. For instance, we can quantify over Reals and Ints
     *              using SMTLIB2Symbol. This would not be possible with VarTerm s as they are TypedTerms
     *
     */
    def forall[ T <: Term ]( x : SMTLIB2Symbol, xs : SMTLIB2Symbol* )( defs : ⇒ TypedTerm[ BoolTerm, T ] ) = {

        //  split the vars into bounded/unbounded
        val groupedVars = splitBounded( x +: xs, defs )

        /*
         * Transform the boundedVars into SortedVars for use in ForAllTerm
         *
         * SortedVar and SortedQId contain the same information but using different
         * SMTLIB2 grammar rules.
         * Note that a SortedVar is pretty printed as (name sort) whereas s SortedQId
         * shows as (as name sort)
         */
        val boundedVars = groupedVars.getOrElse( true, Set[ SortedQId ]() ) map {
            case SortedQId( SymbolId( name ), sort ) ⇒ SortedVar( name, sort )
        }

        /*
         * Get the free variables in the term (the ones that should be declared in
         * the solver)
         */
        val freeVars = groupedVars.getOrElse( false, Set[ SortedQId ]() )

        //  build the TypedTerm
        TypedTerm[ BoolTerm, ForAllTerm ](
            freeVars,
            ForAllTerm(
                boundedVars.toList,
                defs.termDef ) )
    }

    /**
     * Create a ExistTerm
     *
     * A ExistTerm is created by identifying the symbols that should be quantified over
     * (the bounded variables), and writing a term.
     *
     * @param x     An SMTLIB2Symbol to existentially quantify over
     * @param xs    Optional other SMTLIB2Symbols to existentially quantify over
     * @param defs  A term, must be a [[theories.BoolTerm]]
     *
     * @note        By using SMTB2Symbols instead of VarTerm, we allow to quantify
     *              over variables with different type. For instance, we can quantify over Reals and Ints
     *              using SMTLIB2Symbol. This would not be possible with VarTerm s as they are TypedTerms
     *
     */
    def exists[ T <: Term ]( x : SMTLIB2Symbol, xs : SMTLIB2Symbol* )( defs : ⇒ TypedTerm[ BoolTerm, T ] ) = {

        //  split the vars into bounded/unbounded
        val groupedVars = splitBounded( x +: xs, defs )

        /*
         * Transform the boundedVars into SortedVars for use in ForAllTerm
         *
         * SortedVar and SortedQId contain the same information but using different
         * SMTLIB2 grammar rules.
         * Note that a SortedVar is pretty printed as (name sort) whereas s SortedQId
         * shows as (as name sort)
         */
        val boundedVars = groupedVars.getOrElse( true, Set[ SortedQId ]() ) map {
            case SortedQId( SymbolId( name ), sort ) ⇒ SortedVar( name, sort )
        }

        /*
         * Get the free variables in the term (the ones that should be declared in
         * the solver)
         */
        val freeVars = groupedVars.getOrElse( false, Set[ SortedQId ]() )

        //  build the TypedTerm
        TypedTerm[ BoolTerm, ExistsTerm ](
            freeVars,
            ExistsTerm(
                boundedVars.toList,
                defs.termDef ) )
    }
}
