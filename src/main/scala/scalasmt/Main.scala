/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt

/** Parse general response strings */
object Main {

    def main( args : Array[ String ] ) : Unit = {

        import org.bitbucket.franck44.scalasmt.interpreters.SMTSolver
        import org.bitbucket.franck44.scalasmt.configurations.{ SolverConfig, SMTInit }
        import org.bitbucket.franck44.scalasmt.configurations.AppConfig.config

        /* First argument is the solver name */
        val solverName = if ( args.size > 0 ) args( 0 ) else "Z3"

        /* Retrieve solver config from configuration file */
        val solver = new SMTSolver( config.find( _.name == solverName ).get, new SMTInit() )

        repl( solver )
    }
}
