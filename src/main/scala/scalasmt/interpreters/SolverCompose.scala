/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package interpreters

import configurations.{ SMTInit, SolverConfig }
import configurations.AppConfig.config
import scala.concurrent.duration._

object SolverCompose {

    /**
     * Provide combinators to define solvers usage strategies.
     */
    trait SolverStrategy {

        /**
         * The timeout of the strartegy.
         */
        def timeout : scala.concurrent.duration.Duration

        /**
         * The list of new solvers for the stratregy.
         */
        def get : List[ SMTSolver ]

        /**
         * The strategy name.
         */
        def name : String
    }

    case class Single(
        solverName : String,
        initSetUp :  SMTInit = new SMTInit( None, List() ) ) extends SolverStrategy {

        require(
            !config.find( _.name == solverName ).isEmpty,
            s"Solver name ${solverName} not in configuration file" )

        val solverSpec = config.find( _.name == solverName ).get

        val timeout = solverSpec.timeoutPerEval

        def get = List( new SMTSolver( solverSpec, initSetUp ) )

        def name = solverName
    }

    /**
     * Run multiple strategies concurrently.
     */
    case class Parallel( private val xs : List[ SMTSolver ], maxTime : Option[ Duration ] = None ) extends SolverStrategy {

        require ( xs.size >= 1, "At least one solver needed" )

        val timeout = maxTime.getOrElse( xs.map( _.timeout ).max )

        def get = xs.map( { case solver ⇒ new SMTSolver( solver.s, solver.initSetUp ) } )

        def name = s"(${xs.map( _.name ).mkString( " || " )})"

    }

}
