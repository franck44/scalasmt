/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package interpreters

import configurations.{ SMTInit, SolverConfig }
import configurations.AppConfig.config

/**
 * A runnable SMT solver (i.e. interpreter of SMTLIB scripts)
 *
 * @param  s            A solver configuration.
 * @param  initSetUp    The options/logic to initialise the solver with.
 *
 */
class SMTSolver(
    val s :         SolverConfig,
    val initSetUp : SMTInit ) {

    import configurations.SMTOptions.optionTag

    //  Check that SolverConfig supports the logic in initSetUp
    require(
        ( initSetUp.logik.map( s.supportsLogic ) ).getOrElse( true ),
        s"Logic ${initSetUp.logik} not supported by solver ${s.name}" )

    //  Check that SolverConfig supports the options initSetUp
    require(
        ( initSetUp.options.toSet.map( optionTag ) &~ s.supportedOptions ).isEmpty,
        s"Options ${( s.supportedOptions &~ initSetUp.options.toSet.map( optionTag ) )} not supported by solver ${s.name}" )

    //  Secondary constructors

    /**
     * Provide no initial configuration
     */
    def this( s : SolverConfig ) = this( s, new SMTInit( None, List() ) )

    /**
     * Provide a name rather than a SolverConfig
     */
    def this(
        solverName : String,
        initSetUp :  SMTInit = new SMTInit( None, List() ) ) =
        this( config.find( _.name == solverName ).get, initSetUp )

    import scala.concurrent.duration._

    import scala.util.{
        Try,
        Success,
        Failure
    }
    import org.bitbucket.franck44.expect.Expect

    //  logger

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = getLogger( this.getClass )

    /** The solver name. */
    def name : String = s.name

    /** The solver timeout as per configuration file. */
    def timeout = s.timeoutPerEval

    /**
     *  Command string for get-interpolants
     */
    val getInterpolantCmdString : Option[ String ] = s.getInterpolantCmdString

    //  Use Expect to get a running solver

    /*  create an Expect object running the solver */
    private val solverTerminal = Expect( s.executable, s.args.toList )

    import parser.SMTLIB2PrettyPrinter.show

    import parser.{ SMTLIB2Parser, PredefinedParsers }
    import parser.SMTLIB2Syntax.{
        SetOptionCmd,
        PrintSuccess,
        SetLogicCmd,
        SSymbol,
        SuccessResponse,
        UnsupportedResponse,
        ErrorResponse,
        GeneralResponse,
        Command,
        ExitCmd,
        SortedQId
    }

    object usefulParser extends PredefinedParsers
    import usefulParser.parseAsGeneralResponse

    /**
     * Recursive evaluation of configuration script
     * stops t the first fail or error/unsupported respoinse from solver.
     * - either solver did not send back String
     * - or the command produec an error
     *
     * @param cmd       The command to evaluate
     * @param timeout   The timeout in seconds
     *
     * @note The solver's reponse is parsed as GeneralResponse but the
     * negative instances of Generalresponse (Unsupported, Error) are converted
     * into failures.
     */
    private def evalInSolverAsSuccessResponse[ C <: Command ](
        cmd :     C,
        timeout : Option[ Duration ] = None ) : Try[ SuccessResponse ] =
        evalCmdInSolver( cmd, timeout ) flatMap parseAsGeneralResponse match {

            case Success( SuccessResponse() ) ⇒ Success( SuccessResponse() )

            case Success( UnsupportedResponse() ) ⇒
                Failure(
                    InterpreterUnsupportedException( s"Command ${show( cmd )} unsupported" ) )

            case Success( ErrorResponse( e ) ) ⇒
                Failure( InterpreterErrorException(
                    s"Command ${show( cmd )} produced error ${show( ErrorResponse( e ) )}" ) )

            case Failure( e ) ⇒ Failure( e )

        }

    /**
     * Build a sequence of commands to initialise the interpreter with the logics and options
     * @note: the method is made visible for testing.
     */
    private[ interpreters ] lazy val init : Try[ SuccessResponse ] =
        evalInSolverAsSuccessResponse( SetOptionCmd( PrintSuccess( true ) ) ) match {

            case Success( _ ) ⇒

                //  send the initSetUp to solver
                ( for {
                    cmd ← initSetUp.toSMTLIBCmds
                } yield ( cmd, evalInSolverAsSuccessResponse( cmd ) ) ).
                    filter( { case ( _, response ) ⇒ response.isFailure } ) match {
                        case Nil ⇒
                            //  all options were successfully set
                            logger.info( s"All options set successfully" )
                            Success( SuccessResponse() )

                        case l ⇒
                            //  kill the process
                            destroy()

                            logger.error( s"Initialisation with ${l.map( { case ( x, _ ) ⇒ x } ).map { k ⇒ show( k ) }} could not be set for solver ${s.name}" )
                            //  collate the message from the different failures
                            Failure( new Exception( s"l.flatMap(_._2.messages)" ) )
                    }

            case Failure( e ) ⇒
                //  kill the process
                destroy()
                logger.error( s"Could not set options :print-success true for solver ${s.name}" )
                Failure( e )
        }

    //  Interpreter

    /*
     * Stack of declarations on the solver.
     * Every time a Command that declares a variables is pushed we store the
     * type of the declared term. Push/pop are allowed and the stack is updated
     * accordingly.
     */
    private[ scalasmt ] val context =
        scala.collection.mutable.Stack(
            scala.collection.mutable.Set[ SortedQId ]() )

    /**
     * Add get-declarations capability to the solver
     *
     * This command has a timeout so will terminate.
     *
     * @param cmd       The command to evaluate
     * @param timeout   The timeout in seconds
     *
     * @return          Either a Success or Failure.
     *
     */
    def eval[ C <: Command ](
        cmd :     C,
        timeout : Option[ Duration ] = None ) : Try[ String ] = {
        import com.typesafe.scalalogging.Logger
        import org.slf4j.LoggerFactory
        import parser.SMTLIB2PrettyPrinter.show

        import parser.SMTLIB2Syntax.{
            SMTLIB2Symbol,
            SortedQId,
            Command,
            DeclCmdResponseSuccess,
            SuccessResponse,
            PopCmd,
            PushCmd,
            GetDeclCmd,
            DeclareFunCmd,
            FunDecl,
            SSymbol,
            SymbolId
        }

        //  Can be called only if init was successful
        require( init == Success( SuccessResponse() ), s"Solver could not be initialised: ${init match { case Failure( f ) ⇒ f.getMessage; case _ ⇒ "" }}" )

        //  log what is sent to the solver
        val logger = Logger( LoggerFactory.getLogger( "smt2" ) )

        //  Execute command
        cmd match {

            //  This command is not provided by an SMTSolver but by this trait
            case GetDeclCmd() ⇒

                logger.info( "GetDeclCmd command with no call to solver" )

                // collect all the declarations on the stack and union them
                val declarations = context.reduceLeft( _ ++ _ ).toList
                Success( show( DeclCmdResponseSuccess( declarations ) ) )

            //  Declare some simple variables (Ints, Bools, Reals, ArraysEx).
            //  If command is successful we update the context (stack).
            case DeclareFunCmd( FunDecl( e : SMTLIB2Symbol, List(), sort ) ) ⇒

                evalInSolverAsSuccessResponse( cmd, timeout ) match {

                    //  declare command returned a String
                    case Success( SuccessResponse() ) ⇒
                        //  add declaration to context as command was succesful
                        context.top += SortedQId( SymbolId( e ), sort )
                        //  return the string version of SuccessResponse()
                        Success( show( SuccessResponse() ) )

                    //  This should never happen as evalInSolverAsSuccessResponse returns
                    //  Success only for SuccessResponse()
                    case Success( e ) ⇒
                        logger.error( s"Declare command failed: ${e}" )
                        Failure( new Exception( s"$e" ) )

                    //  Failure (solver did not return a String)
                    case Failure( f ) ⇒
                        logger.error( s"Declare command failed: ${f.getMessage}" )
                        Failure( f )
                }

            //  Declare functions (non constant).
            //  This will go through to the solver but the declaration is not
            //  stored in the context
            case DeclareFunCmd( FunDecl( e : SMTLIB2Symbol, _, sort ) ) ⇒

                // a function is declared. Store in context not supported yet
                logger.warn( "Declaration of non-const term. Cannot be added to context" )
                evalCmdInSolver( cmd, timeout )

            //  Pop command
            case p @ PopCmd( x ) ⇒

                logger.info( "Evaluate Pop({}) command ", x )

                //  Send pop command and collect the result as a Try[String]
                //  Check that result is Success before updating context.
                val r : Try[ String ] = evalCmdInSolver( p, timeout )

                ( r flatMap parseAsGeneralResponse ) match {

                    case Success( SuccessResponse() ) ⇒
                        logger.info( "Pop({}) command successfully evaluated", x )
                        //  pop x elements
                        Range( 0, x.toInt ).foreach { _ ⇒ context.pop() }
                        r

                    case Success( e ) ⇒
                        logger.info( "Pop({}) command failed {}", e )
                        Failure( new Exception( "Pop command failed " + e ) )

                    case Failure( e ) ⇒
                        logger.info( "Pop({}) command failed {}", e )
                        Failure( e )
                }

            //      Push command
            case p @ PushCmd( x ) ⇒

                logger.info( "Evaluate Push({}) command ", x )

                //  Send push command and collect result as a Try[String]
                //  Check that result is Success before updatting context.
                val r : Try[ String ] = evalCmdInSolver( p, timeout )

                ( r flatMap parseAsGeneralResponse ) match {

                    case Success( SuccessResponse() ) ⇒
                        logger.info( "Push({}) command successfully evaluated", x )
                        //  push x empty sets on the stack
                        Range( 0, x.toInt ).foreach {
                            _ ⇒
                                context.push(
                                    scala.collection.mutable.Set[ SortedQId ]() )
                        }
                        r

                    case Success( e ) ⇒
                        logger.info( "Push({}) command failed {}", e )
                        Failure( new Exception( "Push command failed " + e ) )

                    case Failure( e ) ⇒
                        logger.info( "Push({}) command failed {}", e )
                        Failure( e )
                }

            // If the command is not monitored, it goes directly to the solver
            case _ ⇒

                logger.info( "command {} is forwarded to the solver ", cmd )
                evalCmdInSolver( cmd, timeout )
        }
    }

    /**
     * Send a command to an SMTLIB2 compliant SMTSolver and get a response.
     *
     * This command has a timeout so will terminate.
     *
     * @param cmd       The command to evaluate
     * @param timeout   The timeout in seconds
     *
     * @return          Either a Success with the solver raw (String)
     *                  response or Failure.
     */
    private def evalCmdInSolver[ C <: Command ](
        cmd :     C,
        timeout : Option[ Duration ] = None ) : Try[ String ] = {

        import parser.SMTLIB2PrettyPrinter.show
        import parser.SMTLIB2Syntax.{ EchoCmd, StringLiteral }
        import scala.util.matching.Regex

        val loggerSMT2 = getLogger( this.getClass, ".smt2" )

        cmd match {

            //  if cmd is exit, send and return Success(emptyString). No Expect
            //  should follow as the solver is dead
            case ExitCmd() ⇒
                logger.info( "Sending Exit command. No expect will follow" )
                loggerSMT2.info( show( cmd ) )

                solverTerminal.send( show( cmd ) ) match {

                    case Success( _ ) ⇒ Success( "Solver exit" )

                    case Failure( e ) ⇒ Failure( e )
                }

            //  otherwise forward command to solver
            case _ ⇒

                //  the solver echo command
                val echoCmd : Command = EchoCmd( StringLiteral( s.cmdSentinelleString ) )

                //  the solver expected prompt after successful command
                val prompt : Regex = s.expectedPromptAfterCommand

                logger.info( "Entering eval" )

                /*
                 * Try to evaluate the cmd within the time bound
                 * @note add a \n as for a few solver (e.g. CVC4)  this is needed
                 *
                 * The evaluation consists in 3 phases:
                 * 1.  send the cmd to the solver terminal-like processing
                 * 2.  send the sentinelle echo Command
                 * 3.  expect a response (within timeout): a string preceeding the prompt
                 *
                 * If any of the preceeding steps fails, a Failure is generated and
                 * propagated (by the flatMap). Otherwise, the Success(_) result of a
                 * command is fed to the next step.
                 *
                 * @note It uses the fact that Try is a monad. Assuming that f returns
                 * a Try[something], Success(v) flatMap f is mapped to Success(f(v))
                 * and Failure(ex) flatMap f to Failure(ex)
                 *
                 */

                loggerSMT2.info( show( cmd ) )
                loggerSMT2.info( show( echoCmd ) )
                logger.info( s"Timeout in expect is ${timeout.getOrElse( s.timeoutPerEval )} for cmd ${show( cmd )}" )

                //  send the cmd to the solver. SolverTerminal.send returns a Try[Unit]
                solverTerminal.send( show( cmd ) + "\n" ) flatMap
                    //
                    //  send the echo sentinelle command
                    //
                    ( _ ⇒ solverTerminal.send( show( echoCmd ) + "\n\n" ) ) flatMap
                    //
                    //  wait for the response, a Try[String], and return it
                    //
                    ( _ ⇒ solverTerminal.expect(
                        prompt,
                        timeout.getOrElse( s.timeoutPerEval ) ) )
        }
    }

    /** Kill (hopefully nicely) the external process running the solver */
    def destroy() : Unit = {

        solverTerminal.destroy()
        logger.info( "solverTerminal (Expect object) destroyed" )

    }
}

/**
 * Exception triggered when the solver response is (error ...)
 */
case class InterpreterErrorException( msg : String ) extends Exception( msg )

/**
 * Exception triggered when the solver response is unsupported
 */
case class InterpreterUnsupportedException( msg : String ) extends Exception( msg )
