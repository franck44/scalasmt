/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package interpreters

/**
 * Provide a method `using` to use a solver in a resource safe manner.
 */
trait Resources {

    import resource._
    import scala.util.{ Try, Success, Failure }
    import interpreters.SMTSolver

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = getLogger( this.getClass )

    /**
     * Provide an implicit Resource object to close un-used resources
     */
    implicit object SolverResource extends Resource[ SMTSolver ] {

        /** Kills the process of the interpreter */
        def close( r : SMTSolver ) : Unit = {

            logger.info( s"Destroying SMTSolver $r" )
            r.destroy()
        }
    }

    /**
     * Run a single solver, monadic version.
     *
     * @tparam T  The type returned by the computation.
     * @param  s  An SMT solver e.g. 'new Z3 with QF_LIA'
     * @param  f  A map that associates a solver computation with an
     *            (<strong>implicit</strong>) SMT solver.
     *
     * @todo      Log the Seq of Throwables in x when Left non empty
     */
    def using[ T ](
        s : SMTSolver )(
        f : ⇒ ( SMTSolver ⇒ Try[ T ] ) ) : Try[ T ] = {

        logger.info( s"Solver $s created and configured" )
        ( managed( s ) map f ).either match {

            //  managed failed if x.nonEmpty.
            case ExtractedEither( Left( x ) ) if x.nonEmpty ⇒
                Failure( x.head )

            //  otherwise return the result of f applied to solver.
            case ExtractedEither( Right( x ) ) ⇒ x
        }
    }

    /**
     * Run Multiple solvers in parallel.
     *
     * @tparam T            The type returned by the computation.
     * @param  strategy     A [[SolverCompose.Parallel]] strategy.
     * @param  f            A map that associates a solver computation with an
     *                      (<strong>implicit</strong>) SMT solver.
     * @note                Threads are created using ewCachedThreadPool(aThreadFactory).
     */
    def using[ T ](
        strategy : SolverCompose.Parallel )(
        f : ⇒ ( SMTSolver ⇒ Try[ T ] ) ) : Try[ T ] = {

        import java.util.concurrent.{ Executors, RejectedExecutionException }
        import scala.concurrent.{ ExecutionContext, ExecutionContextExecutorService }
        import com.twitter.util.{
            Future ⇒ TwitterFuture,
            Await ⇒ TwitterAwait,
            Promise ⇒ TwitterPromise,
            Duration
        }
        import java.util.concurrent.atomic.AtomicBoolean

        //  Whether a solver returned a Success or not
        val done = new AtomicBoolean( false )

        //  GLobal timeout is the max of the timeout of all the solvers.
        val timeout = strategy.timeout

        //  Create Promise to hold the result of the concurrent computations
        val p = TwitterPromise[ ( Try[ T ], SMTSolver ) ]()

        //  Create a list of futures, one for each solver task.
        //  Each one is pushed to the pool that will be provided
        //  by the implicit execution context in the following `try`.
        lazy val tasks : List[ TwitterFuture[ ( Try[ T ], SMTSolver ) ] ] = strategy.get map {
            solver ⇒
                logger.info( s"Creating future for solver: ${solver.name}" )
                TwitterFuture {
                    (
                        using( solver ) {
                            implicit withSolver ⇒ f( withSolver )
                        },
                        solver )
                }
        }

        /*
         * Create an execution context.
         * `newCachedThreadPool(aThreadFactory)` Creates a thread pool that creates new
         * threads as needed, but will reuse previously constructed threads when they are
         * available, and uses the provided ThreadFactory to create new threads when needed.
         */
        implicit lazy val pool : ExecutionContextExecutorService =
            ExecutionContext.fromExecutorService(
                Executors.newCachedThreadPool( Executors.defaultThreadFactory() ) )

        try {
            logger.info( s"Running Solvers ${strategy.name} created and configured" )

            //  Map tasks to success result of the promise `p`.
            tasks foreach {
                t ⇒
                    t foreach {

                        case ( Success( x ), solver ) ⇒
                            logger.info( s"solver ${solver.name} returned success: $x" )
                            //  getAndSet returns the old value and set to true
                            //  atomic action
                            if ( !done.getAndSet( true ) ) {
                                logger.info( s"Setting value of promise to $x" )
                                p.setValue( ( Success( x ), solver ) )
                            }

                        case ( Failure( f ), solver ) ⇒
                            logger.info( s"solver ${solver.name} returned Failure: $f" )
                            logger.info( s"failed" )
                    }
            }

            //  Block until result of promise or timeout
            TwitterAwait.result(
                p,
                Duration.fromMilliseconds( timeout.toMillis ) ) match {
                    case ( r, s ) ⇒
                        logger.info( s"Promise succeeded - solver $s - result: $r " )

                        r
                }
        } catch {

            //  Exceptions due to pool.submit (creartion of Futures).
            case ex : RejectedExecutionException ⇒
                logger.error( s"getResponse task could not be scheduled for execution -- $ex" )
                Failure( ex )

            case ex : NullPointerException ⇒
                logger.error( s"getResponse task is null -- $ex" )
                Failure( ex )

            //  Convert twitter timeoutException into scala counterpart.
            case to : com.twitter.util.TimeoutException ⇒
                logger.error( s"getResponse task timed out -- $to" )
                Failure( new scala.concurrent.TimeoutException( s"Futures timed out after [$timeout]" ) )

            /*
             * There are potentially three types of other exceptions:
             * 1.  InterruptedException: Waiting timeout thread was interrupted
             * 2.  TimeoutException : No prompt before the timeout
             * 3   IllegalArgumentException : Timeout bound undefined
             */
            case other : Throwable ⇒
                logger.error( s"Error occurred in Expect -- $other" )
                Failure( other )
        } finally {
            logger.info( s"Terminate using parallel and shutdown threads " )
            // Cancel all non completed futures
            tasks.map( _.toJavaFuture ).map( _.cancel( true ) )
            // Shutdown the pool
            pool.shutdownNow()
        }
    }

}
