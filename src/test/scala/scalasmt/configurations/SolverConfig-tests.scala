/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package configurations
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import resource._
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources
import parser.PredefinedParsers

/**
 * Check [[PredefinedConfig]] configurations for each solver
 */
class SolverConfigTests extends FunSuite with Matchers with TableDrivenPropertyChecks with Resources
    with PredefinedParsers {

    import parser.SMTLIB2PrettyPrinter.show
    import interpreters.SMTSolver
    import configurations.AppConfig.{ config, enabledSolvers }
    import configurations.{ SolverConfig, SMTInit }
    import configurations.SMTLogics._
    import PredefinedConfig._
    import configurations.SMTOptions.optionTag

    val enabledSolverNames = config.filter( _.enabled ).map( _.name )

    //  list of (solver, List of configs to test)
    val configdSolver = Table(
        ( "Solver", "Configuration" ),
        (
            "Z3-4.5.0",
            List(
                QFLIASatOnlyConfig,
                QFLIASatModelConfig,
                QFLIAFullConfig,
                AUFNIRASatModelConfig,
                QFAUFLIAFullConfig ) ),
        (
            "Z3-4.8.6",
            List(
                QFLIASatOnlyConfig,
                QFLIASatModelConfig,
                AUFNIRASatModelConfig ) ),
        (
            "SMTInterpol",
            List(
                QFLIASatModelConfig,
                QFLIAFullConfig, QFAUFLIASatModelConfig,
                QFAUFLIAFullConfig ) ),
        (
            "CVC4",
            List(
                QFLIASatModelConfig,
                QFAUFLIASatModelConfig ) ),
        (
            "MathSat",
            List(
                QFLIASatModelConfig,
                QFAUFLIASatModelConfig ) ),
        (
            "Yices-2.6.0",
            List(
                QFLIASatModelConfig,
                QFAUFLIASatModelConfig ) ) )

    import parser.SMTLIB2Syntax.{ SuccessResponse, Command, ProduceProofs, UnsupportedResponse }

    forAll ( configdSolver ) {
        ( sName : String, lc : List[ SMTInit ] ) ⇒
            whenever ( enabledSolverNames.contains( sName ) ) {
                //  retrieve solver with name s
                config.find( _.name == sName ) match {
                    case Some( s ) ⇒
                        for ( cs ← lc ) {
                            test( s"[${sName}] Sending  configuration ${cs.show} -- Should succeed" ) {

                                //  check that the logic is decalred as supported by the solver
                                cs.logik.map( s.supportsLogic ).getOrElse( true ) shouldBe true

                                //  check that the options are supported by the solver
                                all ( cs.options.map( s.supports( _ ) ) ) shouldBe true

                                val result = using( new SMTSolver( sName ) ) {
                                    implicit solver ⇒
                                        {
                                            //  smtlib package eval is used
                                            eval( cs.toSMTLIBCmds )
                                        }
                                } flatMap parseAsGeneralResponse

                                //  mathSAT does not support ProduceProofs
                                if ( !( sName == "MathSat" ) ||
                                    !cs.options.contains( ProduceProofs( true ) ) ) {

                                    result shouldBe Success( SuccessResponse() )

                                } else
                                    result should matchPattern {
                                        case Failure( _ ) ⇒
                                    }
                            }
                        }
                    case None ⇒
                        fail( new Exception( s"Could not find config for solver $sName" ) )
                }

            }
    }
}
