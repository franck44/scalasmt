/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package configurations
package tests

//  some useful configurations

private[ tests ] object PredefinedConfig {

    import configurations.SMTLogics._
    import configurations.SMTOptions._
    import configurations.SMTInit

    /* Empty initial set up */
    val EmptyConfig = SMTInit( None, List() )

    /**
     * Linear integer arithmetic
     */

    //  check sat and get a model if sat
    val QFLIASatModelConfig = new SMTInit( QF_LIA, List( SMTProduceModels( true ) ) )

    //  check sat, get a model and interpolants
    val QFLIAFullConfig = new SMTInit( QF_LIA, List( SMTProduceProofs( true ), SMTProduceModels( true ), SMTProduceInterpolants( true ) ) )

    //  check sat only (no interpolants, no model)
    val QFLIASatOnlyConfig = new SMTInit( QF_LIA, List() )

    /**
     * Real arithmetic
     */

    val AUFNIRASatModelConfig = new SMTInit( AUFNIRA, List( SMTProduceModels( true ) ) )

    val QFUFLIRASatModelConfig = new SMTInit( QF_UFLIRA, List( SMTProduceModels( true ) ) )

    // QF_AUFLIA, arrays

    val QFAUFLIASatModelConfig = new SMTInit( QF_AUFLIA, List( SMTProduceModels( true ) ) )

    val QFAUFLIAFullConfig = new SMTInit( QF_AUFLIA, List( SMTProduceProofs( true ), SMTProduceModels( true ), SMTProduceInterpolants( true ) ) )

}
