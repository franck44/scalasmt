/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package typedterms
package tests

import org.scalatest.{ FunSuite, Matchers, NonImplicitAssertions }
import org.scalatest.prop.TableDrivenPropertyChecks
import typedterms.Commands
import interpreters.Resources
import theories.{
    Core,
    IntegerArithmetics,
    BitVectors,
    RealArithmetics,
    ArrayExInt,
    ArrayExBool,
    ArrayExReal,
    ArrayExBV,
    ArrayExOperators
}

/**
 * Test the typedDef component when building Array typedterms
 */
class ArrayTypeDefsTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with IntegerArithmetics
    with ArrayExInt
    with ArrayExBool
    with ArrayExReal
    with ArrayExBV {

    override def suiteName = s"Check the typeDefs built using the Array constructors"

    import parser.SMTLIB2PrettyPrinter.show
    import typedterms.TypedTerm
    import parser.SMTLIB2Syntax.{ Term, QIdTerm }

    //  some useful variable and shortcuts
    val a1 = ArrayInt1( "a" )

    test( s"Check the typeDefs produced by: $a1 -- Should be Set(((as a (Array Int Int)))))" ) {
        a1.typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array Int Int))" ) )
    }

    val a2 = ArrayInt2( "a" )

    test( s"Check the typeDefs produced by: $a2 -- Should be Set(((as a (Array Int (Array Int Int))))))" ) {
        a2.typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array Int (Array Int Int)))" ) )
    }

    val b1 = ArrayBool1( "a" )

    test( s"Check the typeDefs produced by: $b1-- Should be Set(((as a (Array Int Bool)))))" ) {
        b1.typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array Int Bool))" ) )
    }

    val b2 = ArrayBool2( "a" )

    test( s"Check the typeDefs produced by: $b2 -- Should be Set(((as a (Array Int (Array Int Bool))))))" ) {
        b2.typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array Int (Array Int Bool)))" ) )
    }

    val c1 = ArrayReal1( "a" )

    test( s"Check the typeDefs produced by: $c1 -- Should be Set(((as a (Array Int real)))))" ) {
        c1.typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array Int Real))" ) )
    }

    val c2 = ArrayReal2( "a" )
    test( s"Check the typeDefs produced by: $c2 -- Should be Set(((as a (Array Int (Array Int real))))))" ) {
        c2.typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array Int (Array Int Real)))" ) )
    }

    val bv1 = ArrayBV1( "bv1", 2, 4 )
    test( s"Check the typeDefs produced by: $bv1 -- Should be Set(((as bv1 (Array (_ BitVec 2) (_ BitVec 4))))))" ) {
        bv1.typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as bv1 (Array (_ BitVec 2) (_ BitVec 4)))" ) )
    }

    val bv2 = ArrayBV2( "bv2", 3, 5, 8 )
    test( s"Check the typeDefs produced by: $bv2 -- Should be Set(((as bv2 (Array (_ BitVec 3) (Array (_ BitVec 5) (_ BitVec 8)))))))" ) {
        bv2.typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as bv2 (Array (_ BitVec 3) (Array (_ BitVec 5) (_ BitVec 8))))" ) )
    }
}

/**
 * Test the typeDef component for select TypedTerm
 */
class ArraySelectTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with IntegerArithmetics
    with BitVectors
    with ArrayExInt
    with ArrayExBool
    with ArrayExReal
    with ArrayExBV
    with ArrayExOperators {

    override def suiteName = s"Check the typeDefs built with the select operator"

    import parser.SMTLIB2PrettyPrinter.show

    //  some useful variable and shortcuts
    val a1 = ArrayInt1( "a" )

    test( s"Check the TypedTerm produced by: select 1 in $a1" ) {

        ( a1 select Ints( 1 ) ).typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array Int Int))" ) )

        show( ( a1 select 1 ).termDef ) shouldBe
            "(select a 1)"
    }

    val a2 = ArrayInt2( "a" )

    test( s"Check the typeDefs produced by: select 1 in $a2" ) {

        ( a2 select 1 ).typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array Int (Array Int Int)))" ) )

        show( ( a2 select 1 ).termDef ) shouldBe
            "(select a 1)"
    }

    val b1 = ArrayBool1( "a" )

    test( s"Check the typeDefs produced by: select 1 in $b1" ) {

        ( b1 select 1 ).typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array Int Bool))" ) )

        show( ( b1 select 1 ).termDef ) shouldBe
            "(select a 1)"
    }

    val b2 = ArrayBool2( "a" )

    test( s"Check the typeDefs produced by: select 1 in $b2" ) {

        ( b2 select 1 ).typeDefs.map( show( _ ) ) shouldBe
            Set( "(as a (Array Int (Array Int Bool)))" )

        show( ( b2 select 1 ).termDef ) shouldBe
            "(select a 1)"
    }

    val c1 = ArrayReal1( "a" )

    test( s"Check the typeDefs produced by: select 1 in $c1" ) {

        ( c1 select 1 ).typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array Int Real))" ) )

        show( ( c1 select 1 ).termDef ) shouldBe
            "(select a 1)"
    }

    val c2 = ArrayReal2( "a" )

    test( s"Check the typeDefs produced by: select 1 in $c2" ) {

        ( c2 select 1 ).typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array Int (Array Int Real)))" ) )

        show( ( c2 select 1 ).termDef ) shouldBe
            "(select a 1)"
    }

    val bv1 = ArrayBV1( "bv1", 2, 4 )

    test( s"Check the typeDefs produced by: select #b01 in $bv1" ) {
        ( bv1 select BVs( "#b01" ) ).typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as bv1 (Array (_ BitVec 2) (_ BitVec 4)))" ) )

        show( ( bv1 select BVs( "#b01" ) ).termDef ) shouldBe
            "(select bv1 #b01)"
    }

    val bv2 = ArrayBV2( "bv2", 3, 2, 8 )

    test( s"Check the typeDefs produced by: select #b001 #b10 in $bv2" ) {
        ( bv2 select BVs( "#b001" ) select BVs( "#b10" ) ).typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as bv2 (Array (_ BitVec 3) (Array (_ BitVec 2) (_ BitVec 8))))" ) )

        show( ( bv2 select BVs( "#b001" ) select BVs( "#b10" ) ).termDef ) shouldBe
            "(select (select bv2 #b001) #b10)"
    }

    test( s"Check the typeDefs produced by: select true|2.0 $a1 -- Should not typecheck" ) {
        //  format: OFF
        "ArrayInt1( \"a\" ) select Bool( true )"  shouldNot typeCheck
        "ArrayInt1( \"a\" ) select SReal( 2.0 )"   shouldNot typeCheck
        //  format: ON
    }
}

/**
 * Test the typeDef component for store TypedTerm
 */
class ArrayStoreTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with IntegerArithmetics
    with BitVectors
    with ArrayExInt
    with ArrayExBool
    with ArrayExReal
    with ArrayExBV
    with ArrayExOperators {

    override def suiteName = s"Check the typeDefs built with the store operator"

    import parser.SMTLIB2PrettyPrinter.show

    //  some useful variable and shortcuts
    val a1 = ArrayInt1( "a" )

    test( s"Check the TypedTerm produced by: store 1 at index 2 in $a1" ) {

        ( a1 store ( 2, 1 ) ).typeDefs.map( show( _ ) ) shouldBe
            Set( "(as a (Array Int Int))" )

        show( ( a1 store ( 2, 1 ) ).termDef ) shouldBe
            "(store a 2 1)"
    }

    val a2 = ArrayInt2( "a" )

    test( s"Check the TypedTerm produced by: $a1(2) := 1" ) {

        ( a1( 2 ) := 1 ).typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array Int Int))" ) )

        show( ( a1( 2 ) := 1 ).termDef ) shouldBe
            "(store a 2 1)"

    }

    val a12 = ArrayInt1( "b" )

    test( s"Check the TypedTerm produced by: $a1(2) := x" ) {

        ( a1( 2 ) := x ).typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array Int Int))" ), "(as x Int)" )

        show( ( a1( 2 ) := x ).termDef ) shouldBe
            "(store a 2 x)"
    }

    val b1 = ArrayBool1( "a" )

    test( s"Check the TypedTerm produced by: store 1 at index 2 in Array2* -- should not typecheck" ) {

        //  format: OFF
        "( ArrayInt2( \"a\" ) store ( 2, 1 ) )"      shouldNot typeCheck
        "( ArrayBool2( \"a\" )  store ( 2, 1 ) )"    shouldNot typeCheck
        "( ArrayReal2( \"a\" ) store ( 2, 1 ) )"     shouldNot typeCheck
        //  format: ON
    }

    val b2 = ArrayBool1( "a" )

    test( s"Check the TypedTerm produced by: store 1 at index 2 in $b1" ) {

        ( b1 store ( 2, Bools( true ) ) ).typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array Int Bool))" ) )

        show( ( b1 store ( 2, Bools( false ) ) ).termDef ) shouldBe
            "(store a 2 false)"

    }

    val c1 = ArrayReal1( "a" )
    val c2 = ArrayReal1( "a" )
    val x = Ints( "x" )

    test( s"Check the TypedTerm produced by: store ($a2 store 1 2) at index 3 in $a12" ) {

        (
            a2 store (
                3,
                a12 store ( 1, 2 ) ) ).typeDefs.map( show( _ ) ) shouldBe
                Set( ( "(as a (Array Int (Array Int Int)))" ), ( "(as b (Array Int Int))" ) )

        show(
            (
                a2 store (
                    3,
                    a12 store ( 1, 2 ) ) ).termDef ) shouldBe
            "(store a 3 (store b 1 2))"

    }

    val bv1 = ArrayBV1( "bv1", 2, 4 )

    test( s"Check the TypedTerm produced by: store #x2 at index #b01 in $bv1" ) {

        ( bv1 store ( BVs( "#b11" ), BVs( "#x2" ) ) ).typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as bv1 (Array (_ BitVec 2) (_ BitVec 4)))" ) )

        show( ( bv1 store ( BVs( "#b11" ), BVs( "#x2" ) ) ).termDef ) shouldBe
            "(store bv1 #b11 #x2)"

    }

    val bv2 = ArrayBV2( "bv2", 3, 2, 4 )

    test( s"Check the TypedTerm produced by: store #x2 at index #b001 #b10 in $bv2" ) {

        ( bv2 store ( BVs( "#b001" ), bv1 store ( BVs( "#b10" ), BVs( "#x2" ) ) ) ).typeDefs.map( show( _ ) ) shouldBe
            Set( "(as bv2 (Array (_ BitVec 3) (Array (_ BitVec 2) (_ BitVec 4))))", "(as bv1 (Array (_ BitVec 2) (_ BitVec 4)))" )

        show( ( bv2 store ( BVs( "#b001" ), bv1 store ( BVs( "#b10" ), BVs( "#x2" ) ) ) ).termDef ) shouldBe
            "(store bv2 #b001 (store bv1 #b10 #x2))"

    }

}

/**
 * Test the typeDef component for equal array TypedTerm
 */
class ArrayEqualTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with IntegerArithmetics
    with ArrayExInt
    with ArrayExBool
    with ArrayExReal
    with ArrayExBV
    with ArrayExOperators
    with Resources {

    override def suiteName = s"Check the typeDefs built with the Array Equal operator"

    import parser.SMTLIB2PrettyPrinter.show

    //  some useful variable and shortcuts
    val a1 = ArrayInt1( "a" )
    val b1 = ArrayInt1( "b" )

    test( s"Check the TypedTerm produced by 1d array: a === b" ) {

        ( a1 === b1 ).typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array Int Int))" ), ( "(as b (Array Int Int))" ) )

        show( ( a1 === b1 ).termDef ) shouldBe
            "(= a b)"

    }

    val a2 = ArrayBV1( "a", 8, 4 )
    val b2 = ArrayBV1( "b", 8, 4 )

    test( s"Check the TypedTerm produced by 1d BV array: a === b" ) {

        ( a2 === b2 ).typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array (_ BitVec 8) (_ BitVec 4)))" ), ( "(as b (Array (_ BitVec 8) (_ BitVec 4)))" ) )

        show( ( a2 === b2 ).termDef ) shouldBe
            "(= a b)"

    }

    val a3 = ArrayBV2( "a", 8, 16, 4 )
    val b3 = ArrayBV2( "b", 8, 16, 4 )

    test( s"Check the TypedTerm produced by 2d BV array: a === b" ) {

        ( a3 === b3 ).typeDefs.map( show( _ ) ) shouldBe
            Set( ( "(as a (Array (_ BitVec 8) (Array (_ BitVec 16) (_ BitVec 4))))" ), ( "(as b (Array (_ BitVec 8) (Array (_ BitVec 16) (_ BitVec 4))))" ) )

        show( ( a3 === b3 ).termDef ) shouldBe
            "(= a b)"

    }

    test( s"Check the TypedTerm produced by: a === b with a and b of different type -- should not typecheck" ) {
        //  format: OFF
        "( ArrayInt1( \"a\" ) ===  ArrayInt2( \"b\" ))"                 shouldNot typeCheck
        "( ArrayInt1 (\"a\") store ( 2, Bool(true) ) )"                 shouldNot typeCheck
        "( ArrayInt2 (\"a\") store ( 2, 1 ) )"                          shouldNot typeCheck
        "( ArrayInt1 (\"a\") store ( 2, SReal(3.0) ) )"                 shouldNot typeCheck
        "( ArrayBool1 (\"a\") store ( 2, 1 ) )"                         shouldNot typeCheck
        "( ArrayInt1 (\"a\") store ( 2, SReal(1.0) ) )"                 shouldNot typeCheck
        "( ArrayReal2 (\"a\") select 1 === Ints(1) )"                   shouldNot typeCheck
        "( ArrayBV1 (\"a\",2,4) select 1 === Ints(1) )"                 shouldNot typeCheck
        "( ArrayBV1 (\"a\",2,4) select BVs(\"#b01\") === Ints(1) )"     shouldNot typeCheck
        "( ArrayBV2 (\"a\",2,2,4) select BVs(\"#b01\") select BVs(\"#b01\") === Ints(1) )"     shouldNot typeCheck
        //  format: ON
    }
}
