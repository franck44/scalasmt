/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package typedterm
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources
import theories.{
    Core,
    BitVectors,
    RealArithmetics,
    FPBitVectors,
    FPBVTerm,
    RMFPBVTerm,
    BoolTerm
}

class FPBitVectorsFactoryTypeTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with FPBitVectors
    with BitVectors {

    override def suiteName = "Check the typeDefs of FPBitVector terms"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax.{
        Sort,
        Term,
        IndexedId,
        SymbolId,
        SSymbol,
        ISymbol,
        QualifiedId,
        SortedQId,
        FPBitVectorSort,
        FPFloat16,
        FPFloat32,
        FPFloat64,
        FPFloat128,
        RNE,
        RNA,
        RTP,
        RTN,
        RTZ
    }
    import parser.SMTLIB2PrettyPrinter.show
    import typedterms.TypedTerm

    //  create a SortedQId, standard symbol
    def toSortedQId( sort : Sort )( name : String ) : SortedQId =
        SortedQId( SymbolId( SSymbol( name ) ), sort )

    //  create a  SortedQId, standard symbol
    def toSortedQId( name : String )( ebits : Int, sbits : Int ) : SortedQId =
        SortedQId( SymbolId( SSymbol( name ) ), FPBitVectorSort( ebits.toString, sbits.toString ) )

    //  create a SortedId indexed symbol
    def toSortedQId( name : String, index : Int )( ebits : Int, sbits : Int ) : SortedQId =
        SortedQId( SymbolId( ISymbol( name, index ) ), FPBitVectorSort( ebits.toString, sbits.toString ) )

    //  make a variable FPBV
    val x = FPBVs( "x", 5, 11 )

    //  make a variable from a String
    val y = "y".asFPBV( ebits = 8, sbits = 24 )

    //  indexed variable
    val y1 = y indexed 1

    //  Use an implicit rounding
    implicit val rne = RMs ( RNE() )

    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ FPBVTerm, Term ], Set[ SortedQId ] ](
        ( "Expression"          , "TypedTerm"                  , "typeDefs" ),
        // make a constant negative, expo = 10 and significand = 100
        ( "- 0.100^10"          , FPBVs(BVs("#b1"), BVs("#b10"), BVs("#b100")), Set() ),
        ( "Double 2.13"        , 2.13.asFloat64                            , Set() ),

        // make constants FPBVs from BVs
        ( "FPBV from BVs 1"     , FPBVs(BVs("#b0"), BVs(1,3), BVs(5,16))         , Set()),
        ( "FPBV from BVs 2"     , FPBVs(BVs("#b0"), BVs(1,3), BVs("#b10101"))    , Set()),
        ( "FPBV from BVs 3"     , FPBVs(BVs("#b0"), BVs("#b10101"),  BVs(1,3) )  , Set()),
        ( "FPBV from BVs 4"     , FPBVs(BVs("#b0"), BVs("#xa4019"), BVs(1,3) )   , Set()),

        ( "FPBV from FPBVs"     , FPBVs(BVs("#b0"), BVs(2,4),
                                        BVs(16,32)).toFPBV(4, 12)               , Set()),

        ( "+infinity 2 6"       , PlusInfinity( 2,6 )          , Set() ),
        ( "-infinity 4 11"      , MinusInfinity( 4,11 )        , Set() ),
        ( "+zero 2 6"           , PlusZero( 2,6 )              , Set() ),
        ( "-zero 2 6"           , MinusZero( 2,6 )             , Set() ),
        ( "NaN 2 6"             , NaN( 2,6 )                   , Set() ),
        ( "-NaN 2 6"            , -NaN( 2,6 )                   , Set() ),

        ( "x"                   , x                            , Set(toSortedQId("x")(5,11))),
        ( "y1"                  , y1                           , Set(toSortedQId("y",1)(8,24))),
        ( "x Float16"           , "x".asFloat16                , Set(toSortedQId(FPFloat16())("x"))),
        ( "x Float32"           , "x".asFloat32                , Set(toSortedQId(FPFloat32())("x"))),
        ( "x Float64"           , "x".asFloat64                , Set(toSortedQId(FPFloat64())("x"))),
        ( "x Float128"          , "x".asFloat128               , Set(toSortedQId(FPFloat128())("x"))),
        ( "FPBV from FPBVs"     , x.toFPBV(4, 12)              , Set(toSortedQId("x")(5,11))),
        ( "abs(x)"              , x.abs                        , Set(toSortedQId("x")(5,11) ) ),
        ( "-x"                  , - x                          , Set(toSortedQId("x")(5,11))),

        //  Use of implicit rounding first (+ can use an implicit rounding)
        ( "x + y"               , x + y                        , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24)  ) ),
        ( "x - y"               , x - y                        , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24)  ) ),

        //  Explicit rounding
        ( "x + y(RNA)"          , x.+(y)(RMs(RNA())) , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24)  ) ),
        ( "x - y(RNA)"          , x.-(y)(RMs(RTP())) , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24)  ) ),
        ( "x - y + z "          , x - y + "z".asFloat16        , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24), toSortedQId(FPFloat16())( "z") ) ),
        ( "x - y + z "          , x - y + "z".asFloat32        , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24), toSortedQId(FPFloat32())( "z") ) ),
        ( "x - y + z "          , x - y + "z".asFloat64        , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24), toSortedQId(FPFloat64())( "z") ) ),
        ( "x - y + z "          , x - y + "z".asFloat128       , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24), toSortedQId(FPFloat128())( "z") ) ),

        ( "x * y(RNA)"          , x.*(y)(RMs(RNA())) , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24) ) ),
        ( "x * y "              , x * y                        , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24) ) ),
        ( "x / y(RNA)"          , x./(y)(RMs(RNA())) , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24) ) ),
        ( "x / y"               , x / y                        , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24) ) ),

        ( "x % y"               , x % y                        , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24)  ) ),

        ( "(x * y) + z (fma)"   , x.*+(y,"z".asFloat128)       , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24), toSortedQId(FPFloat128())( "z") ) ),

        ( "sqrt(x)"             , x.sqrt                       , Set(toSortedQId("x")(5,11) ) ),

        ( "roundToI(x)"         , x.roundToI(RMs ( RNA())), Set(toSortedQId("x")(5,11) ) ),

        ( "x min y"             , x min y                      , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24)  ) ),
        ( "x max y"             , x max y                      , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24)  ) )

    )
    //  format: ON

    for ( ( e, t, tdef ) ← theTerms1 ) {
        val result = tdef.map( x ⇒ show( x ) )
        test( s"Check the typeDefs built with for term $e -- Should be $result" ) {
            t.typeDefs shouldBe tdef
        }
    }

    //  Comparison terms

    //  format: OFF
    val theTerms2 = Table[ String, TypedTerm[ BoolTerm, Term ], Set[ SortedQId ] ](
        ( "Expression"          , "TypedTerm"                  , "typeDefs" ),

        ( "x <= y"              , x <= y                        , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24)  ) ),
        ( "x <  y"              , x <= y                        , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24)  ) ),
        ( "x >= y"              , x <= y                        , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24)  ) ),
        ( "x > y"               , x <= y                        , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24)  ) ),
        ( "x fpeq y"            , x fpeq y                      , Set(toSortedQId("x")(5,11), toSortedQId("y")(8,24)  ) ),
        ( " x is isNormal"      , x.isNormal                    , Set(toSortedQId("x")(5,11))),
        ( " x is isSubnormal"   , x.isSubNormal                 , Set(toSortedQId("x")(5,11))),
        ( " x is isZero"        , x.isZero                      , Set(toSortedQId("x")(5,11))),
        ( " x is isInfinite"    , x.isInfinite                  , Set(toSortedQId("x")(5,11))),
        ( " x is NaN"           , x.isNaN                       , Set(toSortedQId("x")(5,11))),
        ( " x is isNegative"    , x.isNegative                  , Set(toSortedQId("x")(5,11))),
        ( " x is isPositive"    , x.isPositive                  , Set(toSortedQId("x")(5,11)))

    )
    //  format: ON

    for ( ( e, t, tdef ) ← theTerms2 ) {
        val result = tdef.map( x ⇒ show( x ) )
        test( s"Check the typeDefs built with for term $e -- Should be $result" ) {
            t.typeDefs shouldBe tdef
        }
    }
}

class FPBitVectorsFactoryTermTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with FPBitVectors with BitVectors {

    override def suiteName = "Check the termDef of FPBitVector terms"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax._
    import typedterms.TypedTerm
    import parser.SMTLIB2PrettyPrinter.show

    val x = FPBVs( "x", 8, 16 )
    val y = "y".asFPBV( 5, 11 )
    val y1 = y indexed 1

    val pZeroDouble : Double = +0.0e0
    val mZeroDouble : Double = -0.0e0
    val pZeroFloat : Float = +0.0.toFloat
    val mZeroFloat : Float = -0.0.toFloat

    //  Use an implicit rounding
    implicit val rne = RMs ( RNE () )

    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ FPBVTerm, Term ], Term ](
        ( "Expression"          , "TypedTerm"                   , "termDefs" ),
        ( "- 0.10^4"            , FPBVs(BVs("#b1"), BVs("#b10"), BVs("#b100") )  ,
                                                                    FPBVvalueTerm(
                                                                        ConstantTerm(BinLit("1")),
                                                                        ConstantTerm(BinLit("10")),
                                                                        ConstantTerm(BinLit("100"))
                                                                    )
        ),
        ( "Double 2.13"         , 2.13.asFloat64               , FPBVFromFPBVRealSInt(
                                                                ToFPBV("11","53"),
                                                                ConstantTerm(RoundingModeLit(RNE())),
                                                                ConstantTerm(DecLit("2", "13")
                                                            )
                                                        )
        ),

        ( "Double +oo"          , (1.0/0.0).asFloat64       , ConstantTerm(FPPlusInfinity(11,53))
        ),
        ( "Double -oo"          , (-1.0/0.0).asFloat64       , ConstantTerm(FPMinusInfinity(11,53))
        ),

        ( "+zero "              , (2.0/(1.0/0.0)).asFloat64  , ConstantTerm(FPBVPlusZero(11,53))
        ),

        ( "+0.0000000e0 Double "      , pZeroDouble.asFloat64  ,     ConstantTerm(FPBVPlusZero(11,53))
        ),
        ( "-0.0000000e0 Double "      , mZeroDouble.asFloat64  ,     ConstantTerm(FPBVMinusZero(11,53))
        ),

        ( "+0.0000000e0 Float "      , pZeroFloat.asFloat32  ,     ConstantTerm(FPBVPlusZero(8,24))
        ),
        ( "-0.0000000e0 Float "      , mZeroFloat.asFloat32  ,     ConstantTerm(FPBVMinusZero(8,24))
        ),


        ( "Hex and binary"            , FPBVs(BVs("#b1"), BVs("#xa3b1"), BVs("#b10100101"))  ,
                                                                    FPBVvalueTerm(
                                                                        ConstantTerm(BinLit("1")),
                                                                        ConstantTerm(HexaLit("a3b1")),
                                                                        ConstantTerm(BinLit("10100101"))
                                                                    )
        ),
        ( "FPBV from BVs 1"            , FPBVs(BVs("#b1"), BVs(12,16), BVs("#x120"))  ,
                                                                    FPBVvalueTerm(
                                                                        ConstantTerm(BinLit("1")),
                                                                        ConstantTerm(DecBVLit(BVvalue("12"),"16")),
                                                                        ConstantTerm(HexaLit("120"))
                                                                    )
        ),
        ( "FPBV from BVs 2"            , FPBVs(BVs("#b1"), BVs("#x120"), BVs(12,16) )  ,
                                                                    FPBVvalueTerm(
                                                                        ConstantTerm(BinLit("1")),
                                                                        ConstantTerm(HexaLit("120")),
                                                                        ConstantTerm(DecBVLit(BVvalue("12"),"16"))
                                                                    )
        ),
        ( "FPBV from BVs 3"            , FPBVs(BVs("#b1"), BVs("#b110"), BVs(12,16) )  ,
                                                                    FPBVvalueTerm(
                                                                        ConstantTerm(BinLit("1")),
                                                                        ConstantTerm(BinLit("110")),
                                                                        ConstantTerm(DecBVLit(BVvalue("12"),"16"))
                                                                    )
        ),
        ( "FPBV from BVs 4"            , FPBVs(BVs("#b1"),  BVs(12,16), BVs("#b110") )  ,
                                                                    FPBVvalueTerm(
                                                                        ConstantTerm(BinLit("1")),
                                                                        ConstantTerm(DecBVLit(BVvalue("12"),"16")),
                                                                        ConstantTerm(BinLit("110"))
                                                                    )
        ),

        ( "FPBV from FPBVs"     , FPBVs(
                                        BVs("#b0"), BVs(2,4),
                                        BVs(16,32)).toFPBV(4, 12) , FPBVFromFPBVRealSInt(
                                                                        ToFPBV("4","12"),
                                                                        ConstantTerm(RoundingModeLit(RNE())),
                                                                            FPBVvalueTerm(
                                                                                ConstantTerm(BinLit("0")),
                                                                                ConstantTerm(DecBVLit(BVvalue("2"),"4")),
                                                                                ConstantTerm(DecBVLit(BVvalue("16"),"32")))
                                                                            )
        ),

        ( "+infinity 2 6"       , PlusInfinity( 2,6 )           , ConstantTerm(
                                                                    FPPlusInfinity( 2, 6)
                                                                  )
        ),
        ( "-infinity 4 11"      , MinusInfinity( 4,11 )         , ConstantTerm(
                                                                    FPMinusInfinity( 4, 11)
                                                                  )
        ),
        ( "+zero 2 6"           , PlusZero( 2,6 )               , ConstantTerm(
                                                                    FPBVPlusZero( 2, 6)
                                                                  )
        ),
        ( "-zero 2 6"           , MinusZero( 2,6 )              , ConstantTerm(
                                                                    FPBVMinusZero( 2, 6)
                                                                  )
        ),
        ( "NaN 2 6"             , NaN( 2,6 )                    , ConstantTerm(FPBVNaN( 2, 6)) ),
        ( "-NaN 2 6"             , -NaN( 2,6 )                  , FPBVNegTerm(ConstantTerm(FPBVNaN( 2, 6)) )),

        ( "x"                   , x                             , QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(SSymbol("x"))
                                                                    )
                                                                 )
        ),
        ( "y1"                  , y1                            , QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(ISymbol("y",1))
                                                                    )
                                                                  )
        ),

        ( "x Float16"           , "x".asFloat16                , QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(SSymbol("x"))
                                                                    )
                                                                  )
        ),

        ( "x Float32"           , "x".asFloat32                , QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(SSymbol("x"))
                                                                    )
                                                                  )
        ),

        ( "x Float64"           , "x".asFloat64                , QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(SSymbol("x"))
                                                                    )
                                                                  )
        ),

        ( "x Float128"           , "x".asFloat128                , QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(SSymbol("x"))
                                                                    )
                                                                  )
        ),

        ( "abs(x)"              , x.abs                        , FPBVAbsTerm( QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(SSymbol("x"))
                                                                    )
                                                                )
                                                            )
        ),

        ( "-x"                  , - x                          , FPBVNegTerm( QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(SSymbol("x"))
                                                                    )
                                                                )
                                                            )
        ),

        //  Use of implicit rounding first (+ can use an implicit rounding)
        ( "x + y"               , x + y                        , FPBVAddTerm(
                                                                    ConstantTerm(RoundingModeLit(RNE())),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        ),
        ( "x - y"               , x - y                        , FPBVSubTerm(
                                                                    ConstantTerm(RoundingModeLit(RNE())),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        ),
        ( "x * y"               , x * y                        , FPBVMulTerm(
                                                                    ConstantTerm(RoundingModeLit(RNE())),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        ),
        ( "x / y"               , x / y                        , FPBVDivTerm(
                                                                    ConstantTerm(RoundingModeLit(RNE())),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        ),
        //  Explicit rounding
        ( "x + y(RNA)"               , x.+(y)(RMs(RNA())) , FPBVAddTerm(
                                                                    ConstantTerm(RoundingModeLit(RNA())),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        ),

        ( "x - y(RTP)"               , x.-(y)(RMs(RTP())), FPBVSubTerm(
                                                                    ConstantTerm(RoundingModeLit(RTP())),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        ),

        ( "x * y(RTZ)"               , x.*(y)(RMs(RTZ())) , FPBVMulTerm(
                                                                    ConstantTerm(RoundingModeLit(RTZ())),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        ),

        ( "x / y(RTZ)"               , x./(y)(RMs(RTZ()))             , FPBVDivTerm(
                                                                    ConstantTerm(RoundingModeLit(RTZ())),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        ),

        ( "x % y"               ,x % y                       , FPBVRemTerm(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        ),

        ( "(x * y) + z (fma)"       , x.*+(y,"z".asFloat128)       , FPBVFmaTerm(
                                                                    ConstantTerm(RoundingModeLit(RNE())),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("z"))
                                                                        )
                                                                    )
                                                                )
        ),

        ( "sqrt(x)"              , x.sqrt                        , FPBVSqrtTerm(
                                                                    ConstantTerm(RoundingModeLit(RNE())),
                                                                    QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(SSymbol("x"))
                                                                    )
                                                                )
                                                            )
        ),
        ( "roundToI(x)"              , x.roundToI(RMs(RNA())) , FPBVRoundToITerm(
                                                                    ConstantTerm(RoundingModeLit(RNA())),
                                                                    QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(SSymbol("x"))
                                                                    )
                                                                )
                                                            )
        ),
        //
        ( "x min y"               ,x min y                       , FPBVMinTerm(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        ),
        ( "x max y"               ,x max y                       , FPBVMaxTerm(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        )
        //
    )
    //  format: ON

    for ( ( e, t, tdef ) ← theTerms1 ) {
        test( s"Check the termDef built with for term $e -- Should be $tdef" ) {
            t.termDef shouldBe tdef
        }
    }


    //  format: OFF
    val theTerms2 = Table[ String, TypedTerm[ RMFPBVTerm, Term ], Term ](
        ( "Expression"   , "TypedTerm"      , "termDefs" ),
        ( "RMs RNE"      , RMs(RNE())       , ConstantTerm(RoundingModeLit(RNE()))),
        ( "RMs RNA"      , RMs(RNA())       , ConstantTerm(RoundingModeLit(RNA()))),
        ( "RMs RTP"      , RMs(RTP())       , ConstantTerm(RoundingModeLit(RTP()))),
        ( "RMs RTN"      , RMs(RTN())       , ConstantTerm(RoundingModeLit(RTN()))),
        ( "RMs RTZ"      , RMs(RTZ())       , ConstantTerm(RoundingModeLit(RTZ()))),
        ( "RMs RNeven"   , RMs(RNEven())    , ConstantTerm(RoundingModeLit(RNEven()))),
        ( "RMs RNAway"   , RMs(RNAway())    , ConstantTerm(RoundingModeLit(RNAway()))),
        ( "RMs RTPos"    , RMs(RTPos())     , ConstantTerm(RoundingModeLit(RTPos()))),
        ( "RMs RTNeg"    , RMs(RTNeg())     , ConstantTerm(RoundingModeLit(RTNeg()))),
        ( "RMs RTZero"   , RMs(RTZero())    , ConstantTerm(RoundingModeLit(RTZero())))

    )

    //  format: ON

    for ( ( e, t, tdef ) ← theTerms2 ) {
        test( s"Check the termDef built with for term $e -- Should be $tdef" ) {
            t.termDef shouldBe tdef
        }
    }


    //  format: OFF
    val theTerms3 = Table[ String, TypedTerm[ BoolTerm, Term ], Term ](
        ( "Expression"          , "TypedTerm"                  , "typeDefs" ),

        ( "x <= y"              , x <= y                        , FPBVLeqTerm(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        ),
        ( "x <  y"              , x < y                        , FPBVLtTerm(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        ),
        ( "x >= y"              , x >= y                        , FPBVGeqTerm(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        ),
        ( "x > y"               , x > y                        , FPBVGtTerm(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        ),
        ( "x fpeq y"            , x fpeq y                      , FPBVEqTerm(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                    ),
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    )
                                                                )
        ),
        ( " x is isNormal"      , x.isNormal                    , FPBVisNormal(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                     )
                                                                    )
        ),
        ( " x is isSubnormal"   , x.isSubNormal                 , FPBVisSubnormal(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                     )
                                                                    )
        ),
        ( " x is isZero"        , x.isZero                      , FPBVisZero(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                     )
                                                                    )
        ),
        ( " x is isInfinite"    , x.isInfinite                  , FPBVisInfinite(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                     )
                                                                    )
        ),
        ( " x is NaN"           , x.isNaN                       , FPBVisNaN(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                     )
                                                                    )
        ),
        ( " x is isNegative"    , x.isNegative                  , FPBVisNegative(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                     )
                                                                    )
        ),
        ( " x is isPositive"    , x.isPositive                  , FPBVisPositive(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("x"))
                                                                        )
                                                                     )
                                                                    )
        )

    )
    //  format: ON

    for ( ( e, t, tdef ) ← theTerms3 ) {
        test( s"Check the typeDefs built with for term $e -- Should be $tdef" ) {
            t.termDef shouldBe tdef
        }
    }

}

class FPBitVectorsFactoryShowTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with RealArithmetics with BitVectors with FPBitVectors {

    override def suiteName = "Check the pretty printing for FPBitVector terms"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax.{
        Term,
        QualifiedId,
        SortedQId,
        BitVectorSort,
        ConstantTerm,
        RoundingModeLit,
        RNE,
        RNA,
        RTP
    }
    import parser.SMTLIB2PrettyPrinter.show
    import typedterms.TypedTerm

    val x = FPBVs( "x", 5, 16 )
    val y = FPBVs( "y", 11, 21 )
    val y1 = y indexed 1

    implicit val r = RMs ( RNE () )

    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ _, Term ], String ](
        ( "Expression"          , "TypedTerm"                         , "solver string" ),

        ( "x FP 3 16"           , FPBVs( "x", 3, 16)                  , "x"),
        ( "fp 1 #b0 #b0101"     , FPBVs( BVs("#b1"), BVs("#b0"), BVs("#b0101")) , "(fp #b1 #b0 #b0101)"),
        ( "fp 1 #x1f #b0101"    , FPBVs( BVs("#b1"), BVs("#x1f"), BVs("#b0101"))      , "(fp #b1 #x1f #b0101)"),
        ( "fp 1 BV() BV()"      , FPBVs( BVs("#b0"), BVs(2,4), BVs(16,32)) , "(fp #b0 (_ bv2 4) (_ bv16 32))"),

        ( "bitStringBV to FPBV" , BVs(3, 16).bitStringToFPBV(4,12)    , "((_ to_fp 4 12) (_ bv3 16))"),
        ( "signedBV to FPBV"    , BVs(3, 16).signedToFPBV(4,12)       , "((_ to_fp 4 12) RNE (_ bv3 16))"),
        ( "unsignedBV to FPBV"  , BVs(3, 16).unSignedToFPBV(4,12)       , "((_ to_fp_unsigned 4 12) RNE (_ bv3 16))"),
        ( "FPBV from Reals"     , Reals(12.2344).toFPBV(4,12)         , "((_ to_fp 4 12) RNE 12.2344)"),
        ( "FPBV from FPBVs"     , FPBVs(
                                        BVs("#b0"), BVs(2,4),
                                        BVs(16,32)).toFPBV(4, 12)     , "((_ to_fp 4 12) RNE (fp #b0 (_ bv2 4) (_ bv16 32)))"),

        ( "FPBV to Reals"       , FPBVs( "x", 3, 16).toReal           , "(fp.to_real x)"),
        ( "FPBV to uBV"         , FPBVs( "x", 3, 16).toUBV(19)        , "((_ fp.to_ubv 19) RNE x)"),
        ( "FPBV to sBV"         , FPBVs( "x", 3, 16).toSBV(19)        , "((_ fp.to_sbv 19) RNE x)"),

        ( "+infinity 2 6"       , PlusInfinity( 2,6 )                 , "(_ +oo 2 6)"),
        ( "-infinity 4 11"      , MinusInfinity( 4,11 )               , "(_ -oo 4 11)"),
        ( "+zero 2 6"           , PlusZero( 2,6 )                     , "(_ +zero 2 6)"),
        ( "-zero 2 6"           , MinusZero( 2,6 )                    , "(_ -zero 2 6)"),
        ( "NaN 2 6"             , NaN( 2,6 )                          , "(_ NaN 2 6)"),
        ( "NaN 2 6"             , -NaN( 2,6 )                          , "(fp.neg (_ NaN 2 6))"),

        //
        ( "abs(x)"              , x.abs                               , "(fp.abs x)"),
        ( "-x"                  , - x                                 , "(fp.neg x)"),

        //  Use of implicit rounding first (+ can use an implicit rounding)
        ( "x + y"               , x + y                               , "(fp.add RNE x y)"),
        ( "x - y"               , x - y                               , "(fp.sub RNE x y)"),

        //  Explicit rounding
        ( "x + y(RNA)"          , x.+(y)(RMs(RNA()))    , "(fp.add RNA x y)"),
        ( "x - y(RTP)"          , x.-(y)(RMs(RTP()))    , "(fp.sub RTP x y)"),
        ( "x - y + z (16)"      , x - y + "z".asFloat16               , "(fp.add RNE (fp.sub RNE x y) z)"),

        //
        ( "x * y(RNA)"          , x.*(y)(RMs(RNA()))    , "(fp.mul RNA x y)"),
        ( "x * y "              , x * y                               , "(fp.mul RNE x y)"),
        ( "x / y(RNA)"          , x./(y)(RMs(RNA()))    , "(fp.div RNA x y)"),
        ( "x / y"               , x / y                               , "(fp.div RNE x y)"),
        //
        ( "x % y"               , x % y                               , "(fp.rem x y)"),
        //
        ( "(x * y) + z (fma)"   , x.*+(y,"z".asFloat128)              , "(fp.fma RNE x y z)"),
        //
        ( "sqrt(x)"             , x.sqrt                              , "(fp.sqrt RNE x)"),
        //
        ( "roundToI(x)"         , x.roundToI(RMs(RNA()) ), "(fp.roundToIntegral RNA x)"),
        //
        ( "x min y"             , x min y                             , "(fp.min x y)"),
        ( "x max y"             , x max y                             , "(fp.max x y)")
    )
    //  format: ON

    for ( ( e, t, tdef ) ← theTerms1 ) {
        test( s"Check the typeDefs built with for term $e -- Should be $tdef" ) {
            show( t.termDef ) shouldBe tdef
        }
    }
}
