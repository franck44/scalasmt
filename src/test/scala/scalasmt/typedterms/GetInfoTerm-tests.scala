/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package typedterms
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ Core, IntegerArithmetics }
import interpreters.Resources

class GetInfoCmdTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Resources {

    override def suiteName = "get-info command test suite"

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2Syntax.{
        Raw,
        CheckSatResponses,
        GetInfoResponses,
        GetInfoResponseSuccess,
        GetInfoResponseError
    }
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.show
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.{ SolverConfig, SMTInit }
    import configurations.SMTLogics.QF_LIA
    import parser.SMTLIB2Parser
    import parser.Implicits._

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( QF_LIA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA )

    val parseGetInfoResponse = SMTLIB2Parser[ GetInfoResponses ]

    //  <get-info> test strings
    //  format: OFF
    val getInfoTests = Table[ String, SolverConfig => Boolean](
        ( "input string?"           , "is get-info : yes/no" ) ,
        ("(get-info :all-statistics)", { x  => !(x.name == "MathSat")  }),
        ("(get-info :assertions-stack-levels)", _ => false),
        ("(get-info :authors)", _ => true),
        ("(get-info :error-behaviour)",{ x  => (x.name == "MathSat")  }),
        ("(get-info :reason-unknown)", { x  => (x.name == "Z3-4.8.6")  }),
        ("(get-info :name)", _ => true),
        ("(get-info :version)", _ => true),

        ("(get-info :mykwd)", _ => false),

        ( """|
             | (
             |  get-info
             |    :authors
             |      )
             |
             |""".stripMargin     , _ => true ),
         ( """|
              | (
              |  get-inf
              |    :authors
              |      )
              |
              |""".stripMargin     , _ => false ),
          ( """|
               | (
               |  get-info
               |    x
               |      )
               |
               |""".stripMargin     , _ => false )
    )
    //  format: ON

    for ( s ← theSolvers; ( xt, r ) ← getInfoTests ) {

        test( s"[${s.name}] configured with ${initSeq.show} with $xt -- ${if ( r( s ) ) "should succeed" else "should fail"}" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        eval ( Seq( Raw( xt ) ) )
                    }
            } match {

                case Success( solverResponse ) ⇒

                    logger.info( "Solver returned : {}", solverResponse )
                    parseGetInfoResponse ( solverResponse ) should matchPattern {

                        case Success( GetInfoResponseSuccess( _ ) ) if r( s ) ⇒
                        case Success( GetInfoResponseError( _ ) ) if !r( s )  ⇒
                        case Failure( _ ) if !r( s )                          ⇒
                    }

                case Failure( e ) if !r( s ) ⇒
            }
        }
    }
}
