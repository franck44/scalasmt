/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package typedterms
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ IntegerArithmetics, Core }
import interpreters.Resources

class SimpleLIATests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Commands with Resources {

    override def suiteName = "QF_LIA simple check-sat test suite"

    import scala.util.Success
    import parser.SMTLIB2Syntax.{ Term, Sat, SatResponses }
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.show
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.SMTInit

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic ( QF_LIA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA )

    val theTerms = Table[ Seq[ TypedTerm[ BoolTerm, Term ] ], SatResponses ](
        ( "Terms", "Expected response" ),

        //  1 < 2 should be Sat
        (
            Seq( Ints ( 1 ) < Ints ( 2 ) ),
            Sat() ),

        //  1 < 2 and 2 < 3, Sat
        (
            Seq( Ints ( 1 ) < Ints ( 2 ), Ints ( 2 ) <= Ints ( 3 ) ),
            Sat() ),

        //  x < 2 and 1 < x should be Sat
        (
            Seq(
                Ints( "x" ) < Ints( 2 ),
                Ints( 1 ) <= Ints( "x" ) ),
                Sat() ),

        //  x == -4, should be Sat
        (
            Seq(
                Ints( "x" ) === Ints( -4 ) ),
                Sat() ) )

    for ( s ← theSolvers; ( xt, r ) ← theTerms ) {

        test( s"[${s.name}] configured with ${initSeq.show} to check sat for terms ${xt.map( x ⇒ show( x.termDef ) )} -- should be SAT" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( xt : _* )
                    }
            } shouldBe Success( Sat() )
        }
    }

}

/**
 * We are using the implicits and operators to build complex
 * TypedTerms and check sat
 */
class ArithmeticOperatorsTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Commands with Resources {

    override def suiteName = "QF_LIA simple terms check-sat"

    import scala.util.Success
    import parser.SMTLIB2Syntax.{ Sat, UnSat }
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.SMTInit

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic ( QF_LIA ) ) : _* )
    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    for ( s ← theSolvers ) {

        test( s"[${s.name}] Addition: x + 1 = 3  -- should be SAT" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        isSat( x + 1 === 3 )
                    }
            } shouldBe Success( Sat() )
        }

        test( s"[${s.name}] 3-ary Addition: x + 1 + y = 3  -- should be SAT" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        isSat( x + 1 + y === 3 )
                    }
            } shouldBe Success( Sat() )
        }

        test( s"[${s.name}] Addition with 3 operands:  x + 1 + y = 3  -- should be SAT" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        isSat( x + 1 + y === 3 )
                    }
            } shouldBe Success( Sat() )
        }

        test( s"[${s.name}] Subtraction:  x - 1 = 3  -- should be SAT" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        isSat( x - 1 === 3 )
                    }
            } shouldBe Success( Sat() )
        }

        test( s"[${s.name}] Unary Subtraction:  - y > 3 and x + y == -1 -- should be SAT" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        isSat( ( -y > 3 ) & ( x + y === -1 ) )
                    }
            } shouldBe Success( Sat() )
        }

        test( s"[${s.name}] Unsat test: x < 1 and x >= 2 -- should be UNSAT" ) {
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        isSat( x < 1 & x >= 2 )
                    }
            } shouldBe Success( UnSat() )
        }

        test( s"[${s.name}] Multiplication test: x * 2 < 1 -- should be SAT" ) {
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        isSat( x * 2 < 1 )
                    }
            } shouldBe Success( Sat() )
        }

        test( s"[${s.name}] Unsat test: x == 2,  y > 2, y == x + 1, z == 3, z == x should be UNSAT" ) {
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        isSat(
                            x === 2,
                            y >= 2,
                            y === x + 1,
                            z === 3,
                            z === x )
                    }
            } shouldBe Success( UnSat() )
        }

        test( s"[${s.name}] Sat test: i + j + 1 == 2 -- should be SAT" ) {
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        val i = Ints( "i" )
                        val j = Ints( "j" )
                        isSat(
                            i + j + 1 === 2 )
                    }
            } shouldBe Success( Sat() )
        }
    }
}

class AssertBoolTermTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Commands with Resources {

    import scala.util.Success
    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat, SatResponses }
    import theories.BoolTerm
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.SMTInit

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic ( QF_LIA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA )

    val theTerms = Table[ TypedTerm[ BoolTerm, Term ], SatResponses ](
        ( "Terms", "Response" ),
        ( True(), Sat() ),
        ( False(), UnSat() ) )

    for ( s ← theSolvers; ( t, r ) ← theTerms ) {

        test( s"[${s.name}]  to check sat for terms ${show( t.termDef )} -- should be ${show( r )}" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        isSat( t )
                    }
            } shouldBe Success ( r )
        }
    }
}
