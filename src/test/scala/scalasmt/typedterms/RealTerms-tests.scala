/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package typedterms
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources
import theories.{ Core, RealTerm, IntegerArithmetics, RealArithmetics }

class RealComparisonTests extends FunSuite with TableDrivenPropertyChecks with Matchers with Core with RealArithmetics with Commands with Resources {

    override def suiteName = "Check the termDef built using the comparison operators on RealSort TypedTerms"

    import parser.SMTLIB2PrettyPrinter.show
    import theories.BoolTerm
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm

    val x = Reals( "x" )
    val y = Reals( "y" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ], String ](
        ( "expression" , "TypedTerm"                 , "Solver string" )      ,
        ( "x <= y"     , x <= y                      , "(<= x y)" )         ,
        ( "x < y"      , x < y                       , "(< x y)" )          ,
        ( "x > y"      , x > y                       , "(> x y)" )          ,
        ( "x >= y"     , x >= y                      , "(>= x y)" )         ,
        ( "x == y"     , x === y                     , "(= x y)" )          ,
        ( "x != y"     , x =/= y                     , "(distinct x y)" )   ,
        ( "x != 8.5"   , x =/= 8.5                   , "(distinct x 8.5)" ) ,
        ( "0 < 1.5"    , Reals( 0.0 ) < Reals( 1.5 ) , "(< 0.0 1.5)" )
    )
    //  format: ON

    for ( ( x, t, s ) ← theTerms ) {
        test( s"Check string produced by: $x -- Should be $s" ) {
            show( t.termDef ) shouldBe s
        }
    }
}

class LinearRealArithmeticTests extends FunSuite with TableDrivenPropertyChecks with Matchers with Core with RealArithmetics with Commands with Resources {

    override def suiteName = "Check the termDef and solvers' compatibility for LRA terms"

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm

    val x = Reals( "x" )
    val y = Reals( "y" )
    val z = Reals( "z" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ RealTerm, Term ], String ](
        ( "expression"            , "TypedTerm"                 , "Solver string" )            ,
        ( "real number 0.0"       , Reals( 0.0 )                , "0.0" )                       ,
        ( "real number 1.5"       , Reals( 1.5 )                , "1.5" )                     ,
        ( "real number 0.0000001" , Reals( 0.0000001 )          , "0.00000010" ),
        ( "1.0 + 2.0"             , Reals( 1.0 ) + Reals( 2.0 ) , "(+ 1.0 2.0)" )                ,
        ( "3 + 2.0"               , Reals( 3 ) + Reals( 2.0 )   , "(+ 3.0 2.0)" )                ,
        ( "integer variable x"    , x                           , "x" )                       ,
        ( "-x"                    , -x                          , "(- x)" )                  ,
        ( "-x_1"                  , -(x indexed 1)              , "(- x@1)" )                ,
        ( "x + y"                 , x + y                       , "(+ x y)" )                ,
        ( "x + y + z"             , x + y + z                   , "(+ (+ x y) z)" )         ,
        ( "x + y_3 + z"           , x + (y indexed 3) + z       , "(+ (+ x y@3) z)" )       ,
        ( "x + y + z + 1.5"       , x + y + z + 1.5             , "(+ (+ (+ x y) z) 1.5)") ,
        ( "x - y + z"             , x - y + z                   , "(+ (- x y) z)" )         ,
        ( "x - ( x + z )"         , x - ( y + z )               , "(- x (+ y z))" )
    )
    //  format: ON

    //  check the strings produced by the prettyPrinter
    for ( ( x, t, s ) ← theTerms ) {
        test( s"Real arithmetic. Check string produced by: $x -- Should be $s" ) {
            show( t.termDef ) shouldBe s
        }
    }
}

class RealFactoryTypeTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with RealArithmetics with Commands with Resources {

    override def suiteName = "Check the typeDefs of real arithmetic terms"

    import parser.SMTLIB2Syntax.{
        Term,
        QualifiedId,
        SortedQId,
        SSymbol,
        ISymbol,
        SymbolId,
        RealSort
    }
    import parser.SMTLIB2PrettyPrinter.format
    import typedterms.TypedTerm

    //  create a solver SortedQId, type Int, from a string
    def toSortedQId( name : String ) : SortedQId = SortedQId( SymbolId( SSymbol( name ) ), RealSort() )
    def toIndexedSortedQId( name : String, index : Int ) : SortedQId = SortedQId( SymbolId( ISymbol( name, index ) ), RealSort() )

    val x = Reals( "x" )
    val y = Reals( "y" )

    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ RealTerm, Term ], Set[ SortedQId ] ](
        ( "Expression"  , "TypedTerm"          , "typeDefs" )                           ,
        ( "0"           , Reals( 0 )           , Set() )                                ,
        ( "-1.0"        , Reals( -1.0 )        , Set() )                                ,
        ( "x"           , x                    , Set( "x" ).map( toSortedQId ) )        ,
        ( "x + y"       , x + y                , Set( "x" , "y" ).map( toSortedQId ) )  ,
        ( "x - y_1"     , x - (y indexed 1)    , Set( toSortedQId("x"), toIndexedSortedQId("y",1) ) ),
        ( "x + y + 0.0" , x + y + Reals( 0.0 ) , Set( "x" , "y" ).map( toSortedQId ) )  ,
        ( "0.0 + x + y" , Reals( 0.0 ) + x + y , Set( "x" , "y" ).map( toSortedQId ) )
    )
    //  format: OFF

    import theories.BoolTerm

    //  format: OFF
    val theTerms2 = Table[ String, TypedTerm[ BoolTerm, Term ], Set[ SortedQId ] ](
        ( "Expression" , "TypedTerm"                 , "typeDefs" )                    ,

        ( "1.0 < y"    , Reals( 1.0 ) < y            , Set( "y" ).map( toSortedQId ) ) ,
        ( "1.0 <= y"   , Reals( 1.0 ) <= y           , Set( "y" ).map( toSortedQId ) ) ,
        ( "1.0 > y"    , Reals( 1.0 ) > y            , Set( "y" ).map( toSortedQId ) ) ,
        ( "1.0 >= y"   , Reals( 1.0 ) >= y           , Set( "y" ).map( toSortedQId ) ) ,
        ( "1.0 === y"  , Reals( 1.0 ) === y          , Set( "y" ).map( toSortedQId ) ) ,

        ( "y < 1.0"    , y < Reals( 1.0 )            , Set( "y" ).map( toSortedQId ) ) ,
        ( "y <= 1.0"   , y <= Reals( 1.0 )           , Set( "y" ).map( toSortedQId ) ) ,
        ( "y > 1.0"    , y > Reals( 1.0 )            , Set( "y" ).map( toSortedQId ) ) ,
        ( "y >= 1.0"   , y >= Reals( 1.0 )           , Set( "y" ).map( toSortedQId ) ) ,
        ( "y === 1.0"  , y === Reals( 1.0 )          , Set( "y" ).map( toSortedQId ) ) ,

        ( "x < y"      , x < y                       , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x <= y"     , x <= y                      , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x > y"      , x > y                       , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x > y_3"    , x > (y indexed 4)           , Set( toSortedQId("x") , toIndexedSortedQId("y",4)) ) ,
        ( "x >= y"     , x >= y                      , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x === y"    , x === y                     , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "0 < 1.0"    , Reals( 0.0 ) < Reals( 1.0 ) , Set() )
    )
    //  format: ON

    for ( ( e, t, tdef ) ← theTerms1 ++ theTerms2 ) {
        val result = tdef.map( x ⇒ format( x ).layout )
        test( s"Check the typeDefs built with for term $e -- Should be $result" ) {
            t.typeDefs shouldBe tdef
        }
    }
}

class NonLinearRealArithmeticTests extends FunSuite with TableDrivenPropertyChecks with Matchers with Core with RealArithmetics with IntegerArithmetics
    with Commands
    with Resources {

    override def suiteName = "Check the termDef and solvers' compatibility for NIRA terms"

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm

    val x = Reals( "x" )
    val y = Reals( "y" )
    val z = Reals( "z" )

    val i : TypedTerm[ RealTerm, Term ] = Ints( "i" )
    val j : TypedTerm[ RealTerm, Term ] = Ints( "j" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ RealTerm, Term ], String ](
        ( "expression"                  , "TypedTerm"            , "Solver string" )            ,
        ( "real number 0.0 + Integer 1" , Reals( 0.0 ) + Ints(1) , "(+ 0.0 1)" )              ,
        ( "x + i"                       , x + i                  , "(+ x i)" )                ,
        ( "i + x"                       , i + x                  , "(+ i x)" )                ,
        ( "x + i + y"                   , x + i + y              , "(+ (+ x i) y)" )         ,

        ( "x + y + j + 1.5"             , x + y + j + 1.5        , "(+ (+ (+ x y) j) 1.5)") ,
        ( "x - j + i"                   , x - j + i              , "(+ (- x j) i)" )         ,
        ( "x - ( i + z )"               , x - ( i + z )          , "(- x (+ i z))" )
    )
    //  format: ON

    //  check the strings produced by the prettyPrinter
    for ( ( x, t, s ) ← theTerms ) {
        test( s"Real arithmetic. Check string produced by: $x -- Should be $s" ) {
            show( t.termDef ) shouldBe s
        }
    }

}
