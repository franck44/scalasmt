/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

class FPSimpleBitVectorTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with FPBitVectors
    with BitVectors
    with RealArithmetics
    with Commands
    with Resources {

    override def suiteName = "Check-sat for FPBitVector terms"

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{
        Term,
        Sat,
        UnSat,
        UnKnown,
        SatResponses,
        RNE,
        RoundingMode
    }
    import typedterms.TypedTerm

    val x = FPBVs( "x", 5, 11 )
    val y = FPBVs( "x", 5, 11 )
    val rm = RMs( "rm" )

    //  an implicit roundingMode
    implicit val r = RMs( RNE() )

    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ BoolTerm, Term ], SatResponses ](
        ( "expression"          , "TypedTerm"                               , "Response" ),
        ( "bitvector 1"         , x fpeq BVs( "#x0001" ).bitStringToFPBV(5,11)       , Sat() ),
        ( "x + y = 2.219"       , (x + y) fpeq Reals(2.219).toFPBV(5,11)    , Sat() ),
        ( "x +(rm?) y = 2.219"  , (x.+(y)(rm)) fpeq Reals(2.219).toFPBV(5,11), Sat() ),
        //  assigning y to x does not imply x fpeq y (x and y can be NaN)
        ( " x fpeq y"           , x === y & !(x fpeq y)                     , Sat() )
    )

    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_FPBV
    import configurations.AppConfig.config
    import configurations.SMTInit
    import scala.concurrent.duration._
    //  Solvers to be included in the tests. Should support QF_BV
    val theSolvers = Table(
        "Solver",
        config.filter( c ⇒ c.enabled && c.supportsLogic( QF_FPBV ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_FPBV )

    for ( s ← theSolvers ) {
        forAll( theTerms1 ) {
            ( x : String, t : TypedTerm[ BoolTerm, Term ], r : SatResponses ) ⇒
                test( s"[${s.name}] configured with ${initSeq.show} check-sat $x i.e. fpbitvector term ${show( t.termDef )}  should be Sat" ) {

                    //  with using (monadic)
                    using( new SMTSolver( s, initSeq ) ) {
                        implicit withSolver ⇒
                            isSat( 20.seconds )( t )
                    } should matchPattern {
                        case Success( Sat() ) ⇒
                    }
                }
        }
    }
}

class GetValueFPBitVectorTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with BitVectors
    with FPBitVectors
    with RealArithmetics
    with Commands
    with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName = "Check-sat and get values for FPBitVector terms"

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.{
        RoundingMode,
        RNE,
        QIdTerm,
        SimpleQId,
        Term,
        Sat,
        UnSat,
        UnKnown,
        SatResponses
    }
    import typedterms.{ TypedTerm, Value }
    import scala.concurrent.duration._

    val x = FPBVs( "x", 5, 11 )
    val y = FPBVs( "y", 5, 11 )
    val z = FPBVs( "z", 8, 16 )
    val aBV = BVs( "aBV", 24 )
    val rm = RMs( "rm" )
    //  an implicit roundingMode
    implicit val r = RMs( RNE() )

    //  BVs operators are compatible only for same size BVs
    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ BoolTerm, Term ], SatResponses ](
        ( "expression"              , "TypedTerm"                                       , "Response" ),
        ( "bitvector 1"             , x fpeq BVs( "#x0001" ).bitStringToFPBV(5,11)      , Sat() ),
        ( "x + y = 2.219"           , (x + y) fpeq Reals(2.219).toFPBV(5,11)            , Sat() ),
        ( "x +(rm?) y = 2.219"      , (x.+(y)(rm)) fpeq Reals(2.219).toFPBV(5,11)       , Sat() ),

        //  Conversion from BV tp FPBV
        ( "bitvector 2, bitString conversion" , 
            BVs( 2, 64 ).bitStringToFPBV(11, 53) === 
                FPBVs(
                    BVs("#b0"),
                    BVs("#b00000000000"),
                    BVs("#b0000000000000000000000000000000000000000000000000010"))      , Sat() ),

        ( "bitvector 2, signed conversion" , 
            BVs( 2, 64 ).signedToFPBV(11, 53) === 
                FPBVs(
                    BVs("#b0"),
                    BVs("#b10000000000"),
                    BVs("#b0000000000000000000000000000000000000000000000000000"))      , Sat() ),

        ( "bitvector 2, unsigned conversion" , 
            BVs( 2, 16 ).signedToFPBV(5, 11) === 
                FPBVs(
                    BVs("#b0"),
                    BVs("#b10000"),
                    BVs("#b0000000000"))                                                , Sat() ),                                            

        //  Use hex for exponent and binary for significand
        ( "z = hex"  , 
            z fpeq FPBVs(
                     BVs("#b0"),
                     BVs("#x0f"),
                     BVs("#b101001010010101"))                                          , Sat() ),

        //  conversion from FPBVs to FPBVs
        ( "z.bitStringToFPBV(5,11) === 2.219", 
            z.toFPBV(5,11) fpeq Reals(2.219).toFPBV(5,11)                               , Sat()),

        //  use conversion to signed and unsigned BVs
        ( "z.bitStringToFPBV(5,11) === 2.219", 
            z.toSBV(24) ===  aBV                                                        , Sat()),
        ( "z.bitStringToFPBV(5,11) === 2.219", 
            z.toUBV(24) ===  aBV                                                        , Sat()),

        //  assigning y to x does not imply x fpeq y (x and y can be NaN)
        ( " x fpeq y" , 
            x === y & !(x fpeq y)                                                       , Sat() )
    )

    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_FPBV
    import configurations.SMTOptions.SMTProduceModels

    import configurations.AppConfig.config
    import configurations.SMTInit

    //  Solvers to be included in the tests. Should support QF_BV
    val theSolvers = Table(
        "Solver",
        config.filter( c ⇒ c.enabled && c.supportsLogic( QF_FPBV ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_FPBV, List( SMTProduceModels( true ) ) )

    for ( s ← theSolvers ) {
        forAll( theTerms1 ) {
            ( name : String, t : TypedTerm[ BoolTerm, Term ], r : SatResponses ) ⇒
                test( s"[${s.name}] configured with ${initSeq.show} check-sat for bivector term ${format( t.termDef ).layout}  can be established" ) {

                    //  with using (monadic)

                    val passed = using( new SMTSolver( s, initSeq ) ) {
                        implicit withSolver ⇒
                            isSat( 20.seconds )( t ) shouldBe Success( Sat () )

                            //  get variables declared on the stack
                            getDeclCmd() match {
                                case Failure( e ) ⇒ Failure( e )
                                case Success( xl ) ⇒
                                    logger.info( "getDeclCmd  returned {}", xl )
                                    //  make a TypedTerm from the declarations to extract a value
                                    val y = xl
                                        .map( { x ⇒ TypedTerm( Set( x ), QIdTerm( SimpleQId( x.id ) ) ) } )
                                        .map( getValue )
                                    //  y should be a List
                                    y shouldBe a[ List[ _ ] ]
                                    //  each element of y should be a Value
                                    y.foreach { v ⇒
                                        logger.info( s"Collected value is $v" )
                                        v should matchPattern {
                                            case Success( Value( _ ) ) ⇒
                                        }
                                    }
                                    Success( true )
                            }
                    } shouldBe Success( true )
                }
        }
    }
}
