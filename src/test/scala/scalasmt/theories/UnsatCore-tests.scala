/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import parser.Implicits._
import parser.SMTLIB2Syntax.Term
import typedterms.{ Commands, TypedTerm }
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

/**
 *  Unsat core for LIA terms
 */
class LiaUnsatCoreTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with IntegerArithmetics
    with Commands
    with Resources {

    override def suiteName = "Compute interpolants for LIA terms"

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory
    //  logger
    private val logger = getLogger( this.getClass )

    import parser.SMTLIB2PrettyPrinter.show
    import typedterms.TypedTerm

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    //  format: OFF
    val theLIATerms = Table[List[TypedTerm[ BoolTerm, Term ]] , List[Int]](
        ("TypedTerm", "indices of cores"),
        (List( False() ), List(0)),
        (List( x < 0, x >= 0 ), List( 0 ,1)),
        (List ( x === 0 , x + 1 > 2 ), List( 0 ,1)),
        (List ( x === y + 1 , y <= 2, x >= 4 ), List( 0 ,1 ,2 )),
        (List ( x === y + 1 , y <= 2, y >= 4 ), List( 1 , 2 )),
        (List ( x === z + 1 , z >= 0, y >= x  , y < 1 ),  List( 0 ,1 ,2 ,3)),
        (List ( x === z + 1 ,  x >= 0, y >= 2  , z < -1 ),  List( 0 ,1, 3))
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LIA
    import configurations.SMTOptions.SMTProduceUnsatCores
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat, UnKnown, SuccessResponse }
    import scala.util.{ Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled &&
            s.supportsLogic( QF_LIA ) &&
            s.supports( SMTProduceUnsatCores ) ) : _* )

    //  Initialise sequence
    val initSeqInt = new SMTInit( QF_LIA, List( SMTProduceUnsatCores( true ) ) )

    for ( s ← theSolvers; ( xt, core ) ← theLIATerms ) {

        test( s"[${s.name} configured with ${initSeqInt.show} -- checkSat/compute unsat core for term ${xt.map( _.termDef ).map( show( _ ) )}" ) {

            //  make NamedTerms with an index
            val namedTerms = for { ( tt, n ) ← xt.zipWithIndex } yield tt.named( "P" + n.toString )

            //  with using
            using( new SMTSolver( s, initSeqInt ) ) {
                implicit solver ⇒
                    {
                        isSat( namedTerms : _* ) flatMap {
                            case UnSat() ⇒ Success( UnSat() )
                            case s       ⇒ Failure( new Exception( s"Expected status is UnSat: returned is $s" ) )

                        } flatMap { _ ⇒
                            getUnsatCore()
                        }
                    }
            } shouldBe Success( core.map( "P" + _ ).toSet )
        }
    }
}

/**
 * Unsat cores for LRA terms
 */
class RealUnsatCoreTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with RealArithmetics
    with Commands
    with Resources {

    override def suiteName = "Compute unsat cores for LRA terms"

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory
    //  logger
    private val logger = getLogger( this.getClass )

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat, UnKnown, SuccessResponse }

    import typedterms.TypedTerm

    val x = Reals( "x" )
    val y = Reals( "y" )
    val z = Reals( "z" )

    //  format: OFF
    val theLRATerms = Table[List[TypedTerm[ BoolTerm, Term ]], List[Int] ](
        ("TypedTerm", "indices of cores"),
        (List ( x === 0.0         , x + 1.0 > 2.0 ) , List( 0 ,1 )),
        (List ( x === y + 1.0/2.0 , y <= 2          , x >= 4 ) , List( 0, 1, 2 ) ),
        (List ( x === 1.0/2.0     , y <= 2          , x >= 4 ) , List( 0, 2 ) ),
        (List ( x === z + 1       , z >= 0          , y >= x   , y < 1 ), List( 0, 1, 2, 3) )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LRA
    import configurations.SMTOptions.SMTProduceUnsatCores
    import scala.util.{ Try, Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled &&
            s.supports( SMTProduceUnsatCores ) &&
            s.supportsLogic( QF_LRA ) ) : _* )

    //  Initialise sequence
    val initSeqInt = new SMTInit( QF_LRA, List( SMTProduceUnsatCores( true ) ) )

    for ( s ← theSolvers; ( xt, core ) ← theLRATerms ) {

        test( s"[${s.name} configured with ${initSeqInt.show} -- checkSat/compute unsat core for term ${xt.map( _.termDef ).map( show( _ ) )}" ) {

            //  make NamedTerms with an index
            val namedTerms = for { ( tt, n ) ← xt.zipWithIndex } yield tt.named( "P" + n.toString )

            //  with using
            using( new SMTSolver( s, initSeqInt ) ) {
                implicit solver ⇒
                    {
                        isSat( namedTerms : _* ) flatMap {
                            case UnSat() ⇒ Success( UnSat() )
                            case s       ⇒ Failure( new Exception( s"Expected status is UnSat: returned is $s" ) )

                        } flatMap { _ ⇒
                            getUnsatCore()
                        }
                    }
            } shouldBe Success( core.map( "P" + _ ).toSet )
        }
    }
}

/**
 * Unsat cores for QF_AUFLIA terms (arrays and LIA)
 */
class ArrayIntUnsatCoreTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with IntegerArithmetics
    with ArrayExInt
    with ArrayExOperators
    with Commands
    with Resources {

    override def suiteName = "Compute unsat cores for Array terms"

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory
    //  logger
    private val logger = getLogger( this.getClass )

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat, UnKnown }
    import typedterms.TypedTerm

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )
    val a1 = ArrayInt1( "a1" )
    val a2 = ArrayInt1( "a2" )

    //  format: OFF
    val theArrayTerms = Table[List[TypedTerm[ BoolTerm, Term ]], List[Int]](
        ("TypedTerm", "indices of cores"),
        (List ( x === 0             , a1(1) === 1 , a1(1) <= x ) , List(0, 1, 2) ),
        (List ( a1(1) <= a2(0)      , a2(0) <= 2  , a1(1) >= 4 ) , List(0, 1, 2) ),
        (List ( a1(x) + 2 === a2(x) , a2(z) <= 3  , z === x      , a1(z) > a2(x) + 3  ), List(0, 2, 3))
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_AUFLIA
    import configurations.SMTOptions.SMTProduceUnsatCores
    import scala.util.{ Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled &&
            s.supports( SMTProduceUnsatCores ) &&
            s.supportsLogic( QF_AUFLIA ) ) : _* )

    //  Initialise sequence
    val initSeqInt = new SMTInit( QF_AUFLIA, List( SMTProduceUnsatCores( true ) ) )

    for ( s ← theSolvers; ( xt, core ) ← theArrayTerms ) {

        test( s"[${s.name}] configured with ${initSeqInt.show} for term ${xt.map( _.termDef ).map( show( _ ) )} -- check-sat should be UnSat and get Interpolants" ) {

            //  make NamedTerms with an index
            val namedTerms = for { ( tt, n ) ← xt.zipWithIndex } yield tt.named( "P" + n.toString )

            //  with using
            using( new SMTSolver( s, initSeqInt ) ) {
                implicit solver ⇒
                    {
                        isSat( namedTerms : _* ) flatMap {
                            case UnSat() ⇒ Success( UnSat() )
                            case s       ⇒ Failure( new Exception( s"Expected status is UnSat: returned is $s" ) )

                        } flatMap { _ ⇒
                            getUnsatCore()
                        }
                    }
            } shouldBe Success( core.map( "P" + _ ).toSet )
        }
    }
}
