/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

/**
 * test the assert command on typed terms
 *
 * This command should allow assertions of typedterms and manage variable declarations
 * on-the-fly.
 */
class AssertCommandTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with IntegerArithmetics with Commands with Resources {

    override def suiteName = "Send assert command and check Declared variables stack"

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.SMTInit
    import scala.util.Success
    import parser.SMTLIB2Syntax.{
        Sat,
        UnSat
    }

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter(
            n ⇒ !( n.name contains "nonIncr" ) && n.enabled &&
                n.supportsLogic( QF_LIA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA )

    for ( s ← theSolvers ) {

        test( s"[${s.name}] configured with ${initSeq.show} send simple assert script with multiple check-sat commands" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        val x = Ints( "x" )
                        val y = Ints( "y" )

                        //  assert first term
                        |= ( x <= 4 ) shouldBe a[ Success[ _ ] ]
                        //  assert another term
                        |= ( x >= 1 ) shouldBe a[ Success[ _ ] ]

                        //  checkSat
                        isSat() shouldBe Success( Sat() )

                        isSat() shouldBe Success( Sat() )

                        // assert a new term
                        |= ( y < x + 1 ) shouldBe a[ Success[ _ ] ]

                        checkSat() shouldBe Success( Sat() )
                        checkSat() shouldBe Success( Sat() )

                        // assert a new term that makes the solver stack UnSat
                        isSat( y >= 6 ) shouldBe Success( UnSat() )
                        checkSat()
                    }
            } shouldBe Success( UnSat() )
        }
    }
}

/**
 * test the assert command on typed terms
 *
 * This command should allow assertions of typedterms and manage variable declarations
 * on-the-fly.
 */
class AssertCommandWithTimeoutTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with IntegerArithmetics with Commands with Resources {

    override def suiteName = "Send assert command with a timeout and check Declared variables stack"

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.SMTInit
    import scala.util.Success
    import parser.SMTLIB2Syntax.{
        Sat,
        UnSat
    }

    import scala.concurrent.duration._

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter(
            n ⇒ !( n.name contains "nonIncr" ) && n.enabled &&
                n.supportsLogic( QF_LIA ) ) : _* )
    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA )

    for ( s ← theSolvers ) {

        test( s"[${s.name}] configured with ${initSeq.show} and timeout of 2 seconds send simple assert script with multiple check-sat commands" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        val x = Ints( "x" )
                        val y = Ints( "y" )

                        //  assert first term
                        |= ( x <= 4 ) shouldBe a[ Success[ _ ] ]
                        //  assert another term
                        |= ( x >= 1 ) shouldBe a[ Success[ _ ] ]

                        //  checkSat
                        isSat( 2.seconds )() shouldBe Success( Sat() )

                        isSat( 2.seconds )() shouldBe Success( Sat() )

                        // assert a new term
                        |= ( y < x + 1 ) shouldBe a[ Success[ _ ] ]

                        checkSat( 2.seconds ) shouldBe Success( Sat() )
                        checkSat( 2.seconds ) shouldBe Success( Sat() )

                        // assert a new term that makes the solver stack UnSat
                        isSat( y >= 6 ) shouldBe Success( UnSat() )
                        checkSat()
                    }
            } shouldBe Success( UnSat() )
        }
    }
}

/**
 * test the eval command on empty list
 *
 */
class EvalWithEmptyListTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with IntegerArithmetics with Commands with Resources {

    override def suiteName = "Send assert command with a timeout and check Declared variables stack"

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.SMTInit
    import scala.util.Success
    import parser.SMTLIB2Syntax.SuccessResponse

    // import scala.concurrent.duration._

    //  Solvers to be included in the tests. Should work with of them as actual solver not
    //  called for empty list
    val theSolvers = Table(
        "Solver",
        config : _* )
    //  initialise sequence
    val initSeq = new SMTInit()

    for ( s ← theSolvers ) {

        test( s"[${s.name}] configured with ${initSeq.show} isSat for empty list should yiedn SuccessResponse" ) {

            //  with using
            val r = using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒ { eval( List() ) }
            }
            r.isSuccess shouldBe true
            parseAsSuccess( r.get ) shouldBe Success ( SuccessResponse() )
        }
    }
}
