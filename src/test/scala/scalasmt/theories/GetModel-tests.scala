/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

class IntsGetModelTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Commands with Resources {

    override def suiteName = "Check sat anf get models for LIA"

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{ Sat, UnSat, Term }
    import typedterms.{ TypedTerm, Model, ModelFunDef, ModelValPair }
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ] ](
        ( "expression"         , "TypedTerm")       ,
        ( "integer variable x" , x  === y )         ,
        ( "-x"                 , -x  === 1)         ,
        ( "x + y"              , x + y  === 0 )     ,
        ( "x + y = z"          , x + y  === z )     ,
        ( "2 * x = z"          , x * 2  === z )     ,
        ( "1 + 2 + x"          , x + 1 + 2  === -4)
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LIA
    import configurations.SMTOptions.SMTProduceModels
    import scala.util.{ Success, Failure }

    val theSolvers = Table( "Solver", config.filter( s ⇒ s.enabled && s.supportsLogic( QF_LIA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA, List( SMTProduceModels( true ) ) )

    for ( s ← theSolvers; ( x, t ) ← theTerms ) {

        if ( s.name.startsWith( "Yices" ) )
            test( s"[${s.name}]  to get model for ${show( t.termDef )} -- ${s.name} response is not conformant to SMTLIB2 grammar" ) {
                //  with using
                using( new SMTSolver( s, initSeq ) ) {
                    implicit solver ⇒
                        {
                            //  smtlib package eval is used
                            isSat( t ) should matchPattern {
                                case Success( Sat() ) ⇒
                            }

                            //  now get a model, should not work
                            //  returns a getValue response
                            val r = getModel()
                            logger.info( "Get-model returned: {}", r )

                            r
                        }
                } should matchPattern {
                    case Failure( _ ) ⇒
                }
            }
        else
            test( s"[${s.name}] configured with ${initSeq.show} to get a model for term ${show( t.termDef )} === 1 " ) {

                //  with using
                using( new SMTSolver( s, initSeq ) ) {
                    implicit solver ⇒
                        {
                            //  smtlib package eval is used
                            isSat( t ) should matchPattern {
                                case Success( Sat() ) ⇒
                            }

                            //  now get a model, should always work
                            val r = getModel()
                            logger.info( "Get-model returned: {}", r )
                            r
                        }
                } should matchPattern {
                    case Success( ModelFunDef( x ) )  ⇒
                    case Success( ModelValPair( x ) ) ⇒
                }
            }
    }
}

class RealsGetModelTests extends FunSuite with TableDrivenPropertyChecks with Matchers with RealArithmetics with Core with Commands with Resources {

    override def suiteName = "Check sat and get models for LRA"

    import parser.SMTLIB2PrettyPrinter.show
    import typedterms.{ TypedTerm, Model, ModelFunDef, ModelValPair }
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat, GetDeclCmd }
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    val x = Reals( "x" )
    val y = Reals( "y" )
    val z = Reals( "z" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ] ](
        ( "expression"         , "TypedTerm")       ,
        ( "integer variable x" , x  === y )         ,
        ( "-x"                 , -x  === 1)         ,
        ( "x + y"              , x + y  === 0 )     ,
        ( "x + y = z"          , x + y  === z )     ,
        ( "2 * x = z"          , x * 2.0  === z )     ,
        ( "1 + 2 + x"          , x + 1.0 + 2.4  === -4.66)
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LRA
    import configurations.SMTOptions.SMTProduceModels
    import scala.util.{ Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( s ⇒ s.enabled && s.supportsLogic( QF_LRA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LRA, List( SMTProduceModels( true ) ) )

    for ( s ← theSolvers; ( _, t ) ← theTerms ) {

        if ( s.name.startsWith( "Yices" ) )
            test( s"[${s.name}]  to get model for ${show( t.termDef )} -- ${s.name} response is not conformant to SMTLIB2 grammar -- returns a getValue instead" ) {
                //  with using
                using( new SMTSolver( s, initSeq ) ) {
                    implicit solver ⇒
                        {
                            //  smtlib package eval is used
                            isSat( t ) should matchPattern {
                                case Success( Sat() ) ⇒
                            }

                            //  now get a model, should not work
                            val r = getModel()
                            logger.info( "Get-model returned: {}", r )

                            r
                        }
                } should matchPattern {
                    case Failure( _ ) ⇒
                }
            }
        else
            test( s"[${s.name}] configured with ${initSeq.show} to get a model for term ${show( t.termDef )} === 1" ) {

                //  with using
                using( new SMTSolver( s, initSeq ) ) {
                    implicit solver ⇒
                        {
                            //  smtlib package eval is used
                            isSat( t ) should matchPattern {
                                case Success( Sat() ) ⇒
                            }

                            logger.info( "Declarations: {}", eval( Seq( GetDeclCmd() ) ) )

                            //  now get a model, should always work
                            val r = getModel()
                            logger.info( "Get-model returned: {}", r )
                            r
                        }
                } should matchPattern {
                    case Success( ModelFunDef( x ) )  ⇒
                    case Success( ModelValPair( x ) ) ⇒
                }
            }
    }
}

class ArrayGetModelTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with ArrayExInt with ArrayExBool with ArrayExOperators with Core with Commands with Resources {

    override def suiteName = "Check sat and get model for terms with arrays"

    import parser.SMTLIB2PrettyPrinter.show
    import typedterms.{ TypedTerm, Model, ModelFunDef, ModelValPair }
    import parser.SMTLIB2Syntax.{ Sat, UnSat, GetDeclCmd, Term, CheckSatResponses }
    import scala.util.{ Success, Failure }
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val a1 = ArrayInt1( "a1" )
    val a2 = ArrayInt2( "a2" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ] ](
        ( "expression"  , "TypedTerm")  ,
        ( "array 1"     , a1(1) === y ) ,
        ( "array 2"     , (a1(1) === y) & (a1(2) === y - 1) ),
        ( "array 3"     , a1(1) ===  a2(2)(3)  )

    )
    //  format: ON

    //  check that the solvers accept the terms

    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_AUFLIA
    import configurations.SMTOptions.SMTProduceModels

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( s ⇒ s.enabled && s.supportsLogic( QF_AUFLIA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_AUFLIA, List( SMTProduceModels( true ) ) )

    for ( s ← theSolvers; ( _, t ) ← theTerms ) {

        if ( s.name.startsWith( "Yices" ) )
            //  note: SMTInterpol crashes and I believe this is a bug in interpol
            test( s"[${s.name}] to get model for ${show( t.termDef )} -- ${s.name} response is not conformant to SMTLIB2 grammar -- returns a GetValue instead" ) {

                //  with using
                using( new SMTSolver( s, initSeq ) ) {
                    implicit solver ⇒
                        {
                            //  smtlib package eval is used
                            isSat( t ) should matchPattern {
                                case Success( Sat() ) ⇒
                            }

                            //  now get a model, should not work
                            val r = getModel()
                            logger.info( "Get-model returned: {}", r )

                            r
                        }
                } should matchPattern {
                    case Failure( _ ) ⇒
                }
            }
        else
            test( s"[${s.name}] configured with ${initSeq.show} to get a model for term ${show( t.termDef )} === 1" ) {

                //  with using
                using( new SMTSolver( s, initSeq ) ) {
                    implicit solver ⇒
                        {
                            //  smtlib package eval is used
                            isSat( t ) should matchPattern {
                                case Success( Sat() ) ⇒
                            }

                            logger.info( "Declarations: {}", eval( Seq( GetDeclCmd() ) ) )

                            //  now get a model, should always work
                            val r = getModel()

                            r
                        }
                } should matchPattern {
                    case Success( ModelFunDef( x ) )  ⇒
                    case Success( ModelValPair( x ) ) ⇒
                }
            }
    }
}
