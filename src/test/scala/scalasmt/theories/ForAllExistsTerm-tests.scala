/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ IntegerArithmetics, Core }
import interpreters.Resources
import typedterms.{ Commands, QuantifiedTerm }

class DeclareForAllExistsTermsTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with IntegerArithmetics
    with ArrayExInt
    with Core
    with Commands
    with Resources
    with QuantifiedTerm {

    override def suiteName = "Declare forall/exists terms in solvers test suite"

    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax.{
        Term,
        TermTester,
        CheckSatResponses,
        Sat,
        QualifiedId,
        SimpleQId,
        SymbolId,
        SSymbol,
        SortedQId
    }
    import parser.Implicits._
    import typedterms.{ VarTerm, TypedTerm }
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.AUFNIRA
    import configurations.SMTOptions.MODELS
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.show

    val p1 = SMTLIB2Parser[ TermTester ]

    //  create a solver SortedQId, type Ints, from a string
    def toSimpleQId( name : String ) : QualifiedId = SimpleQId( SymbolId( SSymbol( name ) ) )

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( s ⇒ s.enabled && s.supportsLogic( AUFNIRA ) ) : _* )

    val x = Ints( "x" )
    val y = Ints( "y" )

    //  format: OFF
    val theTerms = Table[  TypedTerm[BoolTerm, Term] ](

        "typed term version",
        (
            forall (Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= 1
            }
        ),

        //  one var not bounded
        (
            (forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= 1
            }) & (x === 3)
        ),

        //  one var not bounded
        (
            (forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= y
            }) & (x === 3)
        ),

        //  indexed variable
        (
            forall(Ints("x1").indexed(1).symbol) {
                val x1 = Ints("x1").indexed(1)
                x1 + 2 <= 1
            }
        ),

        //  nested forall
        (
            forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                forall(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            }
        ),

        //  exists and forall
        (
            exists(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                forall(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            }
        ),

        (
            forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                exists(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            }
        )
    )
    // format: ON

    //  initialise sequence
    val initSeq = new SMTInit( AUFNIRA, List() )

    for ( s ← theSolvers; ( xt ) ← theTerms ) {

        test( s"[${s.name}] configured with ${initSeq.show} assert quantified Term  ${show( xt.termDef )} -- should succeed" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        |=( xt )
                    }
            } should matchPattern {
                case Success( _ ) ⇒
            }
        }
    }
}

class CheckSatForAllExistsTermsTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with IntegerArithmetics
    with ArrayExInt
    with Core
    with Commands
    with Resources
    with QuantifiedTerm {

    override def suiteName = "Declare forall/exists terms in solvers test suite"

    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{
        QualifiedId,
        SimpleQId,
        SymbolId,
        SSymbol,
        SortedQId,
        SatResponses,
        Term,
        TermTester,
        CheckSatResponses,
        Sat,
        UnSat
    }
    import parser.SMTLIB2Parser
    import parser.Implicits._
    import typedterms.{ VarTerm, TypedTerm }
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.AUFNIRA
    import configurations.SMTOptions.MODELS
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.show

    val p1 = SMTLIB2Parser[ TermTester ]

    //  create a solver SortedQId, type Ints, from a string
    def toSimpleQId( name : String ) : QualifiedId = SimpleQId( SymbolId( SSymbol( name ) ) )

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( s ⇒ s.enabled && s.supportsLogic( AUFNIRA ) ) : _* )

    val x = Ints( "x" )
    val y = Ints( "y" )

    //  format: OFF
    val theTerms = Table[  TypedTerm[BoolTerm, Term], SatResponses ](

        ("typed term version", "Response"),
        (
            forall (Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= 1
            },
            UnSat()
        ),

        //  one var not bounded
        (
            (forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= 1
            }) & (x === 3),
            UnSat()
        ),

        //  one var not bounded
        (
            (forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 + 2 <= y
            }) & (x === 3),
            UnSat()
        ),

        //  indexed variable
        (
            forall(Ints("x1").indexed(1).symbol) {
                val x1 = Ints("x1").indexed(1)
                x1 + 2 <= 1
            },
            UnSat()
        ),

        //  nested forall
        (
            forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                forall(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            },
            Sat()
        ),

        //  exists and forall
        (
            exists(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                forall(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            },
            Sat()
        ),

        (
            forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                exists(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            },
            Sat()
        )
    )
    // format: ON

    //  initialise sequence
    val initSeq = new SMTInit( AUFNIRA, List() )

    for ( s ← theSolvers; ( xt, r ) ← theTerms ) {

        test( s"[${s.name}] configured with ${initSeq.show} assert quantified Term  ${show( xt.termDef )} -- should be ${show( r )}" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        |=( xt ) flatMap
                            { _ ⇒ checkSat() }
                    }
            } shouldBe Success( r )
        }
    }
}
