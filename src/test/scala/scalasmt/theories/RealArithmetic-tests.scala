/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

class LinearTermTests extends FunSuite with TableDrivenPropertyChecks with Matchers with Core with RealArithmetics with Commands with Resources {

    override def suiteName = "Check the termDef and solvers' compatibility for LRA terms"

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat }
    import typedterms.TypedTerm

    val x = Reals( "x" )
    val y = Reals( "y" )
    val z = Reals( "z" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ RealTerm, Term ], String ](
        ( "expression"            , "TypedTerm"                 , "Solver string" )            ,
        ( "real number 0.0"       , Reals( 0.0 )                , "0.0 " )                       ,
        ( "real number 1.5"       , Reals( 1.5 )                , "1.5 " )                     ,
        ( "real number 0.0000001" , Reals( 0.0000001 )          , "0.00000010 " ),
        
        ( "1.0 + 2.0"             , Reals( 1.0 ) + Reals( 2.0 ) , "(+ 1.0 2.0 ) " )                ,
        ( "3 + 2.0"               , Reals( 3 ) + Reals( 2.0 )   , "(+ 3.0 2.0 ) " )                ,
        ( "integer variable x"    , x                           , "x " )                       ,
        ( "-x"                    , -x                          , "(- x ) " )                  ,
        ( "-x_1"                  , -(x indexed 1)              , "(- x@1 ) " )                ,
        ( "x + y"                 , x + y                       , "(+ x y ) " )                ,
        ( "x + y + z"             , x + y + z                   , "(+ (+ x y ) z ) " )         ,
        ( "x + y_3 + z"           , x + (y indexed 3) + z       , "(+ (+ x y@3 ) z ) " )       ,
        ( "x + y + z + 1.5"       , x + y + z + 1.5             , "(+ (+ (+ x y ) z ) 1.5 ) ") ,
        ( "x - y + z"             , x - y + z                   , "(+ (- x y ) z ) " )         ,
        ( "x - ( x + z )"         , x - ( y + z )               , "(- x (+ y z ) ) " )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LRA
    import scala.util.Success

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( QF_LRA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LRA, List() )

    for ( s ← theSolvers; ( x, t, _ ) ← theTerms ) {

        test( s"[${s.name}] configured with ${initSeq.show} to check that check-sat for term ${show( t.termDef )} === 1.0 can be established" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t === 1.0 )
                    }
            } should matchPattern {
                case Success( Sat() )   ⇒
                case Success( UnSat() ) ⇒
            }
        }
    }

    //  Check that Reals cannot be created with Infinity of NaN
    test( s"Cannot build termDef when Double NaN" ) {
        val r = 0.0 / 0.0
        the[ Exception ] thrownBy {
            Reals( r )
        } should have message s"requirement failed: $r is not a real number (infinity or NaN) and cannot be converted to SMTLIB Reals"
    }

    test( s"Cannot build termDef when Double is plusInfinity" ) {
        val r = 1.0 / 0.0
        the[ Exception ] thrownBy {
            Reals( r )
        } should have message s"requirement failed: $r is not a real number (infinity or NaN) and cannot be converted to SMTLIB Reals"
    }

    test( s"Cannot build termDef when Double is minusInfinity" ) {
        val r = -1.0 / 0.0
        the[ Exception ] thrownBy {
            Reals( r )
        } should have message s"requirement failed: $r is not a real number (infinity or NaN) and cannot be converted to SMTLIB Reals"
    }

}

class NonLinearMixedTermTests extends FunSuite with TableDrivenPropertyChecks with Matchers with Core with RealArithmetics with IntegerArithmetics
    with Commands
    with Resources {

    override def suiteName = "Check the termDef and solvers' compatibility for NIRA terms"

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat }

    import typedterms.TypedTerm

    val x = Reals( "x" )
    val y = Reals( "y" )
    val z = Reals( "z" )

    val i : TypedTerm[ RealTerm, Term ] = Ints( "i" )
    val j : TypedTerm[ RealTerm, Term ] = Ints( "j" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ RealTerm, Term ], String ](
        ( "expression"                  , "TypedTerm"            , "Solver string" )            ,
        ( "real number 0.0 + Integer 1" , Reals( 0.0 ) + Ints(1) , "(+ 0.0 1 ) " )              ,
        ( "x + i"                       , x + i                  , "(+ x i ) " )                ,
        ( "i + x"                       , i + x                  , "(+ i x ) " )                ,
        ( "x + i + y"                   , x + i + y              , "(+ (+ x i ) y ) " )         ,
        ( "x + y + j + 1.5"             , x + y + j + 1.5        , "(+ (+ (+ x y ) j ) 1.5 ) ") ,
        ( "x - j + i"                   , x - j + i              , "(+ (- x j ) i ) " )         ,
        ( "x - ( i + z )"               , x - ( i + z )          , "(- x (+ i z ) ) " )         ,
        ( "x - ( i * z )"               , x - ( i * z )          , "(- x (* i z ) ) " )         ,
        ( "(x / y ) - ( i * z )"        , (x / y) - ( i * z )    , "(- (/ x y) (* i z ) ) " )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_NIRA
    import scala.util.Success

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( QF_NIRA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_NIRA, List() )

    for ( s ← theSolvers; ( x, t, _ ) ← theTerms ) {

        test( s"[${s.name}] configured with ${initSeq.show} to check that check-sat for term ${show( t.termDef )} === 1.0 can be established" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t === 1.0 )
                    }
            } should matchPattern {
                case Success( Sat() )   ⇒
                case Success( UnSat() ) ⇒
            }
        }
    }
}
