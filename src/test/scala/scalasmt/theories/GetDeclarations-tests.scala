/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import parser.Implicits._
import parser.PredefinedParsers
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

/**
 * Check result of declarationsa
 */
class GetDeclarationsCmdLIATests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with IntegerArithmetics
    with Core
    with Resources {

    override def suiteName = "Push some LIA + Arrays terms on the solver and check GetDeclCmd results"
    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax.{
        Script,
        IdTester,
        SortedQId,
        Sort,
        IntSort,
        BoolSort,
        GetDeclCmd,
        GetDeclCmdResponse,
        DeclCmdResponseSuccess,
        SuccessResponse,
        GeneralResponse
    }

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  build a SortedQId term (as name type) where x._1 is the name and ._2 the sort

    val pScript = SMTLIB2Parser[ Script ]

    //  format: OFF
    val theScripts = Table[ String , Map[ String, Sort ] ](
        ( "Expression" ,  "Expected declarations" ),

        ("""|
            |  (declare-fun x () Int)
         """.stripMargin,
            Map (
                    "x" -> IntSort( )
                )
        ),

        ("""    |
                |   (declare-fun   x   ()   Int)
                |   (declare-fun   y   ()   Int)
                |   (declare-fun   z   ()   Bool)
                |
         """.stripMargin,
                    Map (
                        "x" -> IntSort( ),
                        "y" -> IntSort( ),
                        "z" -> BoolSort( )
                        )
        ),

        ("""    |
                |   (declare-fun   x@1   ()   Int)
                |   (declare-fun   y_@0   ()   Int)
                |   (declare-fun   z1@20   ()   Bool)
                |
         """.stripMargin,
                    Map (
                        "x@1" -> IntSort( ),
                        "y_@0" -> IntSort( ),
                        "z1@20" -> BoolSort( )
                        )
        ),

        ("""    |
                |   (declare-fun   x    ()    Int)
                |   (declare-fun   y1   ()    Int)
                |   (declare-fun   y2   ()    Int)
                |   (declare-fun   z    ()    Int)
                |
         """.stripMargin,
                Map (
                    "x"  -> IntSort( ),
                    "y1" -> IntSort( ),
                    "y2" -> IntSort( ),
                    "z"  -> IntSort( )
                    )
        ),

        ("""    |
                |  (assert true)
                |
         """.stripMargin,
                Map()
        )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LIA

    //  Solvers to be included in the tests should be enabled in config
    val theSolvers = Table( "Solver", config.filter( s ⇒ s.enabled && s.supportsLogic( QF_LIA ) ) : _* )

    //  initialise sequence.
    val initSeq = new SMTInit( QF_LIA )

    val parserDeclCmdResponse = SMTLIB2Parser[ GetDeclCmdResponse ]
    val p = SMTLIB2Parser[ GeneralResponse ]

    for ( s ← theSolvers; ( x, t ) ← theScripts ) {

        test( s"[${s.name}] configured with ${initSeq.show} check the getDeclResponse for $x" ) {

            // parse the SMTLIB2 declare term to build a Script (AST)
            pScript( x ) match {
                case Success( Script( seqCmds ) ) ⇒
                    //  eval the corresponding declare commands
                    using( new SMTSolver( s, initSeq ) ) {
                        implicit solver ⇒
                            val r1 = eval( seqCmds )
                            val r2 = eval( Seq( GetDeclCmd() ) )
                            Success( ( r1, r2 ) )
                    } match {
                        case Success( ( Success( resultOfDeclCmd ), Success( resultOfGetDeclCmd ) ) ) ⇒

                            //  and should be a success
                            p ( resultOfDeclCmd ) shouldBe Success( SuccessResponse() )

                            //  check the response of getDeclCmd
                            parserDeclCmdResponse( resultOfGetDeclCmd ) should matchPattern {
                                case Success(
                                    DeclCmdResponseSuccess( t ) ) ⇒
                            }

                        case Success( ( Failure( f ), _ ) ) ⇒ fail( f )
                        case Success( ( _, Failure( f ) ) ) ⇒ fail( f )
                        case Failure( f )                   ⇒ fail( f )

                    }

                case Failure( f ) ⇒ fail( f )
            }

        }
    }
}

class GetDeclarationsCmdAUFLIATests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Resources with PredefinedParsers {

    override def suiteName = "Push some terms Arrays of Bools and check GetDeclCmd results"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.{
        Script,
        SortedQId,
        QualifiedId,
        Sort,
        SymbolId,
        SSymbol,
        IntSort,
        RealSort,
        BoolSort,
        Array1Sort,
        Array2Sort,
        Array1BV,
        BitVectorSort,
        GetDeclCmd,
        GetDeclCmdResponse,
        DeclCmdResponseSuccess,
        GeneralResponse,
        SuccessResponse,
        ErrorResponse
    }
    import typedterms.TypedTerm
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  build a SortedQId term (as name type)
    def toSortedQId( x : ( String, Sort ) ) = x match {
        case ( name, sort ) ⇒ SortedQId ( SymbolId( SSymbol( name ) ), sort )
    }

    val pScript = SMTLIB2Parser[ Script ]

    import configurations.{ SolverConfig, SMTInit }

    //  format: OFF
    val theScripts = Table[ String , Map[ String, Sort ] , SolverConfig => Boolean ](
        ( "Expression" ,  "Expected declarations" , "Accepted by solver?" ),

        ("""    |
                |  (declare-fun y () (Array Int Bool ) )
                |
         """.stripMargin,
                Map ( "y" ->
                        Array1Sort(
                                BoolSort()
                        )
                    )
                    , { x => !(x.name == "MathSat")}

        ),

        ("""    |
                |  (declare-fun y () (Array Int (Array Int Bool) ) )
                |
         """.stripMargin,
                Map ( "y" ->
                        Array2Sort (
                            Array1Sort ( BoolSort() )
                        )
                    )
                    , { x => !(x.name == "MathSat") }
        ),

        ("""    |
                |  (declare-fun x () (Array Int Int))
                |
         """.stripMargin,
                Map (
                    "x" ->
                        Array1Sort(
                             IntSort( )
                        )
                    )
                    , { x => true}
        ),

        ("""    |
                |  (declare-fun y () (Array Int Int))
                |
         """.stripMargin,
                Map (
                    "y" ->
                        Array1Sort(
                                IntSort( )
                            )
                        )
                        , { x => true}
        ),

        ("""    |
                |  (declare-fun y () (Array Int (Array Int Int) ) )
                |
         """.stripMargin,
                Map ( "y" ->
                        Array2Sort (
                                Array1Sort ( IntSort() )
                        )
                    )
                    , { x => true}

        )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTLogics.QF_AUFLIA
    import scala.util.{ Try, Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( s ⇒ s.enabled && s.supportsLogic( QF_AUFLIA ) ) : _* )

    //  initialise sequence.Logic needs AU
    val initSeq = new SMTInit( QF_AUFLIA )

    for ( s ← theSolvers; ( x, t, ok ) ← theScripts ) {

        test( s"[${s.name}] configured with ${initSeq.show} check the getDeclResponse for $x. ${if ( ok( s ) ) "should success" else "should fail "}" ) {

            // parse the SMTLIB2 declare term to build a Script (AST)
            val xs = pScript( x )
            pScript( x ) match {
                case Success( Script( seqCmds ) ) ⇒

                    //  eval the corresponding declare commands
                    using( new SMTSolver( s, initSeq ) ) {
                        implicit solver ⇒
                            val r1 = eval( seqCmds )
                            val r2 = eval( Seq( GetDeclCmd() ) )
                            //  computation in using must return a Try
                            Success( ( r1, r2 ) )
                    } match {
                        case Success( ( resultOfDeclCmd, resultOfGetDeclCmd ) ) ⇒
                            //  check the eval(cmds) operation returned a string
                            resultOfDeclCmd should matchPattern {
                                case Success( _ )                 ⇒
                                //  if MathSat, it can timeout
                                case Failure( _ ) if ( !ok( s ) ) ⇒
                            }

                            //  Now check the operation succeeded
                            parseAsGeneralResponse ( resultOfDeclCmd.getOrElse( "(error \"declaration failed\")" ) ) should matchPattern {
                                //  all solvers except MathSat
                                case Success( SuccessResponse() ) if ( ok( s ) )   ⇒
                                //  if MathSat it should not succeed
                                case Success( ErrorResponse( _ ) ) if ( !ok( s ) ) ⇒
                            }

                            //  check the response of getDeclCmd i.e. the declarations pushed
                            parserDeclCmdResponse( resultOfGetDeclCmd.get ) should matchPattern {
                                case Success(
                                    DeclCmdResponseSuccess( t ) ) if ( ok( s ) ) ⇒
                                //  if eval did not succeed, nothing should be on the solver stack
                                case Success( DeclCmdResponseSuccess( List() ) ) if ( !ok( s ) ) ⇒
                            }

                        case Failure( f ) ⇒ fail( f )
                    }

                case Success( k ) ⇒ fail( new Exception( s"Expected Scxript but got $k" ) )
                case Failure( f ) ⇒ fail( f )
            }

        }
    }
}

class GetDeclarationsCmdQFABVTests extends FunSuite with TableDrivenPropertyChecks with Matchers with BitVectors with ArrayExBV with Core with Resources with PredefinedParsers {

    override def suiteName = "Push some terms Arrays of BVs and check GetDeclCmd results"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{
        Script,
        SortedQId,
        QualifiedId,
        Sort,
        SymbolId,
        SSymbol,
        Array1BV,
        BitVectorSort,
        GetDeclCmd,
        GetDeclCmdResponse,
        DeclCmdResponseSuccess,
        SuccessResponse
    }
    import typedterms.TypedTerm
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  build a SortedQId term (as name type) where x._1 is the name and ._2 the sort
    def toSortedQId( x : ( String, Sort ) ) = x match {
        case ( name, sort ) ⇒ SortedQId ( SymbolId( SSymbol( name ) ), sort )
    }

    val pScript = SMTLIB2Parser[ Script ]

    //  format: OFF
    val theScripts = Table[ String , Map[ String, Sort ] ](
        ( "Expression" ,  "Expected declarations" ),

        ("""    |
                |  (declare-fun y () (Array  (_ BitVec 4) (_ BitVec 6 ) ) )
                |
         """.stripMargin,
                Map ( "y" ->
                        Array1BV (
                                BitVectorSort("4"), BitVectorSort("6")
                        )
                    )
        )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_ABV
    import scala.util.{ Try, Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter(
        s ⇒ s.enabled && s.supportsLogic( QF_ABV ) ) : _* )

    //  initialise sequence. Logic irrelevant
    val initSeq = new SMTInit( QF_ABV )

    for ( s ← theSolvers; ( x, t ) ← theScripts ) {

        test( s"[${s.name}] configured with ${initSeq.show} check the getDeclResponse for $x" ) {

            // parse the SMTLIB2 declare term to build a Script (AST)
            pScript( x ) match {
                case Success( Script( seqCmds ) ) ⇒
                    //  eval the corresponding declare commands
                    using( new SMTSolver( s, initSeq ) ) {
                        implicit solver ⇒
                            val r1 = eval( seqCmds )
                            val r2 = eval( Seq( GetDeclCmd() ) )
                            //  computation in using must return a Try
                            Success( ( r1, r2 ) )
                    } match {
                        case Success( ( resultOfDeclCmd, resultOfGetDeclCmd ) ) ⇒
                            //  check the eval(cmds) operation returned a string
                            resultOfDeclCmd should matchPattern {
                                case Success( _ ) ⇒
                            }

                            //  Now check the operation succeeded
                            parseAsGeneralResponse ( resultOfDeclCmd.get ) should matchPattern {
                                case Success( SuccessResponse() ) ⇒
                            }

                            resultOfGetDeclCmd should matchPattern {
                                case Success( _ ) ⇒
                            }

                            //  check the response of getDeclCmd i.e. the declarations pushed
                            parserDeclCmdResponse( resultOfGetDeclCmd.get ) should matchPattern {
                                case Success(
                                    DeclCmdResponseSuccess( t ) ) ⇒
                            }

                        case Failure( f ) ⇒ fail( f )
                    }
                case Failure( f ) ⇒ fail( f )
            }

        }
    }
}

class GetDeclarationsCmdArrayBoolRealTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Resources with PredefinedParsers {

    override def suiteName = "Push some terms Arrays of Reals and check GetDeclCmd results"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.{
        Script,
        SortedQId,
        QualifiedId,
        Sort,
        SymbolId,
        SSymbol,
        IntSort,
        RealSort,
        BoolSort,
        Array1Sort,
        Array2Sort,
        GetDeclCmd,
        GetDeclCmdResponse,
        DeclCmdResponseSuccess,
        SuccessResponse
    }
    import typedterms.TypedTerm
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  build a SortedQId term (as name type)
    def toSortedQId( x : ( String, Sort ) ) = x match {
        case ( name, sort ) ⇒ SortedQId ( SymbolId( SSymbol( name ) ), sort )
    }
    val pScript = SMTLIB2Parser[ Script ]

    //  format: OFF
    val theScripts = Table[ String , Map[ String, Sort ] ](
        ( "Expression" ,  "Expected declarations" ),

        ("""    |
                |  (declare-fun y () (Array Int Real ) )
                |
        """.stripMargin,
            Map ( "y" ->
                    Array1Sort(
                        RealSort()
                    )
                )
        ),

        ("""    |
                |  (declare-fun y () (Array Int (Array Int Real) ) )
                |
         """.stripMargin,
                Map ( "y" ->
                        Array2Sort (
                            Array1Sort ( RealSort() )
                        )
                    )
        )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_AUFLIRA
    import scala.util.{ Try, Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter(
        s ⇒ s.enabled && s.supportsLogic( QF_AUFLIRA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_AUFLIRA )

    for ( s ← theSolvers; ( x, t ) ← theScripts ) {

        test( s"[${s.name}] configured with ${initSeq.show} check the getDeclResponse for $x" ) {

            // parse the SMTLIB2 declare term to build a Script (AST)
            pScript( x ) match {

                case Success( Script( seqCmds ) ) ⇒
                    //  eval the corresponding declare commands
                    using( new SMTSolver( s, initSeq ) ) {
                        implicit solver ⇒
                            val r1 = eval( seqCmds )
                            val r2 = eval( Seq( GetDeclCmd() ) )
                            //  computation in using must return a Try
                            Success( ( r1, r2 ) )
                    } match {
                        case Success( ( Success( resultOfDeclCmd ), Success( resultOfGetDeclCmd ) ) ) ⇒
                            parseAsGeneralResponse( resultOfDeclCmd ) should matchPattern {
                                case Success( SuccessResponse() ) ⇒
                            }
                            parserDeclCmdResponse( resultOfGetDeclCmd ) should matchPattern {
                                case Success( DeclCmdResponseSuccess( _ ) ) ⇒
                            }
                        case Success( ( Failure( f1 ), _ ) ) ⇒ fail( f1 )
                        case Success( ( _, Failure( f2 ) ) ) ⇒ fail( f2 )
                        case Failure( f )                    ⇒ fail( f )
                    }

                case Failure( e ) ⇒ fail( e )
            }
        }
    }
}
