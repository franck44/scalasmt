/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

/**
 * test the pop/push commands
 */
class PopPushTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Commands with Resources {

    override def suiteName = "Send pop/push and check size of stack"

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LIA

    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ SuccessResponse, ErrorResponse }

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( QF_LIA ) ) : _* )
    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA, List() )

    for ( s ← theSolvers ) {

        test( s"[${s.name}] configured with ${initSeq.show} push(1)/pop(1) check size of the stack" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        push() should
                            matchPattern { case Success( SuccessResponse() ) ⇒ }
                        solver.context should have length 2

                        pop() should
                            matchPattern { case Success( SuccessResponse() ) ⇒ }
                        solver.context should have length 1

                        //  using must return a Try
                        Success( true )
                    }
            } shouldBe Success( true )
        }

        test( s"[${s.name}] configured with ${initSeq.show} push(1)/pop(2) shoud fail stack" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        push() should
                            matchPattern { case Success( SuccessResponse() ) ⇒ }
                        solver.context should have length 2

                        //  cannot pop more than is on th stack
                        pop( 4 ) should matchPattern {
                            case Failure( _ ) ⇒
                        }

                        //  using must return a Try
                        Success( true )
                    }
            } shouldBe Success( true )
        }

        test( s"[${s.name}] configured with ${initSeq.show} push(2)/pop(1) and check size of stack" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        push( 2 ) should
                            matchPattern { case Success( SuccessResponse() ) ⇒ }
                        solver.context should have length 3

                        //  cannot pop more than is on th stack
                        pop() should matchPattern { case Success( SuccessResponse() ) ⇒ }
                        solver.context should have length 2

                        //  using must return a Try
                        Success( true )
                    }
            } shouldBe Success( true )
        }

        test( s"[${s.name}] configured with ${initSeq.show} pop from initiail stack -- shoud fail stack" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        //  cannot pop more than is on th stack
                        pop()
                    }
            } should matchPattern {
                case Failure( _ ) ⇒
            }
        }

        test( s"[${s.name}] configured with ${initSeq.show} alternating push/pop check size of the stack" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {

                        push( 4 ) should matchPattern { case Success( SuccessResponse() ) ⇒ }
                        solver.context should have length 5

                        pop( 2 ) should matchPattern { case Success( SuccessResponse() ) ⇒ }
                        solver.context should have length 3

                        push( 1 ) should matchPattern { case Success( SuccessResponse() ) ⇒ }
                        solver.context should have length 4

                        pop( 2 ) should matchPattern { case Success( SuccessResponse() ) ⇒ }
                        solver.context should have length 2

                        Success( true )
                    }
            } shouldBe Success( true )
        }

        test( s"[${s.name}] configured with ${initSeq.show} alternating push/pop and forbidden pop -- should fail" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {

                        push() should matchPattern { case Success( SuccessResponse() ) ⇒ }
                        solver.context should have length 2

                        push( 3 ) should matchPattern { case Success( SuccessResponse() ) ⇒ }
                        solver.context should have length 5

                        pop( 2 ) should matchPattern { case Success( SuccessResponse() ) ⇒ }
                        solver.context should have length 3

                        //  cannot pop more than is on th stack
                        pop( 4 ) should matchPattern {
                            case Failure( _ ) ⇒
                        }

                        //  using mjust return a Try
                        Success( true )
                    }
            } shouldBe Success( true )
        }

    }
}

class PopPushCheckDeclarationsTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with IntegerArithmetics with Commands with Resources {

    override def suiteName = "Send pop/push and check declarations pushed on stack"

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LIA
    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{
        GetDeclCmd,
        DeclCmdResponseSuccess,
        DeclCmdResponseError,
        SuccessResponse
    }
    import parser.SMTLIB2Parser

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( QF_LIA ) ) : _* )
    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA, List() )

    for ( s ← theSolvers ) {

        test( s"[${s.name}] configured with ${initSeq.show} push/pop and check declarations of declarations" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        push() should matchPattern { case Success( SuccessResponse() ) ⇒ }

                        //  assert first term
                        val res1 = |= ( x <= 4 )
                        //  assert another term
                        val res2 = |= ( x > 4 )

                        ( eval( Seq( GetDeclCmd() ) ) flatMap
                            //
                            parserDeclCmdResponse ) match {

                                case Success( DeclCmdResponseSuccess( r ) ) ⇒
                                    //  x is a list f declarations
                                    r.toSet shouldBe x.typeDefs

                                case Success( DeclCmdResponseError( e ) ) ⇒ fail( new Exception( "GetDecl failed " + e ) )

                                case Failure( e )                         ⇒ fail( e )
                            }

                        pop() should matchPattern { case Success( SuccessResponse() ) ⇒ }

                        ( eval( Seq( GetDeclCmd() ) ) flatMap
                            //
                            parserDeclCmdResponse ) match {

                                case Success( DeclCmdResponseSuccess( r ) ) ⇒
                                    //  x is a list f declarations
                                    r.toSet shouldBe empty

                                case Success( e ) ⇒ fail( new Exception( "GetDecl failed " + e ) )

                                case Failure( e ) ⇒ fail( e )
                            }

                        //  using must return a Try
                        Success( true )
                    }
            } shouldBe Success( true )
        }

        test( s"[${s.name}] configured with ${initSeq.show} push/pop and check some levels of declarations" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        push() should matchPattern { case Success( SuccessResponse() ) ⇒ }

                        //  assert first term
                        val res1 = |= ( x <= 4 )

                        push() should matchPattern { case Success( SuccessResponse() ) ⇒ }

                        //  assert another term
                        val res2 = |= ( y > 4 )

                        ( eval( Seq( GetDeclCmd() ) ) flatMap
                            //
                            parserDeclCmdResponse ) match {

                                case Success( DeclCmdResponseSuccess( r ) ) ⇒
                                    //  x is a list f declarations
                                    r.toSet shouldBe x.typeDefs ++ y.typeDefs

                                case Success( e ) ⇒ fail( new Exception( "GetDecl failed " + e ) )

                                case Failure( e ) ⇒ fail( e )
                            }
                    }

                    pop() should matchPattern { case Success( SuccessResponse() ) ⇒ }

                    ( eval( Seq( GetDeclCmd() ) ) flatMap
                        //
                        parserDeclCmdResponse ) match {

                            case Success( DeclCmdResponseSuccess( r ) ) ⇒
                                //  x is a list f declarations
                                r.toSet shouldBe x.typeDefs

                            case Success( e ) ⇒ fail( new Exception( "GetDecl failed " + e ) )

                            case Failure( e ) ⇒ fail( e )
                        }

                    //  assert another term
                    val res2 = |= ( y > 4 )

                    ( eval( Seq( GetDeclCmd() ) ) flatMap
                        //
                        parserDeclCmdResponse ) match {

                            case Success( DeclCmdResponseSuccess( r ) ) ⇒
                                //  x is a list f declarations
                                r.toSet shouldBe x.typeDefs ++ y.typeDefs

                            case Success( e ) ⇒ fail( new Exception( "GetDecl failed " + e ) )

                            case Failure( e ) ⇒ fail( e )
                        }

                    //  using must return a Try
                    Success( true )
            } shouldBe Success( true )
        }
    }
}

class PopPushCheckSatTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with IntegerArithmetics with Commands with Resources {

    override def suiteName = "Send pop/push and check-sat"

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    //  logger
    private val logger = getLogger( this.getClass )

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LIA
    import scala.util.Success
    import parser.SMTLIB2Syntax.{
        Sat,
        UnSat,
        UnKnown,
        SatResponses,
        GetDeclCmd
    }

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( QF_LIA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA, List() )

    for ( s ← theSolvers ) {

        test( s"[${s.name}] configured with ${initSeq.show} one push commands" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        push()
                        logger.info( "GeDeclaCmd output: {}", eval( Seq( GetDeclCmd() ) ) )
                        val x = Ints( "x" )
                        val y = Ints( "y" )

                        //  assert first term
                        val res1 = |= ( x <= 4 )
                        //  assert another term
                        val res2 = |= ( x > 4 )
                        logger.info( "GeDeclaCmd output: {}", eval( Seq( GetDeclCmd() ) ) )

                        //  checkSat
                        checkSat() shouldBe Success( UnSat() )

                        pop()
                        logger.info( "GeDeclaCmd output: {}", eval( Seq( GetDeclCmd() ) ) )

                        // assert a new term
                        val res3 = |= ( y < x + 1 )
                        logger.info( "GeDeclaCmd output: {}", eval( Seq( GetDeclCmd() ) ) )

                        checkSat()

                    }
            } shouldBe Success( Sat() )
        }
    }
}
