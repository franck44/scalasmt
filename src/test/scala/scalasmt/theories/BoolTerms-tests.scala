/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks

class BoolOperatorsTests extends FunSuite with TableDrivenPropertyChecks with Matchers with Core with IntegerArithmetics {

    override def suiteName = "Check the termDef built using the boolean  operators on TypedTerms"

    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm

    val x = Ints( "x" )
    val y = Ints( "y" )

    val c = Bools( "c" ) // would like to use a but this a value of Matcher
    val b = Bools( "b" )
    val d = Bools( "d" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ], String ](
        ( "expression"            , "TypedTerm"                  , "Solver string" ),
        ( "True"                  , Bools( true )                , "true" ),
        ( "False"                 , Bools( false )               , "false" ),
        ( "x == 1 && y < x"       , x === 1 & y < x              , "(and (= x 1) (< y x))" ),
        ( "x == 1 || y < x"       , x === 1 | y < x              , "(or (= x 1) (< y x))" ),
        ( "x == 1 imply y < x"    , x === 1 imply y < x          , "(=> (= x 1) (< y x))" ),
        ( "b || c || d || y < x"  , b | c | d | y < x            , "(or b (< y x) d c)" ),
        ( "b || c || d"           , b | c | d                    , "(or b d c)" ),
        ( "b & c & d"             , b & c & d                    , "(and b d c)" ),
        ( "x > 1 || b & c & d"    , x > 1 | b & c & d            , "(or (> x 1) (and b d c))" ),
        ( "b || (c & d)"          , b | (c & d)                  , "(or b (and c d))" ),
        ( "b_1 || (c & d)"        , (b indexed 1) | (c & d)      , "(or b@1 (and c d))" ),
        ( "b & (c || d)"          , b & (c | d)                  , "(and b (or c d))" ),
        ( "x <= 2 & b & (c || d)" , x <= 2 & b & (c | d)         , "(and (<= x 2) (or c d) b)"),
        ( "True | False"          , Bools( true ) | Bools(false) , "(or true false)" )
    )
    //  format: ON

    for ( ( x, t, s ) ← theTerms ) {
        test( s"Check string produced by: $x -- Should be $s" ) {
            format( t.termDef ).layout shouldBe s
        }
    }
}

class BoolTypeTests extends FunSuite with TableDrivenPropertyChecks with Matchers with Core with IntegerArithmetics {

    override def suiteName = "Check the typeDefs built using the boolean operators on TypedTerms"

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{
        Term,
        QualifiedId,
        SortedQId,
        SSymbol,
        SymbolId,
        BoolSort,
        IntSort
    }
    import parser.Implicits._
    import typedterms.TypedTerm

    def makeBool : String ⇒ SortedQId = { x ⇒ SortedQId( SymbolId( SSymbol( x ) ), BoolSort() ) }
    def makeInt : String ⇒ SortedQId = { x ⇒ SortedQId( SymbolId( SSymbol( x ) ), IntSort() ) }

    val x = Ints( "x" )
    val y = Ints( "y" )

    val c = Bools( "c" ) // would like to use a but this a value of Matcher
    val b = Bools( "b" )

    //  FORMAT: OFF
    val theTerms1 = Table[ String, TypedTerm[ BoolTerm, Term ], Set[ SortedQId ] ](
        ( "Expression", "TypedTerm", "typeDefs" ),
        ( "True", Bools( true ), Set() ),
        ( "b (Bool)", b, Set( "b" ).map( makeBool( _ ) ) ),
        ( "c || b ", c | b, Set( "c", "b" ).map( makeBool( _ ) ) ),
        ( "c && b ", c & b, Set( "c", "b" ).map( makeBool( _ ) ) ),

        ( "c imply b (Bool)", c imply b, Set( "c", "b" ).map( makeBool( _ ) ) ),
        ( "x < y imply b", x < y imply b,
            Set( "b" ).map( makeBool( _ ) ) ++ Set( "x", "y" ).map( makeInt( _ ) ) ),

        ( "b imply x < y", b imply y < x,
            Set( "b" ).map( makeBool( _ ) ) ++ Set( "x", "y" ).map( makeInt( _ ) ) ),

        ( "x < y || b", x < y | b,
            Set( "b" ).map( makeBool( _ ) ) ++ Set( "x", "y" ).map( makeInt( _ ) ) ),

        ( "b || x < y", b | x < y,
            Set( "b" ).map( makeBool( _ ) ) ++ Set( "x", "y" ).map( makeInt( _ ) ) ),

        ( "x < y && b", x < y & b,
            Set( "b" ).map( makeBool( _ ) ) ++ Set( "x", "y" ).map( makeInt( _ ) ) ),

        ( "b && x < y", b & x < y,
            Set( "b" ).map( makeBool( _ ) ) ++ Set( "x", "y" ).map( makeInt( _ ) ) ),

        ( "False", Bools( false ), Set() ) )
    // FORMAT: ON
    for ( ( e, t, tdef ) ← theTerms1 ) {
        val result = tdef.map( x ⇒ show( x ) )
        test( s"Check the typeDefs built with for term $e -- Should be $result" ) {
            t.typeDefs shouldBe tdef
        }
    }
}
