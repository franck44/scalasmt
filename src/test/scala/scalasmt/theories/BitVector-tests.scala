/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

class BitVectorsFactoryTypeTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with BitVectors {

    override def suiteName = "Check the typeDefs of BitVector terms"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax.{
        Term,
        QualifiedId,
        SortedQId,
        BitVectorSort,
        SSymbol,
        ISymbol,
        SymbolId
    }
    import parser.SMTLIB2PrettyPrinter.{ format, show }
    import typedterms.TypedTerm

    //  create a parser SortedQId, type Ints, from a string
    def toSortedQId( bits : Int )( name : String ) : SortedQId =
        SortedQId( SymbolId( SSymbol( name ) ), BitVectorSort( bits.toString ) )

    def toIndexedSortedQId( bits : Int )( name : String, index : Int ) : SortedQId =
        SortedQId( SymbolId( ISymbol( name, index ) ), BitVectorSort( bits.toString ) )

    val x = BVs( "x", 16 )
    val y = BVs( "y", 32 )
    val y1 = y indexed 1

    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ BVTerm, Term ], Set[ SortedQId ] ](
        ( "Expression"       , "TypedTerm"                  , "typeDefs" )                          ,
        ( "0 - 1"            , BVs(0,16)                    , Set() )                               ,
        ( "- 1"              , -BVs( 1,16 )                 , Set() )                               ,
        ( "- 2"              , (-2).withBits(16 )           , Set() )                               ,
        ( "#xa9 + 1"         , BVs( "#xa9" ) + BVs(1,16)    , Set() )                               ,
        ( "#xa9 + #b101"     , BVs( "#xa9" ) + BVs("#b101") , Set() )                               ,
        ( "x"                , x                            , Set( "x" ).map( toSortedQId(16) ) )   ,
        ( "y1"               , y1                           , Set( toIndexedSortedQId(32)("y",1) ) ),
        ( "x + y"            , x + y                        , Set( toSortedQId(16)("x"), toSortedQId(32)("y")  ) ) ,
        ( "x - y"            , x - y                        , Set( toSortedQId(16)("x"), toSortedQId(32)("y")  ) ) ,
        ( "x + y + #xaf0"    , x + y + BVs("#xaf0")         , Set( toSortedQId(16)("x"), toSortedQId(32)("y")  ) ) ,
        ( "#b101 + x + y"    , x + y + BVs("#b101")         , Set( toSortedQId(16)("x"), toSortedQId(32)("y")  ) ) ,
        ( "x / 2"            , x / BVs(2,16)                , Set( "x" ).map( toSortedQId(16) ) )   ,
        ( "x sdiv 2"         , x sdiv BVs(2,16)             , Set( "x" ).map( toSortedQId(16) ) )   ,
        ( "x % 2"            , x % BVs(2,16)                , Set( "x" ).map( toSortedQId(16) ) )   ,
        ( "x srem 2"         , x srem BVs(2,16)             , Set( "x" ).map( toSortedQId(16) ) )   ,
        ( "x | y + #b101"    , x or y +  BVs("#b101")       , Set( toSortedQId(16)("x"),  toSortedQId(32)("y")  ) ) ,
        ( "x ^ y + #b101"    , x xor y +  BVs("#b101")      , Set( toSortedQId(16)("x"),  toSortedQId(32)("y")  ) ) ,
        ( "x & y + #b101"    , x and y +  BVs("#b101")      , Set( toSortedQId(16)("x") , toSortedQId(32)("y")  ) ) ,
        ( "x * y"            , x * y                        , Set( toSortedQId(16)("x") , toSortedQId(32)("y")  ) ) ,
        ( "x << y"           , x << y                       , Set( toSortedQId(16)("x") , toSortedQId(32)("y")  ) ) ,
        ( "x >> y"           , x >> y                       , Set( toSortedQId(16)("x") , toSortedQId(32)("y")  ) ) ,
        ( "x ashr y"         , x ashr y                     , Set( toSortedQId(16)("x") , toSortedQId(32)("y")  ) ) ,
        ( "x sext 5"         , x sext 5                     , Set( toSortedQId(16)("x") ) ) ,
        ( "x zext 5"         , x zext 5                     , Set( toSortedQId(16)("x") ) ) ,
        ( "x extract (4, 8)" , x extract (4, 8)             , Set( toSortedQId(16)("x") ) ),
        ( "concat x y "      , x concat y                   , Set( toSortedQId(16)("x"), toSortedQId(32)("y") ) )
    )
    //  format: ON

    for ( ( e, t, tdef ) ← theTerms1 ) {
        val result = tdef.map( x ⇒ show( x ) )
        test( s"Check the typeDefs built with for term $e -- Should be $result" ) {
            t.typeDefs shouldBe tdef
        }
    }
}

class BitVectorsFactoryTermTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with BitVectors {

    override def suiteName = "Check the termDef of BitVector terms"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax._
    import typedterms.TypedTerm

    val x = BVs( "x", 16 )
    val y = BVs( "y", 32 )
    val y1 = y indexed 1

    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ BVTerm, Term ], Term ](
        ( "Expression"    , "TypedTerm"                   , "termDefs" ),
        ( "0"         , BVs( 0,16)                         , ConstantTerm(DecBVLit(BVvalue("0"),"16"))),
        ( "- 1"           , -BVs( 1,16 )                  , BVNegTerm(
                                                                ConstantTerm(DecBVLit(BVvalue("1"),"16")))
        ),
        ( "- 2"           , -2.withBits( 16 )              , BVNegTerm(
                                                                ConstantTerm(DecBVLit(BVvalue("2"),"16")))
        ),
        ( "-2147483648 over 32 bits"  , (-2147483648).withBits( 32 )              ,
                                                                ConstantTerm(BinLit((1::List.fill(31)(0)).mkString))
        ),
        ( "-2147483647 over 32 bits"  , (-2147483647).withBits( 32 )              , BVNegTerm(
                                                                ConstantTerm(DecBVLit(BVvalue("2147483647"),"32")))

        ),
        ( "-128 over 8 bits"  , (-128).withBits( 8 )              ,
                                                                ConstantTerm(BinLit("10000000"))

        ),
        ( "-64 over 7 bits"  , (-64).withBits( 7 )              ,
                                                                ConstantTerm(BinLit("1000000"))

        ),
        ( "-63 over 7 bits"  , (-63).withBits( 7 )              ,
                                                                BVNegTerm(ConstantTerm(DecBVLit(BVvalue("63"),"7")))

        ),
        ( "63 over 7 bits"  , (63).withBits( 7 )              ,
                                                                ConstantTerm(DecBVLit(BVvalue("63"),"7"))

        ),
        ( "#xa9 + 1"      , BVs( "#xa9" ) + BVs(1,16)            , BVAddTerm(
                                                            ConstantTerm(
                                                                HexaLit("a9")),
                                                                ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                            )
        ),
        ( "#xa9 + #b101"  , BVs( "#xa9" ) + BVs("#b101") , BVAddTerm(
                                                            ConstantTerm(
                                                                HexaLit("a9")),
                                                                ConstantTerm(BinLit("101"))
                                                            )
        ),
        ( "x"             , x                            , QIdTerm(
                                                            SimpleQId(SymbolId(SSymbol("x"))))
        ),
        ( "y1"            , y1                           , QIdTerm(
                                                            SimpleQId(SymbolId(ISymbol("y",1))))
        ),
        ( "x + y"         , x + y                        , BVAddTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(SSymbol("x")))),
                                                                    QIdTerm(SimpleQId(SymbolId(SSymbol("y"))))
                                                            )
        ),
        ( "x - y"         , x - y                        , BVSubTerm(
                                                            QIdTerm(
                                                                SimpleQId(SymbolId(SSymbol("x")))),
                                                                QIdTerm(SimpleQId(SymbolId(SSymbol("y"))))
                                                            )
        ),
        ( "x + y + #xaf0" , x + y + BVs("#xaf0")         , BVAddTerm(
                                                            BVAddTerm(
                                                                QIdTerm(
                                                                    SimpleQId(SymbolId(SSymbol("x")))),
                                                                    QIdTerm(SimpleQId(SymbolId(SSymbol("y"))))
                                                            ),
                                                            ConstantTerm(HexaLit("af0"))
                                                           )
        ),
        ( "#b101 + x + y" , x + y + BVs("#b101")         , BVAddTerm(
                                                            BVAddTerm(
                                                                QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(SSymbol("x"))
                                                                    )
                                                                ),
                                                                QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(
                                                                            SSymbol("y")))
                                                                        )
                                                                    ),
                                                                    ConstantTerm(BinLit("101"))
                                                                )
        ),
        ( "x / 2"         , x / BVs(2,16)                        , BVuDivTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(SSymbol("x")))
                                                                ),
                                                                ConstantTerm(DecBVLit(BVvalue("2"),"16"))
                                                            )
        ),
        ( "x sdiv 2"      , x sdiv BVs(2,16)                     , BVsDivTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(SSymbol("x")))
                                                                ),
                                                                ConstantTerm(DecBVLit(BVvalue("2"),"16"))
                                                            )
        ),
        ( "x % 2"         , x % BVs(2,16)                        , BVuRemTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                ),
                                                                ConstantTerm(DecBVLit(BVvalue("2"),"16"))
                                                            )
        ),
        ( "x srem 2"      , x srem BVs(2,16)                     , BVsRemTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                ),
                                                                ConstantTerm(DecBVLit(BVvalue("2"),"16"))
                                                            )
        ),
        ( "x | y + #b101" , x or y +  BVs("#b101")        , BVOrTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                ),
                                                                BVAddTerm(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    ),
                                                                    ConstantTerm(BinLit("101")))
                                                                )
        ),
        ( "x ^ y + #b101" , x xor y +  BVs("#b101")       , BVXorTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                ),
                                                                BVAddTerm(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(SSymbol("y"))
                                                                        )
                                                                    ),
                                                                    ConstantTerm(BinLit("101")))
                                                                )
        ),
        ( "x & y + #b101" , x and y +  BVs("#b101")        , BVAndTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x")
                                                                    )
                                                                )
                                                            ),
                                                            BVAddTerm(
                                                                QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(
                                                                            SSymbol("y"))
                                                                        )
                                                                    ),
                                                                    ConstantTerm(BinLit("101"))
                                                                )
                                                            )
        ),
        ( "x * y"         , x * y                        , BVMultTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x")
                                                                    )
                                                                )
                                                            ),
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(SSymbol("y")))
                                                                )
                                                            )
        ),
        ( "x << y"        , x << y                       , BVShiftLeftTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x")
                                                                    )
                                                                )
                                                            ),
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(SSymbol("y"))
                                                                )
                                                            )
                                                        )
        ),
        ( "x >> y"        , x >> y                       , BVSLogicalShiftRightTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                ),
                                                                QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(
                                                                            SSymbol("y")
                                                                        )
                                                                    )
                                                                )
                                                            )
        ),
        ( "x ashr y"        , x ashr y                  , BVSArithmeticShiftRightTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                ),
                                                                QIdTerm(
                                                                    SimpleQId(
                                                                        SymbolId(
                                                                            SSymbol("y")
                                                                        )
                                                                    )
                                                                )
                                                            )
        ),
        ( "x sext 5"        , x sext 5                 , BVSignExtendTerm(
                                                            NumLit("5"),
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                )
                                                            )
        ),
        ( "x zext 5"        , x zext 5                 , BVZeroExtendTerm(
                                                            NumLit("5"),
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                )
                                                            )
        ),
        ( "x extract (4, 8)", x extract (4, 8)         , BVExtractTerm(
                                                            NumLit("4"),
                                                            NumLit("8"),
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                )
                                                            )
        ),
        ( "x concat (bv2 16)"  ,   x concat BVs(2,16)          , BVConcatTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                ),
                                                                ConstantTerm(DecBVLit(BVvalue("2"),"16"))
                                                            )
        )
    )
    //  format: ON

    for ( ( e, t, tdef ) ← theTerms1 ) {
        test( s"Check the termDef built with for term $e -- Should be $tdef" ) {
            t.termDef shouldBe tdef
        }
    }

    //  Tests for exceptions when creating BVs
    test( s"Cannot build termDef when Int is too small for number of bits (signed)" ) {
        the[ Exception ] thrownBy {
            ( -2147483647 ).withBits( 20 )
        } should have message "requirement failed: -2147483647 cannot be encoded on 20 signed bits"
    }

    test( s"Cannot build termDef when Int is too large for number of bits (unsigned)" ) {
        the[ Exception ] thrownBy {
            2.withUBits( 1 )
        } should have message "requirement failed: 2 cannot be encoded on 1 unsigned bits"
    }

    test( s"Cannot build termDef when Int is too large for number of bits (signed)" ) {
        the[ Exception ] thrownBy {
            2147483647.withBits( 20 )
        } should have message "requirement failed: 2147483647 cannot be encoded on 20 signed bits"
    }

}

class BitVectorsFactoryShowTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with BitVectors {

    override def suiteName = "Check the pretty-printing of BitVector terms"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax.{ QualifiedId, SortedQId, BitVectorSort, Term }
    import parser.SMTLIB2PrettyPrinter.{ format, show }
    import typedterms.TypedTerm

    val x = BVs( "x", 16 )
    val y = BVs( "y", 32 )
    val y1 = y indexed 1

    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ BVTerm, Term ], String ](
        ( "Expression"      , "TypedTerm"                  , "solver string" ),
        ( "0"               , BVs( 0, 16 )                 , "(_ bv0 16)"),
        ( "- 1"             , -BVs( 1, 16 )                , "(bvneg (_ bv1 16))"),
        ( "- 2"             , -2.withBits( 16 )            , "(bvneg (_ bv2 16))"),
        ( "1 1"             , 1.withUBits( 1 )             , "(_ bv1 1)"),
        ( "1 1"             , 1.withUBits( 32 )            , "(_ bv1 32)"),
        ( "1 1"             , 1.withUBits( 64 )            , "(_ bv1 64)"),
        ( "-2147483647 32"  , (-2147483647).withBits( 32 ) , "(bvneg (_ bv2147483647 32))"),
        ( "-2147483648 32"  , (-2147483648).withBits( 32 ) , "#b" + (1::List.fill(31)(0)).mkString),
        ( "2147483646 32"   , 2147483646.withBits( 32 )    , "(_ bv2147483646 32)"),
        ( "-64 7"           , -64.withBits( 7 )            , "#b1000000"),
        ( "-63 7"           , -63.withBits( 7 )            , "(bvneg (_ bv63 7))"),
        ( "63 7"            , 63.withBits( 7 )             , "(_ bv63 7)"),
        ( "#xa9 + 1"        , BVs( "#xa9" ) + BVs(1,16)    , "(bvadd #xa9 (_ bv1 16))"),
        ( "#xa9 + #b101"    , BVs( "#xa9" ) + BVs("#b101") , "(bvadd #xa9 #b101)"),
        ( "x"               , x                            , "x"),
        ( "y1"              , y1                           , "y@1"),
        ( "x + y"           , x + y                        , "(bvadd x y)"),
        ( "x - y"           , x - y                        , "(bvsub x y)"),
        ( "x + y + #xaf0"   , x + y + BVs("#xaf0")         , "(bvadd (bvadd x y) #xaf0)"),
        ( "#b101 + x + y"   , x + y + BVs("#b101")         , "(bvadd (bvadd x y) #b101)"),
        ( "x / 2"           , x / BVs(2,16)                , "(bvudiv x (_ bv2 16))"),
        ( "x sdiv 2"        , x sdiv BVs(2,16)             , "(bvsdiv x (_ bv2 16))"),
        ( "x % 2"           , x % BVs(2,16)                , "(bvurem x (_ bv2 16))"),
        ( "x srem 2"        , x srem BVs(2,16)             , "(bvsrem x (_ bv2 16))"),
        ( "x | y + #b101"   , x or y +  BVs("#b101")       , "(bvor x (bvadd y #b101))"),
        ( "x ^ y + #b101"   , x xor y +  BVs("#b101")      , "(bvxor x (bvadd y #b101))"),
        ( "x & y + #b101"   , x and y +  BVs("#b101")      , "(bvand x (bvadd y #b101))"),
        ( "x * y"           , x * y                        , "(bvmul x y)"),
        ( "x << y"          , x << y                       , "(bvshl x y)"),
        ( "x >> y"          , x >> y                       , "(bvlshr x y)"),
        ( "x ashr y"        , x ashr y                     , "(bvashr x y)"),
        ( "x sext 5"        , x sext 5                     , "((_ sign_extend 5) x)"),
        ( "x zext 5"        , x zext 5                     , "((_ zero_extend 5) x)"),
        ( "x extract (4, 8)", x extract (4, 8)             , "((_ extract 4 8) x)"),
        ( "x concat y"      , x concat y                   , "(concat x y)")
    )
    //  format: ON

    for ( ( e, t, tdef ) ← theTerms1 ) {
        test( s"Check the typeDefs built with for term $e -- Should be $tdef" ) {
            show( t.termDef ) shouldBe tdef
        }
    }
}

class SimpleBitVectorTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with BitVectors with Commands with Resources {

    override def suiteName = "Check-sat for BitVector terms"

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat, UnKnown, SatResponses }
    import typedterms.TypedTerm

    val x = BVs( "x", 16 )
    val y = BVs( "y", 16 )
    val z = BVs( "z", 32 )

    //  BVs operators are compatible only for same size BVs
    //  format: OFF
    val theLIATerms = Table[ String, TypedTerm[ BoolTerm, Term ], Try[SatResponses] ](
        ( "expression"      , "TypedTerm"                           , "Response" )                                     ,
        ( "simple equality" , x === BVs( "#x01" )                   , Failure(new Exception(" Not same fixed size")) ) ,
        ( "bitvector 0"     , x === BVs( "#x0001" )                 , Success( Sat() ) )                               ,
        ( "#xa0 + #b0101"   , BVs( "#xa0" ) === BVs( "#b10100101" ) , Success( UnSat() ) )                             ,
        ( "-y == 2"         , -y  === 2.withBits(16)                , Success( Sat() ) )                               ,
        ( "x + y == 1"      , x + y  === 1.withBits(16)             , Success( Sat() ) )                               ,
        ( "x - y + z == 1"  , x - y + z  === 1.withBits(16)         , Failure(new Exception("z is not 32 bits")) )     ,
        ( "x * y == 1"      , x * y  === 1.withBits(16)             , Success( Sat() ) )                               ,
        ( "x / y == 1"      , x / y  === 1.withBits(16)             , Success( Sat() ) )                               ,
        ( "x sdiv y == 1"   , (x sdiv y)  === 1.withBits(16)        , Success( Sat() ) )                               ,
        ( "x % y == 2"      , x % y  === 2.withBits(16)             , Success( Sat() ) )                               ,
        ( "x srem y == 2"   , (x srem y)  === 2.withBits(16)        , Success( Sat() ) )                               ,
        ( "x | y == 12"     , (x or y)  === 12.withBits(16)         , Success( Sat() ) )                               ,
        ( "x ^ y == 12"     , (x xor y)  === 12.withBits(16)        , Success( Sat() ) )                               ,
        ( "x & y == 2"      , (x and y)  === 2.withBits(16)         , Success( Sat() ) )                               ,
        ( "x << 2 ==  y"    , (x << 2.withBits(16))  === y          , Success( Sat() ) )                               ,
        ( "x >> 4 == y"     , (x >> 4.withBits(16))  === y          , Success( Sat() ) )                               ,
        ( "x ashr 4 == y"   , (x ashr 4.withBits(16))  === y        , Success( Sat() ) )                               ,
        ( "x >> y ult 4"    , (x >> y ult 4.withBits(16))           , Success( Sat() ) )                               ,
        ( "x >> 2 ule y"    , (x >> y ule 4.withBits(16))           , Success( Sat() ) )                               ,
        ( "x >> 3 uge y"    , (x >> y uge 4.withBits(16))           , Success( Sat() ) )                               ,
        ( "x >> y ugt 4"    , (x >> y ugt 4.withBits(16))           , Success( Sat() ) )                               ,
        ( "x >> y slt 4"    , (x >> y slt 4.withBits(16))           , Success( Sat() ) )                               ,
        ( "x >> 2 sle y"    , (x >> y sle 4.withBits(16))           , Success( Sat() ) )                               ,
        ( "x >> 3 sge y"    , (x >> y sge 4.withBits(16))           , Success( Sat() ) )                               ,
        ( "x >> y sgt 4"    , (x >> y sgt 4.withBits(16))           , Success( Sat() ) )
    )

    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_BV
    import configurations.AppConfig.config
    import configurations.SMTInit

    //  Solvers to be included in the tests. Should support QF_BV
    val theSolvers = Table(
        "Solver",
        config.filter( c ⇒ c.enabled && c.supportsLogic( QF_BV ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_BV )

    for ( s ← theSolvers ) {
        forAll( theLIATerms ) {
            ( x : String, t : TypedTerm[ BoolTerm, Term ], r : Try[ SatResponses ] ) ⇒
                test( s"[${s.name}] configured with ${initSeq.show} check-sat $x i.e. bivector term ${show( t.termDef )}  should be ${r match { case Failure( e ) ⇒ e; case Success( b ) ⇒ b }}" ) {

                    //  with using (monadic)
                    using( new SMTSolver( s, initSeq ) ) {
                        implicit withSolver ⇒
                            isSat( t )
                    } should matchPattern {
                        case x @ Success( _ ) if ( r == x )  ⇒
                        case Failure( _ ) if ( r.isFailure ) ⇒
                    }
                }
        }
    }
}

class GetValueBitVectorTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with BitVectors with Commands with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = getLogger( this.getClass )

    override def suiteName = "Check-sat and get values for BitVector terms"

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.{ QIdTerm, SimpleQId, Term, Sat, UnSat, UnKnown, SatResponses }
    import typedterms.{ TypedTerm, Value }

    val x = BVs( "x", 16 )
    val y = BVs( "y", 16 )
    val z = BVs( "z", 32 )

    //  BVs operators are compatible only for same size BVs
    //  format: OFF
    val theLIATerms = Table[ String, TypedTerm[ BoolTerm, Term ], Try[SatResponses] ](
        ( "expression"       , "TypedTerm"                    , "Response" )       ,
        ( "bitvector 0"      , x === BVs( "#x0001" )          , Success( Sat() ) ) ,
        ( "-y"               , -y  === 2.withBits(16)         , Success( Sat() ) ) ,
        ( "x + y"            , x + y  === 1.withBits(16)      , Success( Sat() ) ) ,
        ( "x * y"            , x * y  === 1.withBits(16)      , Success( Sat() ) ) ,
        ( "x / y"            , x / y  === 1.withBits(16)      , Success( Sat() ) ) ,
        ( "x sdiv y"         , (x sdiv y ) === 1.withBits(16) , Success( Sat() ) ) ,
        ( "x % y"            , x % y  === 2.withBits(16)      , Success( Sat() ) ) ,
        ( "x srem y"         , (x srem y)  === 2.withBits(16) , Success( Sat() ) ) ,
        ( "x | y"            , (x or y)  === 12.withBits(16)  , Success( Sat() ) ) ,
        ( "x ^ y"            , (x xor y)  === 12.withBits(16) , Success( Sat() ) ) ,
        ( "x & y"            , (x and y)  === 2.withBits(16)  , Success( Sat() ) ) ,
        //  Shift left
        ( "x << y"           , (x << 2.withBits(16))  === y   , Success( Sat() ) ) ,
        //  Shift right
        ( "x >> y"           , (x >> 4.withBits(16))  === y   , Success( Sat() ) ) ,
        ( "x ashr y"         , (x ashr 4.withBits(16))  === y , Success( Sat() ) ) ,
        ( "x zext 16 == z"   , (x zext 16)  === z             , Success( Sat() ) ) ,
        ( "x sext 16 == z"   , (x sext 16)  === z             , Success( Sat() ) ) ,
        ( "x == z.extract(17,2)", x === z.extract(17 , 2)     , Success( Sat() ) ) ,
        ( "z == concat x y"  , z === (x concat y)             , Success( Sat() ) )
    )

    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_BV
    import configurations.SMTOptions.SMTProduceModels

    import configurations.AppConfig.config
    import configurations.SMTInit

    //  Solvers to be included in the tests. Should support QF_BV
    val theSolvers = Table(
        "Solver",
        config.filter( c ⇒ c.enabled && c.supportsLogic( QF_BV ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_BV, List( SMTProduceModels( true ) ) )

    for ( s ← theSolvers ) {
        forAll( theLIATerms ) {
            ( name : String, t : TypedTerm[ BoolTerm, Term ], r : Try[ SatResponses ] ) ⇒
                test( s"[${s.name}] configured with ${initSeq.show} check-sat for bivector term ${format( t.termDef ).layout}  can be established" ) {

                    //  with using (monadic)

                    val passed = using( new SMTSolver( s, initSeq ) ) {
                        implicit withSolver ⇒
                            isSat( t ) shouldBe Success( Sat () )

                            //  get variables declared on the stack
                            getDeclCmd() match {
                                case Failure( e ) ⇒ Failure( e )
                                case Success( xl ) ⇒
                                    logger.info( "getDeclCmd  returned {}", xl )
                                    //  make a TypedTerm from the declarations to extract a value
                                    val y = xl
                                        .map( { x ⇒ TypedTerm( Set( x ), QIdTerm( SimpleQId( x.id ) ) ) } )
                                        .map( getValue )
                                    //  y should be a List
                                    y shouldBe a[ List[ _ ] ]
                                    //  each element of y should be a Value
                                    y.foreach { v ⇒
                                        logger.info( s"Collected value is $v" )
                                        v should matchPattern {
                                            case Success( Value( _ ) ) ⇒
                                        }
                                    }
                                    Success( true )

                            }
                    } shouldBe Success( true )
                }
        }
    }
}
