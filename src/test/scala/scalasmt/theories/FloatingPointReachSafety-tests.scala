/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

class FPBVnewton11trueUnreachCall
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with BitVectors
    with FPBitVectors
    with RealArithmetics
    with Commands
    with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName = "newton_1_1_true-unreach-call.c from SVCOMP18 using FPBitVector"

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.{
        RNE,
        QIdTerm,
        SimpleQId,
        Term,
        Sat,
        UnSat,
        SatResponses
    }
    import typedterms.{ TypedTerm, Value }
    import scala.concurrent.duration._
    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_FPBV
    import configurations.SMTOptions.SMTProduceModels

    import configurations.AppConfig.config
    import configurations.SMTInit

    val x1 = "x1".asFloat32
    val v = "v".asFloat32
    val in = "in".asFloat32
    val fx = "fx".asFloat32
    val fpx = "fpx".asFloat32

    //  an implicit roundingMode
    implicit val r = RMs( RNE() )

    //  Solvers to be included in the tests. Should support QF_BV
    val theSolvers = Table(
        "Solver",
        config.filter( c ⇒ c.enabled && c.supportsLogic( QF_FPBV ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_FPBV, List( SMTProduceModels( true ) ) )

    for ( s ← theSolvers ) {
        ignore( s"Checking newton_1_1_true-unreach-call.c with ${s.name}" ) {

            //  with using (monadic)
            using( new SMTSolver( s, initSeq ) ) {
                implicit withSolver ⇒
                    isSat( 100.seconds )(
                        v === 0.2f.asFloat32,
                        in > -v & in < v,
                        // f(x) = x - (x*x*x)/6.0f + (x*x*x*x*x)/120.0f + (x*x*x*x*x*x*x)/5040.0f
                        fx === ( in - ( in * in * in ) / 6.0f.asFloat32 ) +
                            ( ( in * in * in * in * in ) / 120.0f.asFloat32 ) +
                            ( in * in * in * in * in * in * in ) / 5040.0f.asFloat32,
                        fpx === ( 1.0f.asFloat32 - ( ( in * in ) / 2.0f.asFloat32 ) ) +
                            ( ( in * in * in * in ) / 24.0f.asFloat32 ) + ( ( in * in * in * in * in * in ) / 720.0f.asFloat32 ),
                        //  x = in - f(in)/fp(in);
                        x1 === in - ( fx / fpx ),
                        !( x1 < 0.1f.asFloat32 ) )
            } shouldBe Success( UnSat() )
        }
    }
}

class FPBVZonotopeTightTrueUnreachCall
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with BitVectors
    with FPBitVectors
    with RealArithmetics
    with Commands
    with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName =
        """ |
            | zonotope_tight/loose_true-unreach-call.c from SVCOMP18 using FPBitVector
            |
            | extern double __VERIFIER_nondet_double();
            | extern void __VERIFIER_assume(int expression);
            | void __VERIFIER_assert(int cond) { if (!(cond)) { ERROR: __VERIFIER_error(); } return; }

            | int main()
            | {
            | double x,y;

            | x = __VERIFIER_nondet_double();
            | __VERIFIER_assume(x >= 0. && x <= 10.);

            | y = x*x - x;
            | if (y >= 0) y = x / 10.;
            | else y = x*x + 2.;

            | __VERIFIER_assert(y >= 0. && y <= 4.); // y <= 105.0 for the loose version
            | return 0;
            | }
            """.stripMargin

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.{
        RNE,
        QIdTerm,
        SimpleQId,
        Term,
        Sat,
        UnSat,
        SatResponses
    }
    import typedterms.{ TypedTerm, Value }
    import scala.concurrent.duration._
    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_FPBV
    import configurations.SMTOptions.SMTProduceModels

    import configurations.AppConfig.config
    import configurations.SMTInit

    val x = "x".asFloat32
    val y = "y".asFloat32
    val y1 = "y1".asFloat32

    //  an implicit roundingMode
    implicit val r = RMs( RNE() )

    //  Solvers to be included in the tests. Should support QF_BV
    val theSolvers = Table(
        "Solver",
        config.filter( c ⇒ c.enabled && c.supportsLogic( QF_FPBV ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_FPBV, List( SMTProduceModels( true ) ) )

    for ( s ← theSolvers; ub ← List( 4.0f, 105.0f ) ) {
        test( s"Checking zonotope_tight/loose_true-unreach-call.c $ub with ${s.name}" ) {

            //  with using (monadic)
            using( new SMTSolver( s, initSeq ) ) {
                implicit withSolver ⇒
                    isSat( 100.seconds )(
                        x >= 0.0f.asFloat32 & x <= 10.0f.asFloat32,
                        y === x * x - x,
                        ( y >= 0.0f.asFloat32 ).ite(
                            y1 === x / 10.0f.asFloat32,
                            y1 === x * x + 2.0f.asFloat32 ),
                        !( y1 >= 0.0f.asFloat32 & y1 <= ub.asFloat32 ) )
            } shouldBe Success( UnSat() )
        }
    }
}

class FPBVSqrtPolyTrueUnreachCall
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with BitVectors
    with FPBitVectors
    with RealArithmetics
    with Commands
    with Resources {

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.{
        RNE,
        QIdTerm,
        SimpleQId,
        Term,
        Sat,
        UnSat,
        SatResponses
    }
    import typedterms.{ TypedTerm, Value }
    import scala.concurrent.duration._
    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_FPBV
    import configurations.SMTOptions.SMTProduceModels

    import configurations.AppConfig.config
    import configurations.SMTInit

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName =
        """ |
            | sqrt_poly_true-unreach-call.c from SVCOMP18 using FPBitVector
            |
            | extern double __VERIFIER_nondet_double();
            | extern void __VERIFIER_assume(int expression);
            | void __VERIFIER_assert(int cond) { if (!(cond)) { ERROR: __VERIFIER_error(); } return; }
            |
            | double sqrt2 = 1.414213538169860839843750;
            |
            | int main()
            | {
            |   double S, I;
            |
            |   I = __VERIFIER_nondet_double();
            |   __VERIFIER_assume(I >= 1. && I <= 3.);
            |
            |   if (I >= 2.) S = sqrt2 * (1.+(I/2.- 1.)*(.5-0.125*(I/2.-1.)));
            |   else S = 1.+(I-1.) * (.5+(I-1.) * (-.125+(I-1.)*.0625));
            |
            |   __VERIFIER_assert(S >= 1. && S <= 2.);
            |   return 0;
            | }
            """.stripMargin

    val sqrt2 = "sqrt2".asFloat32
    val s = "s".asFloat32
    val i = "i".asFloat32

    //  an implicit roundingMode
    implicit val r = RMs( RNE() )

    //  Solvers to be included in the tests. Should support QF_BV
    val theSolvers = Table(
        "Solver",
        config.filter( c ⇒ c.enabled && c.supportsLogic( QF_FPBV ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_FPBV, List( SMTProduceModels( true ) ) )

    for ( sol ← theSolvers; ub ← List( 4.0f, 105.0f ) ) {
        test( s"Checking sqrt_poly_true-unreach-call.c $ub with ${sol.name}" ) {

            //  with using (monadic)
            using( new SMTSolver( sol, initSeq ) ) {
                implicit withSolver ⇒
                    isSat( 100.seconds )(
                        sqrt2 === 1.414213538169860839843750f.asFloat32,
                        i >= 1.0f.asFloat32 & i <= 3.0f.asFloat32,
                        ( i >= 2.0f.asFloat32 ).ite(
                            s === sqrt2 * ( 1.0f.asFloat32 +
                                ( i / 2.0f.asFloat32 - 1.0f.asFloat32 ) *
                                ( 0.5f.asFloat32 - 0.125f.asFloat32 * ( i / 2.0f.asFloat32 - 1.0f.asFloat32 ) ) ),
                            s === 1.0f.asFloat32 + ( i - 1.0f.asFloat32 ) *
                                ( 0.5f.asFloat32 + ( i - 1.0f.asFloat32 ) *
                                    ( -.0125f.asFloat32 + ( i - 1.0f.asFloat32 ) * 0.0625f.asFloat32 ) ) ),
                        !( s >= 1.0f.asFloat32 & s <= 2.0f.asFloat32 ) )
            } shouldBe Success( UnSat() )
        }
    }
}

class FPBVFloat20TrueUnreachCallTrue
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with BitVectors
    with FPBitVectors
    with RealArithmetics
    with Commands
    with Resources {

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2PrettyPrinter.format
    import parser.SMTLIB2Syntax.{
        RNE,
        QIdTerm,
        SimpleQId,
        Term,
        Sat,
        UnSat,
        SatResponses
    }
    import typedterms.{ TypedTerm, Value }
    import scala.concurrent.duration._
    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_FPBV
    import configurations.SMTOptions.SMTProduceModels

    import configurations.AppConfig.config
    import configurations.SMTInit

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName =
        """ |
            | extern void __VERIFIER_assume(int);
            | extern void __VERIFIER_error(void);
            | extern float __VERIFIER_nondet_float(void);
            | extern double __VERIFIER_nondet_double(void);
            |
            | void bug (float min) {
            |   __VERIFIER_assume(min == 0x1.fffffep-105f);
            |   float modifier = (0x1.0p-23 * (1<<1));
            |   float ulpdiff = min * modifier;
            |
            |   if(!(ulpdiff == 0x1p-126f)) __VERIFIER_error();
            |
            |   return;
            | }
            |
            | void bugBrokenOut (float min) {
            |  __VERIFIER_assume(min == 0x1.fffffep-105f);
            |  float modifier = (0x1.0p-23 * (1<<1));
            |  double dulpdiff = (double)min * (double)modifier;
            |  float ulpdiff = (float)dulpdiff;
            |
            |  if(!(ulpdiff == 0x1p-126f)) __VERIFIER_error();
            |
            |  return;
            | }
            |
            | void bugCasting (double d) {
            |    __VERIFIER_assume(d == 0x1.fffffep-127);
            |
            |    float f = (float) d;
            |
            |    if(!(f == 0x1p-126f)) __VERIFIER_error();
            |
            |    return;
            | }

            | int main (void) {
            |    float f =__VERIFIER_nondet_float();
            |    bug(f);
            |
            |    float g =__VERIFIER_nondet_float();
            |    bugBrokenOut(g);
            |
            |    double d =__VERIFIER_nondet_double();
            |    bugCasting(d);
            |
            |    return 1;
            | }
            |
            """.stripMargin

    //  Solvers to be included in the tests. Should support QF_BV
    val theSolvers = Table(
        "Solver",
        config.filter( c ⇒ c.enabled && c.supportsLogic( QF_FPBV ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_FPBV, List( SMTProduceModels( true ) ) )

    /*
     * float should be 32 bits 23 + 8 + 1
     * double should be 64 bits 52 + 11 + 1
     * 0x1p-126f on 64bits -> 0x3810000000000000
     * 0x1.fffffep-105f on 64bits -> 0x396FFFFFE0000000
     * 0x1.0p-23 on 64bits -> 0x3E80000000000000
     * link{https://llvm.org/docs/LangRef.html#simple-constants}
     * link{https://stackoverflow.com/questions/36006770/when-do-c-and-c-compilers-convert-or-promote-a-float-to-double-implicitly?noredirect=1&lq=1}
     */
    for ( sol ← theSolvers ) {
        test( s"Checking bug part of Float20TrueUnrachCallTrue.c with ${sol.name}" ) {

            //  an implicit roundingMode
            implicit val r = RMs( RNE() )

            //  with using (monadic)
            using( new SMTSolver( sol, initSeq ) ) {
                implicit withSolver ⇒
                    //  bug(f)
                    val min = "min".asFloat32
                    val modifier = "modifier".asFloat32
                    val ulpdiff = "ulpdiff".asFloat32

                    isSat( 10.seconds )(
                        min === BVs( "#x396FFFFFE0000000" )
                            .bitStringToFPBV( 11, 53 )
                            .toFPBV( 8, 24 ) & // 0x1.fffffep-105f

                            modifier === ( BVs( "#x3E80000000000000" ).bitStringToFPBV( 11, 53 ) *
                                Reals( 2.0 ).toFPBV( 11, 53 ) ).toFPBV( 8, 24 ) & // 0x1.0p-23 * (1 << 1)
                                ulpdiff === min * modifier &
                                !( ulpdiff.toFPBV( 11, 53 ) fpeq BVs( "#x3810000000000000" ).bitStringToFPBV( 11, 53 ) ) // 0x1p-126f  ->  0x1p-126f
                    )
            } shouldBe Success( UnSat() )
        }

        test( s"Checking bugBrokenOut part of Float20TrueUnrachCallTrue.c with ${sol.name}" ) {

            //  an implicit roundingMode
            implicit val r = RMs( RNE() )

            //  with using (monadic)
            using( new SMTSolver( sol, initSeq ) ) {
                implicit withSolver ⇒
                    val min = "min".asFloat32
                    val modifier = "modifier".asFloat32
                    val ulpdiff = "ulpdiff".asFloat32
                    val dulpdiff = "dulpdiff".asFloat64

                    isSat( 10.seconds )(
                        min === BVs( "#x396FFFFFE0000000" )
                            .bitStringToFPBV( 11, 53 )
                            .toFPBV( 8, 24 ) & // 0x1.fffffep-105f

                            modifier === ( BVs( "#x3E80000000000000" ).bitStringToFPBV( 11, 53 ) *
                                Reals( 2.0 ).toFPBV( 11, 53 ) ).toFPBV( 8, 24 ) & // 0x1.0p-23 * (1 << 1)
                                dulpdiff === min.toFPBV( 11, 53 ) * modifier.toFPBV( 11, 53 ) &
                                ulpdiff === dulpdiff.toFPBV( 8, 24 ) &
                                !( ulpdiff.toFPBV( 11, 53 ) fpeq BVs( "#x3810000000000000" ).bitStringToFPBV( 11, 53 ) ) // 0x1p-126f  ->  0x1p-126f
                    )
            } shouldBe Success( UnSat() )
        }

        test( s"Checking bugCasting part of Float20TrueUnrachCallTrue.c with ${sol.name}" ) {

            //  an implicit roundingMode
            implicit val r = RMs ( RNE() )

            //  with using (monadic)
            using( new SMTSolver( sol, initSeq ) ) {
                implicit withSolver ⇒
                    val d = "d".asFloat32
                    val f = "f".asFloat32

                    isSat( 10.seconds )(
                        d === BVs( "#x380FFFFFE0000000" )
                            .bitStringToFPBV( 11, 53 )
                            .toFPBV( 8, 24 ) & // 0x1.fffffep-105f
                            f === d &
                            !( f.toFPBV( 11, 53 ) fpeq BVs( "#x3810000000000000" ).bitStringToFPBV( 11, 53 ) ) // 0x1p-126f  ->  0x1p-126f
                    )
            } shouldBe Success( UnSat() )
        }
    }
}
