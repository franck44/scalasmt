/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

class IntsComparisonTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with IntegerArithmetics with Resources {

    override def suiteName = "Check the termDef of integer arithmetic comparison"

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.Term
    import typedterms.TypedTerm

    val x = Ints( "x" )
    val y = Ints( "y" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ], String ](
        ( "expression" , "TypedTerm"           , "Solver string" )    ,
        ( "x <= y"     , x <= y                , "(<= x y)" )       ,
        ( "1 <= y"     , Ints(1) <= y          , "(<= 1 y)" )       ,
        ( "x <= 1"     , x <= 1                , "(<= x 1)" )       ,
        ( "x < y"      , x < y                 , "(< x y)" )        ,
        ( "x > y"      , x > y                 , "(> x y)" )        ,
        ( "x >= y"     , x >= y                , "(>= x y)" )       ,
        ( "x == y"     , x === y               , "(= x y)" )        ,
        ( "x != y"     , x =/= y               , "(distinct x y)" ) ,
        ( "1 == 0"     , Ints(1) === Ints(0)   , "(= 1 0)" )        ,
        ( "1 != 0"     , Ints(1) =/= Ints(0)   , "(distinct 1 0)" ) ,
        ( "1 == x"     , Ints(1) === x         , "(= 1 x)" )        ,
        ( "1 != x"     , Ints(1) =/= x         , "(distinct 1 x)" ) ,
        ( "0 < 1"      , Ints( 0 ) < Ints( 1 ) , "(< 0 1)" )
    )
    //  format: ON

    for ( ( x, t, s ) ← theTerms ) {
        test( s"Check string produced by: $x -- Should be $s" ) {
            show( t.termDef ) shouldBe s
        }
    }
}

class LinearIntsArithmeticTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with IntegerArithmetics with Commands with Resources {

    override def suiteName = "Check the termDef and solvers' compatibility for LIA terms"

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat, UnKnown }

    import typedterms.TypedTerm

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    //  format: OFF
    val theLIATerms = Table[ String, TypedTerm[ IntTerm, Term ], String ](
        ( "expression"          , "TypedTerm"           , "Solver string" )             ,
        ( "integer 0"           , Ints( 0 )             , "0" )                        ,
        ( "1 + 2"               , Ints( 1 ) + Ints( 2 ) , "(+ 1 2)" )                 ,
        ( "integer variable x"  , x                     , "x" )                        ,
        ( "indexed variable x"  , x indexed 1           , "x@1" )                      ,
        ( "-x"                  , -x                    , "(- x)" )                   ,
        ( "x + y"               , x + y                 , "(+ x y)" )                 ,
        ( "x - y + z"           , x - y + z             , "(+ (- x y) z)" )          ,
        ( "x + y + z"           , x + y + z             , "(+ (+ x y) z)" )          ,
        ( "x + y + z + 1"       , x + y + z + 1         , "(+ (+ (+ x y) z) 1)" )   ,
        ( "(x + y) + (z + 1)"   , ( x + y ) + ( z + 1 ) , "(+ (+ x y) (+ z 1))" )   ,
        ( "x - ( x + z )"       , x - ( y + z )         , "(- x (+ y z))" )          ,
        ( "abs(x)"              , absI(x)               , "(abs x)" )
    )

    //  modulus and div partially supported (CVC4 does not support them)
    val theModDivTerms = Table[ String, TypedTerm[ IntTerm, Term ], String ](
        ( "expression"          , "TypedTerm"           , "Solver string" )      ,
        ( "x % 3"               , x % 3                 , "(mod x 3)" )        ,
        ( "x_3 % 3"             , (x indexed 3) % 3     , "(mod x@3 3)" )      ,
        ( "x / 5"               , x / 5                 , "(div x 5)" )
    )
    //  format: ON

    //  check the strings produced by the prettyPrinter
    for ( ( x, t, s ) ← theLIATerms ++ theModDivTerms ) {
        test( s"Check string produced by: $x -- Should be $s" ) {
            show( t.termDef ) shouldBe s
        }
    }

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LIA
    import configurations.SMTOptions.MODELS
    import scala.util.{ Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( s ⇒ s.enabled && s.supportsLogic( QF_LIA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA, List() )

    for ( s ← theSolvers; ( x, t, _ ) ← theLIATerms ) {

        test( s"[${s.name}] configured with ${initSeq.show} to check that check-sat for term ${show( t.termDef )} === 1 can be established" ) {

            //  with using (monadic)
            using( new SMTSolver( s, initSeq ) ) {
                implicit withSolver ⇒
                    isSat( t === 1 )
            } should matchPattern {
                case Success( Sat() )   ⇒
                case Success( UnSat() ) ⇒
            }
        }
    }

    //  except CVC4 for Mod/Div terms
    for ( s ← theSolvers; ( x, t, _ ) ← theModDivTerms ) {

        if ( s.name.startsWith( "CVC4" ) )
            test( s"[${s.name}] configured with ${initSeq.show} to check that check-sat for term ${show( t.termDef )} === 1 -- operator not supported by ${s.name}" ) {

                //  with using
                using( new SMTSolver( s, initSeq ) ) {
                    implicit withSolver ⇒
                        isSat( t === 1 )
                } should matchPattern {
                    case Failure( _ ) ⇒
                }
            }
        else
            test( s"[${s.name}] configured with ${initSeq.show} to check that check-sat for term ${show( t.termDef )} === 1 can be established" ) {

                //  with using
                using( new SMTSolver( s, initSeq ) ) {
                    implicit withSolver ⇒
                        isSat( t === 1 )
                } should matchPattern {
                    case Success( Sat() )   ⇒
                    case Success( UnSat() ) ⇒
                }
            }
    }

    for ( s ← theSolvers ) {
        if ( s.name.startsWith( "Z3" ) ) {
            test( s"[${s.name}] configured with ${initSeq.show} to check that check-sat for term x rem 6 === 1 can be established" ) {
                //  with using
                using( new SMTSolver( s, initSeq ) ) {
                    implicit withSolver ⇒
                        isSat( ( x rem 6 ) === 1 )
                } should matchPattern {
                    case Success( Sat() ) ⇒
                }
            }
        } else {
            test( s"[${s.name}] configured with ${initSeq.show} to check that check-sat for term x rem 6 === 1 can be established -- not supported by ${s.name}" ) {
                //  with using
                using( new SMTSolver( s, initSeq ) ) {
                    implicit withSolver ⇒
                        isSat( ( x rem 6 ) === 1 )
                } should matchPattern {
                    case Failure( _ ) ⇒
                }
            }
        }
    }
}

class IntsFactoryTypeTests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with IntegerArithmetics {

    override def suiteName = "Check the typeDefs of Integer Arithmetics terms"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax.{
        QualifiedId,
        SortedQId,
        SSymbol,
        ISymbol,
        SymbolId,
        IntSort,
        Term
    }
    import parser.SMTLIB2PrettyPrinter.format
    import typedterms.TypedTerm

    //  create a solver SortedQId, type Ints, from a string
    def toSortedQId( name : String ) : SortedQId = SortedQId( SymbolId( SSymbol( name ) ), IntSort() )
    def toIndexedSortedQId( name : String, index : Int ) : SortedQId = SortedQId( SymbolId( ISymbol( name, index ) ), IntSort() )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val y1 = y indexed 1

    //  format: OFF
    val theTerms1 = Table[ String, TypedTerm[ IntTerm, Term ], Set[ SortedQId ] ](
        ( "Expression" , "TypedTerm"       , "typeDefs" )                          ,
        ( "0 - 1"      , Ints( 0 ) - 1     , Set() )                               ,
        ( "- 1"        , -Ints( 1 )        , Set() )                               ,
        ( "2 * 1"      , Ints( 2 ) * 1     , Set() )                               ,
        ( "2 / 1"      , Ints( 2 ) / 1     , Set() )                               ,
        ( "0 + 2"      , Ints( 0 ) + 2     , Set() )                               ,
        ( "x"          , x                 , Set( "x" ).map( toSortedQId ) )       ,
        ( "y1"         , y1                , Set( toIndexedSortedQId("y", 1) ) )   ,
        ( "x + y"      , x + y             , Set( "x", "y" ).map( toSortedQId ) )  ,
        ( "x - y"      , x - y             , Set( "x", "y" ).map( toSortedQId ) )  ,
        ( "x + y + 0"  , x + y + Ints( 0 ) , Set( "x", "y" ).map( toSortedQId ) )  ,
        ( "0 + x + y"  , Ints( 0 ) + x + y , Set( "x", "y" ).map( toSortedQId ) )  ,
        ( "x / 2"      , x / 2             , Set( "x" ).map( toSortedQId ) )       ,
        ( "x * y"      , x * y1            , Set( toSortedQId("x"), toIndexedSortedQId("y", 1)) ),
        ( "x + y / 2"  , x + y / 2         , Set( "x", "y" ).map( toSortedQId ) )  ,
        ( "x + 2 / y"  , (x + 2) / y       , Set( "x", "y" ).map( toSortedQId ) )  ,
        ( "x + 2 % y"  , (x + 2) % y       , Set( "x", "y" ).map( toSortedQId ) )  ,
        ( "x + y % 2"  , x + y % 2         , Set( "x", "y" ).map( toSortedQId ) )
    )
    //  format: ON

    //  format: OFF
    val theTerms2 = Table[ String, TypedTerm[ BoolTerm, Term ], Set[ SortedQId ] ](
        ( "Expression" , "TypedTerm"     , "typeDefs" )                    ,

        ( "1 < y"      , Ints( 1 ) < y   , Set( "y" ).map( toSortedQId ) ) ,
        ( "1 <= y"     , Ints( 1 ) <= y  , Set( "y" ).map( toSortedQId ) ) ,
        ( "1 > y"      , Ints( 1 ) > y   , Set( "y" ).map( toSortedQId ) ) ,
        ( "1 >= y"     , Ints( 1 ) >= y  , Set( "y" ).map( toSortedQId ) ) ,
        ( "1 === y"    , Ints( 1 ) === y , Set( "y" ).map( toSortedQId ) ) ,

        ( "y < 1"      , y < Ints( 1 )   , Set( "y" ).map( toSortedQId ) ) ,
        ( "y <= 1"     , y <= Ints( 1 )  , Set( "y" ).map( toSortedQId ) ) ,
        ( "y > 1"      , y > Ints( 1 )   , Set( "y" ).map( toSortedQId ) ) ,
        ( "y >= 1"     , y >= Ints( 1 )  , Set( "y" ).map( toSortedQId ) ) ,
        ( "y === 1"    , y === Ints( 1 ) , Set( "y" ).map( toSortedQId ) ) ,
        ( "y =/= 1"    , y =/= Ints( 1 ) , Set( "y" ).map( toSortedQId ) ) ,

        ( "x < y"      , x < y           , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x <= y"     , x <= y          , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x > y"      , x > y           , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x >= y"     , x >= y          , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x === y"    , x === y         , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "x =/= y"    , x =/= y         , Set( "x" , "y" ).map( toSortedQId ) ) ,
        ( "0 < 1"      , Ints( 0 ) < 1   , Set() )
    )
    //  format: ON

    for ( ( e, t, tdef ) ← theTerms1 ++ theTerms2 ) {
        val result = tdef.map( x ⇒ format( x ).layout )
        test( s"Check the typeDefs built with for term $e -- Should be $result" ) {
            t.typeDefs shouldBe tdef
        }
    }
}
