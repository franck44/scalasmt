/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ IntegerArithmetics, Core }
import interpreters.Resources
import typedterms.{ Commands, QuantifiedTerm }

class DeclareLetTermsTests extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with IntegerArithmetics
    with ArrayExInt
    with Core
    with Commands
    with Resources {

    override def suiteName = "Declare let terms in solvers test suite"

    import scala.util.Success
    import parser.SMTLIB2Syntax.{
        Term,
        TermTester,
        CheckSatResponses,
        SuccessResponse,
        AssertResponseSuccess,
        Sat,
        QualifiedId,
        SimpleQId,
        SymbolId,
        SSymbol
    }
    import parser.SMTLIB2Parser
    import parser.Implicits._
    import typedterms.TypedTerm
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.show
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_AUFLIA

    val p1 = SMTLIB2Parser[ TermTester ]

    //  create a solver SortedQId, type Ints, from a string
    def toSimpleQId( name : String ) : QualifiedId = SimpleQId( SymbolId( SSymbol( name ) ) )

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( QF_AUFLIA ) ) : _* )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )
    val u = Ints( "u" )
    val a1 = ArrayInt1( "a1" )

    val theTerms = Table(
        ( "Terms", "Expected response" ),

        ( """|
             | (
             |  let
             |    (
             |     (  a!1
             |         (>= (+ x (* (- 1 ) (select a1 1 ) ) ) 0 ) )
             |     )
             |    (not a!1 )
             | )
             |""".stripMargin, Set( x, a1 ) ) )

    //  initialise sequence
    val initSeq = new SMTInit( QF_AUFLIA, List() )

    for ( s ← theSolvers; ( xt, vars ) ← theTerms ) {

        val Success( TermTester( yt ) ) = p1( xt )

        val letTypedTerm = TypedTerm[ BoolTerm, Term ]( vars.map( _.typeDefs ).reduceLeft( _ ++ _ ), yt )

        test( s"[${s.name}] configured with ${initSeq.show}} assert letTerm  ${show( letTypedTerm.termDef )} -- should succeed" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        |=( letTypedTerm )

                    }
            } shouldBe Success( AssertResponseSuccess( SuccessResponse() ) )
        }
    }

}

class AssertLetTermsTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Commands with QuantifiedTerm with Resources {

    override def suiteName = "Assert simple let terms LIA in solvers test suite"

    import scala.util.Success
    import parser.SMTLIB2Syntax.{
        Term,
        TermTester,
        CheckSatResponses,
        Sat,
        UnSat,
        Command,
        QualifiedId,
        SimpleQId,
        SymbolId,
        SSymbol
    }
    import parser.SMTLIB2Parser
    import parser.Implicits._
    import typedterms.TypedTerm
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.show
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LIA

    //  create a solver SortedQId, type Ints, from a string
    def toSimpleQId( name : String ) : QualifiedId = SimpleQId( SymbolId( SSymbol( name ) ) )

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( QF_LIA ) ) : _* )

    val x = Ints( "x" )
    val y = Ints( "y" )

    val theTerms = Table(
        ( "Terms", "Expected response" ),

        (
            let {
                val x1 = BoundedVar( "x1", x + 1 )
                val x2 = BoundedVar( "y1", y * 2 )
                x1 <= x2
            },
            Sat() ),

        (
            let {
                val x1 = BoundedVar( "x1", x + 1 )
                let {
                    val x2 = BoundedVar( "y1", x1 - 2 )
                    x1 <= y & y < x2
                }
            },
            UnSat() ) )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA, List() )

    for ( s ← theSolvers; ( xt, r ) ← theTerms ) {

        test( s"[${s.name}] configured with ${initSeq.show} assert the letTerm  ${show( xt.termDef )} -- should be ${show( r )} " ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        isSat( xt )

                    }
            } shouldBe ( Success( r ) )
        }
    }

}

class DeclareNewLetTermsTests extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with IntegerArithmetics
    with ArrayExInt
    with Core
    with Commands
    with Resources {

    override def suiteName = "Declare let terms (new version) in solvers test suite"

    import scala.util.Success
    import parser.SMTLIB2Syntax.{
        Term,
        TermTester,
        CheckSatResponses,
        SuccessResponse,
        AssertResponseSuccess,
        Sat,
        QualifiedId,
        SimpleQId,
        SymbolId,
        SSymbol
    }
    import parser.SMTLIB2Parser
    import parser.Implicits._
    import typedterms.TypedTerm
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.show
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_AUFLIA

    val p1 = SMTLIB2Parser[ TermTester ]

    //  create a solver SortedQId, type Ints, from a string
    def toSimpleQId( name : String ) : QualifiedId = SimpleQId( SymbolId( SSymbol( name ) ) )

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( QF_AUFLIA ) ) : _* )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )
    val u = Ints( "u" )
    val a1 = ArrayInt1( "a1" )

    val theTerms = Table(
        ( "Terms", "Expected response" ),

        ( """|
             | (
             |  let
             |    (
             |     (  a!1
             |         (>= (+ x (* (- 1 ) (select a1 1 ) ) ) 0 ) )
             |     )
             |    (not a!1 )
             | )
             |""".stripMargin, Set( x, a1 ) ) )

    //  initialise sequence
    val initSeq = new SMTInit( QF_AUFLIA, List() )

    for ( s ← theSolvers; ( xt, vars ) ← theTerms ) {

        val Success( TermTester( yt ) ) = p1( xt )

        val letTypedTerm = TypedTerm[ BoolTerm, Term ]( vars.map( _.typeDefs ).reduceLeft( _ ++ _ ), yt )

        test( s"[${s.name}] configured with ${initSeq.show}} assert letTerm  ${show( letTypedTerm.termDef )} -- should succeed" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        |=( letTypedTerm )

                    }
            } shouldBe Success( AssertResponseSuccess( SuccessResponse() ) )
        }
    }

}