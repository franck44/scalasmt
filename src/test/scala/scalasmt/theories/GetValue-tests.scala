/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

class IntsGetValueTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with IntegerArithmetics
    with Core
    with Commands
    with Resources {

    override def suiteName = "Check sat and get interpolants for  LIA terms"

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{ Term, ConstantTerm, NumLit, NegTerm, Sat, UnSat }
    import typedterms.{ TypedTerm, Value }
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ], List[TypedTerm[IntTerm, Term]] ](
        ( "expression"         , "TypedTerm"       , "Values needed") ,
        ( "integer variable x" , x  === y          , List ( x ) )         ,
        ( "-x"                 , -x  === 1         , List ( x ) )         ,
        ( "x + y"              , x + y  === 0      , List (x , y ) )      ,
        ( "x + y = z"          , x + y  === z      , List (x , y , z ) )  ,
        ( "2 * x = z"          , x * 2  === z      , List (x , z ) )      ,
        ( "1 + 2 + x"          , x + 1 + 2  === -4 , List ( x ) )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LIA
    import configurations.SMTOptions.SMTProduceModels
    import scala.util.{ Try, Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( s ⇒ s.enabled && s.supportsLogic( QF_LIA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA, List( SMTProduceModels( true ) ) )

    for ( s ← theSolvers; ( _, t, xv ) ← theTerms ) {

        test( s"[${s.name}] configured with ${initSeq.show} to get values for term ${show( t.termDef )}" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t ) should matchPattern {
                            case Success( Sat() ) ⇒
                        }

                        val r : List[ Try[ Value ] ] = xv map getValue
                        logger.info( s"Get-model returned: $r" )

                        r.foreach( _ should matchPattern {
                            case Success( Value( ConstantTerm( NumLit( _ ) ) ) )            ⇒
                            case Success( Value( NegTerm( ConstantTerm( NumLit( _ ) ) ) ) ) ⇒
                        } )
                        //  using must return a Try
                        Success( true )
                    }
            } shouldBe Success( true )
        }
    }
}

class RealsGetValueTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with RealArithmetics
    with Core
    with Commands
    with Resources {

    override def suiteName = "Check sat and get value for some LRA terms"

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{
        Term,
        ConstantTerm,
        DecLit,
        NegTerm,
        NumLit,
        RealDivTerm,
        Sat,
        UnSat
    }
    import typedterms.{ TypedTerm, Value }
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    val x = Reals( "x" )
    val y = Reals( "y" )
    val z = Reals( "z" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ], List[TypedTerm[RealTerm, Term]] ](
        ( "expression"         , "TypedTerm"              , "Values needed")     ,
        ( "integer variable x" , x  === y                 , List ( x , y ) )     ,
        ( "-x"                 , -x  === 1                , List ( x ) )         ,
        ( "x + y"              , x + y  === 0             , List ( x , y ) )     ,
        ( "x + y = z"          , x + y  === z             , List ( x , y , z ) ) ,
        ( "2 * x = z"          , x * 2.0  === z           , List ( x , z ) )     ,
        ( "1 + 2 + x"          , x + 1.0 + 2.4  === -4.66 , List(x))
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LRA
    import configurations.SMTOptions.SMTProduceModels
    import scala.util.{ Try, Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( s ⇒ s.enabled && s.supportsLogic( QF_LRA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LRA, List( SMTProduceModels( true ) ) )

    for ( s ← theSolvers; ( _, t, xv ) ← theTerms ) {

        test( s"[${s.name}] configured with ${initSeq.show} to get values for term ${show( t.termDef )}" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t ) should matchPattern {
                            case Success( Sat() ) ⇒
                        }

                        val r : List[ Try[ Value ] ] = xv map getValue
                        logger.info( s"Get-model returned: $r" )

                        r.foreach( _ should matchPattern {
                            case Success( Value( ConstantTerm( DecLit( _, _ ) ) ) )            ⇒
                            case Success( Value( NegTerm( ConstantTerm( DecLit( _, _ ) ) ) ) ) ⇒
                            case Success( Value( ConstantTerm( NumLit( _ ) ) ) )               ⇒
                            case Success( Value( NegTerm( ConstantTerm( NumLit( _ ) ) ) ) )    ⇒
                            case Success( Value( RealDivTerm( _, _ ) ) )                       ⇒
                            case Success( Value( NegTerm( RealDivTerm( _, _ ) ) ) )            ⇒
                        } )
                        //  using must return a Try
                        Success( true )
                    }
            } shouldBe Success( true )
        }
    }
}

class BVsGetValueTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with BitVectors
    with Core
    with Commands
    with Resources {

    override def suiteName = "Check sat and get value for some BVs terms"

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{
        Term,
        ConstantTerm,
        HexaLit,
        BinLit,
        DecBVLit,
        Sat,
        UnSat
    }
    import typedterms.{ TypedTerm, Value }
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    val x = BVs( "x", 16 ) // 16 bits
    val y = BVs( "y", 16 )
    val z = BVs( "z", 16 )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[ BoolTerm, Term ], List[TypedTerm[BVTerm, Term]] ](
        ( "expression"         , "TypedTerm"                 , "Values needed")     ,
        ( "BV variable x"      , x  === y                    , List ( x , y ) )     ,
        ( "-x"                 , -x  === 2.withBits(16)      , List ( x ) )         ,
        ( "x + y"              , x + y  === BVs(0,16)        , List ( x , y ) )     ,
        ( "x + y = z"          , x + y  === z                , List ( x , y , z ) ) ,
        ( "2 * x = z"          , x * BVs(2,16)  === z        , List ( x , z ) )     ,
        ( "1 + 2 + x"          , x + BVs(1,16) === BVs(3,16) , List(x) )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_BV
    import configurations.SMTOptions.SMTProduceModels
    import scala.util.{ Try, Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( s ⇒ s.enabled && s.supportsLogic( QF_BV ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_BV, List( SMTProduceModels( true ) ) )

    for ( s ← theSolvers; ( _, t, xv ) ← theTerms ) {

        test( s"[${s.name}] configured with ${initSeq.show} to get values for term ${show( t.termDef )} " ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t ) should matchPattern {
                            case Success( Sat() ) ⇒
                        }

                        val r : List[ Try[ Value ] ] = xv map getValue
                        logger.info( s"Get-model returned: $r" )

                        r.foreach( _ should matchPattern {
                            case Success( Value( ConstantTerm( HexaLit( _ ) ) ) )     ⇒
                            case Success( Value( ConstantTerm( DecBVLit( _, _ ) ) ) ) ⇒
                            case Success( Value( ConstantTerm( BinLit( _ ) ) ) )      ⇒
                        } )

                        //  using must return a Try
                        Success( true )
                    }
            } shouldBe Success( true )
        }
    }
}

class ArrayGetValueTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with IntegerArithmetics
    with ArrayExInt
    with ArrayExBool
    with ArrayExOperators
    with Core
    with Commands
    with Resources {

    override def suiteName = "Check sat and get value for some Array terms"

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{
        Sat,
        UnSat,
        QIdTerm,
        Term,
        ConstantTerm,
        NegTerm,
        NumLit,
        QIdAndTermsTerm,
        ConstArrayTerm,
        ArrayStoreAllTerm,
        StoreTerm,
        CheckSatResponses
    }
    import typedterms.{ TypedTerm, Value, VarTerm }
    import interpreters.SMTSolver
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val a1 = ArrayInt1( "a1" )
    val a2 = ArrayInt2( "a2" )

    //  format: OFF
    val theIntTerms =
        Table[ String, TypedTerm[ BoolTerm, Term ], List[TypedTerm[IntTerm, Term]] ](
        ( "expression"                  , "TypedTerm",                         "terms needed"),
        ( "a1(1) == y"                  , a1(1) === y,                         List ( a1(1) ) ),
        ( "a1(1) == y & a1(2) == y - 1" , (a1(1) === y) & (a1(2) === y - 1),
                                                                        List ( a1(1), a1(2) ) ),
        ( "a1(1) == a2(2,3)"            , a1(1) === a2(2)(3),           List ( a1(1), a2(2)(3) ) )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_AUFLIA
    import configurations.SMTOptions.SMTProduceModels
    import scala.util.{ Try, Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( s ⇒ s.enabled && s.supportsLogic( QF_AUFLIA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_AUFLIA, List( SMTProduceModels( true ) ) )

    for ( s ← theSolvers; ( _, t, xv ) ← theIntTerms ) {

        test( s"[${s.name}] configured with ${initSeq.show} to get to get values for term ${show( t.termDef )} === 1" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t ) should matchPattern {
                            case Success( Sat() ) ⇒
                        }

                        val r : List[ Try[ Value ] ] = xv map getValue
                        logger.info( s"Get-model returned: $r" )

                        r.foreach( _ should matchPattern {
                            case Success( Value( ConstantTerm( NumLit( _ ) ) ) )            ⇒
                            case Success( Value( NegTerm( ConstantTerm( NumLit( _ ) ) ) ) ) ⇒
                        } )

                        logger.info(
                            "values: {}",
                            ( r map {
                                case Success( Value( v ) ) ⇒ show( v )
                                case Failure( e )          ⇒ fail( e )
                            } ).mkString( "\n" ) )
                        //  using must return a Try
                        Success( true )
                    }
            } shouldBe Success( true )
        }
    }

    //  format: OFF
    val theArrayTerms = Table[ String, TypedTerm[ BoolTerm, Term ], List[TypedTerm[ArrayTerm[IntTerm], Term]]] (
        ( "expression"         ,        "TypedTerm",                        "terms needed"),
        ( "a1(1) == y & a1(2) == y - 1" , (a1(1) === y) & (a1(2) === x - 1), List ( a1 ) ),
        ( "a1(1) == a2(2,3)"            , a1(1) === a2(2)(3),           List ( a1, a2(2) ) )
    )
    //  format: ON

    for ( s ← theSolvers; ( _, t, xv ) ← theArrayTerms ) {

        test( s"[${s.name}] configured with ${initSeq.show} to get to get values for term ${show( t.termDef )} " ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t ) should matchPattern {
                            case Success( Sat() ) ⇒
                        }

                        val r : List[ Try[ Value ] ] = xv map getValue
                        logger.info( s"Get-model returned: $r" )

                        r.foreach( _ should matchPattern {
                            //  e.g. Yices can return (a1 @fun_5)
                            case Success( Value( QIdTerm( _ ) ) ) if s.name.startsWith( "Yices" ) ⇒
                            case Success( Value( ConstArrayTerm( _, _ ) ) )                       ⇒
                            case Success( Value( ArrayStoreAllTerm( _, _ ) ) )                    ⇒
                            case Success( Value( StoreTerm( _, _, _ ) ) )                         ⇒
                        } )

                        logger.info(
                            "values: {}",
                            ( r map {
                                case Success( Value( v ) ) ⇒ show( v )
                                case Failure( e )          ⇒ fail( e )
                            } ).mkString( "\n" ) )
                    }
                    Success( true )
            } shouldBe Success( true )
        }
    }
}

class ArrayBVGetValueTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with BitVectors
    with ArrayExBV
    with ArrayExOperators
    with Core
    with Commands
    with Resources {

    override def suiteName = "Check sat and get value for some Array BitVectors terms"

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{
        Term,
        StoreTerm,
        QIdAndTermsTerm,
        ArrayStoreAllTerm,
        ConstArrayTerm,
        ConstantTerm,
        HexaLit,
        DecBVLit,
        BinLit,
        Sat,
        UnSat,
        QIdTerm,
        CheckSatResponses
    }
    import typedterms.{ TypedTerm, Value, VarTerm }
    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    val x = BVs( "x", 8 ) // 8 bits
    val y = BVs( "y", 8 ) // 8 bits
    val a1 = ArrayBV1( "a1", 16, 8 ) // indices on 16 bits and values on 8 bits
    val a2 = ArrayBV2( "a2", 16, 16, 8 ) // as above

    //  format: OFF
    val theBVTerms = Table[ String, TypedTerm[ BoolTerm, Term ], List[TypedTerm[BVTerm, Term]] ](
        ( "expression"         , "TypedTerm",                     "terms needed"),
        ( "bv1 16 == y"        ,  a1(BVs(1,16)) === y,            List ( a1(BVs(1,16)) ) ),
        ( "bv2 16 16 == y"     ,  a2(BVs(1,16))(BVs(2,16)) === y, List ( a2(BVs(1,16))(BVs(2,16)) ) )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_ABV
    import configurations.SMTOptions.SMTProduceModels
    import scala.util.{ Try, Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( s ⇒ s.enabled && s.supportsLogic( QF_ABV ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_ABV, List( SMTProduceModels( true ) ) )

    for ( s ← theSolvers; ( _, t, xv ) ← theBVTerms ) {

        test( s"[${s.name}] configured with ${initSeq.show} to get to get values for term ${show( t.termDef )}" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t ) should matchPattern {
                            case Success( Sat() ) ⇒
                        }

                        val r : List[ Try[ Value ] ] = xv map getValue
                        logger.info( s"Get-model returned: $r" )

                        r.foreach( _ should matchPattern {
                            case Success( Value( ConstantTerm( HexaLit( _ ) ) ) )     ⇒
                            case Success( Value( ConstantTerm( DecBVLit( _, _ ) ) ) ) ⇒
                            case Success( Value( ConstantTerm( BinLit( _ ) ) ) )      ⇒
                        } )

                        logger.info(
                            "values: {}",
                            ( r map {
                                case Success( Value( v ) ) ⇒ show( v )
                                case Failure( e )          ⇒ fail( e )
                            } ).mkString( "\n" ) )

                        //  using must return a Try
                        Success( true )
                    }
            } shouldBe Success( true )
        }
    }

    //  format: OFF
    val theArrayBVTerms = Table[ String, TypedTerm[ BoolTerm, Term ], List[TypedTerm[ArrayTerm[BVTerm], Term]]] (
        ( "expression"           , "TypedTerm",                       "terms needed"),
        ( "bv10 16 == y"         ,  a1(BVs(10,16)) === y,             List ( a1 )),
        ( "bv10 16 bv 7 16 == y" ,  a2(BVs(10,16))(BVs(7,16)) === y,  List ( a2(BVs(10,16)) ))
    )
    //  format: ON

    for ( s ← theSolvers; ( _, t, xv ) ← theArrayBVTerms ) {

        test( s"[${s.name}] configured with ${initSeq.show} to get to get values for term ${show( t.termDef )}" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( t ) should matchPattern {
                            case Success( Sat() ) ⇒
                        }

                        val r : List[ Try[ Value ] ] = xv map getValue
                        logger.info( s"Get-model returned: $r" )

                        val x = r.foreach( _ should matchPattern {
                            case Success( Value( QIdTerm( _ ) ) ) if s.name.startsWith( "Yices" ) ⇒
                            case Success( Value( QIdTerm( _ ) ) ) if s.name == "Boolector"        ⇒
                            case Success( Value( ArrayStoreAllTerm( _, _ ) ) )                    ⇒
                            case Success( Value( ConstArrayTerm( _, _ ) ) )                       ⇒
                            case Success( Value( StoreTerm( _, _, _ ) ) )                         ⇒
                        } )

                        logger.info(
                            "values: {}",
                            ( r map {
                                case Success( Value( v ) ) ⇒ show( v )
                                case Failure( e )          ⇒ fail( e )
                            } ).mkString( "\n" ) )
                    }
                    Success( true )
            } shouldBe Success( true )
        }
    }
}
