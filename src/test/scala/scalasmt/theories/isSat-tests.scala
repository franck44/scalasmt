/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

/**
 * test the isSatWithPrefix command on typed terms
 *
 * This command should stop as soon as it becomes UnSat
 */
class UnSatPrefixesCommandTests extends FunSuite with TableDrivenPropertyChecks with Matchers with Core with IntegerArithmetics with RealArithmetics with Commands with Resources {

    override def suiteName = "Send assert command and check Declared variables stack"

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.SMTInit
    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{
        Sat,
        UnSat
    }

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter(
            n ⇒ !( n.name contains "nonIncr" ) && n.enabled &&
                n.supportsLogic( QF_LIA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA )

    for ( s ← theSolvers ) {

        test( s"[${s.name}] configured with ${initSeq.show} -- iSatPrefix x < 0 , x >=0, y < 0 should be UnSat after second term" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        val x = Ints( "x" )
                        val y = Ints( "y" )

                        //  checkSat
                        isSatWithAssertWhileSat( Seq( x < 0, x >= 0, y < 0 ) : _* ) match {
                            case ( Success( r ), c ) ⇒ Success( ( r, c ) )
                            case ( f, _ )            ⇒ f
                        }
                    }
            } shouldBe Success( ( UnSat(), 2 ) )
        }

        test( s"[${s.name}] configured with ${initSeq.show} -- iSatPrefix x < 0 , x >=0, y < 0 after asserting x >= 4 should be UnSat after first term" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        val x = Ints( "x" )
                        val y = Ints( "y" )

                        |= ( x >= 4 ) shouldBe a[ Success[ _ ] ]

                        //  checkSat
                        isSatWithAssertWhileSat( Seq( x < 0, x >= 0, y < 0 ) : _* ) match {
                            case ( Success( r ), c ) ⇒ Success( ( r, c ) )
                            case ( f, _ )            ⇒ f
                        }
                    }
            } shouldBe Success( ( UnSat(), 1 ) )
        }

        test( s"[${s.name}] configured with ${initSeq.show} -- iSatPrefix x < 0 , x >=0, y < 0 after asserting x >= 4, x < 4 should be UnSat after 0 term" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        val x = Ints( "x" )
                        val y = Ints( "y" )

                        |= ( x >= 4 ) shouldBe a[ Success[ _ ] ]
                        |= ( x < 4 ) shouldBe a[ Success[ _ ] ]

                        //  checkSat
                        isSatWithAssertWhileSat( Seq( x < 0, x >= 0, y < 0 ) : _* ) match {
                            case ( Success( r ), c ) ⇒ Success( ( r, c ) )
                            case ( f, _ )            ⇒ f
                        }
                    }
            } shouldBe Success( ( UnSat(), 0 ) )
        }

        test( s"[${s.name}] configured with ${initSeq.show} -- iSatPrefix x < 0 , x >= -2, y < 0  should be Sat" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        val x = Ints( "x" )
                        val y = Ints( "y" )

                        //  checkSat
                        isSatWithAssertWhileSat( Seq( x < 0, x >= -2, y < 0 ) : _* ) match {
                            case ( Success( r ), c ) ⇒ Success( ( r, c ) )
                            case ( f, _ )            ⇒ f
                        }
                    }
            } shouldBe Success( ( Sat(), 3 ) )
        }

        test( s"[${s.name}] configured with ${initSeq.show} -- iSatPrefix x < 0.2 , x >= -2 should fail because assert fails" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        val x = Reals( "x" )

                        //  checkSat
                        isSatWithAssertWhileSat( Seq( x < 0.2, x >= -2.0 ) : _* ) match {
                            case ( Success( r ), c ) ⇒ Success( ( r, c ) )
                            case ( f, _ )            ⇒ f
                        }
                    }
            } should matchPattern {
                case Success( _ ) if s.name.contains( "MathSat" ) ⇒
                case Failure( _ )                                 ⇒
            }
        }

        //   Same tests but when a subset of indices is used to chekcSat.

        test( s"[${s.name}] configured with ${initSeq.show} -- iSatPrefix x < 0/t , x >=0/f, y < 0/t should be UnSat after third term" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        val x = Ints( "x" )
                        val y = Ints( "y" )

                        //  checkSat
                        isSatWithAssertWhileSat( None )( 0, true, Seq( ( x < 0, true ), ( x >= 0, false ), ( y < 0, true ) ) : _* ) match {
                            case ( Success( r ), c ) ⇒ Success( ( r, c ) )
                            case ( f, _ )            ⇒ f
                        }
                    }
            } shouldBe Success( ( UnSat(), 3 ) )
        }

        test( s"[${s.name}] configured with ${initSeq.show} -- iSatPrefix x < 0/t , x >=0/t, y < 0/t after asserting x >= 4 should be UnSat after first term" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        val x = Ints( "x" )
                        val y = Ints( "y" )

                        |= ( x >= 4 ) shouldBe a[ Success[ _ ] ]

                        //  checkSat
                        isSatWithAssertWhileSat( None )( 0, true, Seq( ( x < 0, true ), ( x >= 0, true ), ( y < 0, true ) ) : _* ) match {
                            case ( Success( r ), c ) ⇒ Success( ( r, c ) )
                            case ( f, _ )            ⇒ f
                        }
                    }
            } shouldBe Success( ( UnSat(), 1 ) )
        }

        test( s"[${s.name}] configured with ${initSeq.show} -- iSatPrefix x < 0/f , x >=0/t, y < 0/t after asserting x >= 4, x < 4 should be UnSat after 0 term" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        val x = Ints( "x" )
                        val y = Ints( "y" )

                        |= ( x >= 4 ) shouldBe a[ Success[ _ ] ]
                        |= ( x < 4 ) shouldBe a[ Success[ _ ] ]

                        //  checkSat
                        isSatWithAssertWhileSat( None )( 0, true, Seq( ( x < 0, false ), ( x >= 0, true ), ( y < 0, true ) ) : _* ) match {
                            case ( Success( r ), c ) ⇒ Success( ( r, c ) )
                            case ( f, _ )            ⇒ f
                        }
                    }
            } shouldBe Success( ( UnSat(), 0 ) )
        }

        test( s"[${s.name}] configured with ${initSeq.show} -- iSatPrefix x < 0/f , y < 0/t, x >= 2/f,   should be Sat as last term ignored" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        val x = Ints( "x" )
                        val y = Ints( "y" )

                        //  checkSat
                        isSatWithAssertWhileSat( None )( 0, true, Seq( ( x < 0, true ), ( y < 0, true ), ( x >= 2, false ) ) : _* ) match {
                            case ( Success( r ), c ) ⇒ Success( ( r, c ) )
                            case ( f, _ )            ⇒ f
                        }
                    }
            } shouldBe Success( ( Sat(), 3 ) )
        }

        test( s"[${s.name}] configured with ${initSeq.show} -- iSatPrefix x < 0.2/t , x >= -2/f should fail because assert fails" ) {

            //  with using
            using( new SMTSolver( s, initSeq ) ) {
                implicit solver ⇒
                    {
                        val x = Reals( "x" )

                        //  checkSat
                        isSatWithAssertWhileSat( None )( 0, true, Seq( ( x < 0.2, true ), ( x >= -2.0, false ) ) : _* ) match {
                            case ( Success( r ), c ) ⇒ Success( ( r, c ) )
                            case ( f, _ )            ⇒ f
                        }
                    }
            } should matchPattern {
                case Success( _ ) if s.name.contains( "MathSat" ) ⇒
                case Failure( _ )                                 ⇒
            }
        }
    }

}

