/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import parser.Implicits._
import parser.SMTLIB2Syntax.Term
import typedterms.{ Commands, TypedTerm }
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

/**
 * Check that a sequence of predicates (I1,I2,...Ik) is an interpolant for
 * P0,P1,..., P_{k+1}
 */
object InterpolantChecker extends Core with Commands with Matchers {

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2Syntax.{ UnSat, SuccessResponse, GeneralResponse }
    import interpreters.SMTSolver

    def checkInterpolants(
        xitp : List[ TypedTerm[ BoolTerm, Term ] ],
        xt :   List[ TypedTerm[ BoolTerm, Term ] ] )(
        implicit
        solver : SMTSolver ) : Try[ Unit ] =
        {

            val extendItp = True() +: xitp :+ False()
            val pairsOfItp = extendItp zip extendItp.tail

            ( for { ( p, ( a, b ) ) ← xt zip pairsOfItp } yield {
                push() flatMap
                    //
                    { _ ⇒ isSat( !( ( a & p ) imply b ) ) } flatMap
                    //
                    { _ ⇒ pop() }
            } ).find ( _.isFailure ) match {
                case None                 ⇒ Success( () )
                case Some( Failure( f ) ) ⇒ Failure( f )
                case Some( Success( _ ) ) ⇒
                    Failure( new Exception( "A find on Failure returned a Success. This is very weird and should not happen" ) )
            }
        }
}

/**
 *  Interpolants for LIA terms
 */
class LiaInterpolantsTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with IntegerArithmetics
    with Commands
    with Resources {

    override def suiteName = "Compute interpolants for LIA terms"

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory
    //  logger
    private val logger = getLogger( this.getClass )

    import parser.SMTLIB2PrettyPrinter.show
    import typedterms.TypedTerm

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    //  format: OFF
    val theLIATerms = Table[List[TypedTerm[ BoolTerm, Term ]] ](
        "TypedTerm",
        List ( x === 0     , x + 1 > 2 ),
        List ( x === y + 1 , y <= 2      , x >= 4 ),
        List ( x === z + 1 , z >= 0      , y >= x  , y < 1 )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LIA
    import configurations.SMTOptions.SMTProduceInterpolants
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat, UnKnown, SuccessResponse }
    import scala.util.{ Try, Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supports( SMTProduceInterpolants.name ) && ( s.name != "MathSat" ) && s.supportsLogic( QF_LIA ) ) : _* )

    //  Initialise sequence
    val initSeqInt = new SMTInit( QF_LIA, List( SMTProduceInterpolants( true ) ) )

    //  solver to check the interpolants are ... inductive interpolants
    val theCheckers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( QF_LIA ) ) : _* )

    //  Initialise sequence
    val initSeq = new SMTInit( QF_LIA, List() )

    for ( s ← theSolvers; xt ← theLIATerms ) {

        test( s"[${s.name} configured with ${initSeqInt.show} to check UnSat-sat for term ${xt.map( _.termDef ).map( show( _ ) )} and get Interpolants" ) {

            //  with using
            using( new SMTSolver( s, initSeqInt ) ) {
                implicit solver ⇒
                    {
                        //  make NamedTerms with an index
                        val namedTerms = for { ( tt, n ) ← xt.zipWithIndex } yield tt.named( "P" + n )

                        //  assert the named terms
                        namedTerms map { x ⇒ |=( x ) }

                        //
                        isSat() shouldBe Success( UnSat() )

                        namedTerms.size should be >= 2

                        //  now get interpolants
                        getInterpolants( namedTerms.head, namedTerms.tail.head, namedTerms.drop( 2 ) : _* )
                    }
            } match {
                case Success( xitp ) ⇒
                    //  if we have n terms, we should get n - 1 interpolants
                    xitp should have length ( xt.size - 1 )

                    logger.info(
                        s"[$s] Interpolants: {}",
                        xitp.map( _.termDef )
                            .map ( show( _ ) ).mkString( "" ) )

                    //  check that the interpolants are inductive interpolants
                    for ( s1 ← theCheckers ) {

                        using( new SMTSolver( s1, initSeq ) ) {
                            implicit solver ⇒
                                InterpolantChecker.checkInterpolants( xitp, xt )

                        } shouldBe Success( () )
                    }

                case Failure( e ) ⇒
                    logger.error( "Error while computing interpolants: {}", e )
                    fail( e )
            }
        }

    }
}

/**
 * Interpolants for LRA terms
 */
class RealInterpolantsTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with RealArithmetics
    with Commands
    with Resources {

    override def suiteName = "Compute interpolants for LRA terms"

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory
    //  logger
    private val logger = getLogger( this.getClass )

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat, UnKnown, SuccessResponse }

    import typedterms.TypedTerm

    val x = Reals( "x" )
    val y = Reals( "y" )
    val z = Reals( "z" )

    //  format: OFF
    val theLRATerms = Table[List[TypedTerm[ BoolTerm, Term ]] ](
        "TypedTerm",
        List ( x === 0.0         , x + 1.0 > 2.0 ) ,
        List ( x === y + 1.0/2.0 , y <= 2          , x >= 4 ) ,
        List ( x === z + 1       , z >= 0          , y >= x   , y < 1 )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LRA
    import configurations.SMTOptions.SMTProduceInterpolants
    import scala.util.{ Try, Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supports( SMTProduceInterpolants.name ) && ( s.name != "MathSat" ) && s.supportsLogic( QF_LRA ) ) : _* )

    //  Initialise sequence
    val initSeqInt = new SMTInit( QF_LRA, List( SMTProduceInterpolants( true ) ) )

    //  solver to check the interpolants are ... inductive interpolants
    val theCheckers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( QF_LRA ) ) : _* )

    //  Initialise sequence
    val initSeq = new SMTInit( QF_LRA, List() )

    for ( s ← theSolvers; xt ← theLRATerms ) {

        test( s"[${s.name}] configured with ${initSeqInt.show} to check UnSat-sat for term ${xt.map( _.termDef ).map( show( _ ) )} and get Interpolants" ) {

            //  with using
            using( new SMTSolver( s, initSeqInt ) ) {
                implicit solver ⇒
                    {
                        //  make NamedTerms with an index
                        val namedTerms = for { ( tt, n ) ← xt.zipWithIndex } yield tt.named( "P" + n )

                        //  assert the named terms
                        namedTerms map { x ⇒ |=( x ) }

                        //
                        isSat() shouldBe Success( UnSat() )

                        namedTerms.size should be >= 2

                        //  now get interpolants
                        getInterpolants( namedTerms.head, namedTerms.tail.head, namedTerms.drop( 2 ) : _* )
                    }
            } match {
                case Success( xitp ) ⇒
                    //  if we have n terms, we should get n - 1 interpolants
                    xitp should have length ( xt.size - 1 )

                    logger.info(
                        s"[$s] Interpolants: {}",
                        xitp.map( _.termDef )
                            .map ( show( _ ) ).mkString( "" ) )

                    //  check that the interpolants are inductive interpolants
                    for ( s1 ← theCheckers ) {

                        using( new SMTSolver( s1, initSeq ) ) {
                            implicit solver ⇒
                                InterpolantChecker.checkInterpolants( xitp, xt )

                        } shouldBe Success( () )
                    }

                case Failure( e ) ⇒
                    logger.error( "Error while computing interpolants: {}", e )
                    fail( e )
            }
        }
    }
}

/**
 * Interpolants for QF_AUFLIA terms (arrays and LIA)
 */
class ArrayIntInterpolantsTests
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers
    with Core
    with IntegerArithmetics
    with ArrayExInt
    with ArrayExOperators
    with Commands
    with Resources {

    override def suiteName = "Compute interpolants for LRA terms"

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory
    //  logger
    private val logger = getLogger( this.getClass )

    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat, UnKnown }
    import typedterms.TypedTerm

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )
    val a1 = ArrayInt1( "a1" )
    val a2 = ArrayInt1( "a2" )

    //  format: OFF
    val theArrayTerms = Table[List[TypedTerm[ BoolTerm, Term ]] ](
        "TypedTerm",
        List ( x === 0             , a1(1) === 1 , a1(1) <= x ) ,
        List ( a1(1) <= a2(0)      , a2(0) <= 2  , a1(1) >= 4 ) ,
        List ( a1(x) + 2 === a2(x) , a2(z) <= 3  , z === x      , a1(z) > a2(x) + 3  )
    )
    //  format: ON

    //  check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.AppConfig.config
    import configurations.SMTInit
    import configurations.SMTLogics.QF_AUFLIA
    import configurations.SMTOptions.SMTProduceInterpolants
    import scala.util.{ Success, Failure }

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supports( SMTProduceInterpolants ) && ( s.name != "MathSat" ) && s.supportsLogic( QF_AUFLIA ) ) : _* )

    //  Initialise sequence
    val initSeqInt = new SMTInit( QF_AUFLIA, List( SMTProduceInterpolants( true ) ) )

    //  solver to check the interpolants are ... inductive interpolants
    val theCheckers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( QF_AUFLIA ) ) : _* )

    //  Initialise sequence
    val initSeq = new SMTInit( QF_AUFLIA, List() )

    for ( s ← theSolvers; xt ← theArrayTerms ) {

        test( s"[${s.name}] configured with ${initSeqInt.show} for term ${xt.map( _.termDef ).map( show( _ ) )} -- check-sat should be UnSat and get Interpolants" ) {
            //  with using
            using( new SMTSolver( s, initSeqInt ) ) {
                implicit solver ⇒
                    {
                        //  make NamedTerms with an index
                        val namedTerms = for { ( tt, n ) ← xt.zipWithIndex } yield tt.named( "P" + n )

                        //  assert the named terms
                        namedTerms map { x ⇒ |=( x ) }

                        //
                        isSat() shouldBe Success( UnSat() )

                        namedTerms.size should be >= 2

                        //  now get interpolants
                        getInterpolants( namedTerms.head, namedTerms.tail.head, namedTerms.drop( 2 ) : _* )
                    }
            } match {
                case Success( xitp ) ⇒
                    //  if we have n terms, we should get n - 1 interpolants
                    xitp should have length ( xt.size - 1 )

                    logger.info(
                        s"[$s] Interpolants: {}",
                        xitp.map( _.termDef )
                            .map ( show( _ ) ).mkString( "" ) )

                    //  check that the interpolants are inductive interpolants
                    for ( s1 ← theCheckers ) {

                        using( new SMTSolver( s1, initSeq ) ) {
                            implicit solver ⇒
                                InterpolantChecker.checkInterpolants( xitp, xt )

                        } shouldBe Success( () )
                    }

                case Failure( e ) ⇒
                    logger.error( "Error while computing interpolants: {}", e )
                    fail( e )
            }
        }
    }
}
