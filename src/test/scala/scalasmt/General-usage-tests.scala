/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package documentation
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import org.bitbucket.franck44.scalasmt.theories.{ Core, IntegerArithmetics, RealArithmetics }
import org.bitbucket.franck44.scalasmt.typedterms.Commands
import org.bitbucket.franck44.scalasmt.interpreters.Resources
import scala.util.{ Try, Success, Failure }
import parser.SMTLIB2Syntax._
import parser.SMTLIB2Parser.ParseErrorException

/**
 * Propositional logic simple examples
 */
class PropLogic
    extends FunSuite
    with Matchers
    with TableDrivenPropertyChecks
    with Core
    with Commands
    with Resources {

    override def suiteName =
        """| ===============================================================
           | Examples from http://rise4fun.com/Z3/tutorial/guide
           | Propositional logic example -- QF_UF
           |    (declare-const p Bool)
           |    (declare-const q Bool)
           |    (declare-const r Bool)
           |    (define-fun conjecture () Bool
           |           (=>
           |                (and (=> p q) (=> q r))
           |                (=> p r)
           |            )
           |    )
           |    (assert (not conjecture))
           |    (check-sat)
           |    )
           |//  should be unsat""".stripMargin

    import configurations.AppConfig.config
    import configurations.SMTLogics.QF_UF
    import configurations.SMTInit
    import interpreters.SMTSolver

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( QF_UF ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_UF )

    for ( s ← theSolvers ) {
        test( s"Checking with [${s.name}] -- unsat" ) {

            //  define 3 propositional variables, p, q, r
            val p = Bools( "p" )
            val q = Bools( "q" )
            val r = Bools( "r" )

            //  define a conjecture
            val c1 = ( p imply q ) & ( q imply r )
            val c2 = ( p imply r )

            val response = using( new SMTSolver( s, initSeq ) ) {
                implicit withSolver ⇒
                    // check that not(c1 => c2) is Sat
                    isSat ( !( c1 imply c2 ) )
            }
            response shouldBe Success( UnSat() )
        }
    }
}

/**
 * More propositional logic
 */
class DeMorgan extends FunSuite
    with Matchers
    with TableDrivenPropertyChecks with Core
    with Commands
    with Resources {

    override def suiteName =
        """| ===============================================================
           | Examples from http://rise4fun.com/Z3/tutorial/guide
           | deMorgan's law -- QF_UF
           |    (declare-const a Bool)
           |    (declare-const b Bool)
           |    (declare-const r Bool)
           |    (define-fun demorgan () Bool
           |        (=
           |            (and a b)
           |            (not (or (not a) (not b)))
           |        )
           |    )
           |    (assert (not demorgan))
           |    (check-sat)
           |    )
           |//  should be unsat""".stripMargin

    import configurations.SMTLogics.QF_UF
    import configurations.AppConfig.config
    import configurations.SMTInit
    import interpreters.SMTSolver

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( QF_UF ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_UF )

    for ( s ← theSolvers ) {
        test( s"Checking with [${s.name}] -- unsat" ) {

            //  define 2 propositional variables a and b
            val a = Bools( "a" )
            val b = Bools( "b" )

            //  define a conjecture
            val dml = ( a & b ) === !( !a | !b )

            val response = using( new SMTSolver( s, initSeq ) ) {
                implicit withSolver ⇒
                    isSat ( !dml )
            }
            response shouldBe Success( UnSat() )
        }
    }
}

/**
 * Linear Arithmetics (integer and real)
 */
class Arithmetic1 extends FunSuite
    with Matchers
    with TableDrivenPropertyChecks
    with Core
    with IntegerArithmetics
    with RealArithmetics
    with Commands
    with Resources {

    override def suiteName =
        """| ===============================================================
           | Examples from http://rise4fun.com/Z3/tutorial/guide
           | Arithmetic -- QF_LRA
           |    (declare-const a Int)
           |    (declare-const b Int)
           |    (declare-const c Int)
           |    (declare-const d Real)
           |    (declare-const e Real)
           |
           |    (assert (> a (+ b 2)))
           |    (assert (= a (+ (* 2 c) 10)))
           |    (assert (<= (+ c b) 1000))
           |    (assert (>= d e))
           |
           |    (check-sat)
           |    (get-model)
           |    )
           |//  should be sat""".stripMargin

    import configurations.SMTLogics.{ AUFNIRA, QF_UFLIRA, QF_LIRA, QF_AUFLIRA, QF_LRA }
    import configurations.SMTOptions.SMTProduceModels
    // import configurations.SMTBoolOptions.{ setOption, MODELS ⇒ BMODELS }
    import configurations.AppConfig.enabledSolvers
    import configurations.{ SMTInit, SolverConfig }
    import interpreters.SMTSolver

    //  format: OFF
    val theSolvers = Table[ String, SMTInit, Try[ SatResponses ] ](
        ( "Solver"   ,        "InitSeq"                             , "Expected Response" )                           ,
        ( "Z3",                new SMTInit(AUFNIRA, List(SMTProduceModels(true)))   , Success( Sat() ) )                              ,
        (  "SMTInterpol",      new SMTInit(QF_UFLIRA,List(SMTProduceModels(true)))  , Success( Sat() ) )                              ,
        (  "Yices",            new SMTInit(QF_LIRA, List(SMTProduceModels(true)))   , Success( Sat() ) )                              ,
        (  "CVC4",             new SMTInit(QF_LRA, List(SMTProduceModels(true)))    , Failure( ParseErrorException( "..." ) ) ) ,
        ( "MathSat",           new SMTInit(QF_AUFLIRA, List(SMTProduceModels(true))), Success( Sat() ) )
    )
    //  format: ON

    for ( ( sName, ic, r ) ← theSolvers if ( enabledSolvers.contains( sName ) ) ) {
        test( s"Checking with [$sName] -- should be $r" ) {

            //  define 3 propositional variables, p, q, r
            val a = Ints( "a" )
            val b = Ints( "b" )
            val c = Ints( "c" )
            val d = Reals( "d" )
            val e = Reals( "e" )

            val response = using( new SMTSolver( sName, ic ) ) {
                implicit withSolver ⇒
                    //  assert
                    |= ( a > b + 2 ) flatMap
                        { _ ⇒ |= ( a === ( Ints( 2 ) * c ) + 10 ) } flatMap
                        { _ ⇒ |= ( c + b <= 1000 ) } flatMap
                        { _ ⇒ |= ( d >= e ) } flatMap
                        { _ ⇒ checkSat() }
            }
            r match {
                case x @ Success( _ ) ⇒ response shouldBe r
                case f @ Failure( _ ) ⇒ f should matchPattern {
                    case Failure( ParseErrorException( _ ) ) ⇒
                }
            }
        }
    }
}

/**
 * Non-linear Arithmetics (integer and real) -- checksat and getmodel, getvalue
 */
class NonLinearArithmetic1 extends FunSuite
    with Matchers
    with TableDrivenPropertyChecks with Core
    with IntegerArithmetics
    with RealArithmetics
    with Commands
    with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName =
        """| ===============================================================
           | Examples from http://rise4fun.com/Z3/tutorial/guide
           | Non Linear Arithmetic -- AUFNIRA
           |    (declare-const a Int)
           |    (assert (> (* a a) 3))
           |
           |    (check-sat)
           |    (get-model)
           |    )
           |//  should be sat""".stripMargin

    import interpreters.SMTSolver
    import configurations.SMTLogics.AUFNIRA
    import configurations.SMTOptions.SMTProduceModels
    import configurations.AppConfig.config
    import configurations.SMTInit

    //  Solvers to be included in the tests
    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( AUFNIRA ) ) : _* )

    val theTests = Table[ Int ](
        "value",
        3,
        9,
        81,
        230,
        -4 )

    //  initialise sequence
    val initSeq = new SMTInit( AUFNIRA, List( SMTProduceModels( true ) ) )

    for ( s ← theSolvers if ( s.enabled ); t ← theTests ) {
        test( s"Checking a * a > [$t] with [${s.name}] -- value should v satisfy v * v > $t" ) {

            //  define int var a
            val a = Ints( "a" )

            val model = using( new SMTSolver( s, initSeq ) ) {
                implicit withSolver ⇒
                    //  assert
                    |= ( a * a > Ints( t ) ) flatMap
                        { _ ⇒ checkSat() } flatMap
                        { _ ⇒ getModel() }
            }

            model match {
                case Success( m ) ⇒
                    //  get the value as an Int
                    val z = m.valueOf( a )
                    z match {
                        case Some( zValue ) ⇒
                            val valz = zValue.show.toInt
                            logger.info( s"Value of a for target $t is: $valz " )
                            valz * valz should be > t
                        case None ⇒ fail ( new Exception( "Could not get value for z" ) )
                    }

                case Failure( e ) ⇒ fail( e.getMessage )
            }
        }
    }
}

/**
 * Non-linear Arithmetics (real) -- check-sat
 */
class NonLinearArithmetic2 extends FunSuite
    with Matchers
    with TableDrivenPropertyChecks with Core
    with RealArithmetics
    with Commands
    with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName =
        """| ===============================================================
           | Examples from http://rise4fun.com/Z3/tutorial/guide
           | Non Linear Arithmetic -- AUFNIRA
           | (echo "Z3 does not always find solutions to non-linear problems")
           | (declare-const b Real)
           | (declare-const c Real)
           |    (assert ( =
           |        (+ (* b b b) (* b c) )
           |        3.0)
           |    )
           |
           |    (check-sat)
           |    (get-model)
           |    )
           |//  should be sat""".stripMargin

    //  format: OFF
    import configurations.SMTLogics.{QF_NRA,AUFNIRA}
            import configurations.SMTOptions.SMTProduceModels
    import configurations.AppConfig.enabledSolvers
    import configurations.{SMTInit, SolverConfig}
    import interpreters.SMTSolver

    val theSolvers = Table[ String, SMTInit, Try[SatResponses] ](
        ("Solver"     ,       "InitSeq"             , "Expected answer")  ,

        ("Z3",  new SMTInit(AUFNIRA, List(SMTProduceModels(true))) , Success(UnKnown())) ,

        //  "When presented only non-linear constraints over reals,
        //  Z3 uses a specialized complete solver"
        //  {@link http://rise4fun.com/Z3/tutorial/guide}
        ("Z3",  new SMTInit(QF_NRA, List(SMTProduceModels(true)))  , Success(Sat())) ,

        //  if Z3 is configured is auto-configured), it can find Sat
        ("Z3",  new SMTInit(List(SMTProduceModels(true)))               , Success(Sat()))
    )
    //  format: ON

    val theTests = Table[ Double ](
        "value",
        3.0 )

    for ( ( sName, ic, r ) ← theSolvers if ( enabledSolvers.contains( sName ) ); t ← theTests ) {
        test(
            """|
               | Checking ( b * b * b ) + ( b * c ) === 3.0  with
               |""".stripMargin + s"with [${sName}] configured with ${ic.show} -- check-sat should give $r" ) {

                //  define int var b
                val b = Reals( "b" )
                val c = Reals( "c" )

                using( new SMTSolver( sName, ic ) ) {
                    implicit withSolver ⇒
                        //  assert
                        |= ( ( b * b * b ) + ( b * c ) === 3.0 ) flatMap
                            { _ ⇒ checkSat() }
                } shouldBe r

            }
    }
}

/**
 * Non-linear Arithmetics (real) -- checksat, getmodel, getvalue
 */
class NonLinearArithmetic3 extends FunSuite
    with Matchers
    with TableDrivenPropertyChecks with Core
    with RealArithmetics
    with Commands
    with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName =
        """| ===============================================================
           | Examples from http://rise4fun.com/Z3/tutorial/guide
           | Non Linear Arithmetic
           | (echo "When presented only non-linear constraints over reals,
           | Z3 uses a specialized complete solver")
           | (declare-const b Real)
           | (declare-const c Real)
           |    (assert ( =
           |        (+ (* b b b) (* b c) )
           |        3.0)
           |    )
           |
           |    (check-sat)
           |    (get-model)
           |    )
           |//  should be sat""".stripMargin

    import configurations.SMTLogics.QF_NRA
    import configurations.SMTOptions.SMTProduceModels
    import configurations.{ SolverConfig, SMTInit }
    import configurations.AppConfig.enabledSolvers
    import interpreters.SMTSolver

    //  format: OFF
    val theSolvers = Table[ String, SMTInit, Try[SatResponses] ](
        ("Solver",                          "InitSeq"                      ,    "Expected answer")  ,

        //  "When presented only non-linear constraints over reals,
        //  Z3 uses a specialized complete solver"
        //  {@link http://rise4fun.com/Z3/tutorial/guide}
        ("Z3" ,  new SMTInit(QF_NRA, List(SMTProduceModels(true)) ), Success(Sat())) ,

        //  if Z3 is configured is auto-configured), it can find Sat
        // (new Z3 with Models              , Success(Sat()))
        ("Z3" ,  new SMTInit(List(SMTProduceModels(true)) ),         Success(Sat()))
    )
    //  format: ON

    val theTests = Table[ Double ](
        "value",
        3.0 )

    for ( ( sName, ic, r ) ← theSolvers if ( enabledSolvers.contains( sName ) ); t ← theTests ) {
        test(
            """|
               | Checking ( b * b * b ) + ( b * c ) === 3.0  with
               |""".stripMargin + s"with [${sName}] configured with ${ic.show} -- check-sat should give $r" ) {
                //  define int var b
                val b = Reals( "b" )
                val c = Reals( "c" )

                val model = using( new SMTSolver( sName, ic ) ) {
                    implicit withSolver ⇒
                        //  assert
                        |= ( ( b * b * b ) + ( b * c ) === 3.0 ) flatMap
                            { _ ⇒ checkSat() } flatMap
                            { _ ⇒ getModel() }
                }

                model match {
                    case Success( m ) ⇒

                        //  get the value as a pair alpha/beta
                        ( m.valueOf( b ), m.valueOf( c ) ) match {
                            case ( Some( valb ), Some( valc ) ) ⇒
                                val z1 = valb.show.split( "/" )
                                val z2 = valc.show.split( "/" )

                                val z1R = z1( 0 ).toDouble / z1( 1 ).toDouble
                                val z2R = z2( 0 ).toDouble / z2( 1 ).toDouble
                                logger.info( s"Value of (b,c) for target $t is: ($z1R,$z2R) " )
                                ( z1R * z1R * z1R ) + ( z1R * z2R ) shouldBe 3.0

                            case ( None, _ ) | ( _, None ) ⇒ fail( new Exception( "Could not get value for b or c" ) )
                        }

                    case Failure( e ) ⇒ fail( e )
                }
            }
    }
}

/**
 *
 */
class NonLinearArithmetic4 extends FunSuite
    with Matchers
    with TableDrivenPropertyChecks with Core
    with IntegerArithmetics
    with Commands
    with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    val logger = Logger( LoggerFactory.getLogger( this.getClass ) )

    override def suiteName =
        """| ===============================================================
           | Examples from http://rise4fun.com/Z3/tutorial/guide
           | Non Linear Arithmetic -- QF_LIA
           | (declare-const a Int)
           | (declare-const r1 Int)
           | (declare-const r2 Int)
           | (declare-const r3 Int)
           | (declare-const r4 Int)
           | (declare-const r5 Int)
           | (declare-const r6 Int)
           | (assert (= a 10))
           | (assert (= r1 (div a 4))) ; integer division
           | (assert (= r2 (mod a 4))) ; mod
           | ;;(assert (= r3 (rem a 4))) ; remainder only Z3
           | (assert (= r4 (div a (- 4)))) ; integer division
           | (assert (= r5 (mod a (- 4)))) ; mod
           | ;;(assert (= r6 (rem a (- 4)))) ; remainder only Z3
           | (check-sat)
           | (get-model)
           |    )
           |//  should be sat""".stripMargin

    //  Solvers to be included in the tests
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_LIA
    import configurations.SMTOptions.SMTProduceModels
    import configurations.AppConfig.config
    import configurations.SMTInit

    val theSolvers = Table(
        "Solver",
        config.filter( s ⇒ s.enabled && s.supportsLogic( QF_LIA ) ) : _* )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA, List( SMTProduceModels( true ) ) )

    for ( s ← theSolvers ) {
        if ( s.name.startsWith( "Yices" ) ) {
            test( s"[${s.name}] does not fully support models" ) {}
        } else
            test( s"Checking (div, mod and rem  with [${s.name}] -- check-sat" ) {

                //  define int var a, b, c
                val a = Ints( "a" )
                //  define 7 variables r1-r6
                val r = Range( 1, 7 ).map( "r" + _ ).map( Ints( _ ) )

                val model = using( new SMTSolver( s, initSeq ) ) {
                    implicit withSolver ⇒
                        //  assert
                        |= ( a === 10 ) flatMap
                            { _ ⇒ |= ( r( 1 ) === ( a / 4 ) ) } flatMap
                            { _ ⇒ |= ( r( 2 ) === ( a % 4 ) ) } flatMap
                            // { _ ⇒ |= ( r( 3 ) === ( a rem 4 ) ) } flatMap
                            { _ ⇒ |= ( r( 4 ) === ( a / -4 ) ) } flatMap
                            { _ ⇒ |= ( r( 5 ) === ( a % -4 ) ) } flatMap
                            // { _ ⇒ |= ( r( 6 ) === ( a rem -4 ) ) } flatMap
                            { _ ⇒ checkSat() } flatMap
                            { _ ⇒ getModel() }
                }
                model match {
                    case Success( m ) ⇒
                        m.valueOf( a ) match {
                            case Some( vala ) ⇒
                                val za = vala.show.toInt
                                //  get values for r(i)

                                val valr1 = m.valueOf( r( 1 ) )
                                val valr2 = m.valueOf( r( 2 ) )
                                // val valr3 = m.valueOf( r( 3 ) )
                                val valr4 = m.valueOf( r( 4 ) )
                                val valr5 = m.valueOf( r( 5 ) )
                                // val valr6 = m.valueOf( r( 6 ) )

                                ( valr1, valr2, valr4, valr5 ) match {
                                    case ( Some( r1 ), Some( r2 ), Some( r4 ), Some( r5 ) ) ⇒
                                        r1.show.toInt shouldBe za / 4
                                        r2.show.toInt shouldBe za % 4
                                        // r3.show.toInt shouldBe za % 4
                                        //
                                        r4.show.toInt shouldBe za / -4
                                        r5.show.toInt shouldBe za % -4
                                    // r6 shouldBe -( za % 4 )
                                    case _ ⇒ fail( new Exception( s"Could not find values for some r(i). $m" ) )
                                }

                            case _ ⇒ fail( new Exception( s"Could not find values for a. $m" ) )
                        }

                    case Failure( e ) ⇒ fail( e )
                }
            }
    }
}
