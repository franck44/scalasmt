/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import org.bitbucket.franck44.scalasmt.theories.{ IntegerArithmetics, Core }
import org.bitbucket.franck44.scalasmt.interpreters.Resources
import org.bitbucket.franck44.scalasmt.typedterms.Commands

class ConcurrentSolverTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Commands with Resources {

    override def suiteName = "Concurrent Solvers basic test suite"

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat, UnKnown, SatResponses }
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.show
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.SMTInit

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA )

    import parser.SMTLIB2Syntax.{ SatResponses }
    import scala.concurrent.{ Future, Promise }
    import scala.concurrent.ExecutionContext.Implicits.global

    test( "Create solvers and run them in parallel" ) {
        val p = Promise[ Try[ ( String, SatResponses ) ] ]()
        val f1 = Future {
            using( new SMTSolver( "Z3-4.8.6", initSeq ) ) {
                implicit solver ⇒
                    {
                        isSat( Ints ( 1 ) < Ints ( 2 ) )
                    }
            }
                .flatMap (
                    { r ⇒ Success( ( "Z3-4.8.6", r ) ) } )
        }

        val f2 = Future {
            using( new SMTSolver( "CVC4", initSeq ) ) {
                implicit solver ⇒
                    {
                        isSat( Ints ( 1 ) < Ints ( 2 ) )
                    }
            }
                .flatMap (
                    { r ⇒ Success( ( "CVC4", r ) ) } )
            // Failure( new Exception( "Immedialtely fails" ) )
        }

        val f3 = Future {
            using( new SMTSolver( "MathSat", initSeq ) ) {
                implicit solver ⇒
                    {
                        isSat( Ints ( 1 ) < Ints ( 2 ) )
                    }
            }
                .flatMap (
                    { r ⇒ Success( ( "MathSat", r ) ) } )
        }

        import scala.concurrent.duration._

        List( f1, f2, f3 ) foreach {
            t ⇒
                t foreach {
                    x ⇒
                        x.isSuccess match {
                            case true ⇒ p.trySuccess( x )
                            case _    ⇒
                        }
                }
        }

        Future {
            Thread.sleep( 10.seconds.toMillis )
            Failure( new Exception( "timeouts" ) )
        } foreach { x ⇒
            p.trySuccess( x )
        }

        p.future.onComplete {
            case Success( Success( ( name, s ) ) ) ⇒ succeed
            case Success( Failure( f ) )           ⇒ fail()
            case Failure( f )                      ⇒ fail()
        }

    }

    import interpreters.SolverCompose.Parallel

    test( "Using solver strategy" ) {

        val strat = Parallel (
            List(
                new SMTSolver( "CVC4", initSeq ),
                new SMTSolver( "Z3-4.8.6", initSeq ),
                new SMTSolver( "MathSat", initSeq ) ) )

        using( strat ) {
            implicit solver ⇒
                {
                    isSat( Ints ( 1 ) < Ints ( 2 ) ) match {
                        case Success( s ) if ( s != UnKnown() ) ⇒ Success( s )
                        case Success( UnKnown() )               ⇒ Failure( new Exception( "Unknow response" ) )
                        case other                              ⇒ other
                    }
                }
        } shouldBe Success( Sat() )
    }

}
