/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ FPBitVectors, Core }
import interpreters.{ Resources, SolverCompose }
import typedterms.{ TypedTerm, Commands }
import SolverCompose.Parallel

/**
 * We are using the implicits and operators to build complex
 * TypedTerms and check sat
 */
class ConcurrentFPBVTests extends FunSuite with TableDrivenPropertyChecks with Matchers with FPBitVectors with Core with Commands with Resources {

    override def suiteName = "FPBV with concurrent solvers arithmetic terms check-sat"

    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ Sat, UnSat, UnKnown }
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_FPBV
    import configurations.AppConfig.config
    import configurations.SMTInit
    import FPBitVectors._
    import scala.concurrent.duration._

    // //  Solvers to be included in the tests
    val theSolvers =
        config.filter( s ⇒ s.name == "Z3-4.8.6" || s.name == "MathSat" )

    //  initialise sequence
    val initSeq = new SMTInit( QF_FPBV )

    val x = FPBVs( "x", 11, 53 )
    val y = FPBVs( "y", 11, 53 )
    val z = FPBVs( "z", 11, 53 )
    implicit val rm = RMs( "rm" )

    val initSolvers = theSolvers.map ( s ⇒ new SMTSolver( s, initSeq ) )

    test( s"[${theSolvers.map( _.name )}] fp.rem: x + 1.0 % 2.0 === 3.0 -- should be UNSAT" ) {

        //  with using
        using( Parallel( initSolvers.reverse, Some( 100.seconds ) ) ) {
            implicit solver ⇒
                {
                    isSat( ( x + 1.asFloat64 ) % 2.asFloat64 === 3.asFloat64 ) match {
                        case Success( s ) if ( s == Sat() || s == UnSat() ) ⇒ Success( s )
                        case Success( r ) ⇒ Failure( new Exception( r.toString ) )
                        case other ⇒ other
                    }
                }
        } shouldBe Success( UnSat() )
    }

    test( s"[${theSolvers.map( _.name )}]: x + 1.0 - 2.0 === 3.0 -- should be SAT" ) {

        //  with using
        using( Parallel( initSolvers.reverse, Some( 100.seconds ) ) ) {
            implicit solver ⇒
                {
                    isSat( ( x + 1.asFloat64 ) - 2.asFloat64 === 3.asFloat64 ) match {
                        case Success( s ) if ( s == Sat() || s == UnSat() ) ⇒ Success( s )
                        case Success( r ) ⇒ Failure( new Exception( r.toString ) )
                        case other ⇒ other
                    }
                }
        } shouldBe Success( Sat() )

        //  with using
        using( Parallel( initSolvers.reverse, Some( 100.seconds ) ) ) {
            implicit solver ⇒
                {
                    isSat( ( x + 1.asFloat64 ) - 2.asFloat64 === 3.asFloat64 ) match {
                        case Success( s ) if ( s == Sat() || s == UnSat() ) ⇒ Success( s )
                        case Success( r ) ⇒ Failure( new Exception( r.toString ) )
                        case other ⇒ other
                    }
                }
        } shouldBe Success( Sat() )
    }

}