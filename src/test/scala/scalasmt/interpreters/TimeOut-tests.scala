/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package interpreters
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import parser.PredefinedParsers
import scala.concurrent.TimeoutException
import interpreters.Resources

/**
 * Timeout tests
 */
class TimeoutTests extends FunSuite with TableDrivenPropertyChecks with Matchers with PredefinedParsers with Resources {

    override def suiteName = "Check solver response timeout"

    import configurations.AppConfig.config

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( _.enabled ) : _* )

    for ( solver ← theSolvers )
        test( s"[${solver.name}] Send a (set-option :print-success false) with 1 seconds timeout -- should time out" ) {

            import scala.concurrent.duration._
            import resource._
            import parser.SMTLIB2Parser.ParseErrorException

            for ( trySolver ← managed ( new SMTSolver( solver ) ) ) {

                trySolver.init.isSuccess shouldBe true

                import trySolver.eval

                (
                    eval ( SetOptionCmd( PrintSuccess ( false ) ), Some( 1.seconds ) ) flatMap
                    //
                    parseAsSuccess ) should
                    matchPattern {
                        case Failure( e ) if solver.name.startsWith( "Z3" ) && e.isInstanceOf[ ParseErrorException ] ⇒
                        case Failure( e ) if e.isInstanceOf[ TimeoutException ]                                      ⇒
                    }
            }
        }
}
