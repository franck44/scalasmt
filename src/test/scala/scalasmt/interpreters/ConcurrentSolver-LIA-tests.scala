/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ IntegerArithmetics, Core }
import interpreters.{ Resources, SolverCompose }
import typedterms.{ TypedTerm, Commands }
import SolverCompose.Parallel

class SimpleConcurrentLIATests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Commands with Resources {

    override def suiteName = "QF_LIA with concurrent solvers simple check-sat test suite"

    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ Term, Sat, UnKnown, SatResponses }
    import theories.BoolTerm
    import parser.SMTLIB2PrettyPrinter.show
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.SMTInit
    //  Solvers to be included in the tests
    val theSolvers =
        config.filter( s ⇒ s.enabled && s.supportsLogic ( QF_LIA ) )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA )

    val theTerms = Table[ Seq[ TypedTerm[ BoolTerm, Term ] ], SatResponses ](
        ( "Terms", "Expected response" ),

        //  1 < 2 should be Sat
        (
            Seq( Ints ( 1 ) < Ints ( 2 ) ),
            Sat() ),

        //  1 < 2 and 2 < 3, Sat
        (
            Seq( Ints ( 1 ) < Ints ( 2 ), Ints ( 2 ) <= Ints ( 3 ) ),
            Sat() ),

        //  x < 2 and 1 < x should be Sat
        (
            Seq(
                Ints( "x" ) < Ints( 2 ),
                Ints( 1 ) <= Ints( "x" ) ),
                Sat() ),

        //  x == -4, should be Sat
        (
            Seq(
                Ints( "x" ) === Ints( -4 ) ),
                Sat() ) )

    for ( ( xt, r ) ← theTerms ) {

        test( s"[${theSolvers.map( _.name )}] configured with ${initSeq.show} to check sat for terms ${xt.map( x ⇒ show( x.termDef ) )} -- should be SAT" ) {

            val initSolvers = theSolvers.map ( s ⇒ new SMTSolver( s, initSeq ) )

            //  with using
            using( Parallel( initSolvers ) ) {
                implicit solver ⇒
                    {
                        //  smtlib package eval is used
                        isSat( xt : _* ) match {
                            case Success( s ) if s != UnKnown() ⇒ Success( s )
                            case Success( UnKnown() )           ⇒ Failure( new Exception( "Unknown" ) )
                            case other                          ⇒ other
                        }
                    }
            } shouldBe Success( Sat() )
        }
    }

}

/**
 * We are using the implicits and operators to build complex
 * TypedTerms and check sat
 */
class ConcurrentArithmeticOperatorsTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Commands with Resources {

    override def suiteName = "QF_LIA with concurrent solvers arithmetic terms check-sat"

    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ Sat, UnSat, UnKnown }
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.SMTInit

    //  Solvers to be included in the tests
    val theSolvers =
        config.filter( s ⇒ s.enabled && s.supportsLogic ( QF_LIA ) )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA )

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    val initSolvers = theSolvers.map ( s ⇒ new SMTSolver( s, initSeq ) )

    test( s"[${theSolvers.map( _.name )}] Addition: x + 1 = 3  -- should be SAT" ) {

        //  with using
        using( Parallel( initSolvers ) ) {
            implicit solver ⇒
                {
                    isSat( x + 1 === 3 ) match {
                        case Success( s ) if s != UnKnown() ⇒ Success( s )
                        case Success( UnKnown() )           ⇒ Failure( new Exception( "Unknown" ) )
                        case other                          ⇒ other
                    }
                }
        } shouldBe Success( Sat() )
    }

    test( s"[${theSolvers.map( _.name )}] 3-ary Addition: x + 1 + y = 3  -- should be SAT" ) {

        //  with using
        using( Parallel( initSolvers ) ) {
            implicit solver ⇒
                {
                    isSat( x + 1 + y === 3 ) match {
                        case Success( s ) if s != UnKnown() ⇒ Success( s )
                        case Success( UnKnown() )           ⇒ Failure( new Exception( "Unknown" ) )
                        case other                          ⇒ other
                    }
                }
        } shouldBe Success( Sat() )
    }

    test( s"[${theSolvers.map( _.name )}] Addition with 3 operands:  x + 1 + y = 3  -- should be SAT" ) {

        //  with using
        using( Parallel( initSolvers ) ) {
            implicit solver ⇒
                {
                    isSat( x + 1 + y === 3 ) match {
                        case Success( s ) if s != UnKnown() ⇒ Success( s )
                        case Success( UnKnown() )           ⇒ Failure( new Exception( "Unknown" ) )
                        case other                          ⇒ other
                    }
                }
        } shouldBe Success( Sat() )
    }

    test( s"[${theSolvers.map( _.name )}] Subtraction:  x - 1 = 3  -- should be SAT" ) {

        //  with using
        using( Parallel( initSolvers ) ) {
            implicit solver ⇒
                {
                    isSat( x - 1 === 3 ) match {
                        case Success( s ) if s != UnKnown() ⇒ Success( s )
                        case Success( UnKnown() )           ⇒ Failure( new Exception( "Unknown" ) )
                        case other                          ⇒ other
                    }
                }
        } shouldBe Success( Sat() )
    }

    test( s"[${theSolvers.map( _.name )}] Unary Subtraction:  - y > 3 and x + y == -1 -- should be SAT" ) {

        //  with using
        using( Parallel( initSolvers ) ) {
            implicit solver ⇒
                {
                    isSat( ( -y > 3 ) & ( x + y === -1 ) ) match {
                        case Success( s ) if s != UnKnown() ⇒ Success( s )
                        case Success( UnKnown() )           ⇒ Failure( new Exception( "Unknown" ) )
                        case other                          ⇒ other
                    }
                }
        } shouldBe Success( Sat() )
    }

    test( s"[${theSolvers.map( _.name )}] Unsat test: x < 1 and x >= 2 -- should be UNSAT" ) {
        using( Parallel( initSolvers ) ) {
            implicit solver ⇒
                {
                    isSat( x < 1 & x >= 2 ) match {
                        case Success( s ) if s != UnKnown() ⇒ Success( s )
                        case Success( UnKnown() )           ⇒ Failure( new Exception( "Unknown" ) )
                        case other                          ⇒ other
                    }
                }
        } shouldBe Success( UnSat() )
    }

    test( s"[${theSolvers.map( _.name )}] Multiplication test: x * 2 < 1 -- should be SAT" ) {
        using( Parallel( initSolvers ) ) {
            implicit solver ⇒
                {
                    isSat( x * 2 < 1 )
                }
        } shouldBe Success( Sat() )
    }

    test( s"[${theSolvers.map( _.name )}] Unsat test: x == 2,  y > 2, y == x + 1, z == 3, z == x should be UNSAT" ) {
        using( Parallel( initSolvers ) ) {
            implicit solver ⇒
                {
                    isSat(
                        x === 2,
                        y >= 2,
                        y === x + 1,
                        z === 3,
                        z === x ) match {
                            case Success( s ) if s != UnKnown() ⇒ Success( s )
                            case Success( UnKnown() )           ⇒ Failure( new Exception( "Unknown" ) )
                            case other                          ⇒ other
                        }
                }
        } shouldBe Success( UnSat() )
    }

    test( s"[${theSolvers.map( _.name )}] Sat test: i + j + 1 == 2 -- should be SAT" ) {
        using( Parallel( initSolvers ) ) {
            implicit solver ⇒
                {
                    val i = Ints( "i" )
                    val j = Ints( "j" )
                    isSat(
                        i + j + 1 === 2 ) match {
                            case Success( s ) if s != UnKnown() ⇒ Success( s )
                            case Success( UnKnown() )           ⇒ Failure( new Exception( "Unknown" ) )
                            case other                          ⇒ other
                        }
                }
        } shouldBe Success( Sat() )
    }
}

class ConcurrentAssertBoolTermTests extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with Commands with Resources {

    import scala.util.{ Success, Failure }
    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat, UnKnown, SatResponses }
    import theories.BoolTerm
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.SMTInit

    override def suiteName = "QF_LIA with concurrent solvers boolean terms check-sat"

    //  Solvers to be included in the tests
    val theSolvers =
        config.filter( s ⇒ s.enabled && s.supportsLogic ( QF_LIA ) )

    //  initialise sequence
    val initSeq = new SMTInit( QF_LIA )

    val theTerms = Table[ TypedTerm[ BoolTerm, Term ], SatResponses ](
        ( "Terms", "Response" ),
        ( True(), Sat() ),
        ( False(), UnSat() ) )

    for ( ( t, r ) ← theTerms ) {

        test( s"[${theSolvers.map( _.name )}]  to check sat for terms ${show( t.termDef )} -- should be ${show( r )}" ) {
            val initSolvers = theSolvers.map ( s ⇒ new SMTSolver( s, initSeq ) )

            //  with using
            using( Parallel( initSolvers ) ) {
                implicit solver ⇒
                    {
                        isSat( t ) match {
                            case Success( s ) if s != UnKnown() ⇒ Success( s )
                            case Success( UnKnown() )           ⇒ Failure( new Exception( "Unknown" ) )
                            case other                          ⇒ other
                        }
                    }
            } shouldBe Success ( r )
        }
    }
}
