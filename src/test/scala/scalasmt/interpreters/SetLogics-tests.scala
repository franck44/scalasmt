/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package interpreters
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import parser.SMTLIB2Parser.ParseErrorException
import parser.PredefinedParsers
import resource._
import interpreters.Resources

class SetSupportedLogicTests extends FunSuite with TableDrivenPropertyChecks with Matchers with PredefinedParsers with Resources {

    import configurations.AppConfig.config

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( _.enabled ) : _* )

    //  test supported logics
    for ( solver ← theSolvers; l ← solver.supportedLogics ) {

        test( s"[${solver.name}] Create (managed) solver and (set-logic $l) for all supported logics -- Should always succeed" ) {

            for ( managedSolver ← managed( new SMTSolver( solver ) ) ) {

                managedSolver.init.isSuccess shouldBe true

                managedSolver.eval ( SetLogicCmd( SSymbol ( l.toString ) ) ) flatMap
                    parseAsGeneralResponse shouldBe
                    Success( SuccessResponse() )
            }
        }
    }

}

class SetUnsupportedLogics extends FunSuite with TableDrivenPropertyChecks with Matchers with PredefinedParsers with Resources {

    import configurations.AppConfig.config

    val theSolvers = Table( "Solver", config.filter( _.enabled ) : _* )

    val badLogic1 = "QFF_LIA"

    for ( solver ← theSolvers ) {

        test( s"[${solver.name}] Create (managed) solver and (set-logic $badLogic1) -- Uses an unsupported logic -- Should yield an error" ) {

            val l = badLogic1

            for ( managedSolver ← managed( new SMTSolver( solver ) ) ) {

                managedSolver.init.isSuccess shouldBe true

                managedSolver.eval ( SetLogicCmd( SSymbol ( l ) ) ) flatMap
                    parseAsGeneralResponse should matchPattern {
                        case Success( SuccessResponse() ) if solver.name.startsWith( "MathSat" ) ⇒
                        case Success( ErrorResponse( _ ) ) if ( solver.name contains "Yices" ) ⇒
                        case Failure( e ) if ( ( solver.name contains "CVC4" ) || solver.name == "Boolector" ) ⇒
                        case Success( UnsupportedResponse() ) ⇒
                    }

                //  send another set-logic
                managedSolver.eval ( SetLogicCmd( SSymbol ( l ) ) ) flatMap
                    parseAsGeneralResponse should
                    matchPattern {
                        case Success( ErrorResponse( _ ) ) if solver.name.startsWith( "MathSat" ) ⇒
                        case Success( ErrorResponse( _ ) ) if ( solver.name contains "Yices" ) ⇒
                        case Failure( e ) if ( ( solver.name.contains( "CVC4" ) ) || solver.name == "Boolector" ) ⇒
                        case Success( UnsupportedResponse() ) ⇒
                    }

                //  send first supported logic in solver
                solver.supportedLogics.toList match {
                    case x :: _ ⇒
                        managedSolver.eval ( SetLogicCmd( SSymbol ( x.toString ) ) ) flatMap
                            parseAsGeneralResponse should
                            matchPattern {
                                case Success( ErrorResponse( _ ) ) if solver.name == "MathSat" ⇒
                                case Failure( e ) if ( ( solver.name contains "CVC4" ) || solver.name == "Boolector" ) ⇒
                                case Success( SuccessResponse() ) ⇒
                            }
                    case Nil ⇒ assert( true )
                }
            }
        }
    }

    val badLogic2 = "123"

    for ( solver ← theSolvers ) {

        test( s"[${solver.name}] Create (managed) solver and (set-logic $badLogic2) -- Uses an unsupported logic -- Should yield an error" ) {

            val l = badLogic2

            for ( managedSolver ← managed( new SMTSolver( solver ) ) ) {

                managedSolver.init.isSuccess shouldBe true

                import managedSolver.eval
                val r1 = eval ( SetLogicCmd( SSymbol ( l ) ) ) flatMap parseAsGeneralResponse

                r1 should matchPattern {
                    case Failure( _ ) if solver.name contains "CVC4"      ⇒
                    case Failure( _ ) if solver.name == "MathSat"         ⇒
                    case Failure( _ ) if solver.name == "Boolector"       ⇒
                    case Failure( _ ) if ( solver.name contains "Yices" ) ⇒
                    case Success( ErrorResponse( _ ) )                    ⇒
                }

                //  send another set-logic
                val r2 = eval ( SetLogicCmd( SSymbol ( l ) ) ) flatMap parseAsGeneralResponse

                r2 should matchPattern {
                    case Failure( _ ) if solver.name contains "CVC4"      ⇒
                    case Failure( _ ) if solver.name == "MathSat"         ⇒
                    case Failure( _ ) if solver.name == "Boolector"       ⇒
                    case Failure( _ ) if ( solver.name contains "Yices" ) ⇒
                    case Success( ErrorResponse( _ ) )                    ⇒
                }

                //  send first supported logic in solver
                solver.supportedLogics.toList match {
                    case x :: _ ⇒
                        val r3 = eval ( SetLogicCmd( SSymbol ( x.toString ) ) )

                        r3.flatMap( parseAsGeneralResponse ) should
                            matchPattern {
                                case Failure( _ ) if solver.name == "MathSat"         ⇒
                                case Failure( _ ) if solver.name contains "CVC4"      ⇒
                                case Failure( _ ) if solver.name == "Boolector"       ⇒
                                case Failure( _ ) if ( solver.name contains "Yices" ) ⇒
                                case Success( SuccessResponse() )                     ⇒
                            }

                    case Nil ⇒ assert( true )
                }
            }
        }
    }
}
