/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package interpreters
package tests

import org.scalatest.{ FunSuite, Matchers }
import scala.util.{ Try, Success, Failure }
import org.scalatest.prop.PropertyChecks
import parser.SMTLIB2Syntax._
import parser.{ SMTLIB2Parser, PredefinedParsers }
import scala.concurrent.TimeoutException

/**
 * Check set Boolean options for the solvers.
 */
class SetBoolOptionTests extends FunSuite with PropertyChecks with Matchers with PredefinedParsers with Resources {

    //  get managed
    import resource._
    import parser.SMTLIB2PrettyPrinter.show
    import configurations.AppConfig.config
    import scala.concurrent.duration._

    //  Solvers to be included in the tests are the enabled ones.
    val theSolvers = config.filter( _.enabled )

    import configurations.SMTOptions.{ allBoolOptions, SMTPrintSuccess }

    //  Test of boolean options.
    for (
        solver ← theSolvers;
        op ← allBoolOptions;
        b ← List( true, false ) if ( b || !( solver.name == "CVC4-1.4" ) )
    ) {
        test( s"[${solver.name}] ${show( SetOptionCmd( op( b ) ) )} $b " ) {
            for ( managedSolver ← managed ( new SMTSolver( solver ) ) ) {

                //  initialise solver with print-success true
                managedSolver.init.isSuccess shouldBe true

                managedSolver.eval ( SetOptionCmd( op( b ) ) )
                    .flatMap ( parseAsGeneralResponse ) should matchPattern {
                        case Success( SuccessResponse() ) if solver.supports( op ) ⇒
                        case Success( ErrorResponse( _ ) ) ⇒
                        case Success( UnsupportedResponse() ) ⇒
                        case Failure( _ ) ⇒
                    }
            }
        }
    }

    //  Test of printSuccess  option.
    for (
        solver ← theSolvers;
        b ← List( true, false )
    ) {
        test( s"[${solver.name}] ${show( SetOptionCmd( SMTPrintSuccess( b ) ) )}" ) {
            for ( managedSolver ← managed ( new SMTSolver( solver ) ) ) {

                //  initialise solver with print-success true
                managedSolver.init.isSuccess shouldBe true

                managedSolver.eval ( SetOptionCmd( SMTPrintSuccess( b ) ), Some( 1.second ) )
                    .flatMap ( parseAsGeneralResponse ) should matchPattern {
                        case Success( SuccessResponse() ) if b ⇒
                        case Failure( _ )                      ⇒
                    }
            }
        }
    }
}

/**
 * Check set Int options for the solvers
 */
class SetIntOptionTests extends FunSuite with PropertyChecks with Matchers with PredefinedParsers with Resources {

    //  get managed
    import resource._
    import parser.SMTLIB2PrettyPrinter.show
    import configurations.AppConfig.config
    import scala.concurrent.duration._

    //  Solvers to be included in the tests are the enabled ones.
    val theSolvers = config.filter( _.enabled )

    import configurations.SMTOptions.{ allIntOptions }

    //  Test of numeric options.
    for (
        solver ← theSolvers;
        op ← allIntOptions;
        n ← List( -1, -25, 12, 245, 102340, 34565566 )
    ) {
        test( s"[${solver.name}] ${show( SetOptionCmd( op( n ) ) )} -- ${solver.supports( op )}" ) {
            for ( managedSolver ← managed ( new SMTSolver( solver ) ) ) {

                //  initialise solver with print-success true
                managedSolver.init.isSuccess shouldBe true

                managedSolver.eval ( SetOptionCmd( op( n ) ) )
                    .flatMap ( parseAsGeneralResponse ) should matchPattern {
                        case Success( SuccessResponse() ) if ( solver.supports( op ) && ( n > 0 || solver.name.startsWith( "CVC4" ) ) ) ⇒
                        case Success( UnsupportedResponse() ) if ( !solver.supports( op ) || ( n < 0 ) ) ⇒
                        case Success( ErrorResponse( _ ) ) ⇒
                        case Failure( _ ) ⇒

                    }
            }
        }
    }
}

/**
 * Check set String options for the solvers
 */
class SetStringOptionTests extends FunSuite with PropertyChecks with Matchers with PredefinedParsers with Resources {

    //  get managed
    import resource._
    import parser.SMTLIB2PrettyPrinter.show
    import configurations.AppConfig.config
    import scala.concurrent.duration._

    //  Solvers to be included in the tests are the enabled ones.
    val theSolvers = config.filter( _.enabled )

    import configurations.SMTOptions.{ allStringOptions }

    //  Test for string options.
    for (
        solver ← theSolvers;
        op ← allStringOptions;
        n ← List( "stdout" )
    ) {
        test( s"[${solver.name}] ${show( SetOptionCmd( op( n ) ) )} $n " ) {
            for ( managedSolver ← managed ( new SMTSolver( solver ) ) ) {

                managedSolver.init.isSuccess shouldBe true

                managedSolver.eval ( SetOptionCmd( op( n ) ), Some( 1.second ) )
                    .flatMap ( parseAsGeneralResponse ) should matchPattern {
                        case Success( SuccessResponse() ) if ( solver.supports( op ) ) ⇒
                        case Failure( _ ) ⇒
                    }
            }
        }
    }
}

/**
 * Check set Attribute options for the solvers. At the moment none of them supports it.
 */
class SetAttributeOptionTests extends FunSuite with PropertyChecks with Matchers with PredefinedParsers with Resources {

    import configurations.AppConfig.config

    //  Solvers to be included in the tests are the enabled ones.
    val theSolvers = config.filter( _.enabled )

    for ( solver ← theSolvers ) {

        ignore( s"[${solver.name}] (set-option  <attribute>) -- not implemented in grammar yet" ) {

        }

    }
}