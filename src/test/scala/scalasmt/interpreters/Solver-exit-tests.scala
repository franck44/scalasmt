/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package interpreters
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import interpreters.Resources
import parser.PredefinedParsers

/**
 * Send ExitCmd() to solver. Should result in Success("Solver exit")
 *
 */
class ExitCmdTests
    extends FunSuite
    with Matchers
    with TableDrivenPropertyChecks
    with Resources
    with PredefinedParsers {

    //  logger

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = getLogger( this.getClass )

    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.{ SolverConfig, SMTInit }

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( _.enabled ) : _* )

    //  Setup initialisation sequence
    val initSeq = new SMTInit()

    forAll( theSolvers ) {
        ( s : SolverConfig ) ⇒
            test( s"[${s.name}] Send ExitCmd() to solver. Solver used with using/implicit" ) {

                //  with using on solvers and flatMap
                using( new SMTSolver( s, initSeq ) )(
                    implicit withSolver ⇒
                        eval ( Seq( ExitCmd() ) ) ).flatMap( parseAsExitCmdResponse ) shouldBe Success ( ExitCmdResponse() )
            }
    }
}
