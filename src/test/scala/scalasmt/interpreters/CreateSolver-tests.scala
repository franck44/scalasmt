/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package interpreters
package tests

import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources

/**
 * Create a running solver and try to initialise it
 */
class CreateSolver extends FunSuite with TableDrivenPropertyChecks with Matchers with Resources {

    override def suiteName = "Create running solver with no logic/option"

    import configurations.AppConfig.config

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( _.enabled ) : _* )

    for ( solver ← theSolvers )

        test( s"[${solver.name}] Create (managed) solver process" ) {

            import resource._
            for ( managedSolver ← managed ( new SMTSolver( solver ) ) ) {

                managedSolver.init.isSuccess shouldBe true

            }
        }
}
