/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package interpreters
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import parser.PredefinedParsers
import interpreters.Resources

/**
 * Use for comprehension to get a solver and dispose of it at the end.
 *
 * Provided by Scala-ARM package.
 */
class ForComprehensionARMUsage
    extends FunSuite
    with Matchers
    with TableDrivenPropertyChecks
    with PredefinedParsers
    with Resources {

    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.{ SolverConfig, SMTInit }

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( _.enabled ) : _* )

    forAll( theSolvers ) {
        ( s : SolverConfig ) ⇒
            test( s"[${s.name}] Use solver with for comprehension" ) {
                //  use for for comprehension
                import resource._
                //  logic and options to initialise the solver
                val initSeq = new SMTInit()

                for ( trySolver ← managed ( new SMTSolver( s, initSeq ) ) ) {
                    trySolver.init.isSuccess shouldBe true

                    import trySolver.eval

                    eval ( SetOptionCmd( PrintSuccess ( true ) ) ) flatMap
                        //
                        parseAsGeneralResponse shouldBe
                        Success( SuccessResponse() )
                }
            }
    }
}

/**
 * Use custom using from smtlib package to get a solver and dispose of it at the end.
 *
 */
class UsingWithImportUsage
    extends FunSuite
    with Matchers
    with TableDrivenPropertyChecks
    with Resources
    with PredefinedParsers {

    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.{ SolverConfig, SMTInit }

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( _.enabled ) : _* )

    //  logic and options to initialise the solver
    val initSeq = new SMTInit()

    forAll( theSolvers ) {
        ( s : SolverConfig ) ⇒
            test( s"[${s.name}] Use solver with using/implicit" ) {
                //  with using
                using( new SMTSolver( s, initSeq ) ) {
                    solver ⇒
                        {
                            import solver.eval
                            eval ( SetOptionCmd( PrintSuccess ( true ) ) ) flatMap
                                //
                                parseAsGeneralResponse
                        }
                } shouldBe Success( SuccessResponse() )
            }
    }
}

/**
 * Use custom using and eval from smtlib package to get a solver
 * and dispose of it at the end.
 *
 */
class UsingWithImplicitEvalUsage
    extends FunSuite
    with Matchers
    with TableDrivenPropertyChecks
    with Resources
    with PredefinedParsers {

    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.SolverConfig

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( _.enabled ) : _* )

    forAll( theSolvers ) {
        ( s : SolverConfig ) ⇒
            test( s"[${s.name}] Use solver with using/implicit" ) {
                //  with using
                using( new SMTSolver( s ) ) {
                    implicit withSolver ⇒
                        {
                            //  smtlib package eval is used
                            eval ( Seq( SetOptionCmd( PrintSuccess ( true ) ) ) ) flatMap
                                //
                                parseAsGeneralResponse
                        }
                } shouldBe Success( SuccessResponse() )
            }
    }
}

/**
 * Use monadic using and eval from smtlib package to get a solver
 * and dispose of it at the end.
 *
 */
class UsingWithFlatMapEvalUsage
    extends FunSuite
    with Matchers
    with TableDrivenPropertyChecks
    with Resources
    with PredefinedParsers {

    //  logger

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    private val logger = getLogger( this.getClass )

    import configurations.SMTLogics.QF_LIA
    import configurations.AppConfig.config
    import configurations.{ SolverConfig, SMTInit }

    //  Solvers to be included in the tests
    val theSolvers = Table( "Solver", config.filter( _.enabled ) : _* )

    //  logic and options to initialise the solver
    val initSeq = new SMTInit()

    forAll( theSolvers ) {
        ( s : SolverConfig ) ⇒
            test( s"[${s.name}] Use solver with using/implicit" ) {

                //  with using on solvers and flatMap
                using( new SMTSolver( s, initSeq ) )(
                    implicit withSolver ⇒
                        eval ( Seq( SetOptionCmd( PrintSuccess ( true ) ) ) ) ).flatMap( parseAsGeneralResponse ) shouldBe Success ( SuccessResponse() )
            }
    }
}
