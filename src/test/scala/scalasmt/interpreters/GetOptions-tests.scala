/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package interpreters
package tests

import org.scalatest.{ FunSuite, Matchers }
import scala.util.{ Try, Success, Failure }
import org.scalatest.prop.PropertyChecks
import parser.SMTLIB2Syntax._
import parser.{ SMTLIB2Parser, PredefinedParsers }
import scala.concurrent.TimeoutException

/**
 * Check get Boolean options for the solvers.
 */
class GetBoolOptionTests extends FunSuite with PropertyChecks with Matchers with PredefinedParsers with Resources {

    //  get managed
    import resource._
    import parser.SMTLIB2PrettyPrinter.show
    import configurations.AppConfig.config
    import scala.concurrent.duration._

    //  Solvers to be included in the tests are the enabled ones.
    val theSolvers = config.filter( _.enabled )

    import configurations.SMTOptions.{ allBoolOptions, SMTPrintSuccess }
    import org.bitbucket.inkytonik.kiama.util.{ Source, StringSource, FileSource }
    val parseAsGetOption : String ⇒ Try[ GetOptionResponses ] = {
        s ⇒
            val parse = SMTLIB2Parser[ GetOptionResponses ]
            parse( StringSource( s ) )
    }

    //  Test of boolean options. MathSat does not support get-option. CVC4-1.4 neither.
    for (
        solver ← theSolvers if !solver.name.startsWith( "Boolector" );
        op ← allBoolOptions if ( solver.supports( op ) && !solver.name.startsWith( "MathSat" ) && !( solver.name == "CVC4-1.4" ) );
        b ← List( true, false )
    ) {
        test( s"[${solver.name}] ${show( SetOptionCmd( op( b ) ) )}  -- ${show( GetOptionCmd( op.keyword ) )}" ) {
            for ( managedSolver ← managed ( new SMTSolver( solver ) ) ) {

                //  initialise solver with print-success true
                managedSolver.init.isSuccess shouldBe true
                managedSolver.eval ( SetOptionCmd( op ( b ) ) )

                managedSolver.eval ( GetOptionCmd( op.keyword ), Some( 1.second ) )
                    .flatMap ( parseAsGetOption ) should matchPattern {
                        case Success( GetOptionResponseError( UnsupportedResponse() ) ) if solver.name.startsWith( "Z3" ) ⇒
                        case Success( GetOptionResponseSuccess( AttValueSymbol( SSymbol( _ ) ) ) )                        ⇒

                    }
            }
        }
    }

    //  Test of printSuccess  option. MathSat does not support get-option.
    for (
        solver ← theSolvers if !solver.name.startsWith( "MathSat" ) && !solver.name.startsWith( "Boolector" );
        b ← List( true )
    ) {
        test( s"[${solver.name}] ${show( SetOptionCmd( SMTPrintSuccess( b ) ) )}  -- ${show( GetOptionCmd( SMTPrintSuccess.keyword ) )}" ) {
            for ( managedSolver ← managed ( new SMTSolver( solver ) ) ) {

                //  initialise solver with print-success true
                managedSolver.init.isSuccess shouldBe true

                managedSolver.eval ( GetOptionCmd( SMTPrintSuccess.keyword ), Some( 1.second ) )
                    .flatMap ( parseAsGetOption ) shouldBe
                    Success( GetOptionResponseSuccess( AttValueSymbol( SSymbol( b.toString ) ) ) )
            }
        }
    }
}

/**
 * Check get Int options for the solvers.
 */
class GetIntOptionTests extends FunSuite with PropertyChecks with Matchers with PredefinedParsers with Resources {

    //  get managed
    import resource._
    import parser.SMTLIB2PrettyPrinter.show
    import configurations.AppConfig.config
    import scala.concurrent.duration._
    import org.bitbucket.inkytonik.kiama.util.{ Source, StringSource, FileSource }
    val parseAsGetOption : String ⇒ Try[ GetOptionResponses ] = {
        s ⇒
            val parse = SMTLIB2Parser[ GetOptionResponses ]
            parse( StringSource( s ) )
    }
    //  Solvers to be included in the tests are the enabled ones.
    val theSolvers = config.filter( _.enabled )

    import configurations.SMTOptions.{ allIntOptions }

    //  Test of numeric options.
    for (
        solver ← theSolvers if !solver.name.startsWith( "Boolector" );
        op ← allIntOptions if solver.supports( op ) && ( solver.name != "MathSat" );
        n ← List( 12, 245, 102340, 34565566 )
    ) {
        test( s"[${solver.name}]  ${show( SetOptionCmd( op( n ) ) )} -- ${show( GetOptionCmd( op.keyword ) )}" ) {
            for ( managedSolver ← managed ( new SMTSolver( solver ) ) ) {

                //  initialise solver with print-success true
                managedSolver.init.isSuccess shouldBe true

                managedSolver.eval ( SetOptionCmd( op ( n ) ) )
                managedSolver.eval ( GetOptionCmd( op.keyword ) )

                managedSolver.eval ( GetOptionCmd( op.keyword ), Some( 1.second ) )
                    .flatMap ( parseAsGetOption ) should matchPattern {
                        case Success( GetOptionResponseSuccess( AttValueSpecConstant( NumLit( _ ) ) ) )                   ⇒
                        case Success( GetOptionResponseError( UnsupportedResponse() ) ) if solver.name.startsWith( "Z3" ) ⇒
                    }
            }
        }
    }
}

/**
 * Check get String options for the solvers.
 */
class GetStringOptionTests extends FunSuite with PropertyChecks with Matchers with PredefinedParsers with Resources {

    //  get managed
    import resource._
    import parser.SMTLIB2PrettyPrinter.show
    import configurations.AppConfig.config
    import scala.concurrent.duration._

    //  Solvers to be included in the tests are the enabled ones.
    val theSolvers = config.filter( _.enabled )

    import configurations.SMTOptions.{ allStringOptions }

    //  Test for string options.
    for (
        solver ← theSolvers if !solver.name.startsWith( "Boolector" );
        op ← allStringOptions;
        n ← List( "stdout" )
    ) {
        test( s"[${solver.name}]  ${show( SetOptionCmd( op( n ) ) )} -- ${show( GetOptionCmd( op.keyword ) )}" ) {
            for ( managedSolver ← managed ( new SMTSolver( solver ) ) ) {

                managedSolver.init.isSuccess shouldBe true

                managedSolver.eval ( SetOptionCmd( op( n ) ), Some( 1.second ) )
                    .flatMap ( parseAsGeneralResponse ) should matchPattern {
                        case Success( SuccessResponse() ) if ( solver.supports( op ) ) ⇒
                        case Failure( _ ) ⇒
                    }
            }
        }
    }
}
