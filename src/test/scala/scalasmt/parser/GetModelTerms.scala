/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package parser
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import Implicits._

//  get Models terms in SMTLIB2 v2.5,

/**
 *  Parsing rule <get_model_response>
 */
class GetModelParserTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing get-model-responses test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseGetModel = SMTLIB2Parser[ ModelResponseTester ]

    //  <models> test strings
    //  format: OFF
    val models = Table[ String, Boolean](
        ( "input string?"           , "Model response: yes/no" ) ,
        ( """|
             | (
             |  model
             |    (define-fun x
             |      () Int 1)
             | )
             |""".stripMargin     , true ),
         ( """|
              | (
              |  model
              |    (define-fun x
              |      () Int 1)
              |
              |     (define-fun y
              |      () Bool true)
              | )
              |""".stripMargin     , true ),
          ( """|
               | (
               |  model
               |    (define- x
               |      () Int 1)
               |
               |     (define-fun y
               |      () Bool true)
               | )
               |""".stripMargin     , false ),
           ( """|
                | (
                |  model
                |    (define-fun x
                |      ( (f V) (h U) ) Int 1)
                |
                |     (define-fun y
                |      () Bool true)
                | )
                |""".stripMargin     , true ),
            ( """|
                 | (
                 |  model
                 |    (define-fun x
                 |      ( (f V) (h U) ) (Array Int Bool) 1)
                 |
                 |     (define-fun y
                 |        (
                 |          ( a (Array Int Real) )
                 |        ) Bool true)
                 | )
                 |""".stripMargin     , true ),

            ( """|
                 | (model
                 | (define-fun z () (Array Int Int)
                 |  (_ as-array k!0)
                 | )
                 |   (define-fun y () Int
                 |    0)
                 | (define-fun k!0 ((x!1 Int)) Int
                 |  (ite (= x!1 2) (- 1)
                 |  (ite (= x!1 1) 0
                 |   (- 1))))
                 | )
                 |""".stripMargin       , true ),


            ( """|
                 |  ;; returned by MathSAT when get-model command issued.
                 |  ;; Not conformant to SMTLIB2
                 | (
                 |   ( z
                 |     (
                 |     (as const (Array Int Int) )
                 |     0
                 |     )
                 |   )
                 |
                 |  (y 0)
                 |)
                 |""".stripMargin       , true ),

             ( """|
                  |  ;; returned by MathSAT when get-model command issued.
                  |  ;; Not conformant to SMTLIB2
                  | (
                  | (a1 (store ((as const (Array Int Int)) 0) 1 (- 1)))
                  | (a2
                  |    (let
                  |      ((.def_20 ((as const (Array Int Int)) 0)))
                  |        (store ((as const (Array Int (Array Int Int))) .def_20) 2
                  |        (store .def_20 3 (- 1))))
                  |    )
                  | )
                  |""".stripMargin       , true ),

             ( """|
                  |  ;; returned by CVC4 when get-model command issued.
                  |
                  |  ( model
                  |      (define-fun a1 () (Array Int Int)
                  |        (store (__array_store_all__ (Array Int Int) 1) 1 0))
                  |      (define-fun a2 () (Array Int (Array Int Int))
                  |            (__array_store_all__ (Array Int (Array Int Int))
                  |            (__array_store_all__ (Array Int Int) 0)))
                  |  )
                  |""".stripMargin       , true ),

              ( """|
                   |  ;; returned by Z3 when get-model command issued.
                   |
                   | ( model
                   |  (define-fun a2 () (Array Int (Array Int Int))
                   |    ((as const (Array Int (Array Int Int))) ((as const (Array Int Int)) 0)))
                   |  (define-fun a1 () (Array Int Int)
                   |    ((as const (Array Int Int)) 0))
                   | )
                   |""".stripMargin       , true )

    )
    //  format: ON

    forAll ( models ) {
        ( s : String, answer : Boolean ) ⇒

            test( s"Trying to parse $s as a <gen_response> -- ${if ( answer ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseGetModel( s ) )

                parseGetModel( s ) should matchPattern {

                    case Failure( _ ) if ( !answer ) ⇒
                    case Success(
                        ModelResponseTester( GetModelFunDefResponseSuccess( _ ) )
                        ) if ( answer ) ⇒
                    case Success(
                        ModelResponseTester( GetModelPairResponseSuccess( _ ) )
                        ) if ( answer ) ⇒
                    case Success(
                        ModelResponseTester( GetModelResponseError( _ ) )
                        ) if ( !answer ) ⇒
                }
            }
    }
}
