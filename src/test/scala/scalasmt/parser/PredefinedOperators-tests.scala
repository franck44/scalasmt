/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package parser
import Implicits._

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

//  Terms in SMTLIB2 v2.5, with predefined operators

/**
 *  Parsing rule <term> := arithmetic operator
 */
class ArithmeticTermTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing Term <term> := <arithmetic operator> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Term

    val parseTerm = SMTLIB2Parser[ TermTester ]

    //  be careful not to write the same pair twice as this is used
    //  to generate the test name and duplicated names are not allowed.
    //  in the tables of type Table[String, T], (x, Some(t:T)) means x should
    //  be correctly parsed as t:T, and None means it should not

    //  arithmetic terms test strings
    //  format: ON
    val arithmeticTerms = Table[ String, Term ](
        ( "input string", "corresponding term" ),

        ( "(+ 2 1)",
            PlusTerm(
                ConstantTerm( NumLit( "2" ) ),
                List(
                    ConstantTerm( NumLit( "1" ) ) ) ) ),

        ( "(- 2)",
            NegTerm(
                ConstantTerm(
                    NumLit( "2" ) ) ) ),

        ( "(+ 2 x)",
            PlusTerm(
                ConstantTerm(
                    NumLit( "2" ) ),
                List(
                    QIdTerm(
                        SimpleQId(
                            SymbolId( SSymbol( "x" ) ) ) ) ) ) ),

        ( "(+ x 2 y)",
            PlusTerm(
                QIdTerm(
                    SimpleQId(
                        SymbolId( SSymbol( "x" ) ) ) ),
                List(
                    ConstantTerm( NumLit( "2" ) ),
                    QIdTerm(
                        SimpleQId(
                            SymbolId( SSymbol( "y" ) ) ) ) ) ) ),

        ( "(* 2 1)",
            MultTerm(
                ConstantTerm( NumLit( "2" ) ),
                List(
                    ConstantTerm( NumLit( "1" ) ) ) ) ),

        ( "(* x 2 y)",
            MultTerm(
                QIdTerm(
                    SimpleQId(
                        SymbolId( SSymbol( "x" ) ) ) ),
                List(
                    ConstantTerm( NumLit( "2" ) ),
                    QIdTerm(
                        SimpleQId(
                            SymbolId( SSymbol( "y" ) ) ) ) ) ) ),

        ( "(- 2 1)",
            SubTerm(
                ConstantTerm( NumLit( "2" ) ),
                List(
                    ConstantTerm( NumLit( "1" ) ) ) ) ),

        ( "(- 2 x)",
            SubTerm(
                ConstantTerm(
                    NumLit( "2" ) ),
                List(
                    QIdTerm(
                        SimpleQId(
                            SymbolId( SSymbol( "x" ) ) ) ) ) ) ),

        ( "(- x 2 y)",
            SubTerm(
                QIdTerm(
                    SimpleQId(
                        SymbolId( SSymbol( "x" ) ) ) ),
                List(
                    ConstantTerm( NumLit( "2" ) ),
                    QIdTerm(
                        SimpleQId(
                            SymbolId( SSymbol( "y" ) ) ) ) ) ) ),

        ( "(div x 2)",
            IntDivTerm(

                QIdTerm(
                    SimpleQId(
                        SymbolId( SSymbol( "x" ) ) ) ),
                List(
                    ConstantTerm(
                        NumLit( "2" ) ) ) ) ),

        ( "(div x 2 y)",
            IntDivTerm(
                QIdTerm(
                    SimpleQId(
                        SymbolId( SSymbol( "x" ) ) ) ),
                List(
                    ConstantTerm( NumLit( "2" ) ),
                    QIdTerm(
                        SimpleQId(
                            SymbolId( SSymbol( "y" ) ) ) ) ) ) ),

        ( "(/ x 2 y)",
            RealDivTerm(
                QIdTerm(
                    SimpleQId(
                        SymbolId( SSymbol( "x" ) ) ) ),
                List(
                    ConstantTerm( NumLit( "2" ) ),
                    QIdTerm(
                        SimpleQId(
                            SymbolId( SSymbol( "y" ) ) ) ) ) ) ),

        ( "(mod x 2)",
            IntModTerm(

                QIdTerm(
                    SimpleQId(
                        SymbolId( SSymbol( "x" ) ) ) ),
                ConstantTerm(
                    NumLit( "2" ) ) ) ),

        ( "(rem x 2)", IntRemTerm(

            QIdTerm(
                SimpleQId(
                    SymbolId( SSymbol( "x" ) ) ) ),
            ConstantTerm(
                NumLit( "2" ) ) ) ),

        ( "(abs x)",
            AbsTerm(
                QIdTerm(
                    SimpleQId(
                        SymbolId(
                            SSymbol( "x" ) ) ) ) ) ) )
    //  format: ON

    forAll ( arithmeticTerms ) {
        ( s : String, answer : Term ) ⇒

            test( s"Trying to parse $s as a <term> ::= <arithmetic operator> -- Should be $answer" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseTerm( s ) )

                parseTerm( s ) shouldBe Success( TermTester( answer ) )

            }
    }
}

/**
 *  Parsing rule <term> := logical operator
 */
class LogicalTermTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing Term <term> := <logical operator> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Term

    val parseTerm = SMTLIB2Parser[ TermTester ]

    //  be careful not to write the same pair twice as this is used
    //  to generate the test name and duplicated names are not allowed.
    //  in the tables of type Table[String, T], (x, Some(t:T)) means x should
    //  be correctly parsed as t:T, and None means it should not

    //  logical terms test strings
    //  format: ON
    val logicalTerms = Table[ String, Term ](
        ( "input string", "corresponding term" ),

        ( "(not (and p q))", NotTerm(
            AndTerm(
                QIdTerm(
                    SimpleQId(
                        SymbolId(
                            SSymbol( "p" ) ) ) ),
                List(
                    QIdTerm(
                        SimpleQId(
                            SymbolId( SSymbol( "q" ) ) ) ) ) ) ) ),

        ( "(and (or p q) r)",
            AndTerm(
                OrTerm(
                    QIdTerm(
                        SimpleQId( SymbolId( SSymbol( "p" ) ) ) ),
                    List(
                        QIdTerm( SimpleQId( SymbolId( SSymbol( "q" ) ) ) ) ) ),
                List(
                    QIdTerm( SimpleQId( SymbolId( SSymbol( "r" ) ) ) ) ) ) ),

        ( "(and p q r)",
            AndTerm(
                QIdTerm(
                    SimpleQId(
                        SymbolId(
                            SSymbol( "p" ) ) ) ),
                List(
                    QIdTerm(
                        SimpleQId( SymbolId( SSymbol( "q" ) ) ) ),
                    QIdTerm(
                        SimpleQId( SymbolId( SSymbol( "r" ) ) ) ) ) ) ),

        ( "(or p q r)",
            OrTerm(
                QIdTerm(
                    SimpleQId(
                        SymbolId(
                            SSymbol( "p" ) ) ) ),
                List(
                    QIdTerm(
                        SimpleQId( SymbolId( SSymbol( "q" ) ) ) ),
                    QIdTerm(
                        SimpleQId( SymbolId( SSymbol( "r" ) ) ) ) ) ) ),

        ( "(xor p q)",
            XorTerm(
                QIdTerm(
                    SimpleQId(
                        SymbolId(
                            SSymbol( "p" ) ) ) ),
                QIdTerm(
                    SimpleQId( SymbolId( SSymbol( "q" ) ) ) ) ) ),

        ( "(ite p q r)",
            IfThenElseTerm(
                QIdTerm(
                    SimpleQId( SymbolId( SSymbol( "p" ) ) ) ),
                QIdTerm(
                    SimpleQId( SymbolId( SSymbol( "q" ) ) ) ),
                QIdTerm(
                    SimpleQId( SymbolId( SSymbol( "r" ) ) ) ) ) ),

        ( "(ite p (+ x 1) (- y 2))",
            IfThenElseTerm(
                QIdTerm(
                    SimpleQId( SymbolId( SSymbol( "p" ) ) ) ),
                PlusTerm(
                    QIdTerm(
                        SimpleQId( SymbolId( SSymbol( "x" ) ) ) ),
                    List(
                        ConstantTerm( NumLit( "1" ) ) ) ),
                SubTerm(
                    QIdTerm(
                        SimpleQId( SymbolId( SSymbol( "y" ) ) ) ),
                    List(
                        ConstantTerm( NumLit ( "2" ) ) ) ) ) ),

        ( "(=> p q)",
            ImplyTerm(
                QIdTerm(
                    SimpleQId(
                        SymbolId( SSymbol( "p" ) ) ) ),
                List(
                    QIdTerm(
                        SimpleQId( SymbolId( SSymbol( "q" ) ) ) ) ) ) ),

        ( "(=> p q r)",
            ImplyTerm(
                QIdTerm(
                    SimpleQId( SymbolId( SSymbol( "p" ) ) ) ),
                List(
                    QIdTerm(
                        SimpleQId( SymbolId( SSymbol( "q" ) ) ) ),
                    QIdTerm(
                        SimpleQId( SymbolId( SSymbol( "r" ) ) ) ) ) ) ),

        ( "(not q)",
            NotTerm( QIdTerm( SimpleQId( SymbolId( SSymbol( "q" ) ) ) ) ) ) )
    //  format: ON

    forAll ( logicalTerms ) {
        ( s : String, answer : Term ) ⇒

            test( s"Trying to parse $s as a <term> ::= <logical operator> -- Should be $answer" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseTerm( s ) )

                parseTerm( s ) shouldBe Success( TermTester( answer ) )

            }
    }
}

/**
 *  Parsing rule <term> := comparison operator
 */
class ComparisonTermTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing Term <term> := <comparison operator> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Term

    val parseTerm = SMTLIB2Parser[ TermTester ]

    //  be careful not to write the same pair twice as this is used
    //  to generate the test name and duplicated names are not allowed.
    //  in the tables of type Table[String, T], (x, Some(t:T)) means x should
    //  be correctly parsed as t:T, and None means it should not

    //  logical terms test strings
    //  format: ON
    val comprisonTerms = Table[ String, Term ](
        ( "input string", "corresponding term" ),

        ( "(= (+ p q) 1)",
            EqualTerm(
                PlusTerm(
                    QIdTerm(
                        SimpleQId(
                            SymbolId( SSymbol( "p" ) ) ) ),
                    List(
                        QIdTerm(
                            SimpleQId(
                                SymbolId( SSymbol( "q" ) ) ) ) ) ),
                ConstantTerm( NumLit( "1" ) ) ) ),

        ( "(<= x 1)",
            LessThanEqualTerm(
                QIdTerm(
                    SimpleQId(
                        SymbolId( SSymbol( "x" ) ) ) ),
                ConstantTerm(
                    NumLit( "1" ) ) ) ),

        ( "(> x 1)",
            GreaterThanTerm(
                QIdTerm(
                    SimpleQId(
                        SymbolId( SSymbol( "x" ) ) ) ),
                ConstantTerm(
                    NumLit( "1" ) ) ) ),

        ( "(>= x 1)",
            GreaterThanEqualTerm(
                QIdTerm(
                    SimpleQId(
                        SymbolId( SSymbol( "x" ) ) ) ),
                ConstantTerm(
                    NumLit( "1" ) ) ) ),
        ( "(distinct x 1)",
            DistinctTerm(
                QIdTerm(
                    SimpleQId(
                        SymbolId( SSymbol( "x" ) ) ) ),
                ConstantTerm(
                    NumLit( "1" ) ) ) ) )
    //  format: ON

    forAll ( comprisonTerms ) {
        ( s : String, answer : Term ) ⇒

            test( s"Trying to parse $s as a <term> ::= comparison operator -- Should be $answer" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseTerm( s ) )

                parseTerm( s ) shouldBe Success( TermTester( answer ) )

            }
    }
}
