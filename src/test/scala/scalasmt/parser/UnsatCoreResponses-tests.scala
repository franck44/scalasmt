/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package parser

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import Implicits._

//  get unsat core terms in SMTLIB2 v2.5,

/**
 *  Parsing rule <unsat_core_response>
 */
class GetUnSatCoreParserTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing get-unsat-core responses test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseUnsatCore = SMTLIB2Parser[ UnSatCoreResponses ]

    //  <get-unsat-core-reponse> test strings
    //  format: OFF
    val unSatCoreTests = Table[ String, Boolean](
        ( "input string?"         , "is get-info : yes/no" ) ,
        ( "(thisOne and another)" , true )                   ,
        ( "(_P1 _p2 __P!P@)"      , true )                   ,
        ( "(p1)"                  , true )                   ,
        ( "(1)"                   , false )                  ,
        ( "( _P1"                 , false )                  ,

        ( """|
             | (
             |  get_
             |    _the1
             |      )
             |
             |""".stripMargin     , true ),

         ( """|
              | (
              |      )
              |
              |""".stripMargin     , true ),

          ( """|
               | (
               |  only-1
               |    x
               |      )
               |
               |""".stripMargin     , true )
    )
    //  format: ON

    forAll ( unSatCoreTests ) {
        ( s : String, answer : Boolean ) ⇒

            test( s"Trying to parse $s as a <unsat_core_response> -- ${if ( answer ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseUnsatCore( s ) )

                parseUnsatCore( s ) should matchPattern {

                    case Success( GetUnSatCoreResponseSuccess( x ) ) if ( answer ) ⇒
                    case Success( GetUnSatCoreResponseError( _ ) ) if ( !answer )  ⇒
                    case Failure( _ ) if ( !answer )                               ⇒

                }
            }
    }
}

/**
 *  Parsing rule <unsat_core_response>
 */
class UnSatCoreResponsePrinterTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Pretty-printing get-unsat-core-reponse test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseUnsatCore = SMTLIB2Parser[ UnSatCoreResponses ]

    //  <get-unsat-core-reponse> test strings
    //  format: OFF
    val unSatCoreTests = Table[ String, String](
        ( "input string?"                       , "is get-info : yes/no" ) ,
        ( "(  thisOne   and   another  )  "     , "(thisOne and another )" ),
        ( "(_P1 _p2 __P!P@)" , "(_P1 _p2 __P!P@ )" )                    ,
        ( "  ( p1   ) "                 , "(p1 )" )                    ,

        ( """|
             | (
             |  get_
             |    _the1
             |      )
             |
             |""".stripMargin     , "(get_ _the1 )" ),

         ( """|
              | (
              |      )
              |
              |""".stripMargin     , "()" ),

          ( """|
               | (
               |  only-1
               |    x
               |      )
               |
               |""".stripMargin     , "(only-1 x )" )
    )
    //  format: ON

    import parser.SMTLIB2PrettyPrinter.format

    forAll ( unSatCoreTests ) {
        ( s : String, r : String ) ⇒

            test( s"Trying to pretty-print $s as a <unsat_core_response>" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseUnsatCore( s ) )

                parseUnsatCore( s ) match {

                    case Success( x ) ⇒ format( x ).layout shouldBe r
                    case Failure( _ ) ⇒ fail

                }
            }
    }
}
