/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package parser
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import Implicits._

//  Commands  in SMTLIB2 v2.5,

/**
 *  Parsing rule <command> := ( declare-sort <symbol> <numeral>)
 */
class DeclareSortTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing <command> := ( declare-sort <symbol> <numeral>) test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Command

    val parseCommand = SMTLIB2Parser[ Command ]

    //  test strings
    //  format: OFF
    val sorts = Table[ String, Int ](
        ( "Name" , "arity" ) ,
        ( "U"    , 0 )       ,
        ( "llll" , 17 )      ,
        ( "Int"  , 0 )       ,  //  notice that in the declare-sort, Int, Real and Bool
        ( "Real" , 0 )       ,  //  are considered new sorts and are not of type SimpleSorts
        ( "Bool" , 0 )
    )
    //  format: ON

    forAll ( sorts ) {
        ( sort : String, index : Int ) ⇒

            val c = s"(declare-sort $sort $index) "
            test( s"Trying to parse $c as a <command> ::= ( declare-sort <symbol> <numeral>) -- Should succeed" ) {

                logger.debug( "checking {} - parse command returned:{}", c, parseCommand( c ) )

                parseCommand( c ) shouldBe Success( DeclareSortCmd( SSymbol( sort ), index.toString ) )

            }
    }
}

/**
 *  Parsing rule <command> := ( declare-sort <symbol> <numeral>)
 */
class KnownSortTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing pre-defined sorts  test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Command

    val parseCommand = SMTLIB2Parser[ SortTester ]

    //  test strings
    //  format: OFF
    val sorts = Table[ String, Sort ](
        ( "Name"                          , "SMTLIB2 sort" )                             ,
        ( "Int"                           , IntSort() )                                  ,
        ( "Bool"                          , BoolSort() )                                 ,
        ( "Real"                          , RealSort() )                                 ,
        ( "(Array Int Int)"               , Array1Sort( IntSort () ))                    ,
        ( "(Array Int Bool)"              , Array1Sort( BoolSort()) )                    ,
        ( "(Array Int Real)"              , Array1Sort( RealSort()))                     ,
        ( "(Array Int (Array  Int Int))"  , Array2Sort(Array1Sort(IntSort())))           ,
        ( "(Array Int (Array  Int Real))" , Array2Sort(Array1Sort(RealSort())))          ,
        ( "(Array Int (Array  Int Bool))" , (Array2Sort(Array1Sort(BoolSort()))))        ,
        ( "(Array Int (Array  Int V))"    ,
                        Array2Sort(Array1Sort(SortId(SymbolId(SSymbol("V"))))))          
    )
    //  format: ON

    forAll ( sorts ) {
        ( s : String, sort : Sort ) ⇒

            test( s"Trying to parse a pre-defined sort type $s -- Should succeed" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseCommand( s ) )

                parseCommand( s ) shouldBe Success( SortTester( sort ) )

            }
    }
}

/**
 *  Parsing rule <command> := ( declare-fun <symbol> ( <sort>* ) <sort>)
 */
class DeclareFunTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing <command> := ( declare-fun <symbol> ( <sort>* ) <sort>) test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Command
    val parseCommand = SMTLIB2Parser[ Command ]

    //  get a parser for sorts
    val sortParser = SMTLIB2Parser[ Sort ]
    //  implicit conversion from String to StringSource
    import parser.Implicits._
    val parseSort : String ⇒ Sort = {
        x ⇒
            sortParser( x ) match {
                case Success( s )     ⇒ s
                case f @ Failure( _ ) ⇒ fail( s"String $x cannot parse as a sort" )
            }
    }

    //
    def makeSortId( name : String ) = SortId( SymbolId( SSymbol( name ) ) )

    //  test strings
    //  format: OFF
    val sorts = Table[ String, List[ String ], String ](
        ( "symbol" , "domain"                          , "image" ),
        ( "U"      , List()                            , "I"     ),
        ( "f"      , List( "O"   , "Bool" )            , "V"     ),
        ( "V"      , List( "Int" , "Int" )             , "Int"   ),
        ( "g"      , List( "O"   , "Bool"    , "Int" ) , "V"     ),
        ( "h"      , List( "O"   , "Bool" )            , "V"     ),
        ( "foo"    , List( "O"   , "Bool" )            , "V"     ),
        ( "foo2"   , List( "A" )                       , "V"     )
    )
    //  format: ON

    forAll ( sorts ) {
        ( fname : String, domain : List[ String ], image : String ) ⇒

            val c = s"(declare-fun $fname (${domain.mkString( " " )}) $image) "
            test( s"Trying to parse $c as a <command> ::= ( declare-fun <symbol> ( <sort>* ) <sort>) -- Should succeed" ) {

                logger.debug( "checking {} - parse command returned:{}", c, parseCommand( c ) )

                parseCommand( c ) shouldBe
                    Success(
                        DeclareFunCmd(
                            FunDecl(
                                SSymbol( fname ),
                                domain.map( x ⇒ parseSort( x ) ),
                                parseSort( image ) ) ) )
            }
    }
}
