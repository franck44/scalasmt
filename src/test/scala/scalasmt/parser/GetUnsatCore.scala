/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package parser

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import Implicits._

//  get unsat core  in SMTLIB2 v2.5,

/**
 *  Parsing rule <get_unsat_core_reponses>
 */
class GetUnsatCoreSuccessParserTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing get-unsat-core-responses test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseGetUnsatCore = SMTLIB2Parser[ UnSatCoreResponses ]

    //  <unsat core reponses> test strings
    //  format: OFF
    val responses = Table[ String, List[String]](
        ( "input string"           , "unsat core response" ) ,
        ( """|
             | (P1 P2    P5)
             |""".stripMargin     , List("P1", "P2", "P5") ),
        ( """|
             | ()
             |""".stripMargin     , List() ),
        ( """|
             | (P_1 a@4 l6 kk@)
             |""".stripMargin     , List("P_1", "a@4", "l6", "kk@") )
    )
    //  format: ON

    forAll ( responses ) {
        ( s : String, answer : List[ String ] ) ⇒

            test( s"Trying to parse $s as a <unsat_core_reponse> -- should succeed" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseGetUnsatCore( s ) )

                parseGetUnsatCore( s ) shouldBe
                    Success( GetUnSatCoreResponseSuccess( answer.map( SepSimpleSymbol( _ ) ) ) )
            }
    }
}

class GetUnsatCoreErrorResponseParserTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing get-unsat-core-responses test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseGetUnsatCore = SMTLIB2Parser[ UnSatCoreResponses ]

    test( s"Trying to parse unsupported as a <unsat_core_reponse> -- should be unsupportedResponse" ) {

        parseGetUnsatCore( "   unsupported   " ) shouldBe
            Success( GetUnSatCoreResponseError( UnsupportedResponse() ) )
    }

    test( s"Trying to parse error as a <unsat_core_reponse> -- should be errorResponses" ) {

        val errorMsg = "something wrong happened"
        val s = "   ( error \"" + errorMsg + "\")   "
        parseGetUnsatCore( s ) shouldBe
            Success( GetUnSatCoreResponseError( ErrorResponse( StringLiteral( errorMsg ) ) ) )
    }
}
