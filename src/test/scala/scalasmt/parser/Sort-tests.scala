/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package parser
package tests

import Implicits._

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

//  Sorts in SMTLIB2 v2.5,

/**
 *  Parsing rule  <sort> ::= <identifier> i.e. SimpleSorts and BitVectorSort
 */
class SortRule1Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing <sort> := <identifier> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Sort

    val parseSort = SMTLIB2Parser[ SortTester ]

    //  <identifier> test strings
    //  format: OFF
    val identifiers = Table[ String, Option[ Sort ] ](
        ( "input string is a good Sort?" , "yes/no" )                                             ,
        ( "Int"                          , Some( IntSort() ) )                                    ,
        ( "Bool"                         , Some( BoolSort() ) )                                   ,
        ( "Real"                         , Some( RealSort() ) )                                   ,
        ( "( _ BitVec 5)"                , Some( BitVectorSort("5")))                             ,
        ( "A"                            , Some( SortId ( SymbolId ( SSymbol ( "A" ) ) ) ) )      ,
        ( "_Qsort"                       , Some( SortId ( SymbolId ( SSymbol ( "_Qsort" ) ) ) ) ) ,
        ( "_12"                          , Some( SortId ( SymbolId ( SSymbol ( "_12" ) ) ) ) )    ,
        ( "12"                           , None )
    )
    //  format: ON

    forAll ( identifiers ) {
        ( s : String, answer : Option[ Sort ] ) ⇒

            test( s"Trying to parse $s as a <sort> ::= <identifier> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseSort( s ) )

                parseSort( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                                    ⇒
                    case Success( x ) if ( answer.isDefined && SortTester( answer.get ) == x ) ⇒
                    // case Success( x ) if ( SortTester( answer.get ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule  <sort> ::= <identifier> <sort>+
 */
class SortRule2Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing <sort> := ( <identifier> <sort>+ )  test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Sort

    val parseSort = SMTLIB2Parser[ SortTester ]

    //  <identifier> <sort>+ test strings

    val identifiersSort = Table[ String, Option[ Sort ] ](
        ( "input string is a good Sort?", "yes/no" ),

        (
            "(  Int  Bool )",
            Some(
                SortIdList(
                    IntSort(),
                    List( BoolSort() ) ) ) ),

        (
            "( Int (Bool Int))",
            Some(
                SortIdList(
                    IntSort(),
                    List(
                        SortIdList(
                            BoolSort(),
                            List ( IntSort() ) ) ) ) ) ),

        (
            "( A (_Int C))",
            Some(
                SortIdList(
                    SortId( SymbolId( SSymbol( "A" ) ) ),
                    List(
                        SortIdList(
                            SortId( SymbolId ( SSymbol( "_Int" ) ) ),
                            List( SortId( SymbolId ( SSymbol( "C" ) ) ) ) ) ) ) ) ),

        ( //  this is not an Array2Sort as another Int is needed in nested Array
            "(Array (Int (Array Int)))", None ),

        ( " ( (A B) C) ", None ) )

    forAll ( identifiersSort ) {
        ( s : String, answer : Option[ Sort ] ) ⇒

            test( s"Trying to parse $s as a <sort> ::= (<identifier> <sort>+) -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseSort( s ) )

                parseSort( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                ⇒
                    case Success( x ) if ( SortTester( answer.get ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule  <sort> ::= <arraySort> i.e. Array1Sort, Array2Sort or Array1BV
 */
class SortRule3Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing <sort> := <array sort> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Sort

    val parseSort = SMTLIB2Parser[ SortTester ]

    //  <identifier> test strings
    //  format: OFF
    val identifiers = Table[ String, Option[ Sort ] ](
        ( "input string is a good Sort?"  , "yes/no" )                                     ,
        ( "(Array Int Int)"               , Some( Array1Sort(IntSort() ) ) )               ,
        ( "(Array Int Bool)"              , Some( Array1Sort(BoolSort() ) ) )              ,
        ( "(Array Int Real)"              , Some( Array1Sort(RealSort() ) ) )              ,
        //  Array Int combined with BV are not allowed
        ( "(Array Int (_ BitVec 6))"      , None )                                         ,
        ( "(Array Int (Array Int Int ))"  , Some( Array2Sort(Array1Sort(IntSort() ) ) ) )  ,
        ( "(Array Int (Array Int Bool ))" , Some( Array2Sort(Array1Sort(BoolSort() ) ) ) ) ,
        ( "(Array Int (Array Int Real ))" , Some( Array2Sort(Array1Sort(RealSort() ) ) ) ) ,
        //  Array1 of BV must be all bitvectors
        ( "(Array (_ BitVec 6) Int)"      , None )                                         ,
        //  Array1 of BV
        ( "(Array (_ BitVec 6) (_ BitVec 3))" ,
                                            Some(
                                                Array1BV(
                                                    BitVectorSort("6"),
                                                    BitVectorSort("3")
                                                )
                                            )
        )
        )
    //  format: ON

    forAll ( identifiers ) {
        ( s : String, answer : Option[ Sort ] ) ⇒

            test( s"Trying to parse $s as a <sort> ::= <array sort> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseSort( s ) )

                parseSort( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                                    ⇒
                    case Success( x ) if ( answer.isDefined && SortTester( answer.get ) == x ) ⇒
                }
            }
    }
}
