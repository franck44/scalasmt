/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package parser
package tests

import Implicits._

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

//  BitVectors

/**
 *  Parsing BitVectors
 */
class ParseBVSortTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing SimpleSorts := <bitvectorsort> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Term

    val parseBVSort = SMTLIB2Parser[ SortTester ]

    //  be careful not to write the same pair twice as this is used
    //  to generate the test name and duplicated names are not allowed.
    //  in the tables of type Table[String, T], (x, Some(t:T)) means x should
    //  be correctly parsed as t:T, and None means it should not

    //  <bitvectorsort> test strings
    //  format: OFF
    val bvsorts = Table[ String, Option[ BitVectorSort ] ](
        ( "Input string"    , "good <bitvectorsort>?" )       ,


        ( "( _ BitVec 12)"  , Some( BitVectorSort( "12" ) ) ) ,
        ( "( _ BitVec 1)"   , Some( BitVectorSort( "1" ) ) )  ,
        ( "( _ BitVec 01)"  , None )                          ,
        ( "( _ BitVec 64)"  , Some( BitVectorSort( "64" ) ) ) ,
        ( "( _ BitVec 0)"   , None )                          ,
        ( "( BitVec 64)"    , None )
    )
    //  format: ON

    forAll ( bvsorts ) {
        ( s : String, answer : Option[ BitVectorSort ] ) ⇒

            test( s"Trying to parse $s as a <SimpleSort> ::= <bitvectorsort> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseBVSort( s ) )

                parseBVSort( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                                         ⇒
                    case Success( SortTester( SortId( IndexedId( _, _ ) ) ) ) if ( answer.isEmpty ) ⇒
                    case Success( x ) if ( SortTester( answer.get ) == x )                          ⇒

                }
            }
    }
}

/**
 *  Parsing BitVectors
 */
class PPrintBVSortTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    //  pretty printer
    import parser.SMTLIB2PrettyPrinter.show

    override def suiteName = "Pretty print BitVectorSort test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  <bitvectorsort> test strings
    //  format: OFF
    val bvsorts = Table[ String, BitVectorSort  ](
        ( "Expected output string" , "A BitVectorSort" )      ,

        ( "(_ BitVec 12)"          , BitVectorSort( "12" )  ) ,
        ( "(_ BitVec 1)"           , BitVectorSort( "1" )  )  ,
        ( "(_ BitVec 64)"          , BitVectorSort( "64" )  )
    )
    //  format: ON

    forAll ( bvsorts ) {
        ( answer : String, s : BitVectorSort ) ⇒

            test( s"Trying to pprint $s as a <bitvectorsort>" ) {

                logger.debug( "Pretty printing {} -- should yield {}", s, answer )

                show( s ) shouldBe answer
            }
    }
}
