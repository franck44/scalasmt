/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package parser
package tests

import Implicits._

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

//  Floating point BitVectors

/**
 *  Parsing FloatingPoint BitVectors terms
 */
class ParseFPBVTermTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing Term := <FPBVTerms> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Term

    val parseBVTerm = SMTLIB2Parser[ TermTester ]

    //  be careful not to write the same pair twice as this is used
    //  to generate the test name and duplicated names are not allowed.
    //  in the tables of type Table[String, T], (x, Some(t:T)) means x should
    //  be correctly parsed as t:T, and None means it should not

    //  Useful abbrevs
    val x = QIdTerm( SimpleQId( SymbolId( SSymbol( "x" ) ) ) )
    val y = QIdTerm( SimpleQId( SymbolId( SSymbol( "y" ) ) ) )
    val fpbv1 = FPBVvalueTerm( ConstantTerm( BinLit( "1" ) ), ConstantTerm( BinLit( "101" ) ), ConstantTerm( BinLit( "1011" ) ) )
    val fpbvPlusInfty = ConstantTerm( FPPlusInfinity( 2, 4 ) )
    val fpbvMinusInfty = ConstantTerm( FPMinusInfinity( 2, 4 ) )
    val fpbvPlusZero = ConstantTerm( FPBVPlusZero( 2, 4 ) )
    val fpbvMinusZero = ConstantTerm( FPBVMinusZero( 2, 4 ) )
    val fpbvNaN = ConstantTerm( FPBVNaN( 2, 4 ) )
    val rm1 = ConstantTerm( RoundingModeLit( RNE() ) )
    val rm1long = ConstantTerm( RoundingModeLit( RNEven() ) )
    val rm2 = ConstantTerm( RoundingModeLit( RNA() ) )
    val rm2long = ConstantTerm( RoundingModeLit( RNAway() ) )
    val rm3 = ConstantTerm( RoundingModeLit( RTP() ) )
    val rm3long = ConstantTerm( RoundingModeLit( RTPos() ) )
    val rm4long = ConstantTerm( RoundingModeLit( RTNeg() ) )
    val rm4 = ConstantTerm( RoundingModeLit( RTN() ) )
    val rm5 = ConstantTerm( RoundingModeLit( RTZ() ) )
    val rm5long = ConstantTerm( RoundingModeLit( RTZero() ) )

    //  <floatingpointbitvectorsort> test strings
    //  format: OFF
    val fpbvterms = Table[ String, Option[ Term ] ](
        ( "Input string"                        , "good <fpbv term>?" ) ,

        //  predefined constants

        ( "(_ +oo 2 4)"                         , Some(ConstantTerm(FPPlusInfinity( 2, 4)))),
        ( "(_ -oo 3 6)"                         , Some(ConstantTerm(FPMinusInfinity( 3, 6)))),
        ( "(_ +zero 3 6)"                       , Some(ConstantTerm(FPBVPlusZero( 3, 6)))),
        ( "(_ -zero 3 6)"                       , Some(ConstantTerm(FPBVMinusZero( 3, 6)))),
        ( "(_ NaN 3 6)"                         , Some(ConstantTerm(FPBVNaN( 3, 6)))),

        ( "RNE"                                 , Some(ConstantTerm(RoundingModeLit(RNE())))),
        ( "RNA"                                 , Some(ConstantTerm(RoundingModeLit(RNA())))),
        ( "RTP"                                 , Some(ConstantTerm(RoundingModeLit(RTP())))),
        ( "RTN"                                 , Some(ConstantTerm(RoundingModeLit(RTN())))),
        ( "RTZ"                                 , Some(ConstantTerm(RoundingModeLit(RTZ())))),
        ( "roundNearestTiesToEven"              , Some(ConstantTerm(RoundingModeLit(RNEven())))),
        ( "roundNearestTiesToAway"              , Some(ConstantTerm(RoundingModeLit(RNAway())))),
        ( "roundTowardPositive"                 , Some(ConstantTerm(RoundingModeLit(RTPos())))),
        ( "roundTowardNegative"                 , Some(ConstantTerm(RoundingModeLit(RTNeg())))),
        ( "roundTowardZero"                     , Some(ConstantTerm(RoundingModeLit(RTZero())))),

        //  Operations
        ( "(fp.abs x)"                          , Some( FPBVAbsTerm( x ))),
        ( "(fp.abs ( fp #b1 #b101 #b1011) )"    , Some( FPBVAbsTerm( fpbv1 ))),
        ( "(fp.neg x)"                          , Some( FPBVNegTerm( x ))),
        ( "(fp.add RNE x y)"                    , Some( FPBVAddTerm( rm1, x, y ))),
        ( "(fp.add  x y)"                       , Some(
                                                        QIdAndTermsTerm(
                                                            SimpleQId(SymbolId(SSymbol("fp.add"))),
                                                            List(x,y))
                                                        )
        ),

        ( "(fp.sub RNA x y)"                    , Some( FPBVSubTerm(rm2, x, y))),
        ( "(fp.sub roundNearestTiesToAway x y)" , Some( FPBVSubTerm(rm2long, x, y))),
        ( "(fp.sub RNA x (_ +oo 2 4))"          , Some( FPBVSubTerm(rm2, x, fpbvPlusInfty))),
        ( "(fp.mul RTP x y)"                    , Some( FPBVMulTerm(rm3, x, y))),
        ( "(fp.mul roundTowardPositive x y)"    , Some( FPBVMulTerm(rm3long, x, y))),
        ( "(fp.mul RTP (_ -oo 2 4) y)"          , Some( FPBVMulTerm(rm3,fpbvMinusInfty,y ))),
        ( "(fp.div RTN x y)"                    , Some( FPBVDivTerm(rm4, x, y ))),
        ( "(fp.div roundTowardNegative x y)"    , Some( FPBVDivTerm(rm4long, x, y ))),
        ( "(fp.add RTZ x y)"                    , Some( FPBVAddTerm(rm5, x, y ))),
        ( "(fp.add roundTowardZero x y)"        , Some( FPBVAddTerm(rm5long, x, y ))),
        ( "(fp.add RTZ (_ +zero 2 4) y)"        , Some( FPBVAddTerm(rm5, fpbvPlusZero,  y))),
        ( "(fp.fma RTZ x y y)"                  , Some( FPBVFmaTerm(rm5, x, y, y))),
        ( "(fp.sqrt RNE x)"                     , Some( FPBVSqrtTerm(rm1, x))),
        ( "(fp.roundToIntegral RNE x)"          , Some( FPBVRoundToITerm(rm1, x))),
        ( "(fp.min  x y)"                       , Some( FPBVMinTerm( x, y ))),
        ( "(fp.min  x  (_ -zero 2 4))"          , Some( FPBVMinTerm( x, fpbvMinusZero ))),
        ( "(fp.max  x y)"                       , Some( FPBVMaxTerm( x, y ))),
        ( "(fp.max x (_ NaN 2 4))"              , Some( FPBVMaxTerm( x, fpbvNaN ))),

        ( "(fp.rem x y)"                        , Some( FPBVRemTerm(  x, y ))),

        //  Predicates
        ( "(fp.leq  x y)"                       , Some( FPBVLeqTerm( x, y ))),
        ( "(fp.lt  (_ +oo 2 4) y)"              , Some( FPBVLtTerm( fpbvPlusInfty, y ))),
        ( "(fp.geq  x y)"                       , Some( FPBVGeqTerm( x, y ))),
        ( "(fp.gt  x y)"                        , Some( FPBVGtTerm( x, y ))),
        ( "(fp.eq  x (_ -oo 2 4))"              , Some( FPBVEqTerm( x, fpbvMinusInfty ))),

        ( "(fp.isNormal  x )"                   , Some( FPBVisNormal( x ))),
        ( "(fp.isSubnormal  x)"                 , Some( FPBVisSubnormal( x ))),
        ( "(fp.isZero  x)"                      , Some( FPBVisZero( x))),
        ( "(fp.isInfinite  x )"                 , Some( FPBVisInfinite( x ))),
        ( "(fp.isNaN  x )"                      , Some( FPBVisNaN( x ))),
        ( "(fp.isNegative  x )"                 , Some( FPBVisNegative( x ))),
        ( "(fp.isPositive  x )"                 , Some( FPBVisPositive( x ))),

        ( "(fp.isNormal  (_ NaN 2 4) )"         , Some( FPBVisNormal( fpbvNaN ))),
        ( "(fp.isSubnormal  (_ +oo 2 4))"       , Some( FPBVisSubnormal( fpbvPlusInfty ))),
        ( "(fp.isZero  ( fp #b1 #b101 #b1011))" , Some( FPBVisZero(fpbv1))),
        ( "(fp.isInfinite  (_ -oo 2 4) )"       , Some( FPBVisInfinite( fpbvMinusInfty ))),
        ( "(fp.isNaN  (_ -zero 2 4) )"          , Some( FPBVisNaN( fpbvMinusZero))),
        ( "(fp.isNegative  (_ +zero 2 4) )"     , Some( FPBVisNegative( fpbvPlusZero ))),
        ( "(fp.isPositive (fp #b1 #b101 #b1011))" , Some( FPBVisPositive( fpbv1))),

        ( "( (_ to_fp 2 4) (_ bv10 6) )"        , Some(
                                                    FPBVFromBV(
                                                        ToFPBV("2", "4"),
                                                        ConstantTerm(
                                                            DecBVLit(BVvalue("10"), "6" )
                                                        )
                                                    )
                                                  )

        ),
        ( "( (_ to_fp 2 4) RNE (fp #b1 #b101 #b1011 ) )" , Some(
                                                            FPBVFromFPBVRealSInt(
                                                                ToFPBV("2", "4"),
                                                                rm1,
                                                                fpbv1
                                                            )
                                                           )

        ),
        ( "( (_ to_fp 2 4) RNE 0.123 )" ,        Some(
                                                    FPBVFromFPBVRealSInt(
                                                        ToFPBV("2", "4"),
                                                        rm1,
                                                        ConstantTerm(DecLit("0", "123"))
                                                    )
                                                 )
        ),
        ( "( (_ to_fp 2 4) RNE (_ bv10 6) )" ,  Some(
                                                    FPBVFromFPBVRealSInt(
                                                        ToFPBV("2", "4"),
                                                        rm1,
                                                        ConstantTerm(
                                                            DecBVLit(BVvalue("10"), "6" )
                                                        )
                                                    )
                                                 )
        ),
        ( "( (_ to_fp_unsigned 2 4) RNE (_ bv10 6) )" , Some(
                                                            FPBVFromUInt(
                                                                ToFPBVu("2", "4"),
                                                                rm1,
                                                                ConstantTerm(
                                                                    DecBVLit(BVvalue("10"), "6" )
                                                                )
                                                            )
                                                        )
        ),
        ( "( (_ fp.to_ubv 2) RNE ( fp #b1 #b101 #b1011))" , Some(FPBVToUBV("2", rm1, fpbv1))),
        ( "( (_ fp.to_sbv 4) RNE ( fp #b1 #b101 #b1011))" , Some(FPBVToSBV("4", rm1, fpbv1))),
        ( "( fp.to_real ( fp #b1 #b101 #b1011))"        ,Some( FPBVToReal( fpbv1 ) ) )

    )
    //  format: ON

    forAll ( fpbvterms ) {
        ( s : String, answer : Option[ Term ] ) ⇒

            test( s"Trying to parse $s as a <Term> ::= <fpbitvectorterm> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseBVTerm( s ) )

                parseBVTerm( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                                         ⇒
                    case Success( SortTester( SortId( IndexedId( _, _ ) ) ) ) if ( answer.isEmpty ) ⇒
                    case Success( x ) if ( TermTester( answer.get ) == x )                          ⇒

                }
            }
    }
}

/**
 *  Pretty-print floating point BitVectors
 */
class PPrintFPBVTermTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    //  pretty printer
    import parser.SMTLIB2PrettyPrinter.show

    override def suiteName = "Pretty print FP BitVector Term test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  Useful abbrevs
    val x = QIdTerm( SimpleQId( SymbolId( SSymbol( "x" ) ) ) )
    val y = QIdTerm( SimpleQId( SymbolId( SSymbol( "y" ) ) ) )
    val fpbv1 = FPBVvalueTerm( ConstantTerm( BinLit( "1" ) ), ConstantTerm( BinLit( "101" ) ), ConstantTerm( BinLit( "1011" ) ) )
    val fpbvPlusInfty = ConstantTerm( FPPlusInfinity( 2, 4 ) )
    val fpbvMinusInfty = ConstantTerm( FPMinusInfinity( 2, 4 ) )
    val fpbvPlusZero = ConstantTerm( FPBVPlusZero( 2, 4 ) )
    val fpbvMinusZero = ConstantTerm( FPBVMinusZero( 2, 4 ) )
    val fpbvNaN = ConstantTerm( FPBVNaN( 2, 4 ) )
    val rm1 = ConstantTerm( RoundingModeLit( RNE() ) )
    val rm1long = ConstantTerm( RoundingModeLit( RNEven() ) )
    val rm2 = ConstantTerm( RoundingModeLit( RNA() ) )
    val rm2long = ConstantTerm( RoundingModeLit( RNAway() ) )
    val rm3 = ConstantTerm( RoundingModeLit( RTP() ) )
    val rm3long = ConstantTerm( RoundingModeLit( RTPos() ) )
    val rm4long = ConstantTerm( RoundingModeLit( RTNeg() ) )
    val rm4 = ConstantTerm( RoundingModeLit( RTN() ) )
    val rm5 = ConstantTerm( RoundingModeLit( RTZ() ) )
    val rm5long = ConstantTerm( RoundingModeLit( RTZero() ) )

    //  <bitvectorsort> test strings
    //  format: OFF
    val fpbvterms = Table[ String, Term ](
        ( "Input string"                             , "good <fpbv term>?" ) ,

        //  constants
        ( "(_ +oo 2 4)"                              , ConstantTerm(FPPlusInfinity( 2, 4 ))),
        ( "(_ -oo 3 6)"                              , ConstantTerm(FPMinusInfinity( 3, 6))),
        ( "(_ +zero 3 6)"                            , ConstantTerm(FPBVPlusZero( 3, 6))),
        ( "(_ -zero 3 6)"                            , ConstantTerm(FPBVMinusZero( 3, 6))),
        ( "(_ NaN 3 6)"                              , ConstantTerm(FPBVNaN( 3, 6))),

        ( "RNE"                                     , ConstantTerm(RoundingModeLit(RNE()))),
        ( "RNA"                                     , ConstantTerm(RoundingModeLit(RNA()))),
        ( "RTP"                                     , ConstantTerm(RoundingModeLit(RTP()))),
        ( "RTN"                                     , ConstantTerm(RoundingModeLit(RTN()))),
        ( "RTZ"                                     , ConstantTerm(RoundingModeLit(RTZ()))),
        ( "roundNearestTiesToEven"                  , ConstantTerm(RoundingModeLit(RNEven()))),
        ( "roundNearestTiesToAway"                  , ConstantTerm(RoundingModeLit(RNAway()))),
        ( "roundTowardPositive"                     , ConstantTerm(RoundingModeLit(RTPos()))),
        ( "roundTowardNegative"                     , ConstantTerm(RoundingModeLit(RTNeg()))),
        ( "roundTowardZero"                         , ConstantTerm(RoundingModeLit(RTZero()))),

        //  Operations
        ( "(fp.abs x)"                               ,  FPBVAbsTerm( x )),
        ( "(fp.abs (fp #b1 #b101 #b1011))"           ,  FPBVAbsTerm( fpbv1 )),
        ( "(fp.neg x)"                               ,  FPBVNegTerm( x )),
        ( "(fp.add RNE x y)"                         ,  FPBVAddTerm( rm1,  x, y )),
        ( "(fp.add x y)"                             ,  QIdAndTermsTerm(
                                                            SimpleQId(
                                                                SymbolId(
                                                                    SSymbol("fp.add")
                                                                )
                                                            ),
                                                            List(x,y)
                                                        )
        ),
        ( "(fp.sub RNA x y)"                         ,  FPBVSubTerm( rm2, x, y )),
        ( "(fp.sub roundNearestTiesToAway x y)"      ,  FPBVSubTerm( rm2long, x, y )),
        ( "(fp.sub RNA x (_ +oo 2 4))"               ,  FPBVSubTerm( rm2, x, fpbvPlusInfty )),
        ( "(fp.mul RTP x y)"                         ,  FPBVMulTerm( rm3, x, y )),
        ( "(fp.mul roundTowardPositive x y)"         ,  FPBVMulTerm( rm3long, x, y )),
        ( "(fp.mul RTP (_ -oo 2 4) y)"               ,  FPBVMulTerm( rm3, fpbvMinusInfty, y )),
        ( "(fp.div RTN x y)"                         ,  FPBVDivTerm( rm4, x, y )),
        ( "(fp.div roundTowardNegative x y)"         ,  FPBVDivTerm( rm4long, x, y )),
        ( "(fp.add RTZ x y)"                         ,  FPBVAddTerm( rm5, x, y )),
        ( "(fp.add roundTowardZero x y)"             ,  FPBVAddTerm( rm5long, x, y )),
        ( "(fp.add RTZ (_ +zero 2 4) y)"             ,  FPBVAddTerm( rm5, fpbvPlusZero, y )),
        ( "(fp.fma RTZ x y y)"                       ,  FPBVFmaTerm( rm5, x, y, y )),
        ( "(fp.sqrt RNE x)"                          ,  FPBVSqrtTerm( rm1, x)),

        ( "(fp.roundToIntegral RNE x)"               ,  FPBVRoundToITerm( rm1, x)),
        ( "(fp.roundToIntegral roundNearestTiesToEven x)",  FPBVRoundToITerm( rm1long, x)),
        ( "(fp.min x y)"                             ,  FPBVMinTerm( x, y ) ),
        ( "(fp.min x (_ -zero 2 4))"                 ,  FPBVMinTerm( x, fpbvMinusZero ) ),
        ( "(fp.max x y)"                             ,  FPBVMaxTerm( x, y ) ),
        ( "(fp.max x (_ NaN 2 4))"                   ,  FPBVMaxTerm( x, fpbvNaN ) ),

        ( "(fp.rem RTZ x y)"                         ,  QIdAndTermsTerm(
                                                            SimpleQId(
                                                                SymbolId(SSymbol("fp.rem"))),
                                                                List(
                                                                    QIdTerm(
                                                                        SimpleQId(
                                                                            SymbolId(
                                                                                SSymbol("RTZ")
                                                                            )
                                                                        )
                                                                    ),
                                                                    x,
                                                                    y
                                                                )
                                                        )
        ),

        ( "(fp.rem x y)"                             ,  FPBVRemTerm(  x, y ) ),

        //  Predicates
        ( "(fp.leq x y)"                             ,  FPBVLeqTerm( x, y ) ),
        ( "(fp.lt (_ +oo 2 4) y)"                    ,  FPBVLtTerm( fpbvPlusInfty, y ) ),
        ( "(fp.geq x y)"                             ,  FPBVGeqTerm( x, y ) ),
        ( "(fp.gt x y)"                              ,  FPBVGtTerm( x, y ) ),
        ( "(fp.eq x (_ -oo 2 4))"                    ,  FPBVEqTerm( x, fpbvMinusInfty ) ),

        ( "(fp.isNormal x)"                          ,  FPBVisNormal( x ) ),
        ( "(fp.isSubnormal x)"                       ,  FPBVisSubnormal( x ) ),
        ( "(fp.isZero x)"                            ,  FPBVisZero( x ) ),
        ( "(fp.isInfinite x)"                        ,  FPBVisInfinite( x ) ),
        ( "(fp.isNaN x)"                             ,  FPBVisNaN( x ) ),
        ( "(fp.isNegative x)"                        ,  FPBVisNegative( x ) ),
        ( "(fp.isPositive x)"                        ,  FPBVisPositive( x ) ),

        ( "(fp.isNormal (_ NaN 2 4))"                ,  FPBVisNormal( fpbvNaN ) ),
        ( "(fp.isSubnormal (_ +oo 2 4))"             ,  FPBVisSubnormal( fpbvPlusInfty ) ),
        ( "(fp.isZero (fp #b1 #b101 #b1011))"        ,  FPBVisZero( fpbv1) ),
        ( "(fp.isInfinite (_ -oo 2 4))"              ,  FPBVisInfinite( fpbvMinusInfty ) ),
        ( "(fp.isNaN (_ -zero 2 4))"                 ,  FPBVisNaN( fpbvMinusZero) ),
        ( "(fp.isNegative (_ +zero 2 4))"            ,  FPBVisNegative( fpbvPlusZero ) ),
        ( "(fp.isPositive (fp #b1 #b101 #b1011))"    ,  FPBVisPositive( fpbv1 ) ),

        ( "((_ to_fp 2 4) (_ bv10 6))"               ,  FPBVFromBV(
                                                            ToFPBV("2", "4"),
                                                            ConstantTerm(
                                                                DecBVLit(BVvalue("10"), "6" )
                                                            )
                                                        )
        ),
        ( "((_ to_fp 2 4) RNE (fp #b1 #b101 #b1011))" , FPBVFromFPBVRealSInt(
                                                                ToFPBV("2", "4"),
                                                                rm1,
                                                                fpbv1
                                                            )
        ),
        ( "((_ to_fp 2 4) RNE 0.123)"                ,  FPBVFromFPBVRealSInt(
                                                            ToFPBV("2", "4"),
                                                            rm1,
                                                            ConstantTerm(DecLit("0", "123"))
                                                        )
        ),
        ( "((_ to_fp 2 4) RNE (_ bv10 6))"           ,
                                                        FPBVFromFPBVRealSInt(
                                                            ToFPBV("2", "4"),
                                                            rm1,
                                                            ConstantTerm(
                                                                DecBVLit(BVvalue("10"), "6" )
                                                            )
                                                        )
        ),
        ( "((_ to_fp_unsigned 2 4) RNE (_ bv10 6))"  ,    FPBVFromUInt(
                                                                ToFPBVu("2", "4"),
                                                                rm1,
                                                                ConstantTerm(
                                                                    DecBVLit(BVvalue("10"), "6" )
                                                                )
                                                            )
        ),
        ( "((_ fp.to_ubv 2) RNE (fp #b1 #b101 #b1011))"  , FPBVToUBV(
                                                                "2",
                                                                rm1,
                                                                fpbv1
                                                            )
        ),
        ( "((_ fp.to_sbv 4) RNE (fp #b1 #b101 #b1011))"  , FPBVToSBV(
                                                                "4",
                                                                rm1,
                                                                fpbv1
                                                            )
        ),
        ( "(fp.to_real (fp #b1 #b101 #b1011))"           , FPBVToReal( fpbv1 ) )

    )
    //  format: ON

    forAll ( fpbvterms ) {
        ( whatIsExpected : String, s : Term ) ⇒

            test( s"Trying to pprint $s as a <fpbitvectorterm>" ) {

                logger.debug( "Pretty printing {} -- should yield {}", s, whatIsExpected )

                show( s ) shouldBe whatIsExpected
            }
    }
}
