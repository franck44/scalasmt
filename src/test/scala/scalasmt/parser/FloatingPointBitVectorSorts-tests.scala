/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package parser
package tests

import Implicits._

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

//  FloatingPoint BitVectors

/**
 *  Parsing FloatingPoint BitVectors
 */
class ParseFPBVSortTest
    extends FunSuite
    with TableDrivenPropertyChecks
    with Matchers {

    override def suiteName = "Parsing Sorts := <FPBitVectorSort> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for Term

    val parseBVSort = SMTLIB2Parser[ SortTester ]

    //  be careful not to write the same pair twice as this is used
    //  to generate the test name and duplicated names are not allowed.
    //  in the tables of type Table[String, T], (x, Some(t:T)) means x should
    //  be correctly parsed as t:T, and None means it should not

    //  <floatingpointbitvectorsort> test strings
    //  format: OFF
    val fpbvsorts = Table[ String, Option[ Sort ] ](
        ( "Input string"                , "good <bitvectorsort>?" ) ,

        ( "( _ FloatingPoint 11 53)"    , Some( FPBitVectorSort( "11" , "53") ) ) ,
        ( "( _ FloatingPoint 1 6)"      , Some( FPBitVectorSort( "1", "6" ) ) )  ,
        ( "( _ FloatingPoint 15 113)"   , Some( FPBitVectorSort( "15" , "113") ) ) ,
        ( "Float16"                     , Some( FPFloat16() ) ) ,
        ( "Float32"                     , Some( FPFloat32() ) ) ,
        ( "Float64"                     , Some( FPFloat64() ) ) ,
        ( "Float128"                    , Some( FPFloat128() ) ) ,
        // ( "RNE"                         , Some( RoundingModeLit( "RNE" ) ) ),
        ( "RoundingMode"                , Some( RoundingModeSort( ) ) ),
        ( "( _ FloatingPoint 01)"       , None ) ,
        ( "( _ FloatingPoint 0)"        , None ) ,
        ( "( FloatingPoint 64)"         , None )
    )
    //  format: ON

    forAll ( fpbvsorts ) {
        ( s : String, answer : Option[ Sort ] ) ⇒

            test( s"Trying to parse $s as a <Sort> ::= <fpbitvectorsort> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseBVSort( s ) )

                parseBVSort( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                                         ⇒
                    case Success( SortTester( SortId( IndexedId( _, _ ) ) ) ) if ( answer.isEmpty ) ⇒
                    case Success( x ) if ( SortTester( answer.get ) == x )                          ⇒

                }
            }
    }
}

/**
 *  Parsing FloatingPoint BitVectors
 */
class PPrintFPBVSortTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    //  pretty printer
    import parser.SMTLIB2PrettyPrinter.show

    override def suiteName = "Pretty print BitVectorSort test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  <bitvectorsort> test strings
    //  format: OFF
    val fpbvsorts = Table[ String, Sort  ](
        ( "Expected output string" , "A BitVectorSort" )      ,

        ( "(_ FloatingPoint 12 45)"     , FPBitVectorSort( "12" , "45")  ) ,
        ( "(_ FloatingPoint 1 24)"      , FPBitVectorSort( "1" , "24" )  )  ,
        ( "(_ FloatingPoint 64 156)"    , FPBitVectorSort( "64" , "156")  ),
        ( "Float16"                     , FPFloat16()  ),
        ( "Float32"                     , FPFloat32()  ),
        ( "Float64"                     , FPFloat64()  ),
        ( "Float128"                    , FPFloat128() ),
        ( "RoundingMode"                , RoundingModeSort() )
    )
    //  format: ON

    forAll ( fpbvsorts ) {
        ( answer : String, s : Sort ) ⇒

            test( s"Trying to pprint $s as a <fpbitvectorsort>" ) {

                logger.debug( "Pretty printing {} -- should yield {}", s, answer )

                show( s ) shouldBe answer
            }
    }
}
