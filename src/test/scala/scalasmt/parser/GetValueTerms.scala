/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package parser
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import Implicits._

//  get Value terms in SMTLIB2 v2.5,

/**
 *  Parsing rule <get_value_response>
 */
class GetValueParserTest extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing get-value-responses test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for GetValueResponseTester

    val parseGetValue = SMTLIB2Parser[ GetValueResponseTester ]

    //  <valuation pairs> test strings
    //  format: OFF
    val valuationPairs = Table[ String, List[ValuationPair] ](
        ( "input string?"           , "Value response" ) ,
        ( """|
             | (
             |  (x 1)
             |
             | )
             |""".stripMargin ,
             List(
                 ValuationPair(
                     QIdTerm(
                         SimpleQId(SymbolId(SSymbol("x")))
                     ),
                     ConstantTerm(NumLit("1")))
            )
         ) ,
         ( """|
              | (
              |  (x 1) (y 3.0)
              |
              | )
              |""".stripMargin     ,
              List(
                    ValuationPair(
                        QIdTerm(
                            SimpleQId(SymbolId(SSymbol("x")))
                        ),
                        ConstantTerm(NumLit("1"))
                    ),
                    ValuationPair(
                        QIdTerm(
                            SimpleQId(SymbolId(SSymbol("y")))
                        ),
                        ConstantTerm(DecLit("3" , "0"))
                    )
              )
          ) ,
          ( """|
               | (
               |  (x 1) (y 3.0)
               |    (z true)
               | )
               |""".stripMargin     ,
               List(
                   ValuationPair(
                       QIdTerm(
                           SimpleQId(SymbolId(SSymbol("x")))
                       ),
                       ConstantTerm(NumLit("1"))
                   ),
                   ValuationPair(
                       QIdTerm(
                           SimpleQId(SymbolId(SSymbol("y")))
                       ),
                       ConstantTerm(DecLit("3", "0"))
                   ),
                   ValuationPair(
                       QIdTerm(
                           SimpleQId(
                               SymbolId(SSymbol("z")))
                           ),
                           QIdTerm(
                               SimpleQId(SymbolId(SSymbol("true")))
                           )
                       )
               )
            ) ,
         ( """|
              | (
              |  (x 1) (y 3.0)
              |    (k true)
              | )
             |""".stripMargin     ,
             List(
                 ValuationPair(
                     QIdTerm(
                         SimpleQId(SymbolId(SSymbol("x"))
                     )
                    ),
                    ConstantTerm(NumLit("1"))
                ),
                ValuationPair(
                    QIdTerm(
                        SimpleQId(SymbolId(SSymbol("y")))
                    ),
                    ConstantTerm(DecLit("3","0"))
                ),
                ValuationPair(
                    QIdTerm(
                        SimpleQId(SymbolId(SSymbol("k")))
                    ),
                    QIdTerm(
                        SimpleQId(SymbolId(SSymbol("true")))
                    )
                )
             )
         ) ,
         ( """|
              | (
              |    ( (select a1 (_ bv10 2) ) #x00 )
              | )
              |""".stripMargin     ,
              List(
                  ValuationPair(
                      SelectTerm(
                          QIdTerm(
                              SimpleQId(SymbolId(SSymbol("a1")))
                          ),
                          ConstantTerm(
                              DecBVLit(BVvalue("10"),"2")
                          )
                      ),
                      ConstantTerm(HexaLit("00"))
                  )
              )
          )
    )
    //  format: ON

    forAll ( valuationPairs ) {
        ( s : String, theAnswer : List[ ValuationPair ] ) ⇒

            test( s"Trying to parse $s as a <get_value_response> -- should succeed" ) {

                val r = parseGetValue( s )

                logger.debug( s"checking $s - parse command returned: $r" )

                //  r should be a GetValueResponseSuccess
                r should matchPattern {
                    case Success( GetValueResponseTester( GetValueResponseSuccess( _ ) ) ) ⇒
                }
                //  cast as a GetValueResponseTester
                val Success( GetValueResponseTester( GetValueResponseSuccess( a ) ) ) = r

                a shouldBe theAnswer

            }
    }

    //  <valuation pairs> test strings
    //  format: OFF
    val badValuationPairs = Table[ String, String ](
        ( "input string not a valuation pair?", "Reason why"  ) ,
        ( """|
             | (
             |  (x 1
             |
             | )
             |""".stripMargin, "Missing )"
         ) ,
         ( """|
              | (
              |  (x 1) (y 3.0.0)
              |
              | )
              |""".stripMargin, "3.0.0 not a decimal number"
          ) ,
          ( """|
               | (
               |  (x 1) (y 3.0)
               |    (#z true)
               | )
               |""".stripMargin, "# not allowed in identifiers"
            ) ,
         ( """|
              | (
              |  (x 1) (y 3.0)
              |    (k )
              | )
             |""".stripMargin, "Missing value for k"
         ) ,
         ( """|
              | (
              |    ( (select a1 (_ bv 2) ) #x00 )
              | )
              |""".stripMargin, "Missing BV value after bv"
          )
    )
    //  format: ON

    import parser.SMTLIB2Parser.ParseErrorException
    forAll ( badValuationPairs ) {
        ( s : String, why : String ) ⇒

            test( s"Trying to parse $s as a <get_value_response> -- should fail. Reason: $why" ) {

                val r = parseGetValue( s )

                logger.debug( s"checking $s - parse command returned: $r" )

                //  r should be a GetValueResponseSuccess
                r should matchPattern {
                    case Failure( ParseErrorException( _ ) ) ⇒
                }
            }
    }
}
