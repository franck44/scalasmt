/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package parser

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import Implicits._
import interpreters.Resources
import org.bitbucket.inkytonik.kiama.util.FileSource

/**
 * FIXME: this test file is in work in progress
 */
class ParseFileTests extends FunSuite with Matchers with Resources {

    override def suiteName = "Parsing simple files test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    ignore( "Read .smt2 file -- file is missing in repository. Check it in" ) {

        //  path where the files are
        val path = "src/test/scala/smtlib/parser/resources/"

        //  Get  a parser for SMTLIB2 script
        val parser = SMTLIB2Parser[ Script ]

        //  Parse the file
        val t = parser( FileSource( path + "ex1.smt2" ) )
        t match {
            case Success( Script( cmds ) ) ⇒

                val status = cmds find { x ⇒
                    x match {
                        case SetInfoCmd(
                            AttributeKeywordAndValue(
                                Keyword( "status" ),
                                AttValueSymbol( SSymbol( _ ) )
                                )
                            ) ⇒ true
                        case _ ⇒ false

                    }
                }

                //  assume status is Some!
                assert( status.isDefined )

            case Success( c ) ⇒
                fail( new Exception( s"Could not parse file ex1.smt2 as a script. Got $c" ) )

            case Failure( f ) ⇒
                fail( new Exception( s"Could not parse file ex1.smt2. ${f.getMessage}" ) )
        }
    }
}
