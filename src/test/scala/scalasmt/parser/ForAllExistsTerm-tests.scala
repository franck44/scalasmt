/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package typedterms
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ IntegerArithmetics, Core }

import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

/**
 * Check the pretty printed term of ForAll/ExistsTerm
 */
class ForAllExistsTermDefsTest extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with QuantifiedTerm {

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    override def suiteName = "Mixed ForAll/ExistTerm termDef test suite"

    import parser.SMTLIB2Syntax.{ Term, ForAllTerm, ExistsTerm, SSymbol }
    import parser.Implicits._
    import parser.SMTLIB2PrettyPrinter.format
    import theories.BoolTerm
    import parser.SMTLIB2Parser

    val x = Ints( "x" )
    val y = Ints( "y" )

    //  format: OFF
    val theTerms = Table[ String, TypedTerm[BoolTerm, Term], String ](

        ( "SMTLIB version" , "typed term version", "Expected pretty printed term" ) ,

        //  nested forall/exists
        (
            """|
               | (forall
               |   (
               |    (x1 Int)
               |   )
               |   (=>
               |    (<= x1 0)
               |    (exists
               |        (
               |         (y1 Int)
               |         (z1 Int)
               |        )
               |        (=>
               |          (>= y1 z1)
               |          (>= y1 x1)
               |        )
               |    )
               |   )
               | )
               |""".stripMargin,
            forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                exists(Ints("y1").symbol, SSymbol("z1")) {
                    val y1 = Ints("y1")
                    val z1 = Ints("z1")
                    y1 >= z1 imply y1 >= x1
                }
            },
            "(forall ((x1 Int) ) (=> (<= x1 0) (exists ((y1 Int) (z1 Int) ) (=> (>= y1 z1) (>= y1 x1)))))"
        ),

        //  nested exists/forall
        (
            """|
               | (exists
               |   (
               |    (x1 Int)
               |   )
               |   (=>
               |    (<= x1 0)
               |    (forall
               |        (
               |         (y1 Int)
               |         (z1 Int)
               |        )
               |        (=>
               |          (>= y1 z1)
               |          (>= y1 x1)
               |        )
               |    )
               |   )
               | )
               |""".stripMargin,
            exists(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                forall(Ints("y1").symbol, SSymbol("z1")) {
                    val y1 = Ints("y1")
                    val z1 = Ints("z1")
                    y1 >= z1 imply y1 >= x1
                }
            },
            "(exists ((x1 Int) ) (=> (<= x1 0) (forall ((y1 Int) (z1 Int) ) (=> (>= y1 z1) (>= y1 x1)))))"
        )
    )
    // format: ON

    for ( ( txt, xt, r ) ← theTerms ) {

        test( s"Check forAllTerm termDef for $txt -- should be $r" ) {

            logger.info( "formated term: {}", format( xt.termDef ).layout )
            format( xt.termDef ).layout shouldBe r
        }
    }
}

/**
 * Check the typedDefs of ForAll/Exists Term
 */
class ForAllExistsTermTypeDefsTest extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core with QuantifiedTerm {

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    override def suiteName = "Mixed ForAll/Exists Term typedDefs test suite"

    import parser.SMTLIB2Syntax.{
        Term,
        QualifiedId,
        ForAllTerm,
        ExistsTerm,
        SortedQId,
        SSymbol,
        SymbolId,
        IntSort
    }
    import parser.Implicits._
    import parser.SMTLIB2PrettyPrinter.format
    import theories.BoolTerm

    //  create a solver SortedQId, type Ints, from a string
    def toSortedQId( name : String ) : SortedQId = SortedQId( SymbolId( SSymbol( name ) ), IntSort() )

    val x = Ints( "x" )
    val y = Ints( "y" )

    //  format: OFF
    val theTerms = Table[ TypedTerm[BoolTerm, Term], Set[SortedQId] ](

        ( "Terms" , "Expected set of typedefs" ) ,

        //  nested forall/exists
        (
            forall(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                exists(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            },
            Set() map toSortedQId
        ),

        //  nested exists/forall
        (
            exists(Ints("x1").symbol) {
                val x1 = Ints("x1")
                x1 <= 0 imply
                forall(Ints("y1").symbol) {
                    val y1 = Ints("y1")
                    y1 >= 0 imply y1 >= x1
                }
            },
            Set() map toSortedQId
        )
    )
    // format: ON

    for ( ( xt, r ) ← theTerms ) {

        test( s"Check forallTerm typeDefs for term  ${format( xt.termDef ).layout} -- should be $r" ) {

            logger.info( "formated term: {}", format( xt.termDef ).layout )
            xt.typeDefs shouldBe r
        }
    }
}
