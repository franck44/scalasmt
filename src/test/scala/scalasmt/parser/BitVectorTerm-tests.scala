/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package parser
package tests

import Implicits._
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import scala.util.{
    Try,
    Success,
    Failure
}

class BitVectorsTermParserTests extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing of BitVector terms"

    import parser.SMTLIB2Parser
    import parser.SMTLIB2Syntax._
    import parser.SMTLIB2PrettyPrinter.{ show }
    import typedterms.TypedTerm

    val parseTerm = SMTLIB2Parser[ TermTester ]

    //  format: OFF
    val theTerms1 = Table[ String,  Term ](
        ( "Expression"                      , "termDef" ),
        ( "(_ bv0 16)"                      , ConstantTerm(DecBVLit(BVvalue("0"),"16"))),

        ( "#b1000000000000000"              , ConstantTerm(BinLit((1::List.fill(15)(0)).mkString))),
        ( "#xffff"                          , ConstantTerm(HexaLit("ffff"))),

        ( "(bvneg (_ bv1 16))"              , BVNegTerm(ConstantTerm(DecBVLit(BVvalue("1"),"16")))),
        ( "(bvnot (_ bv1 16))"              , BVNotTerm(ConstantTerm(DecBVLit(BVvalue("1"),"16")))),
        ( "(bvadd #xa9 (_ bv1 16))"         , BVAddTerm(
                                                    ConstantTerm(
                                                        HexaLit("a9")),
                                                        ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                    )
        ),
        ( "(bvsub #xa9 (_ bv1 16))"         , BVSubTerm(
                                                    ConstantTerm(
                                                        HexaLit("a9")),
                                                        ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                    )
        ),
        ( "(bvmul #xa9 (_ bv1 16))"         , BVMultTerm(
                                                    ConstantTerm(
                                                        HexaLit("a9")),
                                                        ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                    )
        ),
        ( "(bvudiv #xa9 (_ bv1 16))"        , BVuDivTerm(
                                                    ConstantTerm(
                                                        HexaLit("a9")),
                                                        ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                    )
        ),
        ( "(bvurem #xa9 (_ bv1 16))"        , BVuRemTerm(
                                                    ConstantTerm(
                                                        HexaLit("a9")),
                                                        ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                    )
        ), 
        ( "(bvuge (_ bv1 64) (_ bv2 64))"        ,        BVUGreaterThanEqualTerm(
                                                    ConstantTerm(
                                                        (DecBVLit(BVvalue("1"),"64"))),     ConstantTerm(DecBVLit(BVvalue("2"),"64"))
                                                    )
        ),
        ( "(bvsge (_ bv1 64) (_ bv2 64))"        ,        BVSGreaterThanEqualTerm(
                                                    ConstantTerm(
                                                        (DecBVLit(BVvalue("1"),"64"))),     ConstantTerm(DecBVLit(BVvalue("2"),"64"))
                                                    )
        ),
        ( "(bvule (_ bv1 64) (_ bv2 64))"        ,        BVULessThanEqualTerm(
                                                    ConstantTerm(
                                                        (DecBVLit(BVvalue("1"),"64"))),     ConstantTerm(DecBVLit(BVvalue("2"),"64"))
                                                    )
        ),
        ( "(bvsle (_ bv1 64) (_ bv2 64))"        ,        BVSLessThanEqualTerm(
                                                    ConstantTerm(
                                                        (DecBVLit(BVvalue("1"),"64"))),     ConstantTerm(DecBVLit(BVvalue("2"),"64"))
                                                    )
        ),
        ( "(bvsdiv #xa9 (_ bv1 16))"        , BVsDivTerm(
                                                    ConstantTerm(
                                                        HexaLit("a9")),
                                                        ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                    )
        ),
        ( "(bvsrem #xa9 (_ bv1 16))"        , BVsRemTerm(
                                                    ConstantTerm(
                                                        HexaLit("a9")),
                                                        ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                    )
        ),
        ( "(bvor #xa9 (_ bv1 16))"            , BVOrTerm(
                                                    ConstantTerm(
                                                        HexaLit("a9")),
                                                        ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                    )
        ),
        ( "(bvxor #xa9 (_ bv1 16))"           , BVXorTerm(
                                                    ConstantTerm(
                                                        HexaLit("a9")),
                                                        ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                    )
        ),
        ( "(bvand #xa9 (_ bv1 16))"             , BVAndTerm(
                                                    ConstantTerm(
                                                        HexaLit("a9")),
                                                        ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                    )
        ),
        ( "(bvshl (_ bv128 16) (_ bv1 16))"     , BVShiftLeftTerm(
                                                        ConstantTerm(DecBVLit(BVvalue("128"),"16")),
                                                        ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                )
        ),
        ( "(bvlshr (_ bv128 16) (_ bv1 16))"     , BVSLogicalShiftRightTerm(
                                                        ConstantTerm(DecBVLit(BVvalue("128"),"16")),
                                                        ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                )
        ),
        ( "(bvashr (_ bv128 16) (_ bv1 16))"     , BVSArithmeticShiftRightTerm(
                                                        ConstantTerm(DecBVLit(BVvalue("128"),"16")),
                                                        ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                )
        ),
        ( "((_ sign_extend 2) (_ bv1 16))"     , BVSignExtendTerm(
                                                        NumLit("2"),
                                                        ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                )
        ),
        ( "((_ zero_extend 2) (_ bv1 16))"     , BVZeroExtendTerm(
                                                        NumLit("2"),
                                                        ConstantTerm(DecBVLit(BVvalue("1"),"16"))
                                                )
        ),

        ( "((_ extract 4 8) x)"                , BVExtractTerm(
                                                            NumLit("4"),
                                                            NumLit("8"),
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                )
                                                            )
        ),
        ( "(concat x (_ bv2 16))"                , BVConcatTerm(
                                                            QIdTerm(
                                                                SimpleQId(
                                                                    SymbolId(
                                                                        SSymbol("x"))
                                                                    )
                                                                ),
                                                                ConstantTerm(DecBVLit(BVvalue("2"),"16"))
                                                            )
        )
    )
    //  format: ON

    for ( ( s, term ) ← theTerms1 ) {
        test( s"Parse $s as a term. Should be $term" ) {
            parseTerm( s ) shouldBe Success( TermTester( term ) )
        }
    }

    for ( ( s, term ) ← theTerms1 ) {
        test( s"Pretty-printing $term as a term. Should be $s" ) {
            show( term ) shouldBe s
        }
    }
}
