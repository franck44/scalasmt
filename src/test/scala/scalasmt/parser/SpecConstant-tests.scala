/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package parser
package tests
import Implicits._

import org.scalatest.{
    FunSuite,
    Matchers
}
import scala.util.{
    Try,
    Success,
    Failure
}
import org.scalatest.prop.TableDrivenPropertyChecks
import parser.SMTLIB2Syntax._
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

//  SpecConstant in SMTLIB2 v2.5,

/**
 *  Parsing rule <spec_constant> := <numeral>
 */
class SpecConstantRule1Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing SpecConstant <spec_constant> ::= <numeral> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseSpecConstant = SMTLIB2Parser[ SpecConstantTester ]

    //  <numeral> test strings
    //  format: OFF
    val numerals = Table[ String, Option[ SpecConstant ] ](
        ( "input string is a good specConstant?" , "yes/no" )                 ,
        ( "2"                                    , Some( NumLit( "2" ) ) )    ,
        ( "12"                                   , Some( NumLit( "12" ) ) )   ,
        ( "23"                                   , Some( NumLit( "23" ) ) )   ,
        ( "0"                                    , Some( NumLit( "0" ) ) )    ,
        ( "3455"                                 , Some( NumLit( "3455" ) ) ) ,
        ( "00"                                   , None )                     ,
        ( "2 3"                                  , None )                     ,
        ( "3455."                                , None )                     ,
        ( "2301980_"                             , None )                     ,
        ( "_0"                                   , None )
    )
    //  format: ON

    forAll ( numerals ) {
        ( s : String, answer : Option[ SpecConstant ] ) ⇒

            test( s"Trying to parse $s as a <spec_constant> ::= <numeral> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseSpecConstant( s ) )

                parseSpecConstant( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                        ⇒
                    case Success( x ) if ( SpecConstantTester( answer.get ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule <spec_constant> := <decimal>
 */
class SpecConstantRule2Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing SpecConstant <spec_constant> ::= <decimal> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseSpecConstant = SMTLIB2Parser[ SpecConstantTester ]

    // <decimal> i.e. <numeral>.0*<numeral> test strings
    //  format: OFF
    val decimals = Table[ String, Option[ SpecConstant ] ](
        ( "input string is a good specConstant?" , "yes/no" )                         ,
        ( "0.0001"                               , Some( DecLit( "0" , "0001" ) ) )       ,
        ( "0.1"                                  , Some( DecLit( "0", "1" ) ) )          ,
        ( "0.0"                                  , Some( DecLit( "0", "0" ) ) )          ,
        ( "0.01"                                 , Some( DecLit( "0", "01" ) ) )         ,
        ( "23.000000004"                         , Some( DecLit( "23", "000000004" ) ) ) ,
        ( "3455.1"                               , Some( DecLit( "3455" , "1" ) ) )       ,
        ( "2.0"                                  , Some( DecLit( "2", "0" ) ) )          ,
        ( "0.000"                                , Some( DecLit( "0", "000" ) ) )        ,
        ( "123378.0"                             , Some( DecLit( "123378", "0" ) ) )     ,
        ( "23.000000 004"                        , None )                             ,
        ( ".002"                                 , None )                             ,
        ( "34489."                               , None )
    )
    //  format: ON

    forAll ( decimals ) {
        ( s : String, answer : Option[ SpecConstant ] ) ⇒

            test( s"Trying to parse $s as a <spec_constant> ::= <decimal> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseSpecConstant( s ) )

                parseSpecConstant( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                        ⇒
                    case Success( x ) if ( SpecConstantTester( answer.get ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule <spec_constant> := <hexadecimal>
 */
class SpecConstantRule3Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing SpecConstant <spec_constant> ::= <hexadecimal> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseSpecConstant = SMTLIB2Parser[ SpecConstantTester ]

    //  <hexadecimal>: '#x' followed by non-empty sequence of
    //  digits and letters from a to F (case insensitive)
    //  format: OFF
    val hexaDecimals = Table[ String, Option[ SpecConstant ] ](
        ( "input string is a good specConstant?" , "yes/no" )                          ,

        ( "#x1"                                  , Some( HexaLit( ( "1" ) ) ) )        ,
        ( "#x"                                   , None )                              ,
        ( "# x1"                                 , None )                              ,
        ( "#xf"                                  , Some( HexaLit( ( "f" ) ) ) )        ,

        ( "#xAFe0"                               , Some( HexaLit( ( "AFe0" ) ) ) )     ,
        ( "#x45dE2137"                           , Some( HexaLit( ( "45dE2137" ) ) ) ) ,
        ( "#x 345F"                              , None )                              ,

        ( "fa0"                                  , None )                              ,
        ( "x00"                                  , None )

    )
    //   format: ON

    forAll ( hexaDecimals ) {
        ( s : String, answer : Option[ SpecConstant ] ) ⇒

            test( s"Trying to parse $s as a <spec_constant> ::= <hexadecimal> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseSpecConstant( s ) )

                parseSpecConstant( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                        ⇒
                    case Success( x ) if ( SpecConstantTester( answer.get ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule <spec_constant> := <binary>
 */
class SpecConstantRule4Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing SpecConstant <spec_constant> ::= <binary> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseSpecConstant = SMTLIB2Parser[ SpecConstantTester ]

    // <binary>:  '#b' followed by non-empty sequence of 0 and 1
    //  format: OFF
    val binary = Table[ String, Option[ SpecConstant ] ](
        ( "input string is a good specConstant?" , "yes/no" )                         ,

        ( "#b1"                                  , Some( BinLit( ( "1" ) ) ) )        ,
        ( "#b"                                   , None )                             ,
        ( "#b01"                                 , Some( BinLit( ( "01" ) ) ) )       ,
        ( "#b01110010"                           , Some( BinLit( ( "01110010" ) ) ) ) ,
        ( "#b 1"                                 , None )                             ,
        ( "# b11"                                , None )                             ,
        ( "#xi0"                                 , None )                             ,
        ( "#b 345F"                              , None )

    )
    //  format: ON

    forAll ( binary ) {
        ( s : String, answer : Option[ SpecConstant ] ) ⇒

            test( s"Trying to parse $s as a <spec_constant> ::= <binary> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseSpecConstant( s ) )

                parseSpecConstant( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                        ⇒
                    case Success( x ) if ( SpecConstantTester( answer.get ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule <spec_constant> := <string>
 */
class SpecConstantRule5Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing SpecConstant <spec_constant> ::= <string> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseSpecConstant = SMTLIB2Parser[ SpecConstantTester ]

    //  <string>: sequence of white spaces and printabel characters in double quote
    //  with escape sequence ""
    //  format: OFF
    val strings = Table[ String, Option[ SpecConstant ] ](
        ( "input string"             , "good specConstant: yes/no" ),

        ( " \"this is it!\""         , Some( StringLit( StringLiteral( "this is it!" ) ) ) ),
        ( " \" @rt why> 01235\""     , Some( StringLit( StringLiteral( " @rt why> 01235" ) ) ) ),
        ( " \"&^$%12 ttr%\""         , Some( StringLit( StringLiteral( "&^$%12 ttr%" ) ) ) ),
        ( " \" le ts b reA l1st1c\"" , Some( StringLit( StringLiteral( " le ts b reA l1st1c" ) ) ) ),
        ( " \"^\\ is a good one\""   , Some( StringLit( StringLiteral( "^\\ is a good one" ) ) ) ),
        ( " +oo "                    , None ),
        ( " -oo  "                   , None ),
        ( " -zero  "                 , None ),
        ( " +zero  "                 , None ),
        ( " NaN  "                   , None ),
        ( " fp 2 4  "                , None )
    )
    //  format: ON

    //  Terms in SMTLIB2 v2.5, test for rule
    //  <spec_constant> := <string>

    forAll ( strings ) {
        ( s : String, answer : Option[ SpecConstant ] ) ⇒

            test( s"Trying to parse $s as a <spec_constant> ::= <string> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                logger.debug( "checking {} - parse command returned:{}", s, parseSpecConstant( s ) )

                parseSpecConstant( s ) should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                        ⇒
                    case Success( x ) if ( SpecConstantTester( answer.get ) == x ) ⇒

                }
            }
    }
}

/**
 *  Parsing rule <spec_constant> := <FPBVector constants>
 */
class SpecConstantRule6Test extends FunSuite with TableDrivenPropertyChecks with Matchers {

    override def suiteName = "Parsing SpecConstant <spec_constant> ::= <floatingPoint bitvectors> test suite"

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    //  get a test parser for SpecConstant

    val parseTerm = SMTLIB2Parser[ TermTester ]

    //  <string>: sequence of white spaces and printabel characters in double quote
    //  with escape sequence ""
    //  format: OFF
    val strings = Table[ String, Option[ Term ] ](
        ( "input string"             , "good specConstant: yes/no" ),

        ( " (_ +oo 2 4) "               , Some(ConstantTerm(FPPlusInfinity(2, 4)))),
        ( " (_ -oo 2 4) "               , Some(ConstantTerm(FPMinusInfinity(2, 4)))),
        ( " (_ +zero 2 4) "             , Some(ConstantTerm(FPBVPlusZero(2, 4)))),
        ( " (_ -zero 2 4) "             , Some(ConstantTerm(FPBVMinusZero(2, 4)))),
        ( " (_ NaN 2 4) "               , Some(ConstantTerm(FPBVNaN(2, 4)))),
        ( " (_ +oo 4) "                 , None),
        ( " (_ -oo 2 ) "                , None),
        ( " (_ +zero  4) "              , None),
        ( " (_ -zero 2 ) "              , None),
        ( " (_ NaN 2 ) "                , None),
        ( " ( fp #b1 #b101 #b1111 )"    , Some(
                                            FPBVvalueTerm(
                                                ConstantTerm(BinLit("1")),
                                                ConstantTerm(BinLit("101")),
                                                ConstantTerm(BinLit("1111"))
                                            )
                                        )
        ),
        ( " ( fp #b1#b101#b1111 )"    , Some(
                                            FPBVvalueTerm(
                                                ConstantTerm(BinLit("1")),
                                                ConstantTerm(BinLit("101")),
                                                ConstantTerm(BinLit("1111"))
                                            )
                                        )
        ),
        ( " ( fp #b1 #xe7 #b1111 )"     , Some(
                                            FPBVvalueTerm(
                                                ConstantTerm(BinLit("1")),
                                                ConstantTerm(HexaLit("e7")),
                                                ConstantTerm(BinLit("1111"))
                                            )
                                          )
        ),
        ( " ( fp #b1 #xe7 (_ bv10 5) )" , Some(
                                            FPBVvalueTerm(
                                                ConstantTerm(BinLit("1")),
                                                ConstantTerm(HexaLit("e7")),
                                                ConstantTerm(DecBVLit(BVvalue("10"),"5"))
                                            )
                                          )
        ),
        ( " ( fp #b1 #xe7(_ bv10 5)  ) " , Some(
                                            FPBVvalueTerm(
                                                ConstantTerm(BinLit("1")),
                                                ConstantTerm(HexaLit("e7")),
                                                ConstantTerm(DecBVLit(BVvalue("10"),"5"))
                                            )
                                          )
        )
    )

    //  format: ON

    //  Terms in SMTLIB2 v2.5, test for rule
    //  <spec_constant> := <floating point BV>

    forAll ( strings ) {
        ( s : String, answer : Option[ Term ] ) ⇒

            test( s"Trying to parse $s as a <spec_constant> ::= <floating point bitvectors constant> -- ${if ( answer.nonEmpty ) "should succeed" else "should fail"}" ) {

                val res = parseTerm( s )
                res match {
                    case Success( _ ) ⇒
                        logger.debug( "checking {} - parse command returned:{}", s, res )
                    case Failure( f ) ⇒
                        logger.error( "checking {} - parse command returned error:{}", s, f.getMessage )
                }

                res should matchPattern {
                    case Failure( _ ) if ( answer.isEmpty )                ⇒
                    case Success( x ) if ( TermTester( answer.get ) == x ) ⇒

                }
            }
    }
}
