/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package parser
package tests

import org.scalatest.{
    FunSuite,
    Matchers
}
import org.scalatest.prop.TableDrivenPropertyChecks
import theories.{ IntegerArithmetics, Core }

import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

class IsLinearTest extends FunSuite with TableDrivenPropertyChecks with Matchers with IntegerArithmetics with Core {

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    override def suiteName = "test suite"

    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ TermTester }
    import parser.SMTLIB2Parser
    import parser.Implicits._
    import parser.SMTLIB2PrettyPrinter.format

    val p1 = SMTLIB2Parser[ TermTester ]

    //  format: OFF
    val theTerms = Table[ String, Boolean ](

        ( "Terms"                          , "Expected response" ) ,

        //  scalars
        ( "(+ 1 1 2 3)"                    , true )                ,
        ( "(- 1 1 2 3)"                    , true )                ,
        ( "(* 1 1 2 3)"                    , true )                ,
        ( "(* 1 k 2 3)"                    , true )                ,

        //  integer division
        ( "(div 1 1 2 3)"                  , true )                ,
        ( "(div x 2)"                      , true )                ,
        ( "(div 2 x)"                      , false )               ,
        ( "(div x 1 2 3)"                  , true )                ,
        ( "(div x 1 k 3)"                  , false )               ,
        ( "(div (* 1 k 2 3) x)"            , false )               ,
        ( "(mod x (+ 1 1))"                , true )                ,
        ( "(mod x (+ 1 y))"                , false )               ,


        //  real division
        ( "(/ (* (- 1.33) k 2.0 3.1) 1.0)" , true )                ,
        ( "(/ (* 1 k 2 3) x)"              , false )               ,
        ( "(/ 1 x)"                        , false )               ,
        ( "(/ x 1)"                        , true )                ,

        //  linear terms
        ( "(+ x 1)"                        , true )                ,
        ( "(+ 2 1)"                        , true )                ,
        ( "(<= (+ x y) 3)"                 , true )                ,
        ( "(+ (* 1 k 2 3) x)"              , true )                ,
        ( "(and (+ x y) (+ x 1))"          , true )                ,
        ( "(+ x y 1 z)"                    , true )                ,
        ( "(div x 3)"                      , true )                ,
        ( "(div 3 y)"                      , false )               ,
        ( "(mod x 3)"                      , true )                ,
        ( "(mod 3 x)"                      , false )               ,
        ( "(* (+ 1 x) 2)"                  , true )                ,

        //  non-linear terms
        ( "(* x y)"                        , false )               ,
        ( "(and (* x y) (+ x 1))"          , false )               ,
        ( "(* x y 1 3)"                    , false )               ,
        ( "(div x k)"                      , false )               ,
        ( "(mod x k)"                      , false )               ,

        //  array terms (always non-linear)
        ( "(select a1 1)"                  , false )               ,
        ( "(store a1 1 x)"                 , false )               ,
        ( "(= a2 (store a1 1 x))"          , false )

    )
    // format: ON

    for ( ( xt, r ) ← theTerms ) {

        test( s"Check if term $xt is linear -- should be $r" ) {

            p1( xt ) match {

                case Success( TermTester( t ) ) ⇒
                    logger.info( "{} parsed successfully", t )
                    Analysis( t ).isLinear shouldBe r

                case Failure( e ) ⇒
                    logger.error( "Parse error {}", e )
                    fail( e )
            }

        }
    }
}

/**
 * Test collection of QualifiedIds in a term
 */
class CollectIdsTests extends FunSuite with TableDrivenPropertyChecks with Matchers {

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    override def suiteName = "Collect QualifiedIds in terms test suite"

    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ TermTester, SSymbol, SimpleQId, SymbolId }
    import parser.SMTLIB2Parser
    import parser.Implicits._
    import parser.SMTLIB2PrettyPrinter.format

    val p1 = SMTLIB2Parser[ TermTester ]

    //  format: OFF
    val theTerms = Table[ String, Set[String] ](

        ( "Terms"                          , "Expected response" ) ,

        //  scalars
        ( "(+ 1 1 2 3)"                    , Set() )               ,

        //  linear terms
        ( "(+ x 1)"                        , Set("x") )            ,

        //  non-linear terms
        ( "(* x y)"                        , Set("x", "y") )       ,

        (""" |
             | (
             |  let
             |    (
             |     (  a!1
             |         (>= (+ x (* (- 1 ) (select a1 1 ) ) ) 0 ) )
             |    )
             |    (not a!1 )
             | )
             |""".stripMargin ,         Set("x", "a1") )
    )
    // format: ON

    for ( ( xt, r ) ← theTerms ) {

        test( s"Compute the QualifiedIds in $xt -- should be $r" ) {

            p1( xt ) match {

                case Success( TermTester( t ) ) ⇒
                    logger.info( "{} parsed successfully", t )
                    Analysis( t ).ids shouldBe
                        r.map( SSymbol( _ ) ).map( SymbolId ( _ ) ).map( SimpleQId( _ ) )

                case Failure( e ) ⇒
                    logger.error( "Parse error {}", e )
                    fail( e )
            }

        }
    }
}

/**
 * Test collection of QualifiedIds in a term
 */
class UnindexTests extends FunSuite with TableDrivenPropertyChecks with Matchers {

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    override def suiteName = "Remove indices in terms test suite"

    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ TermTester, SSymbol, SimpleQId, SymbolId }
    import parser.SMTLIB2Parser
    import parser.Implicits._
    import parser.SMTLIB2PrettyPrinter.format

    val p1 = SMTLIB2Parser[ TermTester ]

    //  format: OFF
    val theTerms = Table[ String, String ](
        ( "Terms"           , "Unindexed term" ) ,
        ( "(+ 1 1 2)"       , "(+ 1 1 2)" )      ,
        ( "(+ x@1 1)"       , "(+ x 1)" )        ,
        ( "(* x y@2)"       , "(* x y)" )        ,
        ( "(- x@10 y@2)"    , "(- x y)" )        ,

        ( "(+ x@1 x@0)"     , "(+ x x)" )        ,

        //  array indexing
        ("(select a@1 i)"   , "(select a i)" )   ,
        ("(select a i@0)"   , "(select a i)" )   ,
        ("(select a@1 i@3)" , "(select a i)" )   ,

        //  let term
        ("""
            |(let
            |  (
            |   (x@1 1)
            |   (y@0 2)
            |   (z (+ t@2 2) )
            |  )
            | (+ t (- x y) )
            |)
            |""".stripMargin,
        """
            |(let
            |  (
            |   (x 1)
            |   (y 2)
            |   (z (+ t 2) )
            |  )
            | (+ t (- x y) )
            |)
            |""".stripMargin
        )
    )
    // format: ON

    for ( ( srcTerm, unindexedTerm ) ← theTerms ) {

        test( s" $srcTerm unindexed  should be $unindexedTerm" ) {

            ( p1( srcTerm ), p1( unindexedTerm ) ) match {

                case ( Success( TermTester( t1 ) ), Success( TermTester( t2 ) ) ) ⇒
                    logger.info( "{} and {} parsed successfully", t1, t2 )

                    Analysis( t1 ).unIndexed shouldBe t2

                case ( Failure( e ), _ ) ⇒
                    logger.error( "Parse error in first term {}", e )
                    fail( e )
                case ( _, Failure( e ) ) ⇒
                    logger.error( "Parse error in second term {}", e )
                    fail( e )
            }

        }
    }
}

/**
 * Test collection of QualifiedIds in a term
 */
class IndexerTests extends FunSuite with TableDrivenPropertyChecks with Matchers {

    val logger = Logger( LoggerFactory.getLogger( this.getClass() ) )

    override def suiteName = "Add/change indices in terms test suite"

    import scala.util.{ Success, Failure }
    import parser.SMTLIB2Syntax.{ TermTester, SSymbol, ISymbol, SMTLIB2Symbol }
    import parser.SMTLIB2Parser
    import parser.Implicits._
    import parser.SMTLIB2PrettyPrinter.format

    val p1 = SMTLIB2Parser[ TermTester ]

    //  format: OFF
    val theTerms = Table[ String, SMTLIB2Symbol ==> Int, String ](
        ( "Terms"     , "Indexer"                                       , "Indexed term" ) ,
        //  nothing to index
        ( "(+ 1 1 2)" , Map()                                           , "(+ 1 1 2)" )    ,
        ( "(+ x 1)"   , Map(SSymbol("y") -> 1)                          , "(+ x 1)" )      ,
        ( "(* x y@2)" , Map(ISymbol("z", 2)  -> 3, SSymbol("t") -> 0)   , "(* x y@2)" )    ,
        ( "(+ x y@2)" , Map(ISymbol("y", 1)  -> 3, SSymbol("t") -> 0)   , "(+ x y@2)" )    ,

        //  one symbol to index
        ( "(+ x 1)"   , Map(SSymbol("x") -> 1)                          , "(+ x@1 1)" )    ,
        ( "(* x y@2)" , Map(ISymbol("y", 2)  -> 3, SSymbol("x") -> 0)   , "(* x@0 y@3)" )  ,
        ( "(- x y@2)" , Map(SSymbol("x") -> 0)                          , "(- x@0 y@2)" )  ,
        ( "(- x y@2)" , Map(SSymbol("y") -> 0, ISymbol("y", 2) -> 0)    , "(- x y@0)" )    ,
        ( "(+ x y@2)" , Map(SSymbol("x") -> 0, ISymbol("y", 1) -> 0)    , "(+ x@0 y@2)" )  ,

        //  non interference ISymbol/SSymbol
        ( "(+ x 1)"     , Map(ISymbol("x", 0) -> 1)                     , "(+ x 1)" )     ,
        ( "(+ x@1 1)"   , Map(SSymbol("x") -> 1)                        , "(+ x@1 1)" )   ,
        ( "(+ x x@1)"   , Map(SSymbol("x") -> 1)                        , "(+ x@1 x@1)" ) ,
        ( "(+ y@1 x@1)" , Map(ISymbol("x", 1) ->2)                      , "(+ y@1 x@2)" ) ,
        ( "(+ x x@1)"   , Map(ISymbol("x", 1) -> 2)                     , "(+ x x@2)" )   ,

        //  mix indexing
        ( "(+ x z)"     , Map(SSymbol("x") -> 1)                            , "(+ x@1 z)" )    ,
        ( "(* x y@2)"   , Map(ISymbol("y", 2)  -> 0, SSymbol("x") -> 0)     , "(* x@0 y@0)" )  ,
        ( "(- x y)"     , Map(SSymbol("x") -> 0, SSymbol("y") -> 1)         , "(- x@0 y@1)" )  ,
        ( "(- x@1 y@2)" , Map(ISymbol("y",2) -> 0, ISymbol("x", 1) -> 0)    , "(- x@0 y@0)" )  ,

        //  array indexing
        ("(select a i)"  , Map(SSymbol("a") -> 1)    ,   "(select a@1 i)"      ),
        ("(select a i)"  , Map(SSymbol("i") -> 1)    ,   "(select a i@1)"      ),
        ("(select a i)"  , Map(SSymbol("a") ->0, SSymbol("i") -> 1)    ,   "(select a@0 i@1)"      ),

        //  let term
        ("""
            |(let
            |  (
            |   (x 1)
            |   (y 2)
            |   (z (+ t 2) )
            |  )
            | (+ t (- x y) )
            |)
            |""".stripMargin, Map(SSymbol("x") -> 2),
        """
            |(let
            |  (
            |   (x@2 1)
            |   (y 2)
            |   (z (+ t 2) )
            |  )
            | (+ t (- x@2 y) )
            |)
            |""".stripMargin
        )
    )
    // format: ON

    for ( ( srcTerm, indexer, indexedTerm ) ← theTerms ) {

        test( s" $srcTerm indexed by $indexer should be $indexedTerm" ) {

            ( p1( srcTerm ), p1( indexedTerm ) ) match {

                case ( Success( TermTester( t1 ) ), Success( TermTester( t2 ) ) ) ⇒
                    logger.info( "{} and {} parsed successfully", t1, t2 )

                    Analysis( t1 ).withIndex( indexer ) shouldBe t2

                case ( Failure( e ), _ ) ⇒
                    logger.error( "Parse error in first term {}", e )
                    fail( e )
                case ( _, Failure( e ) ) ⇒
                    logger.error( "Parse error in second term {}", e )
                    fail( e )
            }

        }
    }
}
