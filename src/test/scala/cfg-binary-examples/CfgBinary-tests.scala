/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package cfg
package tests

import parser.Implicits._
import typedterms.Commands
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks
import interpreters.Resources
import theories._

class CfgEx1Tests extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with BitVectors with Commands with Resources {

    import com.typesafe.scalalogging.Logger
    import org.slf4j.LoggerFactory

    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2PrettyPrinter.show
    import parser.SMTLIB2Syntax.{ Term, Sat, UnSat, UnKnown, SatResponses, ConstantTerm, HexaLit }
    import typedterms.{ TypedTerm, Value }
    import interpreters.SMTSolver
    import configurations.SMTLogics.QF_BV
    import configurations.SMTOptions.{ SMTProduceModels, SMTProduceInterpolants }
    import configurations.AppConfig.config
    import configurations.SMTInit

    private val logger = getLogger( this.getClass )

    override def suiteName = "Cfg example 1"

    //  initialise sequence
    val initSeq = new SMTInit( QF_BV, List( SMTProduceModels( true ), SMTProduceInterpolants( true ) ) )

    test( s"cfg example 1 -- path 1" ) {

        val r0 = BVs( "r0", 32 )
        val r2 = BVs( "r2", 32 )
        val lr = BVs( "lr", 32 )
        val pc = ( 0 until 4 ).map ( BVs( "pc", 32 ).indexed( _ ) )

        //  first bx lr
        using( new SMTSolver( "Z3-4.5.0", initSeq ) ) {
            implicit withSolver ⇒
                isSat(
                    pc( 0 ) === 0.i32 & //  initial pc
                        pc( 1 ) === 20.i32 & lr === pc( 0 ) + 4.i32 & //  bl 20
                        ( pc( 2 ) === pc( 1 ) + 4.i32 ) & ( r2 === 42.i32 ) & //  mov r2, #42
                        ( pc( 3 ) === lr ) & ( pc( 3 ) slt 0.i32 ) ) //  bx lr
        } shouldBe Success ( UnSat() )

        using( new SMTSolver( "Z3-4.5.0", initSeq ) ) {
            implicit withSolver ⇒
                isSat(
                    pc( 0 ) === 0.i32 &
                        pc( 1 ) === 20.i32 & lr === pc( 0 ) + 4.i32 &
                        ( pc( 2 ) === pc( 1 ) + 4.i32 ) & ( r2 === 42.i32 ) &
                        ( pc( 3 ) === lr ) & ( pc( 3 ) sge 0.i32 ) ) flatMap {
                        case Sat() ⇒ Success( Sat() )
                        case _     ⇒ Failure( new Exception( "Not Sat" ) )
                    } flatMap { _ ⇒ getValue( pc( 3 ) ) }
        } shouldBe Success( Value( ConstantTerm( HexaLit( "00000004" ) ) ) )

        using( new SMTSolver( "Z3-4.5.0", initSeq ) ) {
            implicit withSolver ⇒
                isSat(
                    pc( 0 ) === 0.i32 &
                        pc( 1 ) === 20.i32 & lr === pc( 0 ) + 4.i32 &
                        ( pc( 2 ) === pc( 1 ) + 4.i32 ) & ( r2 === 42.i32 ) &
                        ( pc( 3 ) === lr ) & !( pc( 3 ) === 4.i32 ) )
        } shouldBe Success ( UnSat() )
    }

    test( s"cfg example 1 -- path 2" ) {

        val r0 = BVs( "r0", 32 )
        val r2 = BVs( "r2", 32 )
        val r7 = BVs( "r7", 32 )
        val lr = ( 0 until 2 ).map ( BVs( "lr", 32 ).indexed( _ ) )
        val pc = ( 0 until 7 ).map ( BVs( "pc", 32 ).indexed( _ ) )

        //  second bx lr
        using( new SMTSolver( "Z3-4.5.0", initSeq ) ) {
            implicit withSolver ⇒
                isSat(
                    pc( 0 ) === 0.i32 & //  init
                        pc( 1 ) === 20.i32 & lr( 0 ) === pc( 0 ) + 4.i32 & //  bl 20
                        ( pc( 2 ) === pc( 1 ) + 4.i32 ) & ( r2 === 42.i32 ) & //  mov r2, #42
                        ( pc( 3 ) === 4.i32 ) & //  bx lr, lr == 4
                        ( pc( 4 ) === pc( 3 ) + 4.i32 ) & ( r7 === 42.i32 ) & //  mov r7, #0
                        ( pc( 5 ) === 20.i32 ) & ( lr( 1 ) === pc( 4 ) + 4.i32 ) & //  bl 20
                        ( pc( 6 ) === lr( 1 ) ) & ( pc( 6 ) sge 0.i32 ) // bx lr
                ) flatMap {
                        case Sat() ⇒ Success( Sat() )
                        case s     ⇒ Failure( new Exception( s"Expected status is Sat: returned is $s" ) )
                    } flatMap { _ ⇒ getValue( pc( 6 ) ) }
        } shouldBe Success( Value( ConstantTerm( HexaLit( "0000000c" ) ) ) )
    }

    test( s"cfg example 2 -- path 1 (no loop)" ) {

        val r0 = BVs( "r0", 32 )
        val r2 = BVs( "r2", 32 )
        val r7 = BVs( "r7", 32 )
        val lr = BVs( "lr", 32 )
        val pc = ( 0 until 7 ).map ( BVs( "pc", 32 ).indexed( _ ) )
        val flag0 = Bools( "flag0" )

        //  first bx lr
        using( new SMTSolver( "Z3-4.5.0", initSeq ) ) {
            implicit withSolver ⇒
                isSat(
                    pc( 0 ) === 0.i32 &
                        pc( 1 ) === 20.i32 & lr === pc( 0 ) + 4.i32 & //  bl 20
                        ( pc( 2 ) === pc( 1 ) + 4.i32 ) & ( flag0 === ( r0 === 0.i32 ) ) & //  cmp r0, #0
                        ( flag0 === True() & pc( 3 ) === 34.i32 ) & // beq 34
                        ( pc( 4 ) === lr ) & ( pc( 4 ) slt 0.i32 ) ) //  bx lr
        } shouldBe Success( UnSat() )

        using( new SMTSolver( "Z3-4.5.0", initSeq ) ) {
            implicit withSolver ⇒
                isSat(
                    pc( 0 ) === 0.i32 &
                        pc( 1 ) === 20.i32 & lr === pc( 0 ) + 4.i32 & //  bl 20
                        ( pc( 2 ) === pc( 1 ) + 4.i32 ) & ( flag0 === ( r0 === 0.i32 ) ) & //  cmp r0, #0
                        ( flag0 === True() & pc( 3 ) === 34.i32 ) & // beq 34
                        ( pc( 4 ) === lr ) & ( pc( 4 ) sge 0.i32 ) //  bx lr
                ) flatMap {
                        case Sat() ⇒ Success( Sat() )
                        case s     ⇒ Failure( new Exception( s"Expected status is Sat: returned is $s" ) )

                    } flatMap { _ ⇒ getValue( pc( 4 ) ) }
        } shouldBe Success( Value( ConstantTerm( HexaLit( "00000004" ) ) ) )
    }

    test( s"cfg example 2 -- path 2 (loop)" ) {

        val r0 = ( 0 until 2 ).map ( BVs( "r0", 32 ).indexed( _ ) )
        val r2 = BVs( "r2", 32 )
        val r7 = BVs( "r7", 32 )
        val lr = BVs( "lr", 32 )
        val pc = ( 0 until 9 ).map ( BVs( "pc", 32 ).indexed( _ ) )
        val flag = ( 0 until 2 ).map ( Bools( "flag" ).indexed( _ ) )

        //  first bx lr
        using( new SMTSolver( "Z3-4.5.0", initSeq ) ) {
            implicit withSolver ⇒
                isSat(
                    pc( 0 ) === 0.i32 &
                        pc( 1 ) === 20.i32 & lr === pc( 0 ) + 4.i32 & //  bl 20
                        ( pc( 2 ) === pc( 1 ) + 4.i32 ) & ( flag( 0 ) === ( r0( 0 ) === 0.i32 ) ) & //  cmp r0, #0
                        ( flag( 0 ) === False() & pc( 3 ) === pc( 2 ) + 4.i32 ) & // beq 30
                        ( r0( 1 ) === ( r0( 0 ) - 1.i32 ) ) & pc( 4 ) === pc( 3 ) + 4.i32 &
                        ( pc( 5 ) === 20.i32 ) &
                        ( pc( 6 ) === pc( 5 ) + 4.i32 ) & ( flag( 1 ) === ( r0( 1 ) === 0.i32 ) ) & //  cmp r0, #0
                        ( flag( 1 ) === True() & pc( 7 ) === 30.i32 ) & // beq 30
                        ( pc( 8 ) === lr ) & ( pc( 8 ) slt 0.i32 ) ) //  bx lr
        } shouldBe Success( UnSat() )

        using( new SMTSolver( "Z3-4.5.0", initSeq ) ) {
            implicit withSolver ⇒
                isSat(
                    pc( 0 ) === 0.i32 &
                        pc( 1 ) === 20.i32 & lr === pc( 0 ) + 4.i32 & //  bl 20
                        ( pc( 2 ) === pc( 1 ) + 4.i32 ) & ( flag( 0 ) === ( r0( 0 ) === 0.i32 ) ) & //  cmp r0, #0
                        ( flag( 0 ) === False() & pc( 3 ) === pc( 2 ) + 4.i32 ) & // beq 30
                        ( r0( 1 ) === ( r0( 0 ) - 1.i32 ) ) & ( pc( 4 ) === pc( 3 ) + 4.i32 ) &
                        ( pc( 5 ) === 20.i32 ) &
                        ( pc( 6 ) === pc( 5 ) + 4.i32 ) & ( flag( 1 ) === ( r0( 1 ) === 0.i32 ) ) & //  cmp r0, #0
                        ( flag( 1 ) === True() & pc( 7 ) === 30.i32 ) & // beq 30
                        ( pc( 8 ) === lr ) & ( pc( 8 ) sge 0.i32 ) //  bx lr
                ) flatMap {
                        case Sat() ⇒ Success( Sat() )
                        case s     ⇒ Failure( new Exception( s"Expected status is Sat: returned is $s" ) )
                    } flatMap { _ ⇒ getValue( pc( 8 ) ) }
        } shouldBe Success( Value( ConstantTerm( HexaLit( "00000004" ) ) ) )

        using( new SMTSolver( "Z3-4.5.0", initSeq ) ) {
            implicit withSolver ⇒

                //  The list of predicates encoding the semantics of the path
                val xl = List(
                    pc( 0 ) === 0.i32,
                    pc( 1 ) === 20.i32 & lr === pc( 0 ) + 4.i32,
                    ( pc( 2 ) === pc( 1 ) + 4.i32 ) & ( flag( 0 ) === ( r0( 0 ) === 0.i32 ) ),
                    ( flag( 0 ) === False() & pc( 3 ) === pc( 2 ) + 4.i32 ),
                    ( r0( 1 ) === ( r0( 0 ) - 1.i32 ) ) & ( pc( 4 ) === pc( 3 ) + 4.i32 ),
                    ( pc( 5 ) === 20.i32 ),
                    ( pc( 6 ) === pc( 5 ) + 4.i32 ) & ( flag( 1 ) === ( r0( 1 ) === 0.i32 ) ),
                    ( flag( 1 ) === True() & pc( 7 ) === 30.i32 ),
                    ( pc( 8 ) === lr ) & !( pc( 8 ) === 4.i32 ) )
                //  Name them
                val namedTerms = for { ( tt, n ) ← xl.zipWithIndex } yield tt.named( "P" + n )
                //
                isSat( namedTerms : _* ) flatMap {
                    case UnSat() ⇒ Success( UnSat() )
                    case s       ⇒ Failure( new Exception( s"Expected status is UnSat: returned is $s" ) )

                } flatMap { _ ⇒
                    getInterpolants( namedTerms.head, namedTerms.tail.head, namedTerms.drop( 2 ) : _* )
                }
        } match {
            case Success( xitp ) ⇒
                logger.info( xitp.map( _.termDef )
                    .map ( show( _ ) ).mkString( "\n" ) )

            case Failure( f ) ⇒ fail( f )
        }
    }

    test( s"cfg example 3 -- interpolant error test" ) {

        val r0 = ( 0 until 2 ).map ( BVs( "r0", 32 ).indexed( _ ) )
        val r2 = BVs( "r2", 32 )
        val r7 = BVs( "r7", 32 )
        val lr = ( 0 until 2 ).map ( BVs( "lr", 32 ).indexed( _ ) )
        val pc = ( 0 until 5 ).map ( BVs( "pc", 32 ).indexed( _ ) )
        val n = ( 0 until 2 ).map ( Bools( "n" ).indexed( _ ) )

        //  first bx lr
        using( new SMTSolver( "Z3-4.5.0", initSeq ) ) {
            implicit withSolver ⇒
                isSat(
                    True() &
                        pc( 1 ) === 32.i32 + 8.i32 &
                        True() &
                        lr( 1 ) === pc( 0 ) - 4.i32 &

                        True() &
                        ( n( 1 ) === (
                            ( r0( 0 ).zext( 1 ) +
                                ( !0.i32 ).zext( 1 ) + 1.withBits( 33 ) ).extract( 31, 31 ) ===
                                1.withUBits( 1 ) ) ) )
        } shouldBe Success( Sat() )

    }

}
