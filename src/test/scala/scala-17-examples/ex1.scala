/*
 * This file is part of ScalaSMT.
 *
 * Copyright (C) 2015-2019 Franck Cassez.
 *
 * ScalaSMT is free software: you can redistribute it and/or modify it un-
 * der the terms of the  GNU Lesser General Public License as published by
 * the Free Software Foundation,  either version 3  of the License, or (at
 * your option) any later version.
 *
 * ScalaSMT is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied  warranty of MERCHANTABILITY  or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ScalaSMT. (See files COPYING and COPYING.LESSER.) If not, see
 * <http://www.gnu.org/licenses/>.
 */

package org.bitbucket.franck44.scalasmt
package theories
package tests

import org.bitbucket.franck44.scalasmt.parser.Implicits._
import org.bitbucket.franck44.scalasmt.typedterms.Commands
import org.bitbucket.franck44.scalasmt.interpreters.Resources
import org.scalatest.{ FunSuite, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks

class Scala17Examples extends FunSuite with TableDrivenPropertyChecks with Matchers
    with Core with IntegerArithmetics with Commands with Resources {

    override def suiteName = "Scala 17 paper examples"

    import org.bitbucket.franck44.scalasmt.parser.SMTLIB2PrettyPrinter.show
    import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.Term
    import org.bitbucket.franck44.scalasmt.typedterms.{ Model, TypedTerm }

    // check that the solvers accept the terms
    import interpreters.SMTSolver
    import configurations.SMTInit
    import configurations.SMTLogics.QF_LIA
    import configurations.SMTOptions.{ SMTProduceModels, SMTProduceInterpolants }
    import scala.util.{ Try, Success, Failure }
    import parser.SMTLIB2Syntax.{ Sat, UnSat, UnKnown }

    val x = Ints( "x" )
    val y = Ints( "y" )
    val z = Ints( "z" )

    test( """| Example 1 (listing 2)
             | (set-option :produce-models true)
             | (set-logic QF_LIA)
             | (declare-fun x () Int)
             | (declare-fun y () Int)
             | (assert (<= (+ x 1) y))
             | (assert (and (= (mod y 4) 3) (>= y 2)))
             | ;; Check SAT
             | (check-sat)
             | ;; returns sat
             | ;; Get values for x and y
             | (get-value (x y))
             | ;; returns ((x 2) (y 3))
             | """.stripMargin ) {
        //  ScalaSMT version
        val result : Try[ Model ] =
            using( new SMTSolver( "Z3-4.8.6", new SMTInit( QF_LIA, List( SMTProduceModels( true ) ) ) ) ) {
                implicit withSolver ⇒
                    isSat(
                        x + 1 <= y,
                        y % 4 === 3 & y >= 2 ) match {
                            case Success( Sat() ) ⇒ getModel()
                            case _                ⇒ Failure( new Exception( "failed" ) )
                        }
            }

        result match {
            case Success( m ) ⇒
                ( m.valueOf( x ), m.valueOf( y ) ) match {
                    case ( Some( valx ), Some( valy ) ) ⇒
                        val v = Map( x → valx.show.toInt, y → valy.show.toInt )
                        assert( v( x ) + 1 == v( y ) && v( y ) % 4 == 3 & v( y ) >= 2 )

                    case _ ⇒ fail( new Exception( s"Could not get value from model for Examnple 1. ${m}" ) )
                }

            case Failure( f ) ⇒ fail( new Exception( s"Could not get model for Examnple 1. ${f.getMessage}" ) )
        }

    }

    test( "Example 2, listing 3" ) {
        val p1 = ( x === z + 1 & z >= 0 ).named( "p1" )
        val p2 = ( y >= x & y < 1 ).named( "p2" )
        val interpolants =
            using( new SMTSolver( "SMTInterpol", new SMTInit( QF_LIA, List( SMTProduceInterpolants( true ) ) ) ) ) {
                implicit solver ⇒
                    //  assert ? \textcolor{orange!70!white}{$P_1$ and $P_2$}?
                    |= ( p1 ) flatMap
                        { _ ⇒ |= ( p2 ) } flatMap
                        { _ ⇒ checkSat() } flatMap //  If previous |= are Success then checkSat
                        { _ ⇒ getInterpolants( p1, p2 ) } //  If checkSat is Success then getInterpolants
            }

        //  there should be only one interpolant
        interpolants match {
            case Success( i :: Nil ) ⇒
                //  check that i is an interpolant
                using( new SMTSolver( "CVC4", new SMTInit( QF_LIA, List() ) ) ) {
                    implicit solver ⇒
                        //  assert ? \textcolor{orange!70!white}{$P_1 \implies I$ and $I \wedge P_2$ is UNSAT}?
                        push()
                        isSat ( p1 & !i ) shouldBe Success( UnSat() )
                        pop()
                        isSat ( i & p2 ) shouldBe Success( UnSat() )
                        push()
                        pop()
                }
            case Success( _ ) ⇒
                fail( new Exception( "Should have only one interpolant" ) )
            case Failure( f ) ⇒
                fail( s"Could not get interpolants ${f.getMessage}" )
        }

    }
}
