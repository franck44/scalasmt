
/*
** float-rounder-bug.c
**
** Martin Brain
** martin.brain@cs.ox.ac.uk
** 20/05/13
**
** Another manifestation of the casting bug.
** If the number is in (0,0x1p-126) it is rounded to zero rather than a subnormal number.
*/
#define FULP 1

int bug () {
  float min = 0x1.fffffep-105f;
  //  compute result in double and cast as float
  float modifier = (0x1.0p-23 * (1<<FULP));
  float ulpdiff = min * modifier;

  printf("min (0x1.fffffep-105f) = 0x%lX\n",min);
  printf("0x1p-126f = 0x%llX\n",(float)0x1p-126f);
  printf("modifier = 0x%llX\n", modifier);
  printf("ulpdiff = 0x%llX\n", ulpdiff);

  //  promote ulpdiff to double and compare with double 0x1p-126f
  if(!(ulpdiff == 0x1p-126f)) return 1;    // Should be true

  return 0;
}

int bugBrokenOut () {
  float min = 0x1.fffffep-105f;
  float modifier = (0x1.0p-23 * (1<<FULP));
  double dulpdiff = (double)min * (double)modifier;  // Fine up to here
  float ulpdiff = (float)dulpdiff;  // Error

  printf("min (0x1.fffffep-105f) = 0x%llX\n",min);
  printf("modifier = 0x%llX\n", modifier);
  printf("ulpdiff = 0x%llX\n", ulpdiff);

  if(!(ulpdiff == 0x1p-126f)) return 1; // Should be true

  return 0 ;
}
//
int bugCasting () {
  // semantics seems to be: double for  0x1.fffffep-127 and cast to float RNE
  float d = 0x1.fffffep-127;
  //  standrad assignment
  float f = (float) d;

  printf("d (0x1.fffffep-127) = 0x%lX\n",(float)0x1.fffffep-127);
  printf("f = 0x%lX\n", f);

  //  promote f to double and compare with double 0x1p-126f
  if(!(f == 0x1p-126f)) return 1; // Should be true

  return 0;
}

int main (void) {
  printf("bug status (reachable 0 <-> no ) : %d\n",bug());
  printf("bugBrokenOut status (reachable 0 <-> no ) : %d\n",bugBrokenOut());
  printf("bugCasting status (reachable 0 <-> no ) : %d\n",bugCasting());

  return 0;
}
