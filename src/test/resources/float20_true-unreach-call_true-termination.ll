; ModuleID = '/Users/franck/development/scalasmt/src/test/resources/float20_true-unreach-call_true-termination.c'
source_filename = "/Users/franck/development/scalasmt/src/test/resources/float20_true-unreach-call_true-termination.c"
target datalayout = "e-m:e-p:32:32-f64:32:64-f80:32-n8:16:32-S128"
target triple = "i386-pc-linux-gnu"

; Function Attrs: nounwind
define void @bug(float) local_unnamed_addr #0 !dbg !8 {
  %2 = fcmp oeq float %0, 0x396FFFFFE0000000, !dbg !10
  %3 = zext i1 %2 to i32, !dbg !10
  tail call void @__VERIFIER_assume(i32 %3) #2, !dbg !11
  %4 = fmul float %0, 0x3E90000000000000, !dbg !12
  %5 = fcmp oeq float %4, 0x3810000000000000, !dbg !13
  br i1 %5, label %7, label %6, !dbg !14

; <label>:6:                                      ; preds = %1
  tail call void @__VERIFIER_error() #2, !dbg !15
  br label %7, !dbg !15

; <label>:7:                                      ; preds = %6, %1
  ret void, !dbg !16
}

declare void @__VERIFIER_assume(i32) local_unnamed_addr #1

declare void @__VERIFIER_error() local_unnamed_addr #1

; Function Attrs: nounwind
define void @bugBrokenOut(float) local_unnamed_addr #0 !dbg !17 {
  %2 = fcmp oeq float %0, 0x396FFFFFE0000000, !dbg !18
  %3 = zext i1 %2 to i32, !dbg !18
  tail call void @__VERIFIER_assume(i32 %3) #2, !dbg !19
  %4 = fmul float %0, 0x3E90000000000000, !dbg !20
  %5 = fcmp oeq float %4, 0x3810000000000000, !dbg !21
  br i1 %5, label %7, label %6, !dbg !22

; <label>:6:                                      ; preds = %1
  tail call void @__VERIFIER_error() #2, !dbg !23
  br label %7, !dbg !23

; <label>:7:                                      ; preds = %6, %1
  ret void, !dbg !24
}

; Function Attrs: nounwind
define void @bugCasting(double) local_unnamed_addr #0 !dbg !25 {
  %2 = fcmp oeq double %0, 0x380FFFFFE0000000, !dbg !26
  %3 = zext i1 %2 to i32, !dbg !26
  tail call void @__VERIFIER_assume(i32 %3) #2, !dbg !27
  %4 = fptrunc double %0 to float, !dbg !28
  %5 = fcmp oeq float %4, 0x3810000000000000, !dbg !29
  br i1 %5, label %7, label %6, !dbg !30

; <label>:6:                                      ; preds = %1
  tail call void @__VERIFIER_error() #2, !dbg !31
  br label %7, !dbg !31

; <label>:7:                                      ; preds = %6, %1
  ret void, !dbg !32
}

; Function Attrs: nounwind
define i32 @main() local_unnamed_addr #0 !dbg !33 {
  %1 = tail call float @__VERIFIER_nondet_float() #2, !dbg !34
  %2 = fcmp oeq float %1, 0x396FFFFFE0000000, !dbg !35
  %3 = zext i1 %2 to i32, !dbg !35
  tail call void @__VERIFIER_assume(i32 %3) #2, !dbg !37
  %4 = fmul float %1, 0x3E90000000000000, !dbg !38
  %5 = fcmp oeq float %4, 0x3810000000000000, !dbg !39
  br i1 %5, label %7, label %6, !dbg !40

; <label>:6:                                      ; preds = %0
  tail call void @__VERIFIER_error() #2, !dbg !41
  br label %7, !dbg !41

; <label>:7:                                      ; preds = %0, %6
  %8 = tail call float @__VERIFIER_nondet_float() #2, !dbg !42
  %9 = fcmp oeq float %8, 0x396FFFFFE0000000, !dbg !43
  %10 = zext i1 %9 to i32, !dbg !43
  tail call void @__VERIFIER_assume(i32 %10) #2, !dbg !45
  %11 = fmul float %8, 0x3E90000000000000, !dbg !46
  %12 = fcmp oeq float %11, 0x3810000000000000, !dbg !47
  br i1 %12, label %14, label %13, !dbg !48

; <label>:13:                                     ; preds = %7
  tail call void @__VERIFIER_error() #2, !dbg !49
  br label %14, !dbg !49

; <label>:14:                                     ; preds = %7, %13
  %15 = tail call double @__VERIFIER_nondet_double() #2, !dbg !50
  %16 = fcmp oeq double %15, 0x380FFFFFE0000000, !dbg !51
  %17 = zext i1 %16 to i32, !dbg !51
  tail call void @__VERIFIER_assume(i32 %17) #2, !dbg !53
  %18 = fptrunc double %15 to float, !dbg !54
  %19 = fcmp oeq float %18, 0x3810000000000000, !dbg !55
  br i1 %19, label %21, label %20, !dbg !56

; <label>:20:                                     ; preds = %14
  tail call void @__VERIFIER_error() #2, !dbg !57
  br label %21, !dbg !57

; <label>:21:                                     ; preds = %14, %20
  ret i32 1, !dbg !58
}

declare float @__VERIFIER_nondet_float() local_unnamed_addr #1

declare double @__VERIFIER_nondet_double() local_unnamed_addr #1

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="pentium4" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="pentium4" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!3, !4, !5, !6}
!llvm.ident = !{!7}

!0 = distinct !DICompileUnit(language: DW_LANG_C99, file: !1, producer: "clang version 5.0.0 (tags/RELEASE_500/final)", isOptimized: true, runtimeVersion: 0, emissionKind: LineTablesOnly, enums: !2)
!1 = !DIFile(filename: "/Users/franck/development/scalasmt/src/test/resources/float20_true-unreach-call_true-termination.c", directory: "/Users/franck/development/skink")
!2 = !{}
!3 = !{i32 1, !"NumRegisterParameters", i32 0}
!4 = !{i32 2, !"Dwarf Version", i32 4}
!5 = !{i32 2, !"Debug Info Version", i32 3}
!6 = !{i32 1, !"wchar_size", i32 4}
!7 = !{!"clang version 5.0.0 (tags/RELEASE_500/final)"}
!8 = distinct !DISubprogram(name: "bug", scope: !1, file: !1, line: 17, type: !9, isLocal: false, isDefinition: true, scopeLine: 17, flags: DIFlagPrototyped, isOptimized: true, unit: !0, variables: !2)
!9 = !DISubroutineType(types: !2)
!10 = !DILocation(line: 18, column: 25, scope: !8)
!11 = !DILocation(line: 18, column: 3, scope: !8)
!12 = !DILocation(line: 20, column: 23, scope: !8)
!13 = !DILocation(line: 22, column: 16, scope: !8)
!14 = !DILocation(line: 22, column: 6, scope: !8)
!15 = !DILocation(line: 22, column: 31, scope: !8)
!16 = !DILocation(line: 24, column: 3, scope: !8)
!17 = distinct !DISubprogram(name: "bugBrokenOut", scope: !1, file: !1, line: 27, type: !9, isLocal: false, isDefinition: true, scopeLine: 27, flags: DIFlagPrototyped, isOptimized: true, unit: !0, variables: !2)
!18 = !DILocation(line: 28, column: 25, scope: !17)
!19 = !DILocation(line: 28, column: 3, scope: !17)
!20 = !DILocation(line: 31, column: 19, scope: !17)
!21 = !DILocation(line: 33, column: 16, scope: !17)
!22 = !DILocation(line: 33, column: 6, scope: !17)
!23 = !DILocation(line: 33, column: 31, scope: !17)
!24 = !DILocation(line: 35, column: 3, scope: !17)
!25 = distinct !DISubprogram(name: "bugCasting", scope: !1, file: !1, line: 38, type: !9, isLocal: false, isDefinition: true, scopeLine: 38, flags: DIFlagPrototyped, isOptimized: true, unit: !0, variables: !2)
!26 = !DILocation(line: 39, column: 23, scope: !25)
!27 = !DILocation(line: 39, column: 3, scope: !25)
!28 = !DILocation(line: 41, column: 13, scope: !25)
!29 = !DILocation(line: 43, column: 10, scope: !25)
!30 = !DILocation(line: 43, column: 6, scope: !25)
!31 = !DILocation(line: 43, column: 25, scope: !25)
!32 = !DILocation(line: 45, column: 3, scope: !25)
!33 = distinct !DISubprogram(name: "main", scope: !1, file: !1, line: 48, type: !9, isLocal: false, isDefinition: true, scopeLine: 48, flags: DIFlagPrototyped, isOptimized: true, unit: !0, variables: !2)
!34 = !DILocation(line: 49, column: 11, scope: !33)
!35 = !DILocation(line: 18, column: 25, scope: !8, inlinedAt: !36)
!36 = distinct !DILocation(line: 50, column: 3, scope: !33)
!37 = !DILocation(line: 18, column: 3, scope: !8, inlinedAt: !36)
!38 = !DILocation(line: 20, column: 23, scope: !8, inlinedAt: !36)
!39 = !DILocation(line: 22, column: 16, scope: !8, inlinedAt: !36)
!40 = !DILocation(line: 22, column: 6, scope: !8, inlinedAt: !36)
!41 = !DILocation(line: 22, column: 31, scope: !8, inlinedAt: !36)
!42 = !DILocation(line: 52, column: 11, scope: !33)
!43 = !DILocation(line: 28, column: 25, scope: !17, inlinedAt: !44)
!44 = distinct !DILocation(line: 53, column: 3, scope: !33)
!45 = !DILocation(line: 28, column: 3, scope: !17, inlinedAt: !44)
!46 = !DILocation(line: 31, column: 19, scope: !17, inlinedAt: !44)
!47 = !DILocation(line: 33, column: 16, scope: !17, inlinedAt: !44)
!48 = !DILocation(line: 33, column: 6, scope: !17, inlinedAt: !44)
!49 = !DILocation(line: 33, column: 31, scope: !17, inlinedAt: !44)
!50 = !DILocation(line: 55, column: 12, scope: !33)
!51 = !DILocation(line: 39, column: 23, scope: !25, inlinedAt: !52)
!52 = distinct !DILocation(line: 56, column: 3, scope: !33)
!53 = !DILocation(line: 39, column: 3, scope: !25, inlinedAt: !52)
!54 = !DILocation(line: 41, column: 13, scope: !25, inlinedAt: !52)
!55 = !DILocation(line: 43, column: 10, scope: !25, inlinedAt: !52)
!56 = !DILocation(line: 43, column: 6, scope: !25, inlinedAt: !52)
!57 = !DILocation(line: 43, column: 25, scope: !25, inlinedAt: !52)
!58 = !DILocation(line: 58, column: 3, scope: !33)
