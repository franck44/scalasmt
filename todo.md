
extract:
  1. test parsing

concat:
  1. parsing
  2. sorts
  3. tests in use
  4. add to DSL

constant arrays:
  1. test with arrays dimension 1
  2. test if solvers return models/values that are constant arrays of constant arrays. If yes add tests.


Let/Forall/Exists
  1.  allow let/forall/exists with indexed variables (instead of symbolId)
  2.  forall/exists with TypedTerms rather than SMTLIB2Symbol
  3.  test!