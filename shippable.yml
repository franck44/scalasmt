
# language setting
language: scala

# set which solvers have to be installed
# this should correspond to what is in enabled in application.conf
env:
  global:
    - INSTALLZ3=true
    - INSTALLSMTINTERPOL=true
    - INSTALLMATHSAT=true
    - INSTALLCVC4=true
    - INSTALLYICES=true
    - CODACY_PROJECT_TOKEN=4c6c62c844354d30aae8731d6c33f957

# Install solvers and snaphots of dependencies (scala packages)
before_script:

     # set time zone
     - echo 'Australia/Sydney' | sudo tee /etc/timezone
     - sudo dpkg-reconfigure --frontend noninteractive tzdata

     # create directories for coverage
     - mkdir -p shippable/testresults
     - ln -s ../target/test-reports shippable/testresults
     - which python

     # Install SMTInterpol
     - 'if [[ $INSTALLSMTINTERPOL = true ]]; then
        echo "Installing SMTInterpol 2.5";
        wget --no-check-certificate https://ultimate.informatik.uni-freiburg.de/smtinterpol/smtinterpol-2.5-479-ga49e50b1.jar ;
        cp lib/interpol-shippable /usr/local/bin/interpol;
        chmod u+x /usr/local/bin/interpol;
        mv smtinterpol-2.5-479-ga49e50b1.jar /usr/local/bin/smtinterpol.jar;
        interpol -version;
        echo "SMTInterpol install 2.5";
        fi'

    # Install MathSat
     - 'if [[ $INSTALLMATHSAT = true ]]; then
              echo "Installing MathSat 5.5.4";
              wget --no-check-certificate  http://mathsat.fbk.eu/download.php?file=mathsat-5.5.4-linux-x86_64.tar.gz -O mathsat-5.5.4-linux-x86_64.tar.gz;
              tar zxvf mathsat-5.5.4-linux-x86_64.tar.gz;
              mv mathsat-5.5.4-linux-x86_64/bin/mathsat /usr/local/bin/;
              mathsat -version;
              echo "Installed MathSat 5.5.4";
        fi'

     # Install CVC4
     - 'if [[ $INSTALLCVC4 = true ]]; then
            echo "Installing CVC4 1.6";
            wget --no-check-certificate http://cvc4.cs.stanford.edu/downloads/builds/x86_64-linux-opt/cvc4-1.6-x86_64-linux-opt;
            mv cvc4-1.6-x86_64-linux-opt /usr/local/bin/cvc4;
            chmod u+x /usr/local/bin/cvc4;
            cvc4 --version;
            echo "Installed CVC4 1.6";
       fi'
     

     # Install Yices (from local lib directory) and Yices-2.6.0
     - 'if [[ $INSTALLYICES = true ]]; then
            echo "Installing Yices-2.6.1";
            wget --no-check-certificate http://yices.csl.sri.com/releases/2.6.1/yices-2.6.1-x86_64-pc-linux-gnu-static-gmp.tar.gz;
            tar xvf yices-2.6.1-x86_64-pc-linux-gnu-static-gmp.tar.gz;
            mv yices-2.6.1/bin/yices-smt2 /usr/local/bin/yices-smt2;
            yices-smt2 --version;
            echo "Installed Yices-2.6.1";   
        fi'

     # Pull and install Z3 from master
     - 'if [[ $INSTALLZ3 = true ]]; then
            echo "Installing Z3-4.5.0";
            wget --no-check-certificate  https://github.com/Z3Prover/z3/releases/download/z3-4.5.0/z3-4.5.0-x64-ubuntu-14.04.zip;
            unzip z3-4.5.0-x64-ubuntu-14.04.zip;
            mv z3-4.5.0-x64-ubuntu-14.04/bin/z3 /usr/local/bin/z3-4.5.0;
            z3-4.5.0 --version;
            echo "Installed Z3-4.5.0";

            echo "Installing Z3-4.8.6";
            wget --no-check-certificate  https://github.com/Z3Prover/z3/releases/download/z3-4.8.6/z3-4.8.6-x64-ubuntu-16.04.zip;
            unzip z3-4.8.6-x64-ubuntu-16.04.zip;
            mv z3-4.8.6-x64-ubuntu-16.04/bin/z3 /usr/local/bin/z3-4.8.6;
            z3-4.8.6 --version;
            echo "Installed Z3-4.8.6";
        fi'

script:

after_script:
    # copy coverage and tests results
    - mkdir -p shippable/codecoverage
    - mv target/scala-2.12/coverage-report/cobertura.xml shippable/codecoverage/

build:
    pre_ci_boot:
        #   use image with pre-built scala 2.12.6, sbt 1.2.3; gcc, g++, make (Z3)
        image_name: franck44/docker-scala-sbt
        tag: latest

    pre_ci:
        - shippable_retry sudo docker pull franck44/docker-scala-sbt:latest
    ci:
        - env SBT_OPTS="-Xmx1G -Xss200M"
        # run tests with coverage
        - sbt clean coverage ShippableCITest/test coverageReport codacyCoverage

integrations:
   notifications:
     - integrationName: scalaSMT
       type: slack
       recipients:
        - "#scala-smtlib"
        - "@francksquash"
       branches:
         only:
           - master
       on_success: always
       on_failure: always
       on_start : never
